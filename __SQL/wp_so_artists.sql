

INSERT INTO rt_settings (	"name" , value ) VALUES('site_heading', 'Artists/Songs Rating and reviews');
INSERT INTO rt_settings (	"name" , value ) VALUES('site_subheading', 'You can take participation in your favorite artists/songs rating and reviews');
INSERT INTO rt_settings (	"name" , value ) VALUES('contact_us_phone', '2377-827-318');
INSERT INTO rt_settings (	"name" , value ) VALUES('contact_us_location', 'Chicago, US');
INSERT INTO rt_settings (	"name" , value ) VALUES('contact_us_email', 'contact_us@artists_rating.com');


CREATE TYPE type_cms_item_page_type AS ENUM (
    'E',
    'P',
    'B'
);

DROP SEQUENCE IF EXISTS rt_cms_item_id_seq cascade;
CREATE SEQUENCE rt_cms_item_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_cms_items  cascade;
CREATE TABLE public.rt_cms_items (
	id              integer NOT NULL DEFAULT nextval('rt_cms_item_id_seq'::regclass) NOT NULL,
	title           varchar(100) NOT NULL,
	alias           varchar(100) NOT NULL,
	page_type       type_cms_item_page_type NOT NULL,
	short_descr     varchar(255) DEFAULT '',
	content         text NOT NULL,
	content_hints   text,
	image           varchar(50) NULL,
	icon            varchar(50) NULL,
	user_id         int4 NOT NULL,
	published       bool DEFAULT false,
	created_at      timestamp DEFAULT now() NOT NULL,
	updated_at      timestamp,
	CONSTRAINT rt_cms_items_pkey PRIMARY KEY (id),
	CONSTRAINT rt_cms_items_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.rt_users(id)
) ;
CREATE INDEX ind_cms_item_alias ON public.rt_cms_items (alias) ;
CREATE INDEX ind_cms_item_page_type_published ON public.rt_cms_items (page_type,published) ;
CREATE UNIQUE INDEX ind_rt_cms_items_unique ON public.rt_cms_items (title) ;



INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at, updated_at) VALUES (1, 'About US', 'about_us', 'P', '', '<p>Welcome to <strong>artists-rating.com </strong> - Looking for a artists-rating has never been easier.Australian owned and operated by Serge at Home LTD (ACN 1326).</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>&nbsp;</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>&nbsp;</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, '2234.jpg', 'color_lens', 1, true, '2013-06-06 04:31:22', '2017-03-28 15:23:45.2016');

INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at, updated_at) VALUES (2, 'Contact Us', 'contact_us', 'P', '', '<p>You can leave your message here and our support will answer you in <strong>next 24 hours</strong>.</p>', NULL, '2234.jpg', 'donut_large', 1, true, '2016-11-30 18:22:31', '2017-03-27 17:29:38' ||
 '.841477');

INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at, updated_at) VALUES (3, 'Warranty and Service', 'warranty_and_service', 'P', 'Warranty and Service
What products are warranted?
On the products in our store warranty...', '<p>Warranty and Service
What products are warranted?
On the products in our store warranty...
</p>
</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'local_laundry_service', 1, true, '2016-11-30 16:13:42', '2016-11-30 16:13:42');
INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at, updated_at) VALUES (4, 'TDES Quiz', 'tdes_quiz',
 'P', 'TDES Quiz :
Answer the questions and win TDES smartphones!', '<p>TDES Quiz :
Answer the questions and win TDES smartphones!</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'date_range', 1, true, '2016-11-30 13:42:56', '2016-11-30 13:42:56');


INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at, updated_at) VALUES (5, 'Security & Privacy', 'security_privacy', 'P', 'Your privacy is very important to us. Accordingly...', '<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.</p><p 30px=""><em>- Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</em><br><em>- We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</em><br><em>- We will only retain personal information as long as necessary for the fulfillment of those purposes.</em><br><em>- We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</em><br><em>- Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date.</em><br><em>- We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</em><br><em>- We will make readily available to customers information about our policies and practices relating to the management of personal information.</em></p><p>We are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.</p><h3>External Sites.</h3><p>artists-rating.com is not responsible for the content of external internet sites. You are advised to read the privacy policy of external sites before disclosing any personal information.</p><h3>Cookies</h3><p>A "cookie" is a small data text file that is placed in your browser and allows artists-rating.com to recognize you each time you visit this site(customisation etc). Cookies themselves do not contain any personal information, and (local-)artists-rating.com does not use cookies to collect personal information. Cookies may also be used by 3rd party content providers such as newsfeeds.</p><h3>Remember The Risks Whenever You Use The Internet</h3><p>While we do our best to protect your personal information, we cannot guarantee the security of any information that you transmit to (local-)artists-rating.com and you are solely responsible for maintaining the secrecy of any passwords or other account information. In addition other Internet sites or services that may be accessible through (local-)artists-rating.com have separate data and privacy practices independent of us, and therefore we disclaim any responsibility or liability for their policies or actions.</p><p>Please contact those vendors and others directly if you have any questions about their privacy policies.</p><p></p><p>Serge at Home<br>(local-)artists-rating.com</p>', NULL, NULL, 'gavel', 1, true, '2012-04-29 12:39:36', '2013-07-31 17:09:59');




INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at) VALUES (6, 'Tours soon',
'tours_soon', 'P',  'Tours soon short description...', '<p>Tours soon
</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'color_lens', 1,  true, '2016-12-27 11:42:35');


INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at) VALUES (7, 'Join Us',
'join_us', 'P',  'Join us and have luck with our site...', '<p>Join us and have luck with our site
</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'cached', 1,  true, '2016-12-29 15:12:51');


INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at) VALUES (8, 'Company info',
'company_info', 'P',  'Company info lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor...', '<p>Company info ...
</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'explore', 1,  true, '2016-12-30 11:10:25');


INSERT INTO rt_cms_items (id, title, alias, page_type, short_descr, content, content_hints, image, icon, user_id, published, created_at) VALUES (9, 'Contact us',
'contact_us', 'P',  'Contact us and have luck with our site lorem  ipsum dolor sit amet...', '<p>Contact us
</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...</p><p> lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, qui...
More: <a href="#">by link</a></p>', NULL, NULL, 'flight_land', 1,  true, '2016-12-29 12:53:18');


CREATE TYPE type_active_status AS ENUM (
    'A',
    'I',
    'N'
);

DROP SEQUENCE IF EXISTS rt_artist_id_seq cascade;
CREATE SEQUENCE rt_artist_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_artists cascade;
CREATE TABLE rt_artists (
    id             integer NOT NULL DEFAULT nextval('rt_artist_id_seq'::regclass) NOT NULL,
    name           character varying(50) NOT NULL,
    ordering       integer NOT NULL,
    is_active      type_active_status DEFAULT 'N'::type_active_status NOT NULL,
    has_concerts   boolean DEFAULT false,
    birthday       date NULL,
    day_of_death   date NULL,
    site           character varying(100) NULL,
    info           text,
  	created_at     timestamp NOT NULL DEFAULT now(),
	  updated_at     timestamp NULL,
    PRIMARY KEY (id)

);
CREATE INDEX ind_rt_artists_created_at ON rt_artists ( created_at );
CREATE INDEX ind_rt_artists_is_active_has_concerts_ordering ON rt_artists ( is_active, has_concerts, ordering );
CREATE UNIQUE INDEX ind_rt_artists_name ON rt_artists (name);


INSERT INTO rt_artists (id, name, ordering, is_active, has_concerts, site, birthday, day_of_death, info, updated_at, created_at) VALUES
(1, 'Ella Fitzgerald', 2, 'A', false, 'http://EllaFitzgerald_site.com', '1917-04-25', '1996-06-15', 'Ella Fitzgerald  some long text 1',
'2015-03-12 08:31:21', '2015-03-12 08:31:21'),
(2, 'Demis Roussos', 1, 'A', false, 'http://demis_roussos_site.com', '1946-06-15', '2015-01-25', 'DemisRoussos some long text 22', '2015-03-10 04:35:07', '2015-03-10 04:35:07'),
(3, 'Frank Sinatra', 4, 'A', false, 'http://frank_sinatra_site.com', '1915-12-12', '1998-05-14', 'Frank Sinatra some long text 3', '2015-03-11 05:21:54', '2015-03-11 05:26:54'),
(4, 'Elvis Presley', 3, 'A', false, 'http://elvis_presley_site.com', '1935-01-08', '1977-08-16', 'Elvis Presley some long text 4', '2015-03-11 05:26:54', '2015-03-11 05:26:54');

INSERT INTO rt_artists (id, name, ordering, is_active, has_concerts, site, birthday, day_of_death, info, updated_at, created_at) VALUES
(5, 'Eric Clapton', 5, 'A', true, 'http://artists-rating.com/eric_clapton', '1945-03-30', null, 'Eric Clapton some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
'2018-03-13 07:24:51', '2015-03-13 07:32:42');

 INSERT INTO rt_artists (id, name, ordering, is_active, has_concerts, site, birthday, day_of_death, info, updated_at, created_at) VALUES
(6, 'Bruce Springsteen', 6, 'A', true, 'http://artists-rating.com/bruce_springsteen', '1949-09-23', null, 'Bruce Springsteen some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
'2018-03-31 09:22:31', '2018-03-11 09:25:54');

INSERT INTO rt_artists (id, name, ordering, is_active, has_concerts, site, birthday, day_of_death, info, updated_at, created_at) VALUES
(7, 'Paul McCartney', 7, 'A', true, 'http://artists-rating.com/Paul_McCartney', '1945-03-30', null, 'Paul McCartney some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
'2018-03-31 09:27:21', '2018-03-31 09:28:43');

INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(57, 5, 'Eric Clapton_NJ22Feb83MB_04.jpg', FALSE, FALSE, 'Eric Clapton photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:11:12'),
(58, 5, 'Clapton New Jersey 22 February 1983 Michael Brito_0005.jpg', FALSE, FALSE, 'Clapton New Jersey 22 February 1983 Michael Brito...', '2018-04-02 12:13:54'),

(59, 5, 'w13.jpg', TRUE, TRUE, 'Eric Clapton photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:14:42'),

(60, 5, '14332-2995 EC & AFL.jpg', FALSE, TRUE, 'Eric Clapton photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2018-04-02 12:15:51' );


INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(61, 6, 'springsteen-700x352.jpg', TRUE, FALSE, 'Bruce Springsteen photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:14:52'),
(62, 6, '416x416.jpg', TRUE, TRUE, 'Bruce Springsteen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...', '2018-04-02 12:15:14'),
(63, 6, 'Bruce+Springsteen.jpg', FALSE , TRUE, 'Bruce Springsteen photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:16:05'),
(64, 6, '596.jpg', FALSE, TRUE, 'Bruce Springsteen photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2018-04-02 12:17:23' );


Paul McCartney
INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(65, 7, 'Image-Paul-McCartney-HD-Wallpapers.jpg', TRUE, FALSE, 'Paul McCartney photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:19:20'),
(66, 7, '20300534506289142847820629813_s.jpg', TRUE, TRUE, 'Paul McCartney Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...', '2018-04-02 12:19:59'),
(67, 7, '71MbUkXU66L.png', FALSE , TRUE, 'Paul McCartney photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2018-04-02 12:22:03'),
(68, 7, 'paul-mccartney.jpg', FALSE, FALSE , 'Paul McCartney photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', '2018-04-02 12:24:03' );



CREATE SEQUENCE rt_artist_concert_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_artist_concerts cascade;
CREATE TABLE rt_artist_concerts (
  id              integer NOT NULL DEFAULT nextval('rt_artist_concert_id_seq'::regclass) NOT NULL,
  artist_id       integer NOT NULL,
  place_name      character varying (100) NOT NULL,
  lat             type_location NOT NULL,
  lng             type_location NOT NULL,
  participants    integer DEFAULT NULL,
  event_date      timestamp NOT NULL,
  tour_id         integer DEFAULT NULL,
  country         character varying(30) NOT NULL,
  description     text NOT NULL,
 	created_at      timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY    (id),
  CONSTRAINT     fk_rt_artist_concerts_artist_id FOREIGN KEY (artist_id) REFERENCES rt_artists (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT     fk_rt_artist_concerts_tour_id FOREIGN KEY (tour_id) REFERENCES rt_tours (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX ind_rt_artist_concerts_artist_id_country_event_date ON rt_artist_concerts ( artist_id, country, event_date );


INSERT INTO "rt_artist_concerts" VALUES (1,5,'London',51.5085,-0.1258,25500,'2018-05-16 18:30:00',NULL,'UK','Some festival on <b>London</b>. Join us ' ||
 'and have fun! ','2018-03-13 07:34:58'),
 (2,5,'Birmingham',52.4814,-1.8999,5850,'2018-05-23 18:00:00',NULL,'UK','Big concert in <b>Birmingham</b>. Don'' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-03-13 07:34:58'),
 (3,5,'Manchester',53.4809,-2.2375,3200,'2018-05-30 14:00:00',NULL,'UK','Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ','2018-03-29 08:24:24');


INSERT INTO "rt_artist_concerts" VALUES (4,6,'London',51.5085,-0.1258,25500,'2018-06-11 17:00:00',NULL,'UK','Folk festival in <b>London</b>. Join us ! ','2018-03-31 11:04:42');

INSERT INTO "rt_artist_concerts" VALUES
 (5,7,'Birmingham',52.4814,-1.8999,5850,'2018-05-23 18:00:00',NULL,'UK','Big concert in <b>Birmingham</b>. Don'' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-03-13 07:34:58'),
 (6,7,'Manchester',53.4809,-2.2375,3200,'2018-05-30 14:00:00',NULL,'UK','Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ','2018-03-29 08:24:24');




 INSERT INTO rt_songs VALUES
(74,'Motherless Children','Motherless Children short description...',74,true,'Motherless Children song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:52:41', NULL),
(75,'Badge','Badge short description...',75,true,'Badge song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:53:13', NULL),
(76,'Cocaine','Cocaine short description...',76,true,'Cocaine song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:54:21', NULL),
(77,'Bell Bottom Blues','Bell Bottom Blues short description...',77,true,'Bell Bottom Blues song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:54:59', NULL),
(78,'I Shot the Sheriff','I Shot the Sheriff short description...',78,true,'I Shot the Sheriff song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:55:20', NULL);


INSERT INTO rt_songs VALUES
(79,' Thunder Road',' Thunder Road short description...',79,true,' Thunder Road song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:55:59', NULL),
(80,'The River ','The River  short description...',80,true,'The River  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:56:23', NULL),
(81,'Darkness On The Edge of Town ','Darkness On The Edge of Town  short description...',81,true,'Darkness On The Edge of Town  song description lorem ipsum dolor sit amet, ' ||
 'consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:57:37', NULL),
(82,'I’m Goin’ Down','I’m Goin’ Down short description...',82,true,'I’m Goin’ Down song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:58:58', NULL),
(83,'Jungleland','Jungleland short description...',83,true,'Jungleland song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 09:59:01', NULL);


INSERT INTO rt_songs VALUES
(84,'Hope of Deliverance','Hope of Deliverance short description...',84,true,'Hope of Deliverance song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:02:23', NULL),
(85,'Heart of the Country','Heart of the Country short description...',85,true,'Heart of the Country song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:02:57', NULL),
(86,'FourFiveSeconds','FourFiveSeconds short description...',86,true,'FourFiveSeconds song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:02:23', NULL),
(87,'Say Say Say','Say Say Say short description...',87,true,'Say Say Say song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:03:53', NULL),
(88,'Magneto and Titanium Man','Magneto and Titanium Man short description...',88,true,'Magneto and Titanium Man song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:04:43', NULL),
(89,'The Back Seat of My Car','The Back Seat of My Car short description...',89,true,'The Back Seat of My Car song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:05:51', NULL),
(90,'I''ve Had Enough','I''ve Had Enough short description...',90,true,'I''ve Had Enough song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2018-04-04 10:06:21', NULL);




 ===========================


DROP DOMAIN IF EXISTS type_location cascade;
CREATE DOMAIN type_location AS numeric(22,16);

CREATE SEQUENCE rt_tour_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

DROP TABLE IF EXISTS rt_tours cascade;
CREATE TABLE rt_tours (
  id           integer NOT NULL DEFAULT nextval('rt_tour_id_seq'::regclass) NOT NULL,
  artist_id    integer NOT NULL,
  tour_name    character varying(100) NOT NULL,
  start_date   date NOT NULL,
  end_date     date NOT NULL,
  ordering     integer DEFAULT NULL,
  color        character varying(20) NOT NULL,
  description  text NOT NULL,
  created      timestamp NOT NULL DEFAULT now(),
  PRIMARY KEY  (id),
  CONSTRAINT   fk_rt_tours_artist_id FOREIGN KEY (artist_id) REFERENCES rt_artists (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX ind_rt_tours_artist_id_start_date_end_date ON rt_tours ( artist_id, start_date, end_date );

INSERT INTO "rt_tours" VALUES (1,5,'Small Tour of Vopli Vidopliassova ','2015-12-11','2015-12-22',1,'grey','Small Tour of Vopli Vidopliassova during 10 days ','2016-01-16 05:10:55'),(2,5,'Big Tour of Vopli Vidopliassova ','2016-01-12','2016-02-02',2,'blue','Big Tour of Vopli Vidopliassova during 20 days ','2015-12-12 11:57:03');




CREATE SEQUENCE rt_artist_image_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_artist_images cascade;
CREATE TABLE rt_artist_images (
  id                integer  NOT NULL DEFAULT nextval('rt_artist_image_id_seq'::regclass) NOT NULL,
  artist_id         integer NOT NULL,
  image_name        character varying (100) NOT NULL,
  is_main           boolean DEFAULT false,
  is_home_page  boolean DEFAULT false,
  description  text NOT NULL,
 	created_at        timestamp NOT NULL DEFAULT now(),

  PRIMARY KEY      (id),
  CONSTRAINT       fk_rt_artist_images_artist_id FOREIGN KEY (artist_id) REFERENCES rt_artists (id) ON DELETE NO ACTION ON UPDATE NO ACTION
);
CREATE INDEX       ind_rt_artist_images_is_home_page ON rt_artist_images ( is_home_page );
CREATE INDEX       ind_rt_artist_images_artist_id_is_main ON rt_artist_images ( artist_id, is_main );
CREATE UNIQUE INDEX  ind_rt_artist_images_artist_id_image_name ON rt_artist_images (artist_id, image_name);


INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(1, 1, 'a6414f9f8bc.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:42:43'),
(2, 1, 'Ella-Fitzgerald-008.jpg', FALSE, FALSE, 'Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2015-05-25 04:42:51'),
(3, 1, 'ella.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:42:57'),
(4, 1, 'Ella_Fitzgerald71.jpg', FALSE, FALSE, 'Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2015-05-25 04:43:05'),
(5, 1, 'Ellarodgershart.jpg', TRUE, TRUE, 'Ella Fitzgerald Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?', '2015-05-25 04:43:13'),
(6, 1, 'images.jpeg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:43:30'),
(7, 1, 'Uplifting-Voice.jpg', FALSE, TRUE, 'Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2015-05-25 04:43:40'),
(8, 1, '170px-Ella_Fitzgerald_1940.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:44:10'),
(9, 1, '220px-Ella_Fitzgerald_in_September_1947.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:44:40'),
(10, 1, '170px-Ella_Fitzgerald_1968.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:45:04'),
(11, 1, '170px-Ella_Fitzgerald_1960_by_Erling_Mandelmann.jpg', FALSE, FALSE, 'Ella Fitzgerald', '2015-05-25 04:45:18');


INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(12, 2, '04f-demis-roussos.jpg', FALSE, FALSE, 'Demis Roussos Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.', '2015-05-25 08:33:53'),
(13, 2, '84886743.jpg', FALSE, FALSE, 'Demis Roussos ', '2015-05-25 08:34:01'),
(14, 2, '164201319.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:34:06'),
(15, 2, '164201322.jpg', FALSE, FALSE, 'Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?', '2015-05-25 08:34:16'),
(16, 2, '171297820__843752b.jpg', TRUE, TRUE, 'Demis Roussos', '2015-05-25 08:34:23'),
(17, 2, '462275572.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:34:31'),
(18, 2, '462275664.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:34:39'),
(19, 2, 'Demis-Roussos-630x420.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:34:53'),
(20, 2, 'demis-roussos-died-390x285.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:35:02'),
(21, 2, 'demis.jpg', FALSE, TRUE, 'Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.', '2015-05-25 08:35:10'),
(22, 2, 'Demis+Roussos+9675b7f8201f.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:35:24'),
(23, 2, 'Demis+Roussos+15141232026161.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:35:32'),
(24, 2, 'Demis+Roussos+ac_group.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:35:38'),
(25, 2, 'Demis+Roussos+demis_roussos_best_of_demis_ro.jpg', FALSE, FALSE, 'Demis Roussos', '2015-05-25 08:35:43');


INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(26, 3, '6e35b4c70fa8b1b43cd019f55c36c6f4_large.jpeg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:02:48'),
(27, 3, '61pq3jMocmL._SL290_.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:03:34'),
(28, 3, '600full-frank-sinatra.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:03:40'),
(29, 3, '140304-gp-sinatra-3a_3a5319360f0921138fa376d6596e0972.jpg', FALSE, FALSE, 'Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?', '2015-05-25 09:03:47'),
(30, 3, 'filepicker_XeDQHWSZQsKBNqrXS7iA_Frank_Sinatra.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:03:57'),
(31, 3, 'Frank_Sinatra_Metronome_magazine_November_1950.JPG', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:04:32'),
(32, 3, 'hqdefault.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:04:39'),
(33, 3, 'images.jpeg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:04:46'),
(34, 3, 'imagese.jpeg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:04:53'),
(35, 3, 'imagessa.jpeg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:05:00'),
(36, 3, 'sinatra-stamp.jpg', TRUE, TRUE, 'Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit', '2015-05-25 09:05:06'),
(37, 3, 'sinatra_hands.jpg', FALSE, TRUE, 'Frank Sinatra', '2015-05-25 09:05:13'),
(38, 3, 'tumblr_n1b6e98aOT1tswkwdo1_500.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:05:20'),
(39, 3, 'Eleanor_Roosevelt_Frank_Sinatra_1947.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:06:39'),
(40, 3, 'Frank-Sinatra-Wallpaper-frank-sinatra-5581024-1024-768.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:06:45'),
(41, 3, 'franksinatra.jpg', FALSE, FALSE, 'Frank Sinatra', '2015-05-25 09:06:54');

INSERT INTO "rt_artist_images" ("id", "artist_id", "image_name", "is_main", "is_home_page", "description", "created_at") VALUES
(42, 4, '12476a39366da06f65d904c477db8ff6_large.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:15:32'),
(43, 4, 'AP5606220233.jpg', TRUE, TRUE, 'Elvis Presley', '2015-05-25 09:20:34'),
(44, 4, 'Elvis-Presley-009.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:20:39'),
(45, 4, 'elvis-presley.jpg', FALSE, TRUE, 'Elvis Presley', '2015-05-25 09:20:45'),
(46, 4, 'elvis_50s_308_4.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:21:01'),
(47, 4, 'elvis_50s_308_6.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:21:10'),
(48, 4, 'elvis_50s_308_7.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:21:23'),
(49, 4, 'elvis_50s_308_8.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:21:31'),
(50, 4, 'images.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:22:07'),
(51, 4, 'images5.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:22:14'),
(52, 4, 'index.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:22:19'),
(53, 4, 'indexer.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:22:49'),
(54, 4, 'indwrex.jpeg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:22:55'),
(55, 4, 'INV16955.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:23:01'),
(56, 4, 'INV35425.jpg', FALSE, FALSE, 'Elvis Presley', '2015-05-25 09:23:18');





 1=> 'pop',2=> 'rock',3=> 'jazz', 4=> 'classic', 5=>'rap' );
DROP SEQUENCE IF EXISTS rt_genre_id_seq cascade;
CREATE SEQUENCE rt_genre_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_genres  cascade;
CREATE TABLE public.rt_genres (
	id              integer NOT NULL DEFAULT nextval('rt_genre_id_seq'::regclass) NOT NULL,
	name            varchar(100) NOT NULL,
	description     text NOT NULL,
	published       bool DEFAULT false,
	created_at      timestamp DEFAULT now() NOT NULL,
	updated_at      timestamp,
	CONSTRAINT      rt_genres_pkey PRIMARY KEY (id)
) ;
CREATE INDEX ind_genre_published ON public.rt_genres (published) ;
CREATE UNIQUE INDEX ind_rt_genres_unique ON public.rt_genres (name) ;



INSERT INTO rt_genres (id, name, description, published) VALUES (1, 'Pop', 'Pop Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum ', true);
INSERT INTO rt_genres (id, name, description, published) VALUES (2, 'Rock', 'Rock Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', true);
INSERT INTO rt_genres (id, name, description, published) VALUES (3, 'Jazz', 'Jazz Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', true);
INSERT INTO rt_genres (id, name, description, published) VALUES (4, 'Classic', 'Classic Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ', true);
INSERT INTO rt_genres (id, name, description, published) VALUES (5, 'Rap', 'Rap Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', true);





DROP SEQUENCE IF EXISTS rt_song_id_seq cascade;
CREATE SEQUENCE rt_song_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_songs cascade;
CREATE TABLE rt_songs (
  id          integer DEFAULT nextval('rt_song_id_seq'::regclass) NOT NULL,
  title       varchar(100) NOT NULL DEFAULT '',
	short_descr varchar(255) NOT NULL,

  ordering    integer DEFAULT NULL,
  is_active   boolean DEFAULT false,
	description text NOT NULL,
  created_at  timestamp NOT NULL DEFAULT now(),
	updated_at  timestamp,

  PRIMARY KEY (id)
);
CREATE INDEX ind_rt_songs_is_active_ordering ON rt_songs ( is_active, ordering );
CREATE INDEX ind_rt_songs_title ON rt_songs (title);


INSERT INTO rt_songs VALUES
(1,'Summertime (High Quality - Remastered)','Summertime (High Quality - Remastered) short description...',5,true,'Summertime (High Quality - Remastered) song description lorem ' ||
 'ipsum dolor sit amet,' ||
 ' consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2015-11-19 09:53:40', NULL),
(2,'Mack the Knife (in Berlin)','Mack the Knife (in Berlin) short description...',2,false,'Mack the Knife (in Berlin) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-03 16:17:28', NULL),
(3,'Forget Domani','Forget Domani  short description...',3,true,'Forget Domani lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-22 20:15:26', NULL),
(4,'Rain and Tears','Rain and Tears short description...',1,false,'Rain and Tears lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-15 11:20:27', NULL),
(5,'That\''s Life','That\''s Life short description...',4,true,'That\''s Life', '2015-12-27 04:59:32', NULL),
(6,'THE DOZENS','THE DOZENS short description...',6,true,'THE DOZENS lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-12 21:21:12', NULL),
(7,'Can\''t Hep Falling In Love','Can\''t Hep Falling In Love short description...',88,true,'Can\''t Hep Falling In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2015-12-19 10:57:46', NULL),
(8,'Always On My Mind','Always On My Mind short description...',7,false,'Always On My Mind lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-25 15:55:13', NULL),
(30,'Perd�name','Perd�name short description...',3,false,'Perd�name lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-06 16:52:20', NULL),
(31,'Can\''t Say How Much I LoveYou','Can\''t Say How Much I LoveYou short description...',23,false, 'Can\''t Say How Much I LoveYou lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-21 02:47:08', NULL),
(32,'Souvenirs','Souvenirs short description...',23,true,'Souvenirs lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-05 22:34:16', NULL),
(33,'My Only Fascination','My Only Fascination short description...',34,true,'My Only Fascination lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-23 18:43:32', NULL),
(34,'Goodbye My Love, Goodbye', 'Goodbye My Love, Goodbye short description...',32,true,'Goodbye My Love, Goodbye lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-08 15:03:29', NULL),
(35,'When Forever Has Gone','When Forever Has Gone short description...',23,true,'When Forever Has Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-17 08:41:30', NULL),
(36,'Come Waltz With Me','Come Waltz With Me short description...',23,true,'Come Waltz With Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-29 13:16:59', NULL),
(37,'Lost In Love','Lost In Love short description...',23,true,'Lost In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-18 01:12:39', NULL),
(38,'Someday Somewhere','Someday Somewhere short description...',23,true,'Someday Somewhere lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-24 23:58:36', NULL),
(39,'Happy To Be On An Island In The Sun','Happy To Be On An Island In The Sun short description...',5,true,'Happy To Be On An Island In The Sun lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-10 15:44:22', NULL),
(40,'Sailin\'' Home','Sailin\'' Home short description...',34,true,'Sailin\'' Home lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-24 04:37:23', NULL),
(41,'Only the Lonely','Only the Lonely short description...',54,true,'Only the Lonely lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-20 06:54:24', NULL),
(42,'Angel Eyes','Angel Eyes short description...',45,true,'Angel Eyes lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-28 20:51:48', NULL),
(43,'It\''s a Lonesome Old Town','It\''s a Lonesome Old Town short description...',65,true,'It\''s a Lonesome Old Town lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-23 10:18:20', NULL),
(44,'Willow Weep for Me','Willow Weep for Me short description...',4,true,'Willow Weep for Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-09 12:20:36', NULL),
(45,'Guess I\''ll Hang My Tears Out to Dry','Guess I\''ll Hang My Tears Out to Dry short description...',34,true,'Guess I\''ll Hang My Tears Out to Dry','2015-12-07 10:14:46', NULL),
(46,'Blues in the Night','Blues in the Night short description...',45,true,'Blues in the Night lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...', '2016-01-16 02:22:54', NULL),
(47,'One for My Baby (And One More for the Road)','One for My Baby (And One More for the Road) short description...',43,true,'One for My Baby (And One More for the Road) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-30 00:16:45', NULL),
(48,'Sleep Warm','Sleep Warm short description...',3,true,'Sleep Warm lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-25 19:09:37', NULL),
(49,'Where or When','Where or When short description...',34,true,'Where or When lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-17 22:00:22', NULL),
(50,'Spring Is Here','Spring Is Here short description...',45,true,'Spring Is Here lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-30 19:08:25', NULL),
(51,'A Big Hunk O\'' Love','A Big Hunk O\'' Love short description...',45,true,'A Big Hunk O\'' Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-18 09:44:38', NULL),
(52,'A Boy Like Me, a Girl Like You','A Boy Like Me, a Girl Like You short description...',89,true,'A Boy Like Me, a Girl Like You lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-06 23:15:46', NULL),
(53,'A Cane and a High Starched Collar','A Cane and a High Starched Collar short description...',6,true,'A Cane and a High Starched Collar lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-07 09:32:17', NULL),
(54,'Elvis Presley lyrics -- Elvis A-Z -- SongDataBase','Elvis Presley lyrics -- Elvis A-Z -- SongDataBase short description...',8,true,'Elvis Presley lyrics -- Elvis A-Z -- SongDataBase lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-17 19:12:39', NULL),
(55,'America the Beautiful','America the Beautiful short description...',8,true,'America the Beautiful lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-28 09:24:26', NULL),
(56,'Amazing Grace','Amazing Grace short description...',7,true,'Amazing Grace lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-06 10:04:38', NULL),
(57,'And I Love You So','And I Love You So short description...',6,true,'And I Love You So lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-15 00:58:38', NULL),
(58,'Are You Lonesome Tonight','Are You Lonesome Tonight short description...',3,true,'Are You Lonesome Tonight lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-06 02:02:23', NULL),
(59,'Blue Christmas','Blue Christmas short description...',8,true,'Blue Christmas lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-13 17:00:38', NULL),
(60,'Crying In The Chapel','Crying In The Chapel short description...',6,true,'Crying In The Chapel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-09 03:23:32', NULL),
(61,'Jailhouse Rock','Jailhouse Rock short description...',34,true,'Jailhouse Rock lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-12 20:49:26', NULL),
(62,'Return To Sender','Return To Sender short description...',8,true,'Return To Sender lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-30 23:05:51', NULL),
(63,'Undecided','Undecided short description...',63,true,'Undecided lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-19 09:17:50', NULL),
(64,'A-Tisket, A-Tasket','A-Tisket, A-Tasket short description...',64,true,'A-Tisket, A-Tasket lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-29 06:28:41', NULL),
(65,'Dream a Little Dream of Me','Dream a Little Dream of Me short description...',65,true,'Dream a Little Dream of Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-07 09:19:21', NULL),
(66,'Sugar Blues','Sugar Blues short description...',66,true,'Sugar Blues lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-22 07:43:15', NULL),
(67,'The Starlit Hour','The Starlit Hour short description...',67,true,'The Starlit Hour lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-11-17 16:52:03', NULL),
(68,'Carmen','Carmen short description...',68,true,'Carmen lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-23 05:35:44', NULL),
(69,'Car','Car short description...',69,true,'Car lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-31 08:10:03', NULL),
(70,'I\''ll Come','I\''ll Come short description...',70,true,'I\''ll Come lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-10 04:54:54', NULL),
(71,'Mussa','Mussa short description...',71,true,'Mussa lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2016-01-03 02:44:50', NULL),
(72,'You\''ve Gone','You\''ve Gone short description...',72,true,'You\''ve Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-10 04:31:54','2015-12-10 04:31:54'),
(73,'Winter Again','Winter Again short description...',73,true,'Winter Again lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...','2015-12-23 18:15:22','2015-12-23 18:15:22');






DROP SEQUENCE IF EXISTS rt_song_genre_id_seq cascade;
CREATE SEQUENCE rt_song_genre_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_song_genres cascade;
CREATE TABLE rt_song_genres (
  id            integer DEFAULT nextval('rt_song_genre_id_seq'::regclass) NOT NULL,
  song_id       integer NOT NULL,
  genre_id      integer NOT NULL,
	created_at    timestamp DEFAULT now() NOT NULL,
  PRIMARY KEY   (id),
  CONSTRAINT    fk_rt_song_genres_song_id FOREIGN KEY (song_id) REFERENCES rt_songs (ID) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT    fk_rt_song_genres_genre_id FOREIGN KEY (genre_id) REFERENCES rt_genres (ID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

INSERT INTO rt_song_genres VALUES (1,1,2,'2016-01-17 10:24:53'),(2,1,4,'2016-01-08 11:48:35'),(3,2,1,'2016-01-08 07:25:04'),(4,2,3,'2015-12-10 19:40:55'),(5,3,5,'2016-01-08 21:26:24'),(6,4,2,'2015-12-02 05:06:24'),(20,30,1,'2015-12-31 18:43:43'),(21,31,1,'2015-11-30 20:04:17'),(22,32,1,'2015-11-18 08:09:27'),(23,33,1,'2015-11-30 21:01:45'),(24,34,1,'2015-11-28 07:48:09'),(25,35,1,'2015-12-03 10:03:05'),(26,36,1,'2015-12-11 08:29:15'),(27,37,1,'2015-12-02 11:39:14'),(28,38,1,'2015-12-26 06:57:55'),(29,39,1,'2016-01-01 23:12:44'),(30,40,1,'2015-12-29 06:12:32'),(31,41,1,'2015-12-07 03:18:46'),(32,41,4,'2016-01-05 01:54:59'),(33,42,1,'2015-11-30 20:04:08'),(34,43,4,'2015-11-22 11:22:13'),(35,44,4,'2015-12-28 21:26:39'),(36,45,1,'2015-05-25 08:54:20'),(37,45,4,'2015-05-25 08:54:20'),(38,46,1,'2015-05-25 08:54:47'),(39,46,4,'2015-05-25 08:54:47'),(40,47,1,'2015-05-25 08:55:21'),(41,48,1,'2015-05-25 08:55:50'),(42,48,4,'2015-05-25 08:55:50'),(43,49,1,'2015-05-25 08:56:14'),(44,49,4,'2015-05-25 08:56:14'),(45,50,1,'2015-05-25 08:59:44'),(46,51,2,'2015-05-25 10:17:23'),(47,52,2,'2015-05-25 10:19:13'),(48,52,4,'2015-05-25 10:19:13'),(49,53,2,'2015-05-25 10:20:18'),(50,53,4,'2015-05-25 10:20:18'),(51,54,2,'2015-05-25 10:21:22'),(52,54,4,'2015-05-25 10:21:22'),(53,55,2,'2015-05-25 10:22:03'),(54,55,4,'2015-05-25 10:22:03'),(55,56,2,'2015-05-25 10:22:35'),(56,56,4,'2015-05-25 10:22:35'),(57,57,2,'2015-05-25 10:23:13'),(58,57,4,'2015-05-25 10:23:13'),(59,58,2,'2015-05-25 10:24:52'),(60,58,4,'2015-05-25 10:24:52'),(61,59,2,'2015-05-25 10:25:16'),(62,59,4,'2015-05-25 10:25:16'),(63,60,2,'2015-05-25 10:25:52'),(64,60,4,'2015-05-25 10:25:52'),(65,61,2,'2015-05-25 10:27:24'),(66,61,4,'2015-05-25 10:27:24'),(67,62,2,'2015-12-31 06:33:38'),(68,62,4,'2015-12-22 15:22:57'),(69,63,3,'2016-01-10 11:29:30'),(70,64,3,'2016-01-15 16:16:26'),(71,65,3,'2015-12-22 03:25:31'),(72,66,3,'2015-11-19 10:38:36'),(73,67,3,'2015-12-18 20:49:15');

INSERT INTO rt_song_genres(song_id, genre_id) VALUES
(74,2),
(75,2),
(76,2),
(77,2),
(78,2),
(79,2),
(80,2),
(81,2),
(82,2),
(83,2),
(84,2),
(85,2),
(86,2),
(87,2),
(88,2),
(89,2),
(90,2);

============

DROP SEQUENCE IF EXISTS rt_song_artist_id_seq cascade;
CREATE SEQUENCE rt_song_artist_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
DROP TABLE IF EXISTS rt_song_artists cascade;
CREATE TABLE rt_song_artists (
  id               integer DEFAULT nextval('rt_song_artist_id_seq'::regclass) NOT NULL,
  song_id          integer NOT NULL,
  artist_id        integer NOT NULL,
	created_at       timestamp DEFAULT now() NOT NULL,
  PRIMARY KEY      (id),
  CONSTRAINT       fk_rt_song_artists_song_id FOREIGN KEY (song_id) REFERENCES rt_songs (ID) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT       fk_rt_song_artists_artist_id FOREIGN KEY (artist_id) REFERENCES rt_artists (ID) ON DELETE NO ACTION ON UPDATE NO ACTION
);




INSERT INTO rt_song_artists (id, song_id, artist_id, created_at) VALUES
(1, 1, 1, '2015-12-08 04:53:21'),
(2, 1, 2, '2015-12-25 08:58:07'),
(3, 2, 1, '2016-01-15 06:03:22'),
(4, 2, 2, '2015-12-29 10:52:41'),
(5, 2, 3, '2015-12-28 00:08:15'),
(6, 4, 2, '2015-12-19 10:46:30'),
(7, 3, 3, '2015-11-29 07:07:06'),
(8, 5, 3, '2015-11-23 20:20:24'),
(9, 6, 1, '2016-01-07 04:43:54'),
(10, 6, 3, '2015-11-19 16:00:56'),
(11, 7, 4, '2016-01-06 00:17:59'),
(12, 8, 4, '2015-12-26 02:41:00'),
(34, 2, 1, '2015-12-28 16:14:37'),
(35, 3, 1, '2015-12-30 23:06:23'),
(36, 30, 2, '2015-11-22 03:28:50'),
(37, 31, 2, '2015-12-19 18:14:22'),
(38, 32, 2, '2016-01-08 22:14:45'),
(39, 33, 2, '2015-11-27 05:40:47'),
(40, 34, 2, '2015-12-21 21:47:59'),
(41, 35, 2, '2015-12-27 08:58:25'),
(42, 36, 2, '2015-11-25 03:01:38'),
(45, 39, 2, '2015-12-28 02:00:41'),
(46, 40, 2, '2015-12-25 09:32:30'),
(47, 41, 3, '2016-01-10 17:51:20'),
(48, 42, 3, '2015-12-30 01:04:29'),
(49, 43, 3, '2016-01-08 16:17:44'),
(50, 44, 3, '2015-12-05 00:42:50'),
(51, 45, 3, '2016-01-15 15:14:50'),
(52, 46, 3, '2015-11-29 04:28:13'),
(53, 47, 3, '2016-01-16 09:53:45'),
(54, 48, 3, '2015-11-24 08:15:13'),
(55, 49, 3, '2015-11-25 01:32:59'),
(56, 50, 3, '2015-12-28 13:29:39'),
(57, 51, 4, '2016-01-12 17:23:05'),
(58, 52, 4, '2016-01-13 01:53:25'),
(59, 53, 4, '2015-12-31 01:16:47'),
(60, 54, 4, '2015-11-23 16:59:49'),
(61, 55, 4, '2015-12-28 19:33:09'),
(62, 56, 4, '2016-01-15 18:22:32'),
(63, 57, 4, '2016-01-02 20:31:13'),
(64, 58, 4, '2016-01-10 22:50:53'),
(65, 59, 4, '2015-11-29 04:58:38'),
(66, 60, 4, '2015-12-19 18:03:14'),
(67, 61, 4, '2015-12-19 05:00:28'),
(68, 62, 4, '2016-01-14 20:17:16'),
(69, 63, 1, '2015-12-08 18:25:11'),
(70, 64, 1, '2016-01-07 16:43:39'),
(71, 65, 1, '2015-12-26 04:52:13'),
(72, 66, 1, '2016-01-16 15:07:04'),
(73, 67, 1, '2016-01-14 23:42:22');

INSERT INTO rt_song_artists (song_id, artist_id ) VALUES
(74,5),
(75,5),
(76,5),
(77,5),
(78,5);

INSERT INTO rt_song_artists (song_id, artist_id ) VALUES
(79,6),
(80,6),
(81,6),
(82,6),
(83,6);


INSERT INTO rt_song_artists (song_id, artist_id ) VALUES
(84,7),
(85,7),
(86,7),
(87,7),
(88,7),
(89,7),
(90,7);


LAST:

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rt_artists`
--

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rt_artists`
--
ALTER TABLE `rt_artists`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



CREATE TABLE public.rt_product (
	id int8 NOT NULL DEFAULT nextval('rt_product_id_seq'::regclass),
	title varchar(255) NOT NULL,
	status type_productstatus NOT NULL DEFAULT 'D'::type_productstatus,
	slug varchar(255) NULL,
	sku varchar(255) NOT NULL,
	user_id int4 NOT NULL,
	regular_price type_money NULL DEFAULT 0,
	sale_price type_money NULL DEFAULT 0,
	in_stock bool NOT NULL DEFAULT false,
	has_discount_prices bool NOT NULL DEFAULT false,
	stock_qty type_qty NOT NULL DEFAULT 0,
	is_backorder type_backorder NOT NULL DEFAULT 'N'::type_backorder,
	is_homepage bool NOT NULL DEFAULT false,
	is_featured bool NOT NULL DEFAULT false,
	weight int2 NULL,
	length int2 NULL,
	width int2 NULL,
	height int2 NULL,
	shipping_class_id int2 NULL,
	short_description varchar(255) NOT NULL,
	description text NULL,
	has_attributes bool NOT NULL,
	downloadable bool NOT NULL DEFAULT false,
	virtual bool NOT NULL DEFAULT false,
	rating_count int4 NOT NULL DEFAULT 0,
	rating_summary int4 NOT NULL DEFAULT 0,
	published_at timestamp NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NULL,
	CONSTRAINT ind_product_sku_unique UNIQUE (sku),
	CONSTRAINT ind_product_slug_unique UNIQUE (slug),
	CONSTRAINT rt_product_pkey PRIMARY KEY (id),
	CONSTRAINT rt_product_shipping_class_id_fkey FOREIGN KEY (shipping_class_id) REFERENCES public.rt_shipping_class(id),
	CONSTRAINT rt_product_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.rt_users(id)
)
WITH (
	OIDS=FALSE
) ;
create
    index idx_product_created_at on
    rt_product
        using btree(created_at) ;
create
    index idx_product_in_stock_regular_price_sale_price on
    rt_product
        using btree(
        in_stock,
        regular_price,
        sale_price
    ) ;
create
    index idx_product_is_backorder on
    rt_product
        using btree(is_backorder) ;
create
    index idx_product_published_at on
    rt_product
        using btree(published_at) ;
create
    index idx_product_status_is_featured on
    rt_product
        using btree(
        status,
        is_featured
    ) ;
create
    index idx_product_status_is_homepage on
    rt_product
        using btree(
        status,
        is_homepage
    ) ;
create
    index idx_product_status_sku_title on
    rt_product
        using btree(
        status,
        sku,
        title
    ) ;
