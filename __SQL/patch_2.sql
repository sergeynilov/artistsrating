


CREATE OR REPLACE FUNCTION public.rt_reports_artists_votes_ratings(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE(created_at date, artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT cast( av.created_at as date) AS created_at, a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by cast( av.created_at as date), av.artist_id, artist_name

  ORDER by cast( av.created_at as date) asc,

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$




CREATE OR REPLACE FUNCTION public.rt_reports_artists_votes_summary_by_period(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE( artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by av.artist_id, artist_name

  ORDER by

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$




reports_artists_votes_summary_by_period

select t.* from  reports_artists_votes_summary_by_period( p_artist_list := ARRAY[6,4,9]::integer[], p_created_at_from := '2018-04-01', p_created_at_till := '2018-04-15 23:59:59', p_sort_type := 'by_sum_asc', p_limit := null ) t

CREATE OR REPLACE FUNCTION public.reports_artists_votes_summary_by_period(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE( artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by av.artist_id, artist_name

  ORDER by

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$






CREATE OR REPLACE FUNCTION public.rt_report_orders_sum_by_days(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE(created_at date, artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT cast( av.created_at as date) AS created_at, a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by cast( av.created_at as date), av.artist_id, artist_name

  ORDER by cast( av.created_at as date) asc,

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$




ALTER TABLE public.rt_artists
  ADD COLUMN slug varchar(55) NOT NULL;

CREATE UNIQUE INDEX ind_rt_artists_slug ON public.rt_artists USING btree (slug) ;




ALTER TABLE public.rt_songs
  ADD COLUMN slug varchar(105) NOT NULL;
CREATE UNIQUE INDEX ind_rt_songs_slug ON public.rt_songs USING btree (slug) ;




ALTER TABLE public.rt_users
  ADD COLUMN provider varchar(50) NULL;
CREATE INDEX ind_rt_users_provider ON public.rt_users USING btree (provider) ;

ALTER TABLE public.rt_users
  ADD COLUMN provider_id varchar(255) NULL;
