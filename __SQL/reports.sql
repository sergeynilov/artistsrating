
CREATE TABLE public.rt_artist_translations (
	id serial NOT NULL,
	artist_id int4 NOT NULL,
	"name" varchar(50) NOT NULL,

// column a.name does not exist

CREATE OR REPLACE FUNCTION public.rt_reports_artists_votes_ratings(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE(created_at date, artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT cast( av.created_at as date) AS created_at, ( SELECT a_t.name FROM rt_artist_translations AS a_t WHERE a_t artist_id.id = av.artist_id AND a_t.locale= 'en' ) as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by cast( av.created_at as date), av.artist_id, artist_name

  ORDER by cast( av.created_at as date) asc,

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$





CREATE OR REPLACE FUNCTION public.rt_reports_artists_votes_summary_by_period(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer)
 RETURNS TABLE(artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
 LANGUAGE sql
AS $function$

SELECT a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by av.artist_id, artist_name

  ORDER by

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$function$
