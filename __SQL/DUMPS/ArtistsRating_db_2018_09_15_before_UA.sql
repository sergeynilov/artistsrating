--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Ubuntu 10.5-0ubuntu0.18.04)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: type_active_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.type_active_status AS ENUM (
    'A',
    'I',
    'N'
);


ALTER TYPE public.type_active_status OWNER TO postgres;

--
-- Name: type_cms_item_page_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.type_cms_item_page_type AS ENUM (
    'E',
    'P',
    'B'
);


ALTER TYPE public.type_cms_item_page_type OWNER TO postgres;

--
-- Name: type_location; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.type_location AS numeric(22,16);


ALTER DOMAIN public.type_location OWNER TO postgres;

--
-- Name: rt_f_max_numeric(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_f_max_numeric() RETURNS numeric
    LANGUAGE sql IMMUTABLE
    AS $$SELECT 99999999999::numeric$$;


ALTER FUNCTION public.rt_f_max_numeric() OWNER TO postgres;

--
-- Name: rt_f_max_timestamp(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_f_max_timestamp() RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $$SELECT '2099-12-31 23:59:59'::timestamp without time zone$$;


ALTER FUNCTION public.rt_f_max_timestamp() OWNER TO postgres;

--
-- Name: rt_f_min_numeric(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_f_min_numeric() RETURNS numeric
    LANGUAGE sql IMMUTABLE
    AS $$SELECT -1::numeric$$;


ALTER FUNCTION public.rt_f_min_numeric() OWNER TO postgres;

--
-- Name: rt_f_min_timestamp(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_f_min_timestamp() RETURNS timestamp without time zone
    LANGUAGE sql IMMUTABLE
    AS $$SELECT '1900-01-01 00:00:00'::timestamp without time zone$$;


ALTER FUNCTION public.rt_f_min_timestamp() OWNER TO postgres;

--
-- Name: rt_reports_artists_votes_ratings(integer[], timestamp without time zone, timestamp without time zone, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_reports_artists_votes_ratings(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer) RETURNS TABLE(created_at date, artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
    LANGUAGE sql
    AS $$

SELECT cast( av.created_at as date) AS created_at, a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by cast( av.created_at as date), av.artist_id, artist_name

  ORDER by cast( av.created_at as date) asc,

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$$;


ALTER FUNCTION public.rt_reports_artists_votes_ratings(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying, p_limit integer) OWNER TO postgres;

--
-- Name: rt_reports_artists_votes_summary_by_period(integer[], timestamp without time zone, timestamp without time zone, character varying, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rt_reports_artists_votes_summary_by_period(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying DEFAULT 'by_sum_asc'::character varying, p_limit integer DEFAULT NULL::integer) RETURNS TABLE(artist_name character varying, artist_id integer, vote_sum bigint, vote_qty bigint)
    LANGUAGE sql
    AS $$

SELECT a.name as artist_name,

   av.artist_id,

   sum( av.vote ) AS vote_sum,

   count( av.id ) AS vote_qty

  from rt_artist_votes as av join


      rt_artists as a on a.id = av.artist_id

  WHERE ( CASE when p_artist_list IS NOT NULL THEN av.artist_id = ANY (p_artist_list) else true END ) AND

       ( av.created_at BETWEEN coalesce(p_created_at_from,rt_f_min_timestamp()) AND coalesce(p_created_at_till,rt_f_max_timestamp()) )


  group by av.artist_id, artist_name

  ORDER by 

    	CASE WHEN p_sort_type = 'by_sum_asc' THEN

		    sum( av.vote )

        end ASC,

    	CASE WHEN p_sort_type = 'by_sum_desc' THEN

		    sum( av.vote )

        end DESC,

    	CASE WHEN p_sort_type = 'by_sum_artist_name' THEN

		    a.name

		end ASC

  LIMIT p_limit ;

$$;


ALTER FUNCTION public.rt_reports_artists_votes_summary_by_period(p_artist_list integer[], p_created_at_from timestamp without time zone, p_created_at_till timestamp without time zone, p_sort_type character varying, p_limit integer) OWNER TO postgres;

--
-- Name: rt_artist_concert_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_concert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_concert_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: rt_artist_concert_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_concert_translations (
    id integer NOT NULL,
    artist_concert_id integer NOT NULL,
    place_name character varying(100) NOT NULL,
    description text NOT NULL,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    locale character varying(2) NOT NULL
);


ALTER TABLE public.rt_artist_concert_translations OWNER TO postgres;

--
-- Name: rt_artist_concert_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_concert_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_concert_translations_id_seq OWNER TO postgres;

--
-- Name: rt_artist_concert_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_artist_concert_translations_id_seq OWNED BY public.rt_artist_concert_translations.id;


--
-- Name: rt_artist_concerts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_concerts (
    id integer DEFAULT nextval('public.rt_artist_concert_id_seq'::regclass) NOT NULL,
    artist_id integer NOT NULL,
    lat public.type_location NOT NULL,
    lng public.type_location NOT NULL,
    participants integer,
    event_date timestamp without time zone NOT NULL,
    tour_id integer,
    country character varying(30) NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_artist_concerts OWNER TO postgres;

--
-- Name: rt_artist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_id_seq OWNER TO postgres;

--
-- Name: rt_artist_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_image_id_seq OWNER TO postgres;

--
-- Name: rt_artist_image_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_image_translations (
    id integer NOT NULL,
    artist_image_id integer NOT NULL,
    description text NOT NULL,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    locale character varying(2) NOT NULL
);


ALTER TABLE public.rt_artist_image_translations OWNER TO postgres;

--
-- Name: rt_artist_image_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_image_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_image_translations_id_seq OWNER TO postgres;

--
-- Name: rt_artist_image_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_artist_image_translations_id_seq OWNED BY public.rt_artist_image_translations.id;


--
-- Name: rt_artist_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_images (
    id integer DEFAULT nextval('public.rt_artist_image_id_seq'::regclass) NOT NULL,
    artist_id integer NOT NULL,
    image_name character varying(100) NOT NULL,
    is_main boolean DEFAULT false,
    is_home_page boolean DEFAULT false,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_artist_images OWNER TO postgres;

--
-- Name: rt_artist_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_translations (
    id integer NOT NULL,
    artist_id integer NOT NULL,
    name character varying(50) NOT NULL,
    info text NOT NULL,
    locale character varying(2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_artist_translations OWNER TO postgres;

--
-- Name: rt_artist_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_translations_id_seq OWNER TO postgres;

--
-- Name: rt_artist_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_artist_translations_id_seq OWNED BY public.rt_artist_translations.id;


--
-- Name: rt_artist_vote_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_artist_vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_artist_vote_id_seq OWNER TO postgres;

--
-- Name: rt_artist_votes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artist_votes (
    id integer DEFAULT nextval('public.rt_artist_vote_id_seq'::regclass) NOT NULL,
    artist_id integer NOT NULL,
    user_id integer NOT NULL,
    vote smallint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_artist_votes OWNER TO postgres;

--
-- Name: rt_artists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_artists (
    id integer DEFAULT nextval('public.rt_artist_id_seq'::regclass) NOT NULL,
    ordering integer NOT NULL,
    is_active public.type_active_status DEFAULT 'N'::public.type_active_status NOT NULL,
    has_concerts boolean DEFAULT false,
    birthday date,
    day_of_death date,
    site character varying(100),
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    single boolean DEFAULT true NOT NULL,
    slug character varying(55) NOT NULL
);


ALTER TABLE public.rt_artists OWNER TO postgres;

--
-- Name: rt_cms_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_cms_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_cms_item_id_seq OWNER TO postgres;

--
-- Name: rt_cms_item_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_cms_item_translations (
    id integer NOT NULL,
    cms_item_id integer NOT NULL,
    title character varying(100) NOT NULL,
    short_descr character varying(255) NOT NULL,
    content text NOT NULL,
    locale character varying(2) NOT NULL
);


ALTER TABLE public.rt_cms_item_translations OWNER TO postgres;

--
-- Name: rt_cms_item_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_cms_item_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_cms_item_translations_id_seq OWNER TO postgres;

--
-- Name: rt_cms_item_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_cms_item_translations_id_seq OWNED BY public.rt_cms_item_translations.id;


--
-- Name: rt_cms_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_cms_items (
    id integer DEFAULT nextval('public.rt_cms_item_id_seq'::regclass) NOT NULL,
    alias character varying(100) NOT NULL,
    page_type public.type_cms_item_page_type NOT NULL,
    content_hints text,
    image character varying(50),
    icon character varying(50),
    user_id integer NOT NULL,
    published boolean DEFAULT false,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.rt_cms_items OWNER TO postgres;

--
-- Name: rt_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_genre_id_seq OWNER TO postgres;

--
-- Name: rt_genre_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_genre_translations (
    id integer NOT NULL,
    genre_id integer NOT NULL,
    name character varying(100) NOT NULL,
    description text NOT NULL,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    locale character varying(2) NOT NULL
);


ALTER TABLE public.rt_genre_translations OWNER TO postgres;

--
-- Name: rt_genre_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_genre_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_genre_translations_id_seq OWNER TO postgres;

--
-- Name: rt_genre_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_genre_translations_id_seq OWNED BY public.rt_genre_translations.id;


--
-- Name: rt_genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_genres (
    id integer DEFAULT nextval('public.rt_genre_id_seq'::regclass) NOT NULL,
    published boolean DEFAULT false,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    slug character varying(105) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.rt_genres OWNER TO postgres;

--
-- Name: rt_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_groups (
    id smallint NOT NULL,
    name character varying(20) NOT NULL,
    description character varying(100) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_groups OWNER TO postgres;

--
-- Name: rt_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_groups_id_seq OWNER TO postgres;

--
-- Name: rt_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_groups_id_seq OWNED BY public.rt_groups.id;


--
-- Name: rt_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.rt_migrations OWNER TO postgres;

--
-- Name: rt_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_migrations_id_seq OWNER TO postgres;

--
-- Name: rt_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_migrations_id_seq OWNED BY public.rt_migrations.id;


--
-- Name: rt_model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_model_has_permissions (
    permission_id integer NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.rt_model_has_permissions OWNER TO postgres;

--
-- Name: rt_model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_model_has_roles (
    role_id integer NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.rt_model_has_roles OWNER TO postgres;

--
-- Name: rt_order_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_order_items (
    id integer NOT NULL,
    order_id integer,
    product_id integer,
    product_type character varying(10) NOT NULL,
    qty integer NOT NULL,
    price double precision NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_order_items OWNER TO postgres;

--
-- Name: rt_order_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_order_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_order_items_id_seq OWNER TO postgres;

--
-- Name: rt_order_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_order_items_id_seq OWNED BY public.rt_order_items.id;


--
-- Name: rt_orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_orders (
    id integer NOT NULL,
    user_id integer,
    card_owner character varying(100) NOT NULL,
    discount integer DEFAULT 0,
    discount_code character varying(255),
    qty_count integer NOT NULL,
    price_total numeric(10,2) NOT NULL,
    payment character varying(255) NOT NULL,
    completed boolean DEFAULT false NOT NULL,
    error_message character varying(255),
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.rt_orders OWNER TO postgres;

--
-- Name: rt_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_orders_id_seq OWNER TO postgres;

--
-- Name: rt_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_orders_id_seq OWNED BY public.rt_orders.id;


--
-- Name: rt_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_permissions (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_permissions OWNER TO postgres;

--
-- Name: rt_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_permissions_id_seq OWNER TO postgres;

--
-- Name: rt_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_permissions_id_seq OWNED BY public.rt_permissions.id;


--
-- Name: rt_role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_role_has_permissions (
    permission_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.rt_role_has_permissions OWNER TO postgres;

--
-- Name: rt_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_roles OWNER TO postgres;

--
-- Name: rt_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_roles_id_seq OWNER TO postgres;

--
-- Name: rt_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_roles_id_seq OWNED BY public.rt_roles.id;


--
-- Name: rt_settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_settings (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_settings OWNER TO postgres;

--
-- Name: rt_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_settings_id_seq OWNER TO postgres;

--
-- Name: rt_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_settings_id_seq OWNED BY public.rt_settings.id;


--
-- Name: rt_shoppingcart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_shoppingcart (
    identifier character varying(255) NOT NULL,
    instance character varying(255) NOT NULL,
    content text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_shoppingcart OWNER TO postgres;

--
-- Name: rt_song_artist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_song_artist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_song_artist_id_seq OWNER TO postgres;

--
-- Name: rt_song_artists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_song_artists (
    id integer DEFAULT nextval('public.rt_song_artist_id_seq'::regclass) NOT NULL,
    song_id integer NOT NULL,
    artist_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_song_artists OWNER TO postgres;

--
-- Name: rt_song_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_song_genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_song_genre_id_seq OWNER TO postgres;

--
-- Name: rt_song_genres; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_song_genres (
    id integer DEFAULT nextval('public.rt_song_genre_id_seq'::regclass) NOT NULL,
    song_id integer NOT NULL,
    genre_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_song_genres OWNER TO postgres;

--
-- Name: rt_song_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_song_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_song_id_seq OWNER TO postgres;

--
-- Name: rt_song_translations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_song_translations (
    id integer NOT NULL,
    song_id integer NOT NULL,
    title character varying(100) NOT NULL,
    short_descr character varying(255) NOT NULL,
    description text NOT NULL,
    locale character varying(2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.rt_song_translations OWNER TO postgres;

--
-- Name: rt_song_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_song_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_song_translations_id_seq OWNER TO postgres;

--
-- Name: rt_song_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_song_translations_id_seq OWNED BY public.rt_song_translations.id;


--
-- Name: rt_song_vote_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_song_vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_song_vote_id_seq OWNER TO postgres;

--
-- Name: rt_song_votes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_song_votes (
    id integer DEFAULT nextval('public.rt_song_vote_id_seq'::regclass) NOT NULL,
    song_id integer NOT NULL,
    user_id integer NOT NULL,
    vote smallint NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_song_votes OWNER TO postgres;

--
-- Name: rt_songs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_songs (
    id integer DEFAULT nextval('public.rt_song_id_seq'::regclass) NOT NULL,
    ordering integer,
    is_active boolean DEFAULT false,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone,
    slug character varying(105) NOT NULL
);


ALTER TABLE public.rt_songs OWNER TO postgres;

--
-- Name: rt_tour_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_tour_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_tour_id_seq OWNER TO postgres;

--
-- Name: rt_tours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_tours (
    id integer DEFAULT nextval('public.rt_tour_id_seq'::regclass) NOT NULL,
    artist_id integer NOT NULL,
    tour_name character varying(100) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    ordering integer,
    color character varying(20) NOT NULL,
    description text NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_tours OWNER TO postgres;

--
-- Name: rt_users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255),
    password character varying(255),
    status character varying(255) DEFAULT 'N'::character varying NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    phone character varying(50),
    website character varying(50),
    remember_token character varying(100),
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    updated_at timestamp(0) without time zone,
    avatar character varying(100),
    provider_name character varying(50),
    provider_id character varying(255),
    verified boolean DEFAULT false NOT NULL,
    verification_token character varying(255),
    CONSTRAINT rt_users_status_check CHECK (((status)::text = ANY (ARRAY[('N'::character varying)::text, ('A'::character varying)::text, ('I'::character varying)::text])))
);


ALTER TABLE public.rt_users OWNER TO postgres;

--
-- Name: COLUMN rt_users.status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.rt_users.status IS ' N => New(Waiting activation), A=>Active, I=>Inactive';


--
-- Name: rt_users_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_users_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rt_users_groups OWNER TO postgres;

--
-- Name: rt_users_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_users_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_users_groups_id_seq OWNER TO postgres;

--
-- Name: rt_users_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_users_groups_id_seq OWNED BY public.rt_users_groups.id;


--
-- Name: rt_users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_users_id_seq OWNER TO postgres;

--
-- Name: rt_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_users_id_seq OWNED BY public.rt_users.id;


--
-- Name: rt_users_logins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rt_users_logins (
    id integer NOT NULL,
    provider_name character varying(50) NOT NULL,
    username character varying(255) NOT NULL,
    user_id integer,
    remote_addr character varying(50) NOT NULL,
    with_success boolean NOT NULL,
    created_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.rt_users_logins OWNER TO postgres;

--
-- Name: rt_users_logins_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rt_users_logins_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rt_users_logins_id_seq OWNER TO postgres;

--
-- Name: rt_users_logins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rt_users_logins_id_seq OWNED BY public.rt_users_logins.id;


--
-- Name: rt_artist_concert_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concert_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_artist_concert_translations_id_seq'::regclass);


--
-- Name: rt_artist_image_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_image_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_artist_image_translations_id_seq'::regclass);


--
-- Name: rt_artist_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_artist_translations_id_seq'::regclass);


--
-- Name: rt_cms_item_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_item_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_cms_item_translations_id_seq'::regclass);


--
-- Name: rt_genre_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genre_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_genre_translations_id_seq'::regclass);


--
-- Name: rt_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_groups ALTER COLUMN id SET DEFAULT nextval('public.rt_groups_id_seq'::regclass);


--
-- Name: rt_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_migrations ALTER COLUMN id SET DEFAULT nextval('public.rt_migrations_id_seq'::regclass);


--
-- Name: rt_order_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_order_items ALTER COLUMN id SET DEFAULT nextval('public.rt_order_items_id_seq'::regclass);


--
-- Name: rt_orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_orders ALTER COLUMN id SET DEFAULT nextval('public.rt_orders_id_seq'::regclass);


--
-- Name: rt_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_permissions ALTER COLUMN id SET DEFAULT nextval('public.rt_permissions_id_seq'::regclass);


--
-- Name: rt_roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_roles ALTER COLUMN id SET DEFAULT nextval('public.rt_roles_id_seq'::regclass);


--
-- Name: rt_settings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_settings ALTER COLUMN id SET DEFAULT nextval('public.rt_settings_id_seq'::regclass);


--
-- Name: rt_song_translations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_translations ALTER COLUMN id SET DEFAULT nextval('public.rt_song_translations_id_seq'::regclass);


--
-- Name: rt_users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users ALTER COLUMN id SET DEFAULT nextval('public.rt_users_id_seq'::regclass);


--
-- Name: rt_users_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_groups ALTER COLUMN id SET DEFAULT nextval('public.rt_users_groups_id_seq'::regclass);


--
-- Name: rt_users_logins id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_logins ALTER COLUMN id SET DEFAULT nextval('public.rt_users_logins_id_seq'::regclass);


--
-- Data for Name: rt_artist_concert_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_concert_translations (id, artist_concert_id, place_name, description, created_at, locale) FROM stdin;
1	26	Russian VERSION : Bridge Street, United Kingdom	Russian VERSION : CZC	2018-08-30 06:55:30	ru
2	26	Spain VERSION : Bridge Street, United Kingdom	Spain VERSION : CZC	2018-08-30 06:55:30	es
3	26	Bridge Street, United Kingdom	CZC	2018-08-30 06:55:30	en
4	33	Russian VERSION : Washington Street, United States	Russian VERSION : Washington Street, Plainville, MA, USA with 340 man\n111\n22222	2018-08-30 06:55:30	ru
5	33	Spain VERSION : Washington Street, United States	Spain VERSION : Washington Street, Plainville, MA, USA with 340 man\n111\n22222	2018-08-30 06:55:30	es
6	33	Washington Street, United States	Washington Street, Plainville, MA, USA with 340 man\n111\n22222	2018-08-30 06:55:30	en
7	34	Russian VERSION : Paris, France	Russian VERSION : bnvb	2018-08-30 06:55:30	ru
8	34	Spain VERSION : Paris, France	Spain VERSION : bnvb	2018-08-30 06:55:30	es
9	34	Paris, France	bnvb	2018-08-30 06:55:30	en
10	1	Russian VERSION : London	Russian VERSION : Some festival on <b>London</b>. Join us and have fun! 	2018-08-30 06:55:30	ru
11	1	Spain VERSION : London	Spain VERSION : Some festival on <b>London</b>. Join us and have fun! 	2018-08-30 06:55:31	es
12	1	London	Some festival on <b>London</b>. Join us and have fun! 	2018-08-30 06:55:31	en
13	28	Russian VERSION : Scotland Street West, United Kingdom	Russian VERSION : Beyonce concert with 3000 participants\n111111\n22222\n333333\n4444444	2018-08-30 06:55:31	ru
14	28	Spain VERSION : Scotland Street West, United Kingdom	Spain VERSION : Beyonce concert with 3000 participants\n111111\n22222\n333333\n4444444	2018-08-30 06:55:31	es
15	28	Scotland Street West, United Kingdom	Beyonce concert with 3000 participants\n111111\n22222\n333333\n4444444	2018-08-30 06:55:31	en
16	31	Russian VERSION : Scotland Street, United Kingdom	Russian VERSION : Beyonce concert at \nScotland Street, United Kingdom\ntttt\nrr\neeee\nwwwwww\nqqqqqqq	2018-08-30 06:55:31	ru
17	31	Spain VERSION : Scotland Street, United Kingdom	Spain VERSION : Beyonce concert at \nScotland Street, United Kingdom\ntttt\nrr\neeee\nwwwwww\nqqqqqqq	2018-08-30 06:55:31	es
18	31	Scotland Street, United Kingdom	Beyonce concert at \nScotland Street, United Kingdom\ntttt\nrr\neeee\nwwwwww\nqqqqqqq	2018-08-30 06:55:31	en
19	2	Russian VERSION : Birmingham	Russian VERSION : Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	ru
20	2	Spain VERSION : Birmingham	Spain VERSION : Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	es
21	2	Birmingham	Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	en
22	5	Russian VERSION : Birmingham	Russian VERSION : Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	ru
23	5	Spain VERSION : Birmingham	Spain VERSION : Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	es
24	5	Birmingham	Big concert in <b>Birmingham</b>. Don' miss lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	en
25	29	Russian VERSION : United Kingdom House	Russian VERSION : Evanescence at United Kingdom House \n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:55:31	ru
42	30	South Close, United Kingdom	Evanescence concert at South Close, United Kingdom\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n55555\n444444	2018-08-30 06:55:31	en
43	21	Russian VERSION : London Wall, United Kingdom	Russian VERSION : 2500 parti...	2018-08-30 06:55:31	ru
44	21	Spain VERSION : London Wall, United Kingdom	Spain VERSION : 2500 parti...	2018-08-30 06:55:31	es
26	29	Spain VERSION : United Kingdom House	Spain VERSION : Evanescence at United Kingdom House \n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:55:31	es
27	29	United Kingdom House	Evanescence at United Kingdom House \n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:55:31	en
28	3	Russian VERSION : Manchester	Russian VERSION : Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	ru
29	3	Spain VERSION : Manchester	Spain VERSION : Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	es
30	3	Manchester	Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-08-30 06:55:31	en
31	23	Russian VERSION : London Road, United Kingdom	Russian VERSION : 4500 04-27  : 17 00\n11111\n22222\n3333333\n444444	2018-08-30 06:55:31	ru
32	23	Spain VERSION : London Road, United Kingdom	Spain VERSION : 4500 04-27  : 17 00\n11111\n22222\n3333333\n444444	2018-08-30 06:55:31	es
33	23	London Road, United Kingdom	4500 04-27  : 17 00\n11111\n22222\n3333333\n444444	2018-08-30 06:55:31	en
34	6	Russian VERSION : Manchester	Russian VERSION : Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 	2018-08-30 06:55:31	ru
35	6	Spain VERSION : Manchester	Spain VERSION : Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 	2018-08-30 06:55:31	es
36	6	Manchester	Event in <b>Manchester</b>. Join us lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 	2018-08-30 06:55:31	en
37	4	Russian VERSION : London	Russian VERSION : Folk festival in <b>London</b>. Join us ! 	2018-08-30 06:55:31	ru
38	4	Spain VERSION : London	Spain VERSION : Folk festival in <b>London</b>. Join us ! 	2018-08-30 06:55:31	es
39	4	London	Folk festival in <b>London</b>. Join us ! 	2018-08-30 06:55:31	en
40	30	Russian VERSION : South Close, United Kingdom	Russian VERSION : Evanescence concert at South Close, United Kingdom\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n55555\n444444	2018-08-30 06:55:31	ru
41	30	Spain VERSION : South Close, United Kingdom	Spain VERSION : Evanescence concert at South Close, United Kingdom\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n55555\n444444	2018-08-30 06:55:31	es
45	21	London Wall, United Kingdom	2500 parti...	2018-08-30 06:55:31	en
46	35	Russian VERSION : Long Beach Boulevard, United States	Russian VERSION : dxbvxc	2018-08-30 06:55:31	ru
47	35	Spain VERSION : Long Beach Boulevard, United States	Spain VERSION : dxbvxc	2018-08-30 06:55:31	es
48	35	Long Beach Boulevard, United States	dxbvxc	2018-08-30 06:55:31	en
\.


--
-- Data for Name: rt_artist_concerts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_concerts (id, artist_id, lat, lng, participants, event_date, tour_id, country, created_at) FROM stdin;
23	6	52.5949677000000000	-1.0713796000000000	450923	2018-05-27 06:11:00	\N	uk	2018-04-10 08:21:07.312112
4	6	51.5085000000000000	-0.1258000000000000	25500	2018-06-11 17:00:00	\N	uk	2018-03-31 11:04:42
5	7	52.4814000000000000	-1.8999000000000000	5850	2018-05-23 18:00:00	\N	uk	2018-03-13 07:34:58
6	7	53.4809000000000000	-2.2375000000000000	3200	2018-05-30 14:00:00	\N	uk	2018-03-29 08:24:24
21	6	51.5175095000000000	-0.0902717999999820	2500	2018-06-26 15:00:00	\N	uk	2018-04-10 08:13:00.048032
26	5	51.5009745000000000	-0.1250771999999600	23	2017-12-04 15:08:00	\N	uk	2018-04-14 14:17:28.754979
2	5	52.4814000000000000	-1.8999000000000000	5850	2018-05-19 18:00:00	\N	uk	2018-03-13 07:34:58
1	5	51.5085000000000000	-0.1258000000000000	25500	2018-05-05 18:30:00	\N	uk	2018-03-13 07:34:58
3	6	53.4809000000000000	-2.2375000000000000	3200	2018-05-25 14:00:00	\N	uk	2018-03-29 08:24:24
28	9	55.8501750000000000	-4.2845645000000000	3000	2018-05-18 17:30:00	\N	uk	2018-04-27 07:23:25.071544
29	8	51.5159930000000000	-0.1392256000000300	6000	2018-05-25 12:00:00	\N	uk	2018-04-28 10:33:11.380597
30	8	51.3908115000000000	-0.2024105000000400	45000	2018-06-21 19:30:00	\N	uk	2018-04-28 10:36:30.695749
31	9	53.3854801000000000	-1.4743652000000000	5000	2018-05-18 19:45:00	\N	uk	2018-04-28 10:40:13.216115
33	9	42.0404995000000000	-71.3051189000000000	340	2018-05-04 19:00:00	\N	us	2018-05-01 14:43:07.685
34	9	43.4941437000000000	5.8932809000000000	450	2018-05-05 12:00:00	\N	fr	2018-05-01 14:45:32.962502
35	6	33.8242001000000000	-118.1893675000000000	455	2018-07-27 16:00:00	\N	us	2018-07-09 14:38:18.774546
\.


--
-- Data for Name: rt_artist_image_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_image_translations (id, artist_image_id, description, created_at, locale) FROM stdin;
1	12	Russian VERSION : Demis Roussos Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	ru
2	12	Spain VERSION : Demis Roussos Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	es
3	12	Demis Roussos Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	en
4	102	Russian VERSION : Evanescence\n3333\n5555555\n66666666666666\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	ru
5	102	Spain VERSION : Evanescence\n3333\n5555555\n66666666666666\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	es
6	102	Evanescence\n3333\n5555555\n66666666666666\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	en
7	104	Russian VERSION : Evanescence on main page\n666\n55555\n444444\n33333333	2018-08-30 06:45:19	ru
8	104	Spain VERSION : Evanescence on main page\n666\n55555\n444444\n33333333	2018-08-30 06:45:19	es
9	104	Evanescence on main page\n666\n55555\n444444\n33333333	2018-08-30 06:45:19	en
10	42	Russian VERSION : Elvis Presley	2018-08-30 06:45:19	ru
11	42	Spain VERSION : Elvis Presley	2018-08-30 06:45:19	es
12	42	Elvis Presley	2018-08-30 06:45:19	en
13	29	Russian VERSION : Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:19	ru
14	29	Spain VERSION : Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:19	es
15	29	Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:19	en
16	60	Russian VERSION : Eric Clapton photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	ru
17	60	Spain VERSION : Eric Clapton photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	es
18	60	Eric Clapton photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	en
19	14	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
20	14	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
21	14	Demis Roussos	2018-08-30 06:45:19	en
53	64	Spain VERSION : Bruce Springsteen photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	es
54	64	Bruce Springsteen photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	en
55	28	Russian VERSION : Frank Sinatra	2018-08-30 06:45:19	ru
22	15	Russian VERSION : Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?	2018-08-30 06:45:19	ru
23	15	Spain VERSION : Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?	2018-08-30 06:45:19	es
24	15	Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?	2018-08-30 06:45:19	en
25	8	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:19	ru
26	8	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:19	es
27	8	Ella Fitzgerald	2018-08-30 06:45:19	en
28	11	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:19	ru
29	11	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:19	es
30	11	Ella Fitzgerald	2018-08-30 06:45:19	en
31	10	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:19	ru
32	10	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:19	es
33	10	Ella Fitzgerald	2018-08-30 06:45:19	en
34	16	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
35	16	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
36	16	Demis Roussos	2018-08-30 06:45:19	en
37	66	Russian VERSION : Paul McCartney Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	ru
38	66	Spain VERSION : Paul McCartney Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	es
39	66	Paul McCartney Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	en
40	9	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:19	ru
41	9	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:19	es
42	9	Ella Fitzgerald	2018-08-30 06:45:19	en
43	62	Russian VERSION : Bruce Springsteen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	ru
44	62	Spain VERSION : Bruce Springsteen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	es
45	62	Bruce Springsteen Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum...	2018-08-30 06:45:19	en
46	17	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
47	17	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
48	17	Demis Roussos	2018-08-30 06:45:19	en
49	18	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
50	18	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
51	18	Demis Roussos	2018-08-30 06:45:19	en
52	64	Russian VERSION : Bruce Springsteen photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:19	ru
56	28	Spain VERSION : Frank Sinatra	2018-08-30 06:45:19	es
57	28	Frank Sinatra	2018-08-30 06:45:19	en
58	27	Russian VERSION : Frank Sinatra	2018-08-30 06:45:19	ru
59	27	Spain VERSION : Frank Sinatra	2018-08-30 06:45:19	es
60	27	Frank Sinatra	2018-08-30 06:45:19	en
61	26	Russian VERSION : Frank Sinatra	2018-08-30 06:45:19	ru
62	26	Spain VERSION : Frank Sinatra	2018-08-30 06:45:19	es
63	26	Frank Sinatra	2018-08-30 06:45:19	en
64	67	Russian VERSION : Paul McCartney photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	ru
65	67	Spain VERSION : Paul McCartney photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	es
66	67	Paul McCartney photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	en
67	13	Russian VERSION : Demis Roussos 	2018-08-30 06:45:19	ru
68	13	Spain VERSION : Demis Roussos 	2018-08-30 06:45:19	es
69	13	Demis Roussos 	2018-08-30 06:45:19	en
70	1	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:19	ru
71	1	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:19	es
72	1	Ella Fitzgerald	2018-08-30 06:45:19	en
73	43	Russian VERSION : Elvis Presley	2018-08-30 06:45:19	ru
74	43	Spain VERSION : Elvis Presley	2018-08-30 06:45:19	es
75	43	Elvis Presley	2018-08-30 06:45:19	en
76	105	Russian VERSION : beyonce grammys red carpet billboard\n111111\n222\n3333	2018-08-30 06:45:19	ru
77	105	Spain VERSION : beyonce grammys red carpet billboard\n111111\n222\n3333	2018-08-30 06:45:19	es
78	105	beyonce grammys red carpet billboard\n111111\n222\n3333	2018-08-30 06:45:19	en
79	106	Russian VERSION : beyonce werrt	2018-08-30 06:45:19	ru
80	106	Spain VERSION : beyonce werrt	2018-08-30 06:45:19	es
81	106	beyonce werrt	2018-08-30 06:45:19	en
82	63	Russian VERSION : Bruce Springsteen photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	ru
83	63	Spain VERSION : Bruce Springsteen photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	es
84	63	Bruce Springsteen photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	en
85	58	Russian VERSION : Clapton New Jersey 22 February 1983 Michael Brito...	2018-08-30 06:45:19	ru
86	58	Spain VERSION : Clapton New Jersey 22 February 1983 Michael Brito...	2018-08-30 06:45:19	es
87	58	Clapton New Jersey 22 February 1983 Michael Brito...	2018-08-30 06:45:19	en
88	21	Russian VERSION : Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	ru
89	21	Spain VERSION : Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	es
90	21	Demis Roussos Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:19	en
91	23	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
92	23	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
93	23	Demis Roussos	2018-08-30 06:45:19	en
94	19	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
95	19	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
96	19	Demis Roussos	2018-08-30 06:45:19	en
97	22	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
98	22	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
99	22	Demis Roussos	2018-08-30 06:45:19	en
100	24	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
101	24	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
102	24	Demis Roussos	2018-08-30 06:45:19	en
103	25	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
104	25	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
105	25	Demis Roussos	2018-08-30 06:45:19	en
106	20	Russian VERSION : Demis Roussos	2018-08-30 06:45:19	ru
107	20	Spain VERSION : Demis Roussos	2018-08-30 06:45:19	es
108	20	Demis Roussos	2018-08-30 06:45:19	en
109	107	Russian VERSION : beyonce\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	ru
128	46	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
129	46	Elvis Presley	2018-08-30 06:45:20	en
130	47	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
131	47	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
132	47	Elvis Presley	2018-08-30 06:45:20	en
133	48	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
134	48	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
135	48	Elvis Presley	2018-08-30 06:45:20	en
110	107	Spain VERSION : beyonce\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	es
111	107	beyonce\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	en
112	39	Russian VERSION : Frank Sinatra	2018-08-30 06:45:19	ru
113	39	Spain VERSION : Frank Sinatra	2018-08-30 06:45:19	es
114	39	Frank Sinatra	2018-08-30 06:45:19	en
115	2	Russian VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	ru
116	2	Spain VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	es
117	2	Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	en
118	4	Russian VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	ru
119	4	Spain VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	es
120	4	Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:19	en
121	3	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:20	ru
122	3	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:20	es
123	3	Ella Fitzgerald	2018-08-30 06:45:20	en
124	5	Russian VERSION : Ella Fitzgerald Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:20	ru
125	5	Spain VERSION : Ella Fitzgerald Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:20	es
126	5	Ella Fitzgerald Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?	2018-08-30 06:45:20	en
127	46	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
136	49	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
137	49	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
138	49	Elvis Presley	2018-08-30 06:45:20	en
139	44	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
140	44	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
141	44	Elvis Presley	2018-08-30 06:45:20	en
142	45	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
143	45	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
144	45	Elvis Presley	2018-08-30 06:45:20	en
145	57	Russian VERSION : Eric Clapton photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	ru
146	57	Spain VERSION : Eric Clapton photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	es
147	57	Eric Clapton photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	en
148	103	Russian VERSION : Evanescence\n777777777\n88888888888\n99999999999999999\n0000000000000	2018-08-30 06:45:20	ru
149	103	Spain VERSION : Evanescence\n777777777\n88888888888\n99999999999999999\n0000000000000	2018-08-30 06:45:20	es
150	103	Evanescence\n777777777\n88888888888\n99999999999999999\n0000000000000	2018-08-30 06:45:20	en
151	30	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
152	30	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
153	30	Frank Sinatra	2018-08-30 06:45:20	en
154	41	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
155	41	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
156	41	Frank Sinatra	2018-08-30 06:45:20	en
157	31	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
158	31	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
159	31	Frank Sinatra	2018-08-30 06:45:20	en
160	40	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
161	40	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
162	40	Frank Sinatra	2018-08-30 06:45:20	en
163	32	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
164	32	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
165	32	Frank Sinatra	2018-08-30 06:45:20	en
166	65	Russian VERSION : Paul McCartney photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	ru
167	65	Spain VERSION : Paul McCartney photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	es
168	65	Paul McCartney photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	en
169	51	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
170	51	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
171	51	Elvis Presley	2018-08-30 06:45:20	en
172	34	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
173	34	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
174	34	Frank Sinatra	2018-08-30 06:45:20	en
175	6	Russian VERSION : Ella Fitzgerald	2018-08-30 06:45:20	ru
176	6	Spain VERSION : Ella Fitzgerald	2018-08-30 06:45:20	es
177	6	Ella Fitzgerald	2018-08-30 06:45:20	en
178	33	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
179	33	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
180	33	Frank Sinatra	2018-08-30 06:45:20	en
181	50	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
182	50	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
183	50	Elvis Presley	2018-08-30 06:45:20	en
184	35	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
185	35	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
186	35	Frank Sinatra	2018-08-30 06:45:20	en
187	53	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
188	53	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
189	53	Elvis Presley	2018-08-30 06:45:20	en
190	52	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
191	52	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
192	52	Elvis Presley	2018-08-30 06:45:20	en
193	54	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
194	54	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
195	54	Elvis Presley	2018-08-30 06:45:20	en
196	55	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
197	55	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
198	55	Elvis Presley	2018-08-30 06:45:20	en
199	56	Russian VERSION : Elvis Presley	2018-08-30 06:45:20	ru
200	56	Spain VERSION : Elvis Presley	2018-08-30 06:45:20	es
201	56	Elvis Presley	2018-08-30 06:45:20	en
202	100	Russian VERSION : Evanescence Main image :\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	ru
203	100	Spain VERSION : Evanescence Main image :\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	es
204	100	Evanescence Main image :\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.\n\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	en
205	68	Russian VERSION : Paul McCartney photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:20	ru
206	68	Spain VERSION : Paul McCartney photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:20	es
207	68	Paul McCartney photo # 3 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.	2018-08-30 06:45:20	en
208	37	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
209	37	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
210	37	Frank Sinatra	2018-08-30 06:45:20	en
211	36	Russian VERSION : Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit	2018-08-30 06:45:20	ru
212	36	Spain VERSION : Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit	2018-08-30 06:45:20	es
213	36	Frank Sinatra Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit	2018-08-30 06:45:20	en
214	61	Russian VERSION : Bruce Springsteen photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	ru
215	61	Spain VERSION : Bruce Springsteen photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	es
216	61	Bruce Springsteen photo # 1 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	en
217	38	Russian VERSION : Frank Sinatra	2018-08-30 06:45:20	ru
218	38	Spain VERSION : Frank Sinatra	2018-08-30 06:45:20	es
219	38	Frank Sinatra	2018-08-30 06:45:20	en
220	7	Russian VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	ru
221	7	Spain VERSION : Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	es
222	7	Ella Fitzgerald Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.	2018-08-30 06:45:20	en
223	59	Russian VERSION : Eric Clapton photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	ru
224	59	Spain VERSION : Eric Clapton photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	es
225	59	Eric Clapton photo # 2 Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo.	2018-08-30 06:45:20	en
\.


--
-- Data for Name: rt_artist_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_images (id, artist_id, image_name, is_main, is_home_page, created_at) FROM stdin;
1	1	a6414f9f8bc.jpg	f	f	2015-05-25 04:42:43
2	1	Ella-Fitzgerald-008.jpg	f	f	2015-05-25 04:42:51
3	1	ella.jpg	f	f	2015-05-25 04:42:57
4	1	Ella_Fitzgerald71.jpg	f	f	2015-05-25 04:43:05
5	1	Ellarodgershart.jpg	t	t	2015-05-25 04:43:13
6	1	images.jpeg	f	f	2015-05-25 04:43:30
7	1	Uplifting-Voice.jpg	f	t	2015-05-25 04:43:40
8	1	170px-Ella_Fitzgerald_1940.jpg	f	f	2015-05-25 04:44:10
9	1	220px-Ella_Fitzgerald_in_September_1947.jpg	f	f	2015-05-25 04:44:40
10	1	170px-Ella_Fitzgerald_1968.jpg	f	f	2015-05-25 04:45:04
11	1	170px-Ella_Fitzgerald_1960_by_Erling_Mandelmann.jpg	f	f	2015-05-25 04:45:18
12	2	04f-demis-roussos.jpg	f	f	2015-05-25 08:33:53
13	2	84886743.jpg	f	f	2015-05-25 08:34:01
14	2	164201319.jpg	f	f	2015-05-25 08:34:06
15	2	164201322.jpg	f	f	2015-05-25 08:34:16
16	2	171297820__843752b.jpg	t	t	2015-05-25 08:34:23
17	2	462275572.jpg	f	f	2015-05-25 08:34:31
18	2	462275664.jpg	f	f	2015-05-25 08:34:39
19	2	Demis-Roussos-630x420.jpg	f	f	2015-05-25 08:34:53
20	2	demis-roussos-died-390x285.jpg	f	f	2015-05-25 08:35:02
21	2	demis.jpg	f	t	2015-05-25 08:35:10
22	2	Demis+Roussos+9675b7f8201f.jpg	f	f	2015-05-25 08:35:24
23	2	Demis+Roussos+15141232026161.jpg	f	f	2015-05-25 08:35:32
24	2	Demis+Roussos+ac_group.jpg	f	f	2015-05-25 08:35:38
25	2	Demis+Roussos+demis_roussos_best_of_demis_ro.jpg	f	f	2015-05-25 08:35:43
26	3	6e35b4c70fa8b1b43cd019f55c36c6f4_large.jpeg	f	f	2015-05-25 09:02:48
27	3	61pq3jMocmL._SL290_.jpg	f	f	2015-05-25 09:03:34
28	3	600full-frank-sinatra.jpg	f	f	2015-05-25 09:03:40
29	3	140304-gp-sinatra-3a_3a5319360f0921138fa376d6596e0972.jpg	f	f	2015-05-25 09:03:47
30	3	filepicker_XeDQHWSZQsKBNqrXS7iA_Frank_Sinatra.jpg	f	f	2015-05-25 09:03:57
31	3	Frank_Sinatra_Metronome_magazine_November_1950.JPG	f	f	2015-05-25 09:04:32
32	3	hqdefault.jpg	f	f	2015-05-25 09:04:39
33	3	images.jpeg	f	f	2015-05-25 09:04:46
34	3	imagese.jpeg	f	f	2015-05-25 09:04:53
35	3	imagessa.jpeg	f	f	2015-05-25 09:05:00
36	3	sinatra-stamp.jpg	t	t	2015-05-25 09:05:06
37	3	sinatra_hands.jpg	f	t	2015-05-25 09:05:13
38	3	tumblr_n1b6e98aOT1tswkwdo1_500.jpg	f	f	2015-05-25 09:05:20
39	3	Eleanor_Roosevelt_Frank_Sinatra_1947.jpg	f	f	2015-05-25 09:06:39
40	3	Frank-Sinatra-Wallpaper-frank-sinatra-5581024-1024-768.jpg	f	f	2015-05-25 09:06:45
41	3	franksinatra.jpg	f	f	2015-05-25 09:06:54
42	4	12476a39366da06f65d904c477db8ff6_large.jpeg	f	f	2015-05-25 09:15:32
43	4	AP5606220233.jpg	t	t	2015-05-25 09:20:34
44	4	Elvis-Presley-009.jpg	f	f	2015-05-25 09:20:39
45	4	elvis-presley.jpg	f	t	2015-05-25 09:20:45
46	4	elvis_50s_308_4.jpg	f	f	2015-05-25 09:21:01
47	4	elvis_50s_308_6.jpg	f	f	2015-05-25 09:21:10
48	4	elvis_50s_308_7.jpg	f	f	2015-05-25 09:21:23
49	4	elvis_50s_308_8.jpg	f	f	2015-05-25 09:21:31
50	4	images.jpeg	f	f	2015-05-25 09:22:07
51	4	images5.jpeg	f	f	2015-05-25 09:22:14
52	4	index.jpeg	f	f	2015-05-25 09:22:19
53	4	indexer.jpeg	f	f	2015-05-25 09:22:49
54	4	indwrex.jpeg	f	f	2015-05-25 09:22:55
55	4	INV16955.jpg	f	f	2015-05-25 09:23:01
56	4	INV35425.jpg	f	f	2015-05-25 09:23:18
57	5	Eric Clapton_NJ22Feb83MB_04.jpg	f	f	2018-04-02 12:11:12
58	5	Clapton New Jersey 22 February 1983 Michael Brito_0005.jpg	f	f	2018-04-02 12:13:54
59	5	w13.jpg	t	t	2018-04-02 12:14:42
60	5	14332-2995 EC & AFL.jpg	f	t	2018-04-02 12:15:51
61	6	springsteen-700x352.jpg	t	f	2018-04-02 12:14:52
63	6	Bruce+Springsteen.jpg	f	t	2018-04-02 12:16:05
64	6	596.jpg	f	t	2018-04-02 12:17:23
65	7	Image-Paul-McCartney-HD-Wallpapers.jpg	t	f	2018-04-02 12:19:20
62	6	416x416.jpg	t	t	2018-04-02 12:15:14
100	8	main.jpg	t	t	2018-04-28 09:58:24.647865
102	8	088352_.jpg	f	f	2018-04-28 10:30:30.324673
103	8	evanescence.jpg	f	f	2018-04-28 10:31:04.309402
104	8	0a.jpg	f	t	2018-04-28 10:31:48.710649
105	9	beyonce-grammys-.jpg	f	f	2018-04-28 11:00:40.106017
106	9	beyonce-hero-487.jpg	t	t	2018-04-28 11:01:57.224747
66	7	20300534506289142847820629813_s.jpg	t	t	2018-04-02 12:19:59
67	7	71MbUkXU66L.png	f	t	2018-04-02 12:22:03
68	7	paul-mccartney.jpg	f	f	2018-04-02 12:24:03
107	9	download.jpeg	t	t	2018-04-28 11:02:27.074698
\.


--
-- Data for Name: rt_artist_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_translations (id, artist_id, name, info, locale, created_at, updated_at) FROM stdin;
6	9	Beyonce	Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/; born September 4, 1981)[2][3][4] is an American singer, songwriter and actress. Born and raised in Houston, Texas, Beyoncé performed in various singing and dancing competitions as a child. Beyoncé rose to fame in the late 1990s as lead singer of the R&B girl-group Destiny's Child. Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups in history. Their hiatus saw Beyoncé's theatrical film debut in Austin Powers in Goldmember (2002) and the release of her debut album, Dangerously in Love (2003). The album established her as a solo artist worldwide, earned five Grammy Awards, and featured the Billboard Hot 100 number one singles "Crazy in Love" and "Baby Boy". Following the break-up of Destiny's Child in 2006, she released her second solo album, B'Day (2006), which contained the top ten singles "Déjà Vu", "Irreplaceable", and "Beautiful Liar". Beyoncé also continued her acting career, with starring roles in The Pink Panther (2006), Dreamgirls (2006), and Obsessed (2009). Her marriage to rapper Jay-Z and portrayal of Etta James in Cadillac Records (2008) influenced her third album, I Am... Sasha Fierce (2008), which saw the introduction of her alter-ego Sasha Fierce and earned a record-setting six Grammy Awards in 2010, including Song of the Year for "Single Ladies (Put a Ring on It)". Beyoncé took a hiatus from music in 2010 and took over management of her career; her fourth album 4 (2011) was subsequently mellower in tone, exploring 1970s funk, 1980s pop, and 1990s soul.[5] Her critically acclaimed fifth album, Beyoncé (2013), was distinguished from previous releases by its experimental production and exploration of darker themes. Her sixth album, Lemonade (2016), also received widespread critical acclaim, and subsequently became the best-selling album of 2016.[6] Throughout her career, Beyoncé has sold 100 million records worldwide,[7] making her one of the world's best-selling music artists.[8][9] She has won 22 Grammy Awards and is the most nominated woman in the award's history. She is also the most awarded artist at the MTV Video Music Awards, with 24 wins.[10][11] The Recording Industry Association of America recognized Beyoncé as the Top Certified Artist in America, during the 2000s decade.[12][13] In 2009, Billboard named her the Top Radio Songs Artist of the Decade, the Top Female Artist of the 2000s decade, and awarded her their Millennium Award in 2011.[17] In 2014, she became the highest-paid black musician in history and was listed among Time's 100 most influential people in the world for a second year in a row.[18] Forbes ranked her as the most powerful female in entertainment on their 2015 and 2017 lists,[19][20] and in 2016 she occupied the sixth place for Time's Person of the Year.[21]	en	2018-07-02 10:38:59	\N
9	6	Bruce Springsteen	Bruce Springsteen some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	en	2018-07-02 10:38:59	\N
10	2	Russian VERSION : Demis Roussos	Russian VERSION : DemisRoussos some long text 22	ru	2018-07-02 10:38:59	\N
11	2	Spain VERSION : Demis Roussos	Spain VERSION : DemisRoussos some long text 22	es	2018-07-02 10:38:59	\N
12	2	Demis Roussos	DemisRoussos some long text 22	en	2018-07-02 10:38:59	\N
13	1	Russian VERSION : Ella Fitzgerald	Russian VERSION : Ella Fitzgerald some long text 1	ru	2018-07-02 10:38:59	\N
14	1	Spain VERSION : Ella Fitzgerald	Spain VERSION : Ella Fitzgerald some long text 1	es	2018-07-02 10:38:59	\N
15	1	Ella Fitzgerald	Ella Fitzgerald some long text 1	en	2018-07-02 10:38:59	\N
16	4	Russian VERSION : Elvis Presley	Russian VERSION : Elvis Presley some long text 4	ru	2018-07-02 10:38:59	\N
17	4	Spain VERSION : Elvis Presley	Spain VERSION : Elvis Presley some long text 4	es	2018-07-02 10:38:59	\N
18	4	Elvis Presley	Elvis Presley some long text 4	en	2018-07-02 10:38:59	\N
19	5	Russian VERSION : Eric Clapton	Russian VERSION : Eric Clapton some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru	2018-07-02 10:38:59	\N
20	5	Spain VERSION : Eric Clapton	Spain VERSION : Eric Clapton some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es	2018-07-02 10:38:59	\N
21	5	Eric Clapton	Eric Clapton some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	en	2018-07-02 10:38:59	\N
22	8	Russian VERSION : Evanescence	Russian VERSION : Evanescence (/ˌɛvəˈnɛsns/[1]) is an American rock band founded in Little Rock, Arkansas, in 1995 by singer/pianist Amy Lee and guitarist Ben Moody.[2][3] After recording independent albums, the band released their first full-length album, Fallen, on Wind-up Records in 2003. Fallen sold more than 17 million copies worldwide and helped the band win two Grammy Awards out of seven nominations. A year later, Evanescence released their first live album, Anywhere but Home, which sold more than one million copies worldwide. In 2006, the band released their second studio album, The Open Door, which sold more than five million copies.[4]	ru	2018-07-02 10:38:59	\N
8	6	Spain VERSION : Bruce Springsteen	Spain VERSION : Bruce Springsteen some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es	2018-07-02 10:38:59	\N
23	8	Spain VERSION : Evanescence	Spain VERSION : Evanescence (/ˌɛvəˈnɛsns/[1]) is an American rock band founded in Little Rock, Arkansas, in 1995 by singer/pianist Amy Lee and guitarist Ben Moody.[2][3] After recording independent albums, the band released their first full-length album, Fallen, on Wind-up Records in 2003. Fallen sold more than 17 million copies worldwide and helped the band win two Grammy Awards out of seven nominations. A year later, Evanescence released their first live album, Anywhere but Home, which sold more than one million copies worldwide. In 2006, the band released their second studio album, The Open Door, which sold more than five million copies.[4]	es	2018-07-02 10:38:59	\N
4	9	Russian VERSION : Beyonce	Russian VERSION : Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/; born September 4, 1981)[2][3][4] is an American singer, songwriter and actress. Born and raised in Houston, Texas, Beyoncé performed in various singing and dancing competitions as a child. Beyoncé rose to fame in the late 1990s as lead singer of the R&B girl-group Destiny's Child. Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups in history. Their hiatus saw Beyoncé's theatrical film debut in Austin Powers in Goldmember (2002) and the release of her debut album, Dangerously in Love (2003). The album established her as a solo artist worldwide, earned five Grammy Awards, and featured the Billboard Hot 100 number one singles "Crazy in Love" and "Baby Boy". Following the break-up of Destiny's Child in 2006, she released her second solo album, B'Day (2006), which contained the top ten singles "Déjà Vu", "Irreplaceable", and "Beautiful Liar". Beyoncé also continued her acting career, with starring roles in The Pink Panther (2006), Dreamgirls (2006), and Obsessed (2009). Her marriage to rapper Jay-Z and portrayal of Etta James in Cadillac Records (2008) influenced her third album, I Am... Sasha Fierce (2008), which saw the introduction of her alter-ego Sasha Fierce and earned a record-setting six Grammy Awards in 2010, including Song of the Year for "Single Ladies (Put a Ring on It)". Beyoncé took a hiatus from music in 2010 and took over management of her career; her fourth album 4 (2011) was subsequently mellower in tone, exploring 1970s funk, 1980s pop, and 1990s soul.[5] Her critically acclaimed fifth album, Beyoncé (2013), was distinguished from previous releases by its experimental production and exploration of darker themes. Her sixth album, Lemonade (2016), also received widespread critical acclaim, and subsequently became the best-selling album of 2016.[6] Throughout her career, Beyoncé has sold 100 million records worldwide,[7] making her one of the world's best-selling music artists.[8][9] She has won 22 Grammy Awards and is the most nominated woman in the award's history. She is also the most awarded artist at the MTV Video Music Awards, with 24 wins.[10][11] The Recording Industry Association of America recognized Beyoncé as the Top Certified Artist in America, during the 2000s decade.[12][13] In 2009, Billboard named her the Top Radio Songs Artist of the Decade, the Top Female Artist of the 2000s decade, and awarded her their Millennium Award in 2011.[17] In 2014, she became the highest-paid black musician in history and was listed among Time's 100 most influential people in the world for a second year in a row.[18] Forbes ranked her as the most powerful female in entertainment on their 2015 and 2017 lists,[19][20] and in 2016 she occupied the sixth place for Time's Person of the Year.[21]	ru	2018-07-02 10:38:59	\N
5	9	Spain VERSION : Beyonce	Spain VERSION : Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/; born September 4, 1981)[2][3][4] is an American singer, songwriter and actress. Born and raised in Houston, Texas, Beyoncé performed in various singing and dancing competitions as a child. Beyoncé rose to fame in the late 1990s as lead singer of the R&B girl-group Destiny's Child. Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups in history. Their hiatus saw Beyoncé's theatrical film debut in Austin Powers in Goldmember (2002) and the release of her debut album, Dangerously in Love (2003). The album established her as a solo artist worldwide, earned five Grammy Awards, and featured the Billboard Hot 100 number one singles "Crazy in Love" and "Baby Boy". Following the break-up of Destiny's Child in 2006, she released her second solo album, B'Day (2006), which contained the top ten singles "Déjà Vu", "Irreplaceable", and "Beautiful Liar". Beyoncé also continued her acting career, with starring roles in The Pink Panther (2006), Dreamgirls (2006), and Obsessed (2009). Her marriage to rapper Jay-Z and portrayal of Etta James in Cadillac Records (2008) influenced her third album, I Am... Sasha Fierce (2008), which saw the introduction of her alter-ego Sasha Fierce and earned a record-setting six Grammy Awards in 2010, including Song of the Year for "Single Ladies (Put a Ring on It)". Beyoncé took a hiatus from music in 2010 and took over management of her career; her fourth album 4 (2011) was subsequently mellower in tone, exploring 1970s funk, 1980s pop, and 1990s soul.[5] Her critically acclaimed fifth album, Beyoncé (2013), was distinguished from previous releases by its experimental production and exploration of darker themes. Her sixth album, Lemonade (2016), also received widespread critical acclaim, and subsequently became the best-selling album of 2016.[6] Throughout her career, Beyoncé has sold 100 million records worldwide,[7] making her one of the world's best-selling music artists.[8][9] She has won 22 Grammy Awards and is the most nominated woman in the award's history. She is also the most awarded artist at the MTV Video Music Awards, with 24 wins.[10][11] The Recording Industry Association of America recognized Beyoncé as the Top Certified Artist in America, during the 2000s decade.[12][13] In 2009, Billboard named her the Top Radio Songs Artist of the Decade, the Top Female Artist of the 2000s decade, and awarded her their Millennium Award in 2011.[17] In 2014, she became the highest-paid black musician in history and was listed among Time's 100 most influential people in the world for a second year in a row.[18] Forbes ranked her as the most powerful female in entertainment on their 2015 and 2017 lists,[19][20] and in 2016 she occupied the sixth place for Time's Person of the Year.[21]	es	2018-07-02 10:38:59	\N
24	8	Evanescence	Evanescence (/ˌɛvəˈnɛsns/[1]) is an American rock band founded in Little Rock, Arkansas, in 1995 by singer/pianist Amy Lee and guitarist Ben Moody.[2][3] After recording independent albums, the band released their first full-length album, Fallen, on Wind-up Records in 2003. Fallen sold more than 17 million copies worldwide and helped the band win two Grammy Awards out of seven nominations. A year later, Evanescence released their first live album, Anywhere but Home, which sold more than one million copies worldwide. In 2006, the band released their second studio album, The Open Door, which sold more than five million copies.[4]	en	2018-07-02 10:38:59	\N
25	3	Russian VERSION : Frank Sinatra	Russian VERSION : Frank Sinatra some long text 3	ru	2018-07-02 10:38:59	\N
26	3	Spain VERSION : Frank Sinatra	Spain VERSION : Frank Sinatra some long text 3	es	2018-07-02 10:38:59	\N
27	3	Frank Sinatra	Frank Sinatra some long text 3	en	2018-07-02 10:38:59	\N
28	39	Russian VERSION : New	Russian VERSION : vfvgds	ru	2018-07-02 10:38:59	\N
29	39	Spain VERSION : New	Spain VERSION : vfvgds	es	2018-07-02 10:38:59	\N
30	39	New	vfvgds	en	2018-07-02 10:38:59	\N
31	7	Russian VERSION : Paul McCartney	Russian VERSION : Paul McCartney some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru	2018-07-02 10:38:59	\N
32	7	Spain VERSION : Paul McCartney	Spain VERSION : Paul McCartney some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es	2018-07-02 10:38:59	\N
33	7	Paul McCartney	Paul McCartney some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	en	2018-07-02 10:38:59	\N
7	6	Russian VERSION : Bruce Springsteen	Russian VERSION : Bruce Springsteen some long text 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru	2018-07-02 10:38:59	\N
38	46	name Tue Aug 28 2018 09:54:43	artist_info : Tue Aug 28 2018 09:54:43 GMT+0300 (Eastern European Summer Time)	en	\N	\N
39	47	name Thu Aug 30 2018 10:31:	artist_info : Thu Aug 30 2018 10:31:04 GMT+0300 (Eastern European Summer Time)	en	\N	\N
40	48	name Thu Aug 30 2018 10:39:02	artist_info : Thu Aug 30 2018 10:39:02 GMT+0300 (Eastern European Summer Time)	en	\N	\N
41	49	name Thu Aug 30 2018 14:04:1	artist_info : Thu Aug 30 2018 14:04:19 GMT+0300 (Eastern European Summer Time)use Auth;\nuse Validator;\nuse IlluminateDatabaseEventsTransactionBeginning;\nuse IlluminateDatabaseEventsTransactionCommitted;\nuse IlluminateDatabaseEventsTransactionRolledBack;\nuse IlluminateSupportFacadesGate;\nuse AppHttpTraitsfuncsTrait;\nuse AppUser;\nuse AppVoteCategory;\nuse AppVoteItem;	en	\N	\N
43	51	name Thu Aug 30 2018 14:10:59 GMT	artist_info : Thu Aug 30 2018 14:10:59 GMT+0300 (Eastern European Summer Time)	en	\N	\N
44	52	name Thu Aug 30 2018 14:15:49 GM	artist_info : Thu Aug 30 2018 14:15:49 GMT+0300 (Eastern European Summer Time)	en	\N	\N
45	53	name Thu Aug 30 2018 14:19:27	artist_info : Thu Aug 30 2018 14:19:27 GMT+0300 (Eastern European Summer Time)	en	\N	\N
46	54	name Thu Aug 30 2018 14:27:00 GMT+0300 (Easte	artist_info : Thu Aug 30 2018 14:27:00 GMT+0300 (Eastern European Summer Time)	en	\N	\N
47	55	name Thu Aug 30 2018 14:29:35 GMT+0300 (Easte	artist_info : Thu Aug 30 2018 14:29:35 GMT+0300 (Eastern European Summer Time)	en	\N	\N
48	56	name Thu Aug 30 2018 14:48:09 GMT+0300 (Easte	artist_info : Thu Aug 30 2018 14:48:09 GMT+0300 (Eastern European Summer Time)	en	\N	\N
49	59	name Thu Aug 30 2018 15:32:39 GMT+0300 (Easte	artist_info : Thu Aug 30 2018 15:32:39 GMT+0300 (Eastern European Summer Time)	en	\N	\N
50	60	name Thu Aug 30 2018 15:39:24 GMT+0300 (Easte	artist_info : Thu Aug 30 2018 15:39:24 GMT+0300 (Eastern European Summer Time)	en	\N	\N
51	61	F rank Sina tra	artist_info : Thu Aug 30 2018 16:42:48 GMT+0300 (Eastern European Summer Time)	en	\N	\N
52	62	zzzz	artist_info : Thu Aug 30 2018 16:53:03 GMT+0300 (Eastern European Summer Time)	en	\N	\N
55	65	name Thu Sep 13 2018 07:21:37 GMT+0300 (Easte	artist_info : Thu Sep 13 2018 07:21:37 GMT+0300 (Eastern European Summer Time)	en	\N	\N
56	66	name Thu Sep 13 2018 07:31:05 GMT+0300 (Easte	artist_info : Thu Sep 13 2018 07:31:05 GMT+0300 (Eastern European Summer Time)	en	\N	\N
57	67	name Thu Sep 13 2018 07:54:05 GMT+0300 (Easte 51	artist_info : Thu Sep 13 2018 07:54:05 GMT+0300 (Eastern European Summer Time) 51\n2222\n3333333\n444444\n5555555555555	en	\N	\N
58	67	51 RU 1	51 RU 2\n3333\n44444\n555555555	ru	\N	\N
59	67	51 ES 1	51 ES 2\n333\n444444\n55555\n6666666666	es	\N	\N
\.


--
-- Data for Name: rt_artist_votes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artist_votes (id, artist_id, user_id, vote, created_at) FROM stdin;
5	6	4	5	2018-04-04 16:02:29
7	1	4	4	2018-04-05 21:50:13
9	5	4	2	2018-04-03 02:41:38
10	8	4	2	2018-04-20 09:50:02
11	3	4	4	2018-04-17 13:43:33
13	7	4	3	2018-04-06 10:34:19
21	9	5	5	2018-04-09 22:51:49
22	6	5	1	2018-04-04 13:39:55
23	2	5	1	2018-04-06 09:27:45
24	1	5	5	2018-04-16 18:39:01
25	4	5	4	2018-04-11 10:00:00
26	5	5	4	2018-04-22 01:28:43
27	8	5	1	2018-04-24 00:23:37
28	3	5	3	2018-04-03 01:01:03
30	7	5	3	2018-04-05 12:00:52
39	6	6	5	2018-04-15 05:37:12
40	2	6	2	2018-04-03 06:47:05
41	1	6	3	2018-04-07 07:59:21
42	4	6	2	2018-04-28 15:10:12
43	5	6	1	2018-04-22 09:48:36
44	8	6	2	2018-04-08 17:04:25
45	3	6	3	2018-04-22 07:49:52
47	7	6	4	2018-04-28 03:58:31
56	6	1	5	2018-04-01 05:41:41
57	2	1	1	2018-03-31 18:08:26
58	1	1	4	2018-04-10 05:55:41
59	4	1	4	2018-04-07 00:46:01
60	5	1	5	2018-04-10 19:16:13
61	8	1	3	2018-04-15 23:24:42
62	3	1	2	2018-04-04 18:12:29
64	7	1	1	2018-04-09 23:08:23
72	9	7	5	2018-04-21 03:03:33
73	6	7	4	2018-04-14 06:34:07
74	2	7	2	2018-04-15 09:19:20
75	1	7	2	2018-04-19 22:56:50
76	4	7	4	2018-04-18 02:09:16
77	5	7	1	2018-04-13 10:16:50
78	8	7	5	2018-04-21 00:13:18
79	3	7	1	2018-04-29 01:44:58
81	7	7	3	2018-04-09 03:35:39
90	6	10	1	2018-04-06 18:00:39
91	2	10	3	2018-03-31 17:27:35
92	1	10	2	2018-04-19 13:41:01
94	5	10	4	2018-04-25 03:57:09
95	8	10	4	2018-04-02 07:58:46
96	3	10	3	2018-04-18 15:55:02
98	7	10	4	2018-04-22 22:07:20
106	9	8	5	2018-04-11 09:22:50
107	6	8	5	2018-04-10 19:44:58
109	1	8	4	2018-03-31 10:51:56
110	4	8	4	2018-04-20 18:18:04
111	5	8	2	2018-04-16 00:03:26
112	8	8	3	2018-04-14 23:41:58
113	3	8	1	2018-04-07 08:51:43
115	7	8	1	2018-04-24 04:02:06
124	6	2	5	2018-04-11 10:04:20
126	1	2	2	2018-04-08 12:45:59
127	4	2	3	2018-04-05 15:05:30
128	5	2	2	2018-04-03 02:23:16
129	8	2	2	2018-04-25 09:35:17
130	3	2	4	2018-04-06 11:49:30
132	7	2	3	2018-04-24 12:59:41
141	6	12	3	2018-04-06 15:17:39
142	2	12	2	2018-04-27 09:34:22
143	1	12	3	2018-04-04 18:55:42
144	4	12	5	2018-04-29 03:18:53
145	5	12	4	2018-03-31 10:59:00
146	8	12	5	2018-04-28 16:57:42
147	3	12	3	2018-04-05 17:19:34
149	7	12	5	2018-04-03 03:11:17
157	9	9	5	2018-04-15 23:12:06
4	9	4	5	2018-04-27 12:11:16
38	9	6	5	2018-03-31 18:36:55
55	9	1	5	2018-04-04 13:25:17
89	9	10	5	2018-04-18 02:00:04
123	9	2	5	2018-04-15 03:32:06
140	9	12	5	2018-04-09 20:19:31
6	2	4	2	2018-04-06 05:44:09
108	2	8	1	2018-04-27 16:46:02
125	2	2	1	2018-04-23 15:08:23
8	4	4	4	2018-03-31 13:52:02
93	4	10	5	2018-04-20 12:11:29
164	3	9	5	2018-04-07 14:06:12
174	9	3	5	2018-04-08 01:49:02
175	6	3	4	2018-04-03 12:07:15
177	1	3	3	2018-04-10 17:19:23
178	4	3	5	2018-04-10 06:27:14
179	5	3	3	2018-04-12 01:24:56
180	8	3	1	2018-03-31 18:53:43
181	3	3	5	2018-04-04 22:05:24
183	7	3	5	2018-04-25 16:11:19
158	6	9	5	2018-04-01 09:55:15
160	1	9	5	2018-04-26 20:11:50
161	4	9	5	2018-04-10 02:41:35
162	5	9	5	2018-04-08 15:11:08
163	8	9	5	2018-04-10 11:38:29
166	7	9	5	2018-04-24 01:07:31
176	2	3	2	2018-04-04 22:11:19
159	2	9	3	2018-04-17 05:33:41
\.


--
-- Data for Name: rt_artists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_artists (id, ordering, is_active, has_concerts, birthday, day_of_death, site, created_at, updated_at, single, slug) FROM stdin;
6	6	A	t	1949-09-23	\N	http://artists-rating.com/bruce_springsteen	2018-03-11 09:25:54	2018-05-17 04:22:16	t	bruce-springsteen
2	1	A	f	1946-06-15	2015-01-25	http://demis_roussos_site.com	2015-03-10 04:35:07	2018-05-17 04:23:04	t	demis-roussos
4	3	A	f	1935-01-08	1977-08-16	http://elvis_presley_site.com	2015-03-11 05:26:54	2018-05-17 04:23:11	t	elvis-presley
5	5	N	t	1945-03-30	\N	http://artists-rating.com/eric_clapton	2015-03-13 07:32:42	2018-05-17 04:23:18	t	eric-clapton
1	2	A	f	1917-04-25	1996-06-15	http://EllaFitzgerald_site.com	2015-03-12 08:31:21	2018-05-17 04:21:51	t	ella-fitzgerald
9	9	A	t	1981-09-04	\N	\N	2018-04-08 14:18:14.88945	2018-05-17 04:22:10	t	beyonce
8	8	N	t	\N	\N	Evanescence.com	2018-04-08 10:21:08.939635	2018-05-17 04:23:25	f	evanescence
3	4	A	f	1915-12-12	1998-05-14	http://frank_sinatra_site.com	2015-03-11 05:26:54	2018-05-17 04:23:34	t	frank-sinatra
7	7	A	t	1945-03-30	\N	http://artists-rating.com/Paul_McCartney	2018-03-31 09:28:43	2018-05-17 04:23:41	t	paul-mccartney
39	44	A	f	2018-05-01	2018-05-31	fasfsdf	2018-05-09 17:49:41.161372	2018-05-17 04:23:51	t	new
45	1	A	t	2018-08-16	\N	\N	2018-08-28 09:34:46.025731	\N	f	-1
46	1	N	t	1915-12-12	1915-12-16	site Tue Aug 28 2018 09:54:43 GMT+0300 (Eastern European Summer Time)	2018-08-28 09:55:27.772379	\N	t	-2
47	1	N	t	1915-12-12	1915-12-24	site Thu Aug 30 2018 10:31:04 GMT+0300 (Eastern European Summer Time)	2018-08-30 10:32:08.698254	\N	f	-3
48	1	N	t	1915-12-12	1915-12-30	site Thu Aug 30 2018 10:39:02 GMT+0300 (Eastern European Summer Time)	2018-08-30 10:39:23.976062	\N	t	-4
49	1	N	t	1915-12-12	1915-12-15	site Thu Aug 30 2018 14:04:19 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:05:42.765648	\N	t	-5
51	1	N	t	1915-12-12	1915-12-21	site Thu Aug 30 2018 14:10:59 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:12:52.42972	\N	t	-6
52	1	N	t	1915-12-12	1915-12-21	site Thu Aug 30 2018 14:15:49 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:16:03.13155	\N	t	-7
53	1	N	t	1915-12-12	1915-12-30	site Thu Aug 30 2018 14:19:27 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:19:52.743014	\N	t	-8
54	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 14:27:00 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:27:11.633512	\N	t	-9
55	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 14:29:35 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:29:43.487406	\N	t	-10
56	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 14:48:09 GMT+0300 (Eastern European Summer Time)	2018-08-30 14:48:14.010159	\N	t	-11
60	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 15:39:24 GMT+0300 (Eastern European Summer Time)	2018-08-30 15:39:30.506779	\N	t	name-thu-aug-30-2018-15-39-24-gmt-0300-easte
61	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 16:42:48 GMT+0300 (Eastern European Summer Time)	2018-08-30 16:43:58.593754	\N	t	f-rank-sina-tra
59	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 15:32:39 GMT+0300 (Eastern European Summer Time)	2018-08-30 15:38:26.35274	\N	t	zzzzA
62	1	N	t	1915-12-12	1918-12-12	site Thu Aug 30 2018 16:53:03 GMT+0300 (Eastern European Summer Time)	2018-08-30 16:53:42.372514	2018-08-30 13:54:12	t	zzzz-1
65	1	N	t	1915-12-12	1918-12-12	site Thu Sep 13 2018 07:21:37 GMT+0300 (Eastern European Summer Time)	2018-09-13 07:21:47.703745	\N	t	name-thu-sep-13-2018-07-21-37-gmt-0300-easte
66	1	N	t	1915-12-12	1918-12-12	site Thu Sep 13 2018 07:31:05 GMT+0300 (Eastern European Summer Time)	2018-09-13 07:31:57.645749	\N	t	name-thu-sep-13-2018-07-31-05-gmt-0300-easte
67	1	N	t	1915-12-12	1919-02-21	site Thu Sep 13 2018 07:54:05 GMT+0300 (Eastern European Summer Time) 51	2018-09-13 07:55:34.744872	\N	t	name-thu-sep-13-2018-07-54-05-gmt-0300-easte-51
\.


--
-- Data for Name: rt_cms_item_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_cms_item_translations (id, cms_item_id, title, short_descr, content, locale) FROM stdin;
3	29	aaa BBB ccc	aaa BBB ccc	aaa BBB ccc\naaa BBB ccc\naaa BBB ccc	en
7	8	RU VERSION : Company info	RU VERSION : Company info lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...	RU VERSION : Company info ... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	ru
8	8	ES VERSION : Company info	ES VERSION : Company info lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...	ES VERSION : Company info ... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	es
9	8	Company info	Company info lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...	Company info ... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	en
4	1	RU VERSION : About US	RU VERSION_** : Welcome**_ to page	RU VERSION : Welcome to arti_**sts-ra**_ting.com - Looking for a artists-rating has never been easier.Australian owned and operated by Serge at Home LTD (ACN 1326). Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru
16	7	RU VERSION : Join Us	RU VERSION : Join us and have luck with our site... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	RU VERSION : Join us and have luck with our site\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	ru
10	10	RU VERSION : Concerts soon	RU VERSION : Concerts soon Lorem ipsum dolor sit amet, consectetur...	RU VERSION : **Concerts soon **Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation _ullamco laboris nisi ut aliquip ex ea _ commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt **mollit anim id est laborum**. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru
11	10	ES VERSION : Concerts soon	ES VERSION : Concerts soon Lorem ipsum dolor sit amet, consectetur...	ES VERSION : **Concerts soon **Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation _ullamco laboris nisi ut aliquip ex ea _ commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt **mollit anim id est laborum**. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es
12	10	Concerts soon	Concerts soon Lorem ipsum dolor sit amet, consectetur...	**Concerts soon **Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation _ullamco laboris nisi ut aliquip ex ea _ commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt **mollit anim id est laborum**. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	en
13	9	RU VERSION : Contact us	RU VERSION : Contact us and have luck with our site lorem ipsum dolor sit amet...	RU VERSION : **Contact** us lorem ipsum _dolor sit amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem _ipsum dolor sit a_met, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: by link	ru
14	9	ES VERSION : Contact us	ES VERSION : Contact us and have luck with our site lorem ipsum dolor sit amet...	ES VERSION : **Contact** us lorem ipsum _dolor sit amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem _ipsum dolor sit a_met, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: by link	es
19	5	RU VERSION : Security & Privacy	RU VERSION : Your privacy is very important to us. Accordingly...	RU VERSION : Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.\n\n_- Before or at the time of collecting personal information, we will identify the purposes for which information is being collected._  \n_- We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law._  \n_- We will only retain personal information as long as necessary for the fulfillment of those purposes._  \n_- We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned._  \n_- Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date._  \n_- We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification._  \n_- We will make readily available to customers information about our policies and practices relating to the management of personal information._\n\nWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.\n\n### External Sites.\n\nartists-rating.com is not responsible for the content of external internet sites. You are advised to read the privacy policy of external sites before disclosing any personal information.\n\n### Cookies\n\nA "cookie" is a small data text file that is placed in your browser and allows artists-rating.com to recognize you each time you visit this site(customisation etc). Cookies themselves do not contain any personal information, and (local-)artists-rating.com does not use cookies to collect personal information. Cookies may also be used by 3rd party content providers such as newsfeeds.\n\n### Remember The Risks Whenever You Use The Internet\n\nWhile we do our best to protect your personal information, we cannot guarantee the security of any information that you transmit to (local-)artists-rating.com and you are solely responsible for maintaining the secrecy of any passwords or other account information. In addition other Internet sites or services that may be accessible through (local-)artists-rating.com have separate data and privacy practices independent of us, and therefore we disclaim any responsibility or liability for their policies or actions.\n\nPlease contact those vendors and others directly if you have any questions about their privacy policies.\n\nSerge at Home  \n(local-)artists-rating.com	ru
20	5	ES VERSION : Security & Privacy	ES VERSION : Your privacy is very important to us. Accordingly...	ES VERSION : Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.\n\n_- Before or at the time of collecting personal information, we will identify the purposes for which information is being collected._  \n_- We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law._  \n_- We will only retain personal information as long as necessary for the fulfillment of those purposes._  \n_- We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned._  \n_- Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date._  \n_- We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification._  \n_- We will make readily available to customers information about our policies and practices relating to the management of personal information._\n\nWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.\n\n### External Sites.\n\nartists-rating.com is not responsible for the content of external internet sites. You are advised to read the privacy policy of external sites before disclosing any personal information.\n\n### Cookies\n\nA "cookie" is a small data text file that is placed in your browser and allows artists-rating.com to recognize you each time you visit this site(customisation etc). Cookies themselves do not contain any personal information, and (local-)artists-rating.com does not use cookies to collect personal information. Cookies may also be used by 3rd party content providers such as newsfeeds.\n\n### Remember The Risks Whenever You Use The Internet\n\nWhile we do our best to protect your personal information, we cannot guarantee the security of any information that you transmit to (local-)artists-rating.com and you are solely responsible for maintaining the secrecy of any passwords or other account information. In addition other Internet sites or services that may be accessible through (local-)artists-rating.com have separate data and privacy practices independent of us, and therefore we disclaim any responsibility or liability for their policies or actions.\n\nPlease contact those vendors and others directly if you have any questions about their privacy policies.\n\nSerge at Home  \n(local-)artists-rating.com	es
21	5	Security & Privacy	Your privacy is very important to us. Accordingly...	Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.\n\n_- Before or at the time of collecting personal information, we will identify the purposes for which information is being collected._  \n_- We will collect and use of personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law._  \n_- We will only retain personal information as long as necessary for the fulfillment of those purposes._  \n_- We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned._  \n_- Personal data should be relevant to the purposes for which it is to be used, and, to the extent necessary for those purposes, should be accurate, complete, and up-to-date._  \n_- We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification._  \n_- We will make readily available to customers information about our policies and practices relating to the management of personal information._\n\nWe are committed to conducting our business in accordance with these principles in order to ensure that the confidentiality of personal information is protected and maintained.\n\n### External Sites.\n\nartists-rating.com is not responsible for the content of external internet sites. You are advised to read the privacy policy of external sites before disclosing any personal information.\n\n### Cookies\n\nA "cookie" is a small data text file that is placed in your browser and allows artists-rating.com to recognize you each time you visit this site(customisation etc). Cookies themselves do not contain any personal information, and (local-)artists-rating.com does not use cookies to collect personal information. Cookies may also be used by 3rd party content providers such as newsfeeds.\n\n### Remember The Risks Whenever You Use The Internet\n\nWhile we do our best to protect your personal information, we cannot guarantee the security of any information that you transmit to (local-)artists-rating.com and you are solely responsible for maintaining the secrecy of any passwords or other account information. In addition other Internet sites or services that may be accessible through (local-)artists-rating.com have separate data and privacy practices independent of us, and therefore we disclaim any responsibility or liability for their policies or actions.\n\nPlease contact those vendors and others directly if you have any questions about their privacy policies.\n\nSerge at Home  \n(local-)artists-rating.com	en
18	7	Join Us	Join us and have luck with our site... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	Join us and have luck with our site\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	en
25	6	RU VERSION : Tours soon	RU VERSION : Tours soon short description...	RU VERSION : **Tours soon** lorem ipsum dolor sit amet, _consectetur adipiscing elit_, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsu\n> m dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	ru
26	6	ES VERSION : Tours soon	ES VERSION : Tours soon short description...	ES VERSION : **Tours soon** lorem ipsum dolor sit amet, _consectetur adipiscing elit_, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsu\n> m dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	es
27	6	Tours soon	Tours soon short description...	**Tours soon** lorem ipsum dolor sit amet, _consectetur adipiscing elit_, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsu\n> m dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	en
28	3	RU VERSION : Warranty and Service	RU VERSION : Warranty and Service What products are warranted? On the products in our store warranty...	RU VERSION : Warranty and Service What products are warranted? On the products in our store warranty...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	ru
22	4	RU VERSION : TDES Quiz	RU VERSION : TDES Quiz : Answer the questions and win TDES smartphones! Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	RU VERSION : TDES Quiz : **Answer the questions **and win TDES smartphones! lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	ru
24	4	TDES Quiz	TDES Quiz : Answer the questions and win TDES smartphones! Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	TDES Quiz : **Answer the questions **and win TDES smartphones! lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	en
29	3	ES VERSION : Warranty and Service	ES VERSION : Warranty and Service What products are warranted? On the products in our store warranty...	ES VERSION : Warranty and Service What products are warranted? On the products in our store warranty...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	es
30	3	Warranty and Service	Warranty and Service What products are warranted? On the products in our store warranty...	Warranty and Service What products are warranted? On the products in our store warranty...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	en
42	30	Content example ES	Content example _**ES Lorem ipsum **_dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.	Content example ES _**Lorem ipsum dolor**_ sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es
43	30	Content example RUuuuu	Content example _**RU : Lorem**_ ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,	Content example _**RU : Lorem ips**_um dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	ru
40	29	ru111111	22222aaaaaaxxxx	233333333333333aaaaaaaxxxxxxxx	ru
41	30	Content example EN	Content example short 1234ffggf\nhjghjhjhj	Content example content 3333333\n233333333333333\n333333	en
53	43	content example	AA	AA	en
5	1	ES VERSION : About US	ES VERSION : Welcome to page Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	ES VERSION : Welcome to artists-rating.com - Looking for a artists-rating has never been easier.Australian owned and operated by Serge at Home LTD (ACN 1326). Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	es
6	1	About US	Welcome to page Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	Welcome to artists-rating.com - Looking for a artists-rating has never been easier.Australian owned and operated by Serge at Home LTD (ACN 1326). Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	en
38	29	111esA	22222eessss\n2222\n2222222AAAAA	33333sss\n]3ss\n3AAAAAAAAAAAA	es
23	4	ES VERSION : TDES Quiz	ES VERSION : TDES Quiz : Answer the questions and win TDES smartphones! Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	ES VERSION : TDES Quiz : **Answer the questions **and win TDES smartphones! lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	es
17	7	ES VERSION : Join Us	ES VERSION : Join us and have luck with our site... Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore	ES VERSION : Join us and have luck with our site\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui...\n\n lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: [by link](#)	es
54	44	AAATTT	AA111	AA111	en
55	45	AAABBB	AA BBB	AABBB	en
56	46	AAAGGG	AAGGG	AAGGG	en
57	47	AAAKK 47	AAKKK 47	AAKKK	en
15	9	Contact us	Contact us and have luck with our site lorem ipsum dolor sit amet...	888**Contact** us lorem ipsum _dolor sit amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem _ipsum dolor sit a_met, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui... More: by link	en
58	48	50	AA50-----	AA50	en
59	49	AAA6	AA66	AA66	en
60	48	50_RU	50_RU 2	50_RU 3	ru
61	48	50_ES	50_ES 2	50_ES 33	es
62	50	AAA 51 RRRRRRRRR	AA AAA 52	AA AAA 53	en
63	50	AAA 51 RU SSSSSSs	AAA 51 RU 22 SSSSSSSSSSSSSSs	AAA 51 RU 33 SSSSSSSSSSSSs	ru
64	45	25 ES 1	25 ES 2	25 ES 3	es
65	45	45 RU 1	45 RU 2	45 RU 3	ru
66	51	AAA 51	AA	AA	en
\.


--
-- Data for Name: rt_cms_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_cms_items (id, alias, page_type, content_hints, image, icon, user_id, published, created_at, updated_at) FROM stdin;
1	about_us	P	\N	About_Us.jpg	color_lens	1	t	2013-06-06 04:31:22	2018-05-15 11:53:47
3	warranty_and_service	P	\N	\N	local_laundry_service	1	t	2016-11-30 16:13:42	2016-11-30 16:13:42
4	tdes_quiz	P	\N	quiz.png	date_range	1	t	2016-12-30 13:42:56	2018-04-20 11:52:10
8	company_info	P	\N	\N	explore	1	f	2016-12-30 11:10:25	2018-05-15 11:53:25
7	join_us	P	\N	\N	cached	1	t	2016-12-29 15:12:51	\N
6	tours_soon	P	\N	Tours_soon.jpg	color_lens	1	t	2016-12-27 11:42:35	2018-04-20 11:12:16
10	concerts_soon	P	\N	concerts_soon.jpeg	music_note	5	t	2018-04-20 15:32:55.049829	2018-04-20 12:51:56
5	security_privacy	P	\N	\N	gavel	1	t	2012-04-29 12:39:36	2013-07-31 17:09:59
29	aaa-bbb-ccc	P	\N	1.png	\N	5	t	2018-06-19 10:17:35.344646	\N
30	content-example	P	\N	1.png	\N	5	t	2018-06-23 08:10:16.923907	2018-06-23 05:12:48
43	content-example-1	P	\N	\N	AAA	5	f	2018-08-30 17:18:32.943872	\N
44	aaattt	P	\N	1.png	AAA111	5	t	2018-09-07 09:04:42.863668	\N
45	aaabbb	E	\N	1.png	AAABBB	5	t	2018-09-07 09:45:55.87918	\N
46	aaaggg	E	\N	4.jpeg	AAA	5	t	2018-09-07 09:54:51.099085	\N
47	aaakk	B	\N	2.jpeg	A47	5	t	2018-09-07 09:56:54.921818	2018-09-07 07:29:10
9	contact_us	P	\N	contact-us.jpg	flight_land	1	t	2016-12-29 12:53:18	2018-09-10 07:49:33
48	50	E	\N	1.png	AAA50	5	t	2018-09-10 10:50:49.068179	2018-09-10 07:52:34
49	aaa6	P	\N	inMonday.jpg	AAA	5	t	2018-09-12 13:03:43.746705	\N
50	aaa-51	P	\N	wizdom.jpg	AAA	5	t	2018-09-12 13:17:22.99091	2018-09-12 10:18:33
51	aaa-51-1	P	\N	inMonday.jpg	AAA	5	t	2018-09-13 08:04:34.345281	\N
\.


--
-- Data for Name: rt_genre_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_genre_translations (id, genre_id, name, description, created_at, locale) FROM stdin;
2	4	Spain VERSION : Classic	Spain VERSION : Classic Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-09-04 07:56:42	es
3	4	Classic	Classic Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-09-04 07:56:42	en
5	6	Spain VERSION : Falk	Spain VERSION : Falk description...dddddd	2018-09-04 07:56:42	es
6	6	Falk	Falk description...dddddd	2018-09-04 07:56:42	en
8	3	Spain VERSION : Jazz	Spain VERSION : Jazz Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	es
9	3	Jazz	Jazz Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	en
4	6	Народная	Russian VERSION : Falk description...dddddd	2018-09-04 07:56:42	ru
7	3	Джаз	Russian VERSION : Jazz Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	ru
10	1	Поп музыка	Russian VERSION : Pop Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum 	2018-09-04 07:56:42	ru
11	1	Spain VERSION : Pop	Spain VERSION : Pop Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum 	2018-09-04 07:56:42	es
12	1	Pop	Pop Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum 	2018-09-04 07:56:42	en
14	5	Spain VERSION : Rap	Spain VERSION : Rap Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	es
15	5	Rap	Rap Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	en
17	2	Spain VERSION : Rock	Spain VERSION : Rock Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	es
18	2	Rock	Rock Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	en
16	2	Рок	Russian VERSION : Rock Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	ru
1	4	Классика	Russian VERSION : Classic Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-09-04 07:56:42	ru
13	5	Рэп	Russian VERSION : Rap Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum	2018-09-04 07:56:42	ru
21	8	a	aaaa	2018-09-13 13:46:26	en
22	10	ff	ffffff	2018-09-13 13:56:30	en
25	10	ff10 RU 111	ff10 RU 1112222	2018-09-13 14:13:26	ru
23	10	ff10 ES 111	ff10 ES 222	2018-09-13 14:11:12	es
26	11	Electronic rock	Electronic rock Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2018-09-15 09:42:43	en
\.


--
-- Data for Name: rt_genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_genres (id, published, created_at, updated_at, slug) FROM stdin;
1	t	2018-03-23 07:02:26.800584	\N	pop
2	t	2018-03-23 07:02:26.806538	\N	rock
3	t	2018-03-23 07:02:26.811621	\N	jazz
4	t	2018-03-23 07:02:26.816696	\N	classic
5	t	2018-03-23 07:02:26.823931	\N	rap
6	f	2018-04-07 07:12:35.362847	2018-04-11 11:21:15	falk
8	t	2018-09-13 13:46:25.656767	\N	1
10	t	2018-09-13 13:56:29.948548	\N	ff
11	t	2018-09-15 09:42:43.384844	\N	electronic-rock
\.


--
-- Data for Name: rt_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_groups (id, name, description, created_at) FROM stdin;
1	Admin	Administrator	2018-03-16 17:38:31
2	Manager	Manager description...	2018-03-16 17:38:31
3	Developer	Developer description...	2018-03-16 17:38:31
4	Employee	Employee description...	2018-03-16 17:38:31
5	User	User description...	2018-06-18 13:52:21
\.


--
-- Data for Name: rt_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2017_11_23_055108_create_users_groups_table	1
3	2017_11_23_145312_create_settings_table	1
5	2018_05_29_124957_create_user_logins_table	2
9	2018_06_21_101634_create_cms_item_translations_table	3
11	2018_06_21_110033_modify_cms_items_clear_text_fields	4
12	2018_06_28_122735_modify_cms_item_translations_add_unique_key	5
19	2018_06_28_142918_create_artist_translations_table	6
20	2018_06_28_142924_create_song_translations_table	6
21	2018_07_02_112400_modify_artist_translations_table_drop_locale_fields	7
22	2018_08_22_103239_create_permission_tables	8
24	2018_08_22_145205_modify_users_add_unique_keys	9
31	2018_08_25_063318_create_shoppingcart_table	10
32	2018_08_30_055954_create_artist_images_translations_table	10
33	2018_08_30_060002_create_artist_concerts_translations_table	10
36	2018_09_04_074231_create_genres_translations_table	11
57	2018_09_04_111430_modify_genres_table_add_slug_field	12
58	2018_09_04_121935_create_orders_table	12
59	2018_09_04_121940_create_order_items_table	12
60	2016_09_13_070520_add_verification_to_user_table	13
\.


--
-- Data for Name: rt_model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_model_has_permissions (permission_id, model_type, model_id) FROM stdin;
6	App\\User	4
7	App\\User	4
1	App\\User	5
6	App\\User	5
7	App\\User	5
4	App\\User	6
6	App\\User	6
7	App\\User	6
1	App\\User	1
3	App\\User	1
6	App\\User	1
7	App\\User	1
2	App\\User	7
6	App\\User	7
7	App\\User	7
6	App\\User	8
7	App\\User	8
2	App\\User	2
6	App\\User	2
7	App\\User	2
6	App\\User	12
7	App\\User	12
6	App\\User	9
7	App\\User	9
4	App\\User	3
5	App\\User	3
6	App\\User	3
7	App\\User	3
2	App\\User	5
3	App\\User	5
5	App\\User	5
4	App\\User	5
1	App\\User	10
2	App\\User	10
\.


--
-- Data for Name: rt_model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_model_has_roles (role_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: rt_order_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_order_items (id, order_id, product_id, product_type, qty, price, created_at, updated_at) FROM stdin;
1	3	3	artist	1	1500.5	\N	\N
2	3	61	song	1	2000.25	\N	\N
3	3	123	song	1	2000.25	\N	\N
\.


--
-- Data for Name: rt_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_orders (id, user_id, card_owner, discount, discount_code, qty_count, price_total, payment, completed, error_message, created_at) FROM stdin;
1	5	gdfgdfgds	0	\N	2	3500.75	stripe	t	\N	2018-09-12 09:16:30
3	5	Owner	0	\N	3	5501.00	stripe	t	\N	2018-09-12 10:03:04
\.


--
-- Data for Name: rt_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
1	In backend edit system dictionaries	web	2018-08-23 10:55:51	2018-08-23 10:55:51
2	In backend edit users data	web	2018-08-23 10:55:51	2018-08-23 10:55:51
3	In backend set users role/status	web	2018-08-23 10:55:51	2018-08-23 10:55:51
4	In backend edit content(CMS) data	web	2018-08-23 10:55:51	2018-08-23 10:55:51
5	In backend publish content(CMS) data	web	2018-08-23 10:55:51	2018-08-23 10:55:51
6	In frontend to have access to pages under logged user(profile) and possibility to vote	web	2018-08-23 10:55:51	2018-08-23 10:55:51
7	In frontend to have access to all public pages	web	2018-08-23 10:55:51	2018-08-23 10:55:51
\.


--
-- Data for Name: rt_role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_role_has_permissions (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
2	2
4	3
5	3
6	4
7	5
\.


--
-- Data for Name: rt_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	Admin	web	2018-08-23 10:55:51	2018-08-23 10:55:51
2	UsersManager	web	2018-08-23 10:55:51	2018-08-23 10:55:51
3	ContentEditor	web	2018-08-23 10:55:51	2018-08-23 10:55:51
4	LoggedUser	web	2018-08-23 10:55:51	2018-08-23 10:55:51
5	AnonymousUser	web	2018-08-23 10:55:51	2018-08-23 10:55:51
\.


--
-- Data for Name: rt_settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_settings (id, name, value, created_at, updated_at) FROM stdin;
10	most_rating_artists_limit	8	2018-03-16 17:38:31	\N
11	most_rating_songs_limit	8	2018-03-16 17:38:31	\N
13	site_name_ru	Рейтинг артистов	2018-06-25 10:21:42	\N
14	site_subheading_ru	Вы можете принять участие в голосовании по вашим любимым артистам и исполнителям	2018-06-25 10:24:03	\N
15	copyright_text_ru	Все права сохранены	2018-06-25 10:24:27	\N
3	copyright_text_en	All rights reserved	2018-03-16 17:38:31	\N
2	site_name_en	Artists Rating	2018-03-16 17:38:31	\N
4	site_heading_en	Artists/Songs Rating and reviews	2018-03-21 09:55:13	\N
5	site_subheading_en	You can take participation in your favorite artists/songs rating and reviews	2018-03-21 09:55:13	\N
8	contact_us_email_en	contact_us@rating.com	2018-03-23 18:13:06	\N
6	contact_us_phone_en	2377-827-318	2018-03-23 18:13:06	\N
7	contact_us_location_en	Chicago, US	2018-03-23 18:13:06	\N
1	items_per_page_en	20	2018-03-16 17:38:31	\N
16	contact_us_phone_ru	345-678-6543	2018-08-28 14:12:32	\N
17	contact_us_email_ru	contact_us_ru@rating.com	2018-08-28 14:12:32	\N
18	contact_us_location_ru	Чикаго, США	2018-08-28 14:13:48	\N
12	site_heading_ru	Проснись и пой 	2018-06-25 10:18:56	\N
\.


--
-- Data for Name: rt_shoppingcart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_shoppingcart (identifier, instance, content, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: rt_song_artists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_song_artists (id, song_id, artist_id, created_at) FROM stdin;
1	1	1	2015-12-08 04:53:21
2	1	2	2015-12-25 08:58:07
3	2	1	2016-01-15 06:03:22
4	2	2	2015-12-29 10:52:41
5	2	3	2015-12-28 00:08:15
6	4	2	2015-12-19 10:46:30
7	3	3	2015-11-29 07:07:06
8	5	3	2015-11-23 20:20:24
9	6	1	2016-01-07 04:43:54
10	6	3	2015-11-19 16:00:56
11	7	4	2016-01-06 00:17:59
12	8	4	2015-12-26 02:41:00
34	2	1	2015-12-28 16:14:37
35	3	1	2015-12-30 23:06:23
36	30	2	2015-11-22 03:28:50
37	31	2	2015-12-19 18:14:22
38	32	2	2016-01-08 22:14:45
39	33	2	2015-11-27 05:40:47
40	34	2	2015-12-21 21:47:59
41	35	2	2015-12-27 08:58:25
42	36	2	2015-11-25 03:01:38
45	39	2	2015-12-28 02:00:41
46	40	2	2015-12-25 09:32:30
47	41	3	2016-01-10 17:51:20
48	42	3	2015-12-30 01:04:29
49	43	3	2016-01-08 16:17:44
50	44	3	2015-12-05 00:42:50
51	45	3	2016-01-15 15:14:50
52	46	3	2015-11-29 04:28:13
53	47	3	2016-01-16 09:53:45
54	48	3	2015-11-24 08:15:13
55	49	3	2015-11-25 01:32:59
56	50	3	2015-12-28 13:29:39
57	51	4	2016-01-12 17:23:05
58	52	4	2016-01-13 01:53:25
59	53	4	2015-12-31 01:16:47
60	54	4	2015-11-23 16:59:49
61	55	4	2015-12-28 19:33:09
62	56	4	2016-01-15 18:22:32
63	57	4	2016-01-02 20:31:13
64	58	4	2016-01-10 22:50:53
65	59	4	2015-11-29 04:58:38
66	60	4	2015-12-19 18:03:14
67	61	4	2015-12-19 05:00:28
68	62	4	2016-01-14 20:17:16
69	63	1	2015-12-08 18:25:11
70	64	1	2016-01-07 16:43:39
71	65	1	2015-12-26 04:52:13
72	66	1	2016-01-16 15:07:04
73	67	1	2016-01-14 23:42:22
136	74	5	2018-05-02 13:55:13.956377
137	75	5	2018-05-02 13:55:13.956377
138	76	5	2018-05-02 13:55:13.956377
139	77	5	2018-05-02 13:55:13.956377
140	78	5	2018-05-02 13:55:13.956377
141	79	6	2018-05-02 13:55:13.962976
142	80	6	2018-05-02 13:55:13.962976
143	81	6	2018-05-02 13:55:13.962976
144	82	6	2018-05-02 13:55:13.962976
145	83	6	2018-05-02 13:55:13.962976
146	84	7	2018-05-02 13:55:13.969458
147	85	7	2018-05-02 13:55:13.969458
148	86	7	2018-05-02 13:55:13.969458
149	87	7	2018-05-02 13:55:13.969458
150	88	7	2018-05-02 13:55:13.969458
151	89	7	2018-05-02 13:55:13.969458
152	90	7	2018-05-02 13:55:13.969458
155	116	9	2018-05-09 15:04:06.253869
156	117	9	2018-05-09 15:05:39.185359
157	118	9	2018-05-09 15:06:47.973412
158	119	8	2018-05-09 15:09:59.377932
159	120	8	2018-05-09 15:10:35.251087
160	121	8	2018-05-09 15:11:33.704545
161	122	8	2018-05-09 15:12:30.832712
162	123	39	2018-05-12 17:26:19.213576
164	94	9	2018-05-17 07:29:42.588808
\.


--
-- Data for Name: rt_song_genres; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_song_genres (id, song_id, genre_id, created_at) FROM stdin;
1	1	2	2016-01-17 10:24:53
2	1	4	2016-01-08 11:48:35
3	2	1	2016-01-08 07:25:04
4	2	3	2015-12-10 19:40:55
5	3	5	2016-01-08 21:26:24
6	4	2	2015-12-02 05:06:24
20	30	1	2015-12-31 18:43:43
21	31	1	2015-11-30 20:04:17
22	32	1	2015-11-18 08:09:27
23	33	1	2015-11-30 21:01:45
24	34	1	2015-11-28 07:48:09
25	35	1	2015-12-03 10:03:05
26	36	1	2015-12-11 08:29:15
27	37	1	2015-12-02 11:39:14
28	38	1	2015-12-26 06:57:55
29	39	1	2016-01-01 23:12:44
30	40	1	2015-12-29 06:12:32
31	41	1	2015-12-07 03:18:46
32	41	4	2016-01-05 01:54:59
33	42	1	2015-11-30 20:04:08
34	43	4	2015-11-22 11:22:13
35	44	4	2015-12-28 21:26:39
36	45	1	2015-05-25 08:54:20
37	45	4	2015-05-25 08:54:20
38	46	1	2015-05-25 08:54:47
39	46	4	2015-05-25 08:54:47
40	47	1	2015-05-25 08:55:21
41	48	1	2015-05-25 08:55:50
42	48	4	2015-05-25 08:55:50
43	49	1	2015-05-25 08:56:14
44	49	4	2015-05-25 08:56:14
45	50	1	2015-05-25 08:59:44
46	51	2	2015-05-25 10:17:23
47	52	2	2015-05-25 10:19:13
48	52	4	2015-05-25 10:19:13
49	53	2	2015-05-25 10:20:18
50	53	4	2015-05-25 10:20:18
51	54	2	2015-05-25 10:21:22
52	54	4	2015-05-25 10:21:22
53	55	2	2015-05-25 10:22:03
54	55	4	2015-05-25 10:22:03
55	56	2	2015-05-25 10:22:35
56	56	4	2015-05-25 10:22:35
57	57	2	2015-05-25 10:23:13
58	57	4	2015-05-25 10:23:13
59	58	2	2015-05-25 10:24:52
60	58	4	2015-05-25 10:24:52
61	59	2	2015-05-25 10:25:16
62	59	4	2015-05-25 10:25:16
63	60	2	2015-05-25 10:25:52
64	60	4	2015-05-25 10:25:52
65	61	2	2015-05-25 10:27:24
66	61	4	2015-05-25 10:27:24
67	62	2	2015-12-31 06:33:38
68	62	4	2015-12-22 15:22:57
69	63	3	2016-01-10 11:29:30
70	64	3	2016-01-15 16:16:26
71	65	3	2015-12-22 03:25:31
72	66	3	2015-11-19 10:38:36
73	67	3	2015-12-18 20:49:15
94	74	2	2018-04-05 15:10:14.870043
95	75	2	2018-04-05 15:10:14.870043
96	76	2	2018-04-05 15:10:14.870043
97	77	2	2018-04-05 15:10:14.870043
98	78	2	2018-04-05 15:10:14.870043
99	79	2	2018-04-05 15:10:14.870043
100	80	2	2018-04-05 15:10:14.870043
101	81	2	2018-04-05 15:10:14.870043
102	82	2	2018-04-05 15:10:14.870043
103	83	2	2018-04-05 15:10:14.870043
104	84	2	2018-04-05 15:10:14.870043
105	85	2	2018-04-05 15:10:14.870043
106	86	2	2018-04-05 15:10:14.870043
107	87	2	2018-04-05 15:10:14.870043
108	88	2	2018-04-05 15:10:14.870043
109	89	2	2018-04-05 15:10:14.870043
110	90	2	2018-04-05 15:10:14.870043
120	92	4	2018-04-07 09:15:43.812812
121	94	4	2018-04-07 09:20:16.25808
126	103	5	2018-04-07 14:28:54.864523
127	104	5	2018-04-07 14:46:12.693149
128	91	5	2018-04-07 14:52:14.170006
122	91	4	2018-04-07 09:34:12.837596
132	94	2	2016-01-17 10:24:53
133	95	4	2016-01-08 11:48:35
135	43	4	2018-05-13 13:52:14.381472
136	124	6	2018-05-15 14:14:49.61213
139	124	6	2018-05-17 07:31:14.383468
148	126	6	2018-05-17 08:28:05.752724
149	126	6	2018-05-17 08:28:18.378095
150	126	6	2018-05-17 08:28:26.722824
151	63	2	2018-09-04 14:07:52.820582
152	63	4	2018-09-04 14:07:52.840831
\.


--
-- Data for Name: rt_song_translations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_song_translations (id, song_id, title, short_descr, description, locale, created_at, updated_at) FROM stdin;
370	107	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
371	107	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
372	107	1	1	1	en	2018-08-29 14:58:11	\N
373	4	Russian VERSION : Rain and Tears	Russian VERSION : Rain and Tears short description...	Russian VERSION : Rain and Tears lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
374	4	Spain VERSION : Rain and Tears	Spain VERSION : Rain and Tears short description...	Spain VERSION : Rain and Tears lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
375	4	Rain and Tears	Rain and Tears short description...	Rain and Tears lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
376	110	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
377	110	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
378	110	1	1	1	en	2018-08-29 14:58:11	\N
379	104	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
380	104	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
381	104	1	1	1	en	2018-08-29 14:58:11	\N
382	98	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
383	98	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
384	98	1	1	1	en	2018-08-29 14:58:11	\N
385	99	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
386	99	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
387	99	1	1	1	en	2018-08-29 14:58:11	\N
388	105	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
389	105	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
390	105	1	1	1	en	2018-08-29 14:58:11	\N
391	116	Russian VERSION : Crazy in Love	Russian VERSION : Crazy in Love short description text \n1111\n222222	Russian VERSION : Crazy in Love long description text \n1111\n222222\n33333333	ru	2018-08-29 14:58:11	\N
392	116	Spain VERSION : Crazy in Love	Spain VERSION : Crazy in Love short description text \n1111\n222222	Spain VERSION : Crazy in Love long description text \n1111\n222222\n33333333	es	2018-08-29 14:58:11	\N
393	116	Crazy in Love	Crazy in Love short description text \n1111\n222222	Crazy in Love long description text \n1111\n222222\n33333333	en	2018-08-29 14:58:11	\N
394	109	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
395	109	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
396	109	1	1	1	en	2018-08-29 14:58:11	\N
397	103	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
398	103	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
399	103	1	1	1	en	2018-08-29 14:58:11	\N
400	100	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
401	100	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
402	100	1	1	1	en	2018-08-29 14:58:11	\N
403	106	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
404	106	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
405	106	1	1	1	en	2018-08-29 14:58:11	\N
406	119	Russian VERSION : Bring Me to Life	Russian VERSION : Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut a	Russian VERSION : Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	ru	2018-08-29 14:58:11	\N
407	119	Spain VERSION : Bring Me to Life	Spain VERSION : Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut a	Spain VERSION : Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	es	2018-08-29 14:58:11	\N
419	2	Spain VERSION : Mack the Knife (in Berlin)	Spain VERSION : Mack the Knife (in Berlin) short description...	Spain VERSION : Mack the Knife (in Berlin) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
408	119	Bring Me to Life	Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo	Bring Me to Life Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	en	2018-08-29 14:58:11	\N
409	102	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
410	102	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
411	102	1	1	1	en	2018-08-29 14:58:11	\N
412	101	Russian VERSION : 1	Russian VERSION : 1	Russian VERSION : 1	ru	2018-08-29 14:58:11	\N
413	101	Spain VERSION : 1	Spain VERSION : 1	Spain VERSION : 1	es	2018-08-29 14:58:11	\N
414	101	1	1	1	en	2018-08-29 14:58:11	\N
415	117	Russian VERSION : Baby Boy	Russian VERSION : Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, con...	Russian VERSION : Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	ru	2018-08-29 14:58:11	\N
416	117	Spain VERSION : Baby Boy	Spain VERSION : Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, con...	Spain VERSION : Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	es	2018-08-29 14:58:11	\N
625	85	Russian VERSION : Heart of the Country	Russian VERSION : Heart of the Country short description...	Russian VERSION : Heart of the Country song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
626	85	Spain VERSION : Heart of the Country	Spain VERSION : Heart of the Country short description...	Spain VERSION : Heart of the Country song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
627	85	Heart of the Country	Heart of the Country short description...	Heart of the Country song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
628	86	Russian VERSION : FourFiveSeconds	Russian VERSION : FourFiveSeconds short description...	Russian VERSION : FourFiveSeconds song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
629	86	Spain VERSION : FourFiveSeconds	Spain VERSION : FourFiveSeconds short description...	Spain VERSION : FourFiveSeconds song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
630	86	FourFiveSeconds	FourFiveSeconds short description...	FourFiveSeconds song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
631	87	Russian VERSION : Say Say Say	Russian VERSION : Say Say Say short description...	Russian VERSION : Say Say Say song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
632	87	Spain VERSION : Say Say Say	Spain VERSION : Say Say Say short description...	Spain VERSION : Say Say Say song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
633	87	Say Say Say	Say Say Say short description...	Say Say Say song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
634	88	Russian VERSION : Magneto and Titanium Man	Russian VERSION : Magneto and Titanium Man short description...	Russian VERSION : Magneto and Titanium Man song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
635	88	Spain VERSION : Magneto and Titanium Man	Spain VERSION : Magneto and Titanium Man short description...	Spain VERSION : Magneto and Titanium Man song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
636	88	Magneto and Titanium Man	Magneto and Titanium Man short description...	Magneto and Titanium Man song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
637	7	Russian VERSION : Can't Hep Falling In Love	Russian VERSION : Can't Hep Falling In Love short description...	Russian VERSION : Can't Hep Falling In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
638	7	Spain VERSION : Can't Hep Falling In Love	Spain VERSION : Can't Hep Falling In Love short description...	Spain VERSION : Can't Hep Falling In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
639	7	Can't Hep Falling In Love	Can't Hep Falling In Love short description...	Can't Hep Falling In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
640	52	Russian VERSION : A Boy Like Me, a Girl Like You	Russian VERSION : A Boy Like Me, a Girl Like You short description...	Russian VERSION : A Boy Like Me, a Girl Like You lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
641	52	Spain VERSION : A Boy Like Me, a Girl Like You	Spain VERSION : A Boy Like Me, a Girl Like You short description...	Spain VERSION : A Boy Like Me, a Girl Like You lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
642	52	A Boy Like Me, a Girl Like You	A Boy Like Me, a Girl Like You short description...	A Boy Like Me, a Girl Like You lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
643	89	Russian VERSION : The Back Seat of My Car	Russian VERSION : The Back Seat of My Car short description...	Russian VERSION : The Back Seat of My Car song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
644	89	Spain VERSION : The Back Seat of My Car	Spain VERSION : The Back Seat of My Car short description...	Spain VERSION : The Back Seat of My Car song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
645	89	The Back Seat of My Car	The Back Seat of My Car short description...	The Back Seat of My Car song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
646	90	Russian VERSION : I've Had Enough	Russian VERSION : I've Had Enough short description...	Russian VERSION : I've Had Enough song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
647	90	Spain VERSION : I've Had Enough	Spain VERSION : I've Had Enough short description...	Spain VERSION : I've Had Enough song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
648	90	I've Had Enough	I've Had Enough short description...	I've Had Enough song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
649	91	Russian VERSION : Hello	Russian VERSION : Hello song short description...	Russian VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
650	91	Spain VERSION : Hello	Spain VERSION : Hello song short description...	Spain VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
651	91	Hello	Hello song short description...	Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
652	92	Russian VERSION : Hello	Russian VERSION : Hello song short description...	Russian VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
653	92	Spain VERSION : Hello	Spain VERSION : Hello song short description...	Spain VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
654	92	Hello	Hello song short description...	Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
655	93	Russian VERSION : Hello	Russian VERSION : Hello short description...	Russian VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
656	93	Spain VERSION : Hello	Spain VERSION : Hello short description...	Spain VERSION : Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
657	93	Hello	Hello short description...	Hello song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
658	94	Russian VERSION : Surrender	Russian VERSION : Surrender short description...	Russian VERSION : Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
659	94	Spain VERSION : Surrender	Spain VERSION : Surrender short description...	Spain VERSION : Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
660	94	Surrender	Surrender short description...	Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
661	95	Russian VERSION : Surrender	Russian VERSION : Surrender short description...	Russian VERSION : Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
662	95	Spain VERSION : Surrender	Spain VERSION : Surrender short description...	Spain VERSION : Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
663	95	Surrender	Surrender short description...	Surrender song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
417	117	Baby Boy	Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, con...	Baby Boy Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	en	2018-08-29 14:58:11	\N
420	2	Mack the Knife (in Berlin)	Mack the Knife (in Berlin) short description...	Mack the Knife (in Berlin) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
421	120	Russian VERSION : Going Under	Russian VERSION : Going Under \n111\n222\n3333	Russian VERSION : Going Under Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	ru	2018-08-29 14:58:11	\N
422	120	Spain VERSION : Going Under	Spain VERSION : Going Under \n111\n222\n3333	Spain VERSION : Going Under Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	es	2018-08-29 14:58:11	\N
423	120	Going Under	Going Under \n111\n222\n3333	Going Under Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur.	en	2018-08-29 14:58:11	\N
424	48	Russian VERSION : Sleep Warm	Russian VERSION : Sleep Warm short description...	Russian VERSION : Sleep Warm lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
418	2	Russian VERSION : Mack the Knife (in Berlin)	Russian VERSION : Mack the Knife (in Berlin) short description...	Russian VERSION : Mack the Knife (in Berlin) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
425	48	Spain VERSION : Sleep Warm	Spain VERSION : Sleep Warm short description...	Spain VERSION : Sleep Warm lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
426	48	Sleep Warm	Sleep Warm short description...	Sleep Warm lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
427	58	Russian VERSION : Are You Lonesome Tonight	Russian VERSION : Are You Lonesome Tonight short description...	Russian VERSION : Are You Lonesome Tonight lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
428	58	Spain VERSION : Are You Lonesome Tonight	Spain VERSION : Are You Lonesome Tonight short description...	Spain VERSION : Are You Lonesome Tonight lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
429	58	Are You Lonesome Tonight	Are You Lonesome Tonight short description...	Are You Lonesome Tonight lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
430	118	Russian VERSION : Single Ladies (Put a Ring on It)	Russian VERSION : Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Russian VERSION : Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	ru	2018-08-29 14:58:11	\N
431	118	Spain VERSION : Single Ladies (Put a Ring on It)	Spain VERSION : Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Spain VERSION : Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	es	2018-08-29 14:58:11	\N
432	118	Single Ladies (Put a Ring on It)	Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	en	2018-08-29 14:58:11	\N
433	3	Russian VERSION : Forget Domani	Russian VERSION : Forget Domani  short description...	Russian VERSION : Forget Domani lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
434	3	Spain VERSION : Forget Domani	Spain VERSION : Forget Domani  short description...	Spain VERSION : Forget Domani lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
435	3	Forget Domani	Forget Domani  short description...	Forget Domani lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
436	30	Russian VERSION : Perd�name	Russian VERSION : Perd�name short description...	Russian VERSION : Perd�name lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
437	30	Spain VERSION : Perd�name	Spain VERSION : Perd�name short description...	Spain VERSION : Perd�name lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:11	\N
438	30	Perd�name	Perd�name short description...	Perd�name lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:11	\N
439	121	Russian VERSION : My Immortal	Russian VERSION : My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium	Russian VERSION : My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n222222\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n33333\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n444444444444	ru	2018-08-29 14:58:11	\N
440	121	Spain VERSION : My Immortal	Spain VERSION : My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium	Spain VERSION : My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n222222\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n33333\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n444444444444	es	2018-08-29 14:58:11	\N
441	121	My Immortal	My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium	My Immortal Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n222222\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n33333\nSed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium\n444444444444	en	2018-08-29 14:58:11	\N
442	114	Russian VERSION : 4	Russian VERSION : 444	Russian VERSION : 4444444\neww\n222223444	ru	2018-08-29 14:58:11	\N
443	114	Spain VERSION : 4	Spain VERSION : 444	Spain VERSION : 4444444\neww\n222223444	es	2018-08-29 14:58:11	\N
444	114	4	444	4444444\neww\n222223444	en	2018-08-29 14:58:11	\N
445	122	Russian VERSION : Lithium	Russian VERSION : Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Russian VERSION : Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n1111111111111111111111\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n22222222222222\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	ru	2018-08-29 14:58:11	\N
446	122	Spain VERSION : Lithium	Spain VERSION : Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Spain VERSION : Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n1111111111111111111111\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n22222222222222\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	es	2018-08-29 14:58:11	\N
447	122	Lithium	Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	Lithium Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n1111111111111111111111\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. \n22222222222222\nLorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua.	en	2018-08-29 14:58:11	\N
448	5	Russian VERSION : That's Life	Russian VERSION : That's Life short description...	Russian VERSION : That's Life	ru	2018-08-29 14:58:11	\N
449	5	Spain VERSION : That's Life	Spain VERSION : That's Life short description...	Spain VERSION : That's Life	es	2018-08-29 14:58:11	\N
450	5	That's Life	That's Life short description...	That's Life	en	2018-08-29 14:58:11	\N
451	44	Russian VERSION : Willow Weep for Me	Russian VERSION : Willow Weep for Me short description...	Russian VERSION : Willow Weep for Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:11	\N
452	44	Spain VERSION : Willow Weep for Me	Spain VERSION : Willow Weep for Me short description...	Spain VERSION : Willow Weep for Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
453	44	Willow Weep for Me	Willow Weep for Me short description...	Willow Weep for Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
454	1	Russian VERSION : Summertime (High Quality - Remastered)	Russian VERSION : Summertime (High Quality - Remastered) short description...	Russian VERSION : Summertime (High Quality - Remastered) song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
455	1	Spain VERSION : Summertime (High Quality - Remastered)	Spain VERSION : Summertime (High Quality - Remastered) short description...	Spain VERSION : Summertime (High Quality - Remastered) song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
456	1	Summertime (High Quality - Remastered)	Summertime (High Quality - Remastered) short description...	Summertime (High Quality - Remastered) song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
457	39	Russian VERSION : Happy To Be On An Island In The Sun	Russian VERSION : Happy To Be On An Island In The Sun short description...	Russian VERSION : Happy To Be On An Island In The Sun lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
458	39	Spain VERSION : Happy To Be On An Island In The Sun	Spain VERSION : Happy To Be On An Island In The Sun short description...	Spain VERSION : Happy To Be On An Island In The Sun lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
459	39	Happy To Be On An Island In The Sun	Happy To Be On An Island In The Sun short description...	Happy To Be On An Island In The Sun lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
460	6	Russian VERSION : THE DOZENS	Russian VERSION : THE DOZENS short description...	Russian VERSION : THE DOZENS lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
461	6	Spain VERSION : THE DOZENS	Spain VERSION : THE DOZENS short description...	Spain VERSION : THE DOZENS lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
462	6	THE DOZENS	THE DOZENS short description...	THE DOZENS lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
463	53	Russian VERSION : A Cane and a High Starched Collar	Russian VERSION : A Cane and a High Starched Collar short description...	Russian VERSION : A Cane and a High Starched Collar lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
464	53	Spain VERSION : A Cane and a High Starched Collar	Spain VERSION : A Cane and a High Starched Collar short description...	Spain VERSION : A Cane and a High Starched Collar lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
465	53	A Cane and a High Starched Collar	A Cane and a High Starched Collar short description...	A Cane and a High Starched Collar lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
466	60	Russian VERSION : Crying In The Chapel	Russian VERSION : Crying In The Chapel short description...	Russian VERSION : Crying In The Chapel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
467	60	Spain VERSION : Crying In The Chapel	Spain VERSION : Crying In The Chapel short description...	Spain VERSION : Crying In The Chapel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
468	60	Crying In The Chapel	Crying In The Chapel short description...	Crying In The Chapel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
469	57	Russian VERSION : And I Love You So	Russian VERSION : And I Love You So short description...	Russian VERSION : And I Love You So lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
470	57	Spain VERSION : And I Love You So	Spain VERSION : And I Love You So short description...	Spain VERSION : And I Love You So lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
471	57	And I Love You So	And I Love You So short description...	And I Love You So lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
472	56	Russian VERSION : Amazing Grace	Russian VERSION : Amazing Grace short description...	Russian VERSION : Amazing Grace lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
473	56	Spain VERSION : Amazing Grace	Spain VERSION : Amazing Grace short description...	Spain VERSION : Amazing Grace lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
474	56	Amazing Grace	Amazing Grace short description...	Amazing Grace lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
475	8	Russian VERSION : Always On My Mind	Russian VERSION : Always On My Mind short description...	Russian VERSION : Always On My Mind lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
476	8	Spain VERSION : Always On My Mind	Spain VERSION : Always On My Mind short description...	Spain VERSION : Always On My Mind lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
477	8	Always On My Mind	Always On My Mind short description...	Always On My Mind lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
478	59	Russian VERSION : Blue Christmas	Russian VERSION : Blue Christmas short description...	Russian VERSION : Blue Christmas lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
479	59	Spain VERSION : Blue Christmas	Spain VERSION : Blue Christmas short description...	Spain VERSION : Blue Christmas lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
480	59	Blue Christmas	Blue Christmas short description...	Blue Christmas lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
481	55	Russian VERSION : America the Beautiful	Russian VERSION : America the Beautiful short description...	Russian VERSION : America the Beautiful lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
482	55	Spain VERSION : America the Beautiful	Spain VERSION : America the Beautiful short description...	Spain VERSION : America the Beautiful lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
483	55	America the Beautiful	America the Beautiful short description...	America the Beautiful lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
484	54	Russian VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase	Russian VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase short description...	Russian VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
485	54	Spain VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase	Spain VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase short description...	Spain VERSION : Elvis Presley lyrics -- Elvis A-Z -- SongDataBase lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
486	54	Elvis Presley lyrics -- Elvis A-Z -- SongDataBase	Elvis Presley lyrics -- Elvis A-Z -- SongDataBase short description...	Elvis Presley lyrics -- Elvis A-Z -- SongDataBase lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
487	62	Russian VERSION : Return To Sender	Russian VERSION : Return To Sender short description...	Russian VERSION : Return To Sender lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
488	62	Spain VERSION : Return To Sender	Spain VERSION : Return To Sender short description...	Spain VERSION : Return To Sender lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
489	62	Return To Sender	Return To Sender short description...	Return To Sender lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
490	126	Russian VERSION : a s  c d fsda f dsfzzzzzzzzzz a a A A A A	Russian VERSION : daca	Russian VERSION : saddad	ru	2018-08-29 14:58:12	\N
491	126	Spain VERSION : a s  c d fsda f dsfzzzzzzzzzz a a A A A A	Spain VERSION : daca	Spain VERSION : saddad	es	2018-08-29 14:58:12	\N
492	126	a s  c d fsda f dsfzzzzzzzzzz a a A A A A	daca	saddad	en	2018-08-29 14:58:12	\N
493	124	Russian VERSION : zz	Russian VERSION : 22	Russian VERSION : ss	ru	2018-08-29 14:58:12	\N
494	124	Spain VERSION : zz	Spain VERSION : 22	Spain VERSION : ss	es	2018-08-29 14:58:12	\N
495	124	zz	22	ss	en	2018-08-29 14:58:12	\N
496	38	Russian VERSION : Someday Somewhere	Russian VERSION : Someday Somewhere short description...	Russian VERSION : Someday Somewhere lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
497	38	Spain VERSION : Someday Somewhere	Spain VERSION : Someday Somewhere short description...	Spain VERSION : Someday Somewhere lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
498	38	Someday Somewhere	Someday Somewhere short description...	Someday Somewhere lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
499	31	Russian VERSION : Can't Say How Much I LoveYou	Russian VERSION : Can't Say How Much I LoveYou short description...	Russian VERSION : Can't Say How Much I LoveYou lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
500	31	Spain VERSION : Can't Say How Much I LoveYou	Spain VERSION : Can't Say How Much I LoveYou short description...	Spain VERSION : Can't Say How Much I LoveYou lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
501	31	Can't Say How Much I LoveYou	Can't Say How Much I LoveYou short description...	Can't Say How Much I LoveYou lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
502	36	Russian VERSION : Come Waltz With Me	Russian VERSION : Come Waltz With Me short description...	Russian VERSION : Come Waltz With Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
503	36	Spain VERSION : Come Waltz With Me	Spain VERSION : Come Waltz With Me short description...	Spain VERSION : Come Waltz With Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
504	36	Come Waltz With Me	Come Waltz With Me short description...	Come Waltz With Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
505	37	Russian VERSION : Lost In Love	Russian VERSION : Lost In Love short description...	Russian VERSION : Lost In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
506	37	Spain VERSION : Lost In Love	Spain VERSION : Lost In Love short description...	Spain VERSION : Lost In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
507	37	Lost In Love	Lost In Love short description...	Lost In Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
508	35	Russian VERSION : When Forever Has Gone	Russian VERSION : When Forever Has Gone short description...	Russian VERSION : When Forever Has Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
509	35	Spain VERSION : When Forever Has Gone	Spain VERSION : When Forever Has Gone short description...	Spain VERSION : When Forever Has Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
510	35	When Forever Has Gone	When Forever Has Gone short description...	When Forever Has Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
511	32	Russian VERSION : Souvenirs	Russian VERSION : Souvenirs short description...	Russian VERSION : Souvenirs lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
512	32	Spain VERSION : Souvenirs	Spain VERSION : Souvenirs short description...	Spain VERSION : Souvenirs lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
513	32	Souvenirs	Souvenirs short description...	Souvenirs lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
514	34	Russian VERSION : Goodbye My Love, Goodbye	Russian VERSION : Goodbye My Love, Goodbye short description...	Russian VERSION : Goodbye My Love, Goodbye lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
515	34	Spain VERSION : Goodbye My Love, Goodbye	Spain VERSION : Goodbye My Love, Goodbye short description...	Spain VERSION : Goodbye My Love, Goodbye lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
516	34	Goodbye My Love, Goodbye	Goodbye My Love, Goodbye short description...	Goodbye My Love, Goodbye lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
517	115	Russian VERSION : w	Russian VERSION : dsAD	Russian VERSION : SDAD	ru	2018-08-29 14:58:12	\N
518	115	Spain VERSION : w	Spain VERSION : dsAD	Spain VERSION : SDAD	es	2018-08-29 14:58:12	\N
519	115	w	dsAD	SDAD	en	2018-08-29 14:58:12	\N
520	49	Russian VERSION : Where or When	Russian VERSION : Where or When short description...	Russian VERSION : Where or When lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
521	49	Spain VERSION : Where or When	Spain VERSION : Where or When short description...	Spain VERSION : Where or When lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
522	49	Where or When	Where or When short description...	Where or When lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
523	45	Russian VERSION : Guess I'll Hang My Tears Out to Dry	Russian VERSION : Guess I'll Hang My Tears Out to Dry short description...	Russian VERSION : Guess I'll Hang My Tears Out to Dry	ru	2018-08-29 14:58:12	\N
524	45	Spain VERSION : Guess I'll Hang My Tears Out to Dry	Spain VERSION : Guess I'll Hang My Tears Out to Dry short description...	Spain VERSION : Guess I'll Hang My Tears Out to Dry	es	2018-08-29 14:58:12	\N
525	45	Guess I'll Hang My Tears Out to Dry	Guess I'll Hang My Tears Out to Dry short description...	Guess I'll Hang My Tears Out to Dry	en	2018-08-29 14:58:12	\N
526	61	Russian VERSION : Jailhouse Rock	Russian VERSION : Jailhouse Rock short description...	Russian VERSION : Jailhouse Rock lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
527	61	Spain VERSION : Jailhouse Rock	Spain VERSION : Jailhouse Rock short description...	Spain VERSION : Jailhouse Rock lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
528	61	Jailhouse Rock	Jailhouse Rock short description...	Jailhouse Rock lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
529	40	Russian VERSION : Sailin\\' Home	Russian VERSION : Sailin\\' Home short description...	Russian VERSION : Sailin' Home lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
530	40	Spain VERSION : Sailin\\' Home	Spain VERSION : Sailin\\' Home short description...	Spain VERSION : Sailin' Home lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
531	40	Sailin\\' Home	Sailin\\' Home short description...	Sailin' Home lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
532	33	Russian VERSION : My Only Fascination	Russian VERSION : My Only Fascination short description...	Russian VERSION : My Only Fascination lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
533	33	Spain VERSION : My Only Fascination	Spain VERSION : My Only Fascination short description...	Spain VERSION : My Only Fascination lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
534	33	My Only Fascination	My Only Fascination short description...	My Only Fascination lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
535	47	Russian VERSION : One for My Baby (And One More for the Road)	Russian VERSION : One for My Baby (And One More for the Road) short description...	Russian VERSION : One for My Baby (And One More for the Road) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
536	47	Spain VERSION : One for My Baby (And One More for the Road)	Spain VERSION : One for My Baby (And One More for the Road) short description...	Spain VERSION : One for My Baby (And One More for the Road) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
537	47	One for My Baby (And One More for the Road)	One for My Baby (And One More for the Road) short description...	One for My Baby (And One More for the Road) lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
538	123	Russian VERSION : NewNew	Russian VERSION : NewNew	Russian VERSION : NewNew	ru	2018-08-29 14:58:12	\N
539	123	Spain VERSION : NewNew	Spain VERSION : NewNew	Spain VERSION : NewNew	es	2018-08-29 14:58:12	\N
540	123	NewNew	NewNew	NewNew	en	2018-08-29 14:58:12	\N
541	46	Russian VERSION : Blues in the Night	Russian VERSION : Blues in the Night short description...	Russian VERSION : Blues in the Night lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
542	46	Spain VERSION : Blues in the Night	Spain VERSION : Blues in the Night short description...	Spain VERSION : Blues in the Night lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
543	46	Blues in the Night	Blues in the Night short description...	Blues in the Night lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
544	50	Russian VERSION : Spring Is Here	Russian VERSION : Spring Is Here short description...	Russian VERSION : Spring Is Here lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
545	50	Spain VERSION : Spring Is Here	Spain VERSION : Spring Is Here short description...	Spain VERSION : Spring Is Here lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
546	50	Spring Is Here	Spring Is Here short description...	Spring Is Here lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
547	42	Russian VERSION : Angel Eyes	Russian VERSION : Angel Eyes short description...	Russian VERSION : Angel Eyes lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
548	42	Spain VERSION : Angel Eyes	Spain VERSION : Angel Eyes short description...	Spain VERSION : Angel Eyes lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
549	42	Angel Eyes	Angel Eyes short description...	Angel Eyes lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
550	51	Russian VERSION : A Big Hunk O' Love	Russian VERSION : A Big Hunk O' Love short description...	Russian VERSION : A Big Hunk O' Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
551	51	Spain VERSION : A Big Hunk O' Love	Spain VERSION : A Big Hunk O' Love short description...	Spain VERSION : A Big Hunk O' Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
552	51	A Big Hunk O' Love	A Big Hunk O' Love short description...	A Big Hunk O' Love lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
553	41	Russian VERSION : Only the Lonely	Russian VERSION : Only the Lonely short description...	Russian VERSION : Only the Lonely lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
554	41	Spain VERSION : Only the Lonely	Spain VERSION : Only the Lonely short description...	Spain VERSION : Only the Lonely lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
555	41	Only the Lonely	Only the Lonely short description...	Only the Lonely lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
556	63	Russian VERSION : Undecided	Russian VERSION : Undecided short description...	Russian VERSION : Undecided lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
557	63	Spain VERSION : Undecided	Spain VERSION : Undecided short description...	Spain VERSION : Undecided lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
558	63	Undecided	Undecided short description...	Undecided lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
559	64	Russian VERSION : A-Tisket, A-Tasket	Russian VERSION : A-Tisket, A-Tasket short description...	Russian VERSION : A-Tisket, A-Tasket lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
560	64	Spain VERSION : A-Tisket, A-Tasket	Spain VERSION : A-Tisket, A-Tasket short description...	Spain VERSION : A-Tisket, A-Tasket lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
561	64	A-Tisket, A-Tasket	A-Tisket, A-Tasket short description...	A-Tisket, A-Tasket lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
562	65	Russian VERSION : Dream a Little Dream of Me	Russian VERSION : Dream a Little Dream of Me short description...	Russian VERSION : Dream a Little Dream of Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
563	65	Spain VERSION : Dream a Little Dream of Me	Spain VERSION : Dream a Little Dream of Me short description...	Spain VERSION : Dream a Little Dream of Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
564	65	Dream a Little Dream of Me	Dream a Little Dream of Me short description...	Dream a Little Dream of Me lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
565	43	Russian VERSION : It's a Lonesome Old Town	Russian VERSION : It's a Lonesome Old Town short description...	Russian VERSION : It's a Lonesome Old Town lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
566	43	Spain VERSION : It's a Lonesome Old Town	Spain VERSION : It's a Lonesome Old Town short description...	Spain VERSION : It's a Lonesome Old Town lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
567	43	It's a Lonesome Old Town	It's a Lonesome Old Town short description...	It's a Lonesome Old Town lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
568	66	Russian VERSION : Sugar Blues	Russian VERSION : Sugar Blues short description...	Russian VERSION : Sugar Blues lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
569	66	Spain VERSION : Sugar Blues	Spain VERSION : Sugar Blues short description...	Spain VERSION : Sugar Blues lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
570	66	Sugar Blues	Sugar Blues short description...	Sugar Blues lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
571	67	Russian VERSION : The Starlit Hour	Russian VERSION : The Starlit Hour short description...	Russian VERSION : The Starlit Hour lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
572	67	Spain VERSION : The Starlit Hour	Spain VERSION : The Starlit Hour short description...	Spain VERSION : The Starlit Hour lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:12	\N
573	67	The Starlit Hour	The Starlit Hour short description...	The Starlit Hour lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:12	\N
574	68	Russian VERSION : Carmen	Russian VERSION : Carmen short description...	Russian VERSION : Carmen lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:12	\N
575	68	Spain VERSION : Carmen	Spain VERSION : Carmen short description...	Spain VERSION : Carmen lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
576	68	Carmen	Carmen short description...	Carmen lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
577	69	Russian VERSION : Car	Russian VERSION : Car short description...	Russian VERSION : Car lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
578	69	Spain VERSION : Car	Spain VERSION : Car short description...	Spain VERSION : Car lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
579	69	Car	Car short description...	Car lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
580	70	Russian VERSION : I'll Come	Russian VERSION : I'll Come short description...	Russian VERSION : I'll Come lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
581	70	Spain VERSION : I'll Come	Spain VERSION : I'll Come short description...	Spain VERSION : I'll Come lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
582	70	I'll Come	I'll Come short description...	I'll Come lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
583	71	Russian VERSION : Mussa	Russian VERSION : Mussa short description...	Russian VERSION : Mussa lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
584	71	Spain VERSION : Mussa	Spain VERSION : Mussa short description...	Spain VERSION : Mussa lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
585	71	Mussa	Mussa short description...	Mussa lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
586	72	Russian VERSION : You've Gone	Russian VERSION : You've Gone short description...	Russian VERSION : You've Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
587	72	Spain VERSION : You've Gone	Spain VERSION : You've Gone short description...	Spain VERSION : You've Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
588	72	You've Gone	You've Gone short description...	You've Gone lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
589	73	Russian VERSION : Winter Again	Russian VERSION : Winter Again short description...	Russian VERSION : Winter Again lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
590	73	Spain VERSION : Winter Again	Spain VERSION : Winter Again short description...	Spain VERSION : Winter Again lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
591	73	Winter Again	Winter Again short description...	Winter Again lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
592	74	Russian VERSION : Motherless Children	Russian VERSION : Motherless Children short description...	Russian VERSION : Motherless Children song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
593	74	Spain VERSION : Motherless Children	Spain VERSION : Motherless Children short description...	Spain VERSION : Motherless Children song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
594	74	Motherless Children	Motherless Children short description...	Motherless Children song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
595	75	Russian VERSION : Badge	Russian VERSION : Badge short description...	Russian VERSION : Badge song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
596	75	Spain VERSION : Badge	Spain VERSION : Badge short description...	Spain VERSION : Badge song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
597	75	Badge	Badge short description...	Badge song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
598	76	Russian VERSION : Cocaine	Russian VERSION : Cocaine short description...	Russian VERSION : Cocaine song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
599	76	Spain VERSION : Cocaine	Spain VERSION : Cocaine short description...	Spain VERSION : Cocaine song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
600	76	Cocaine	Cocaine short description...	Cocaine song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
601	77	Russian VERSION : Bell Bottom Blues	Russian VERSION : Bell Bottom Blues short description...	Russian VERSION : Bell Bottom Blues song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
602	77	Spain VERSION : Bell Bottom Blues	Spain VERSION : Bell Bottom Blues short description...	Spain VERSION : Bell Bottom Blues song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
603	77	Bell Bottom Blues	Bell Bottom Blues short description...	Bell Bottom Blues song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
604	78	Russian VERSION : I Shot the Sheriff	Russian VERSION : I Shot the Sheriff short description...	Russian VERSION : I Shot the Sheriff song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
605	78	Spain VERSION : I Shot the Sheriff	Spain VERSION : I Shot the Sheriff short description...	Spain VERSION : I Shot the Sheriff song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
606	78	I Shot the Sheriff	I Shot the Sheriff short description...	I Shot the Sheriff song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
607	79	Russian VERSION : Thunder Road	Russian VERSION : Thunder Road short description...	Russian VERSION : Thunder Road song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
608	79	Spain VERSION : Thunder Road	Spain VERSION : Thunder Road short description...	Spain VERSION : Thunder Road song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
609	79	Thunder Road	Thunder Road short description...	Thunder Road song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
610	80	Russian VERSION : The River	Russian VERSION : The River  short description...	Russian VERSION : The River  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
611	80	Spain VERSION : The River	Spain VERSION : The River  short description...	Spain VERSION : The River  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
612	80	The River	The River  short description...	The River  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
613	81	Russian VERSION : Darkness On The Edge of Town	Russian VERSION : Darkness On The Edge of Town  short description...	Russian VERSION : Darkness On The Edge of Town  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
614	81	Spain VERSION : Darkness On The Edge of Town	Spain VERSION : Darkness On The Edge of Town  short description...	Spain VERSION : Darkness On The Edge of Town  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
615	81	Darkness On The Edge of Town	Darkness On The Edge of Town  short description...	Darkness On The Edge of Town  song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
616	82	Russian VERSION : I’m Goin’ Down	Russian VERSION : I’m Goin’ Down short description...	Russian VERSION : I’m Goin’ Down song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
617	82	Spain VERSION : I’m Goin’ Down	Spain VERSION : I’m Goin’ Down short description...	Spain VERSION : I’m Goin’ Down song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
618	82	I’m Goin’ Down	I’m Goin’ Down short description...	I’m Goin’ Down song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
619	83	Russian VERSION : Jungleland	Russian VERSION : Jungleland short description...	Russian VERSION : Jungleland song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
620	83	Spain VERSION : Jungleland	Spain VERSION : Jungleland short description...	Spain VERSION : Jungleland song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
621	83	Jungleland	Jungleland short description...	Jungleland song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
622	84	Russian VERSION : Hope of Deliverance	Russian VERSION : Hope of Deliverance short description...	Russian VERSION : Hope of Deliverance song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	ru	2018-08-29 14:58:13	\N
623	84	Spain VERSION : Hope of Deliverance	Spain VERSION : Hope of Deliverance short description...	Spain VERSION : Hope of Deliverance song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	es	2018-08-29 14:58:13	\N
624	84	Hope of Deliverance	Hope of Deliverance short description...	Hope of Deliverance song description lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum...	en	2018-08-29 14:58:13	\N
\.


--
-- Data for Name: rt_song_votes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_song_votes (id, song_id, user_id, vote, created_at) FROM stdin;
1	103	4	4	2018-04-09 20:17:13
2	107	4	1	2018-04-25 22:42:02
3	101	4	3	2018-04-07 16:02:27
4	105	4	4	2018-04-27 20:07:20
5	109	4	4	2018-04-06 01:59:05
6	102	4	2	2018-04-10 18:08:23
7	4	4	1	2018-04-09 09:54:06
8	99	4	3	2018-04-08 20:19:08
9	100	4	1	2018-04-09 18:46:55
10	104	4	1	2018-04-09 11:06:13
11	106	4	3	2018-04-05 13:11:52
12	110	4	5	2018-04-22 22:58:21
14	98	4	4	2018-04-28 14:04:34
15	2	4	3	2018-04-15 10:36:24
17	48	4	5	2018-04-05 00:08:10
18	3	4	5	2018-04-17 04:37:17
19	30	4	2	2018-04-07 07:54:54
20	58	4	4	2018-04-13 09:45:47
21	5	4	5	2018-04-05 10:57:36
22	44	4	1	2018-04-02 04:45:45
23	114	4	3	2018-04-06 04:54:30
24	39	4	3	2018-04-16 22:34:20
25	1	4	2	2018-04-12 11:28:24
26	60	4	1	2018-04-08 14:00:33
27	53	4	1	2018-04-26 07:47:26
28	57	4	1	2018-04-06 20:54:36
29	6	4	2	2018-04-21 07:07:14
30	56	4	3	2018-04-16 22:26:39
31	8	4	1	2018-04-18 00:27:34
32	59	4	3	2018-04-16 16:07:17
33	54	4	2	2018-04-11 01:17:19
34	55	4	3	2018-04-04 21:55:44
35	62	4	4	2018-04-05 02:47:28
36	32	4	3	2018-04-08 17:16:47
37	31	4	4	2018-04-02 13:23:26
38	36	4	1	2018-04-23 20:40:22
39	38	4	3	2018-04-24 22:51:50
40	37	4	4	2018-04-18 07:42:40
41	35	4	5	2018-04-13 02:20:04
42	34	4	2	2018-04-05 09:42:59
43	115	4	2	2018-04-03 18:45:24
44	33	4	4	2018-04-11 08:09:04
45	45	4	5	2018-04-03 12:21:36
46	49	4	4	2018-04-09 16:19:41
47	40	4	1	2018-04-19 23:13:04
48	61	4	5	2018-04-04 02:40:37
49	47	4	4	2018-04-18 06:32:54
50	51	4	1	2018-04-02 16:53:48
51	46	4	2	2018-04-11 08:47:04
52	42	4	3	2018-04-26 02:22:36
53	50	4	1	2018-04-18 02:21:36
54	41	4	3	2018-04-01 21:22:06
55	63	4	5	2018-04-16 05:40:57
56	64	4	2	2018-04-29 11:19:06
57	65	4	4	2018-04-01 11:03:50
58	43	4	4	2018-04-21 13:55:07
59	66	4	4	2018-04-09 15:22:51
60	67	4	3	2018-04-06 05:06:10
61	68	4	1	2018-04-19 05:28:33
62	69	4	4	2018-04-25 23:24:45
63	70	4	1	2018-04-09 20:59:12
64	71	4	1	2018-04-04 07:33:15
65	72	4	3	2018-04-08 02:47:52
66	73	4	3	2018-04-26 07:52:53
67	74	4	5	2018-04-06 10:53:20
68	75	4	4	2018-04-30 18:22:01
69	76	4	4	2018-04-09 06:05:59
70	77	4	1	2018-04-13 04:16:21
71	78	4	2	2018-04-30 08:42:49
72	79	4	5	2018-04-18 05:24:37
73	80	4	2	2018-04-15 18:36:49
74	81	4	1	2018-04-15 17:18:49
75	82	4	1	2018-04-28 17:33:21
77	84	4	2	2018-04-17 22:43:57
78	85	4	2	2018-04-29 22:03:06
79	86	4	1	2018-04-17 04:28:49
80	87	4	4	2018-04-14 09:24:40
81	88	4	2	2018-04-24 14:29:06
82	7	4	2	2018-04-12 15:17:16
83	89	4	5	2018-04-14 03:48:17
84	52	4	1	2018-04-30 21:38:58
85	90	4	2	2018-04-01 12:06:21
86	91	4	3	2018-04-21 18:18:21
87	92	4	2	2018-04-11 18:39:44
88	93	4	4	2018-04-16 02:34:29
89	94	4	3	2018-04-26 10:44:10
90	95	4	5	2018-04-17 04:50:03
91	103	5	4	2018-04-25 21:47:50
92	107	5	4	2018-04-29 21:15:10
93	101	5	3	2018-04-10 15:57:53
94	105	5	5	2018-04-15 06:56:23
95	109	5	5	2018-04-19 16:10:46
96	102	5	3	2018-04-21 08:34:18
97	4	5	5	2018-04-24 01:14:14
98	99	5	2	2018-04-01 22:34:16
99	100	5	1	2018-04-25 15:45:08
100	104	5	3	2018-04-30 18:07:27
101	106	5	4	2018-04-06 23:34:16
102	110	5	4	2018-04-09 23:09:08
104	98	5	3	2018-04-20 20:12:22
105	2	5	5	2018-04-21 08:24:34
107	48	5	2	2018-04-03 03:22:39
108	3	5	2	2018-04-18 10:41:39
109	30	5	5	2018-04-10 20:02:58
110	58	5	1	2018-04-12 07:52:26
111	5	5	1	2018-04-16 23:15:13
112	44	5	4	2018-04-06 12:33:17
113	114	5	1	2018-04-13 05:50:09
114	39	5	1	2018-04-13 13:04:03
115	1	5	3	2018-04-20 16:58:56
116	60	5	2	2018-04-12 03:08:50
117	53	5	5	2018-04-26 01:04:58
118	57	5	5	2018-04-12 21:31:33
119	6	5	2	2018-04-24 05:43:19
120	56	5	1	2018-04-09 15:11:00
121	8	5	4	2018-04-15 03:05:32
122	59	5	3	2018-04-16 23:22:20
123	54	5	5	2018-04-20 19:42:47
124	55	5	4	2018-04-15 18:03:51
125	62	5	1	2018-04-26 18:47:54
126	32	5	1	2018-04-02 16:56:14
127	31	5	5	2018-04-16 14:06:20
128	36	5	2	2018-04-03 20:37:33
129	38	5	2	2018-04-30 14:44:34
130	37	5	3	2018-04-23 03:37:58
131	35	5	2	2018-04-29 19:44:22
132	34	5	4	2018-04-12 08:36:54
133	115	5	3	2018-04-22 04:21:39
134	33	5	3	2018-04-08 18:30:38
135	45	5	1	2018-04-30 22:21:29
136	49	5	5	2018-04-08 17:03:39
137	40	5	1	2018-04-23 11:06:58
138	61	5	4	2018-04-04 08:12:00
139	47	5	4	2018-04-01 06:03:38
140	51	5	5	2018-04-07 12:34:28
141	46	5	2	2018-04-16 11:06:29
142	42	5	4	2018-04-29 02:51:00
143	50	5	2	2018-04-10 13:59:41
144	41	5	2	2018-04-11 11:37:02
145	63	5	3	2018-04-23 20:20:13
146	64	5	3	2018-04-18 11:06:13
147	65	5	4	2018-04-18 05:20:36
148	43	5	4	2018-04-28 15:51:19
149	66	5	1	2018-04-12 08:07:27
150	67	5	2	2018-04-02 03:43:40
151	68	5	4	2018-04-02 23:20:44
152	69	5	5	2018-04-25 01:17:06
153	70	5	3	2018-04-09 10:38:18
154	71	5	1	2018-04-04 02:02:46
155	72	5	5	2018-04-07 12:48:26
156	73	5	3	2018-04-09 18:49:15
157	74	5	3	2018-04-29 20:19:18
158	75	5	4	2018-04-09 05:05:43
159	76	5	5	2018-04-04 23:07:12
160	77	5	5	2018-04-15 23:06:38
161	78	5	2	2018-04-01 07:17:41
162	79	5	3	2018-04-08 19:45:21
163	80	5	4	2018-04-19 16:35:13
164	81	5	3	2018-04-23 18:57:18
165	82	5	5	2018-04-11 13:59:51
167	84	5	2	2018-04-12 00:16:01
168	85	5	1	2018-04-16 15:11:18
169	86	5	1	2018-04-14 22:26:04
170	87	5	4	2018-04-12 01:42:32
171	88	5	4	2018-04-10 04:28:52
172	7	5	3	2018-04-12 12:47:34
173	89	5	1	2018-04-08 17:06:35
174	52	5	5	2018-04-15 04:51:38
175	90	5	5	2018-04-06 13:44:20
176	91	5	3	2018-04-05 10:52:58
177	92	5	1	2018-04-23 21:18:07
178	93	5	4	2018-04-21 19:04:37
179	94	5	2	2018-04-16 18:19:27
180	95	5	3	2018-04-24 19:53:11
181	103	6	3	2018-04-28 14:55:33
182	107	6	5	2018-04-27 22:00:04
183	101	6	4	2018-04-20 10:35:23
184	105	6	1	2018-04-11 09:22:23
185	109	6	1	2018-04-16 12:49:14
186	102	6	2	2018-04-06 19:46:59
187	4	6	1	2018-04-09 03:58:20
188	99	6	1	2018-04-28 22:31:26
189	100	6	5	2018-04-06 18:09:49
190	104	6	5	2018-04-12 03:56:45
191	106	6	2	2018-04-18 16:14:30
192	110	6	5	2018-04-26 03:01:01
194	98	6	3	2018-04-30 03:48:01
195	2	6	3	2018-04-20 10:22:35
197	48	6	3	2018-04-12 02:56:44
198	3	6	2	2018-04-16 05:17:27
199	30	6	4	2018-04-13 09:31:13
200	58	6	3	2018-04-02 05:45:38
201	5	6	2	2018-04-25 04:23:50
202	44	6	1	2018-04-08 19:29:33
203	114	6	5	2018-04-04 13:55:51
204	39	6	5	2018-04-10 22:58:12
205	1	6	3	2018-04-05 11:52:04
206	60	6	3	2018-04-17 08:18:44
207	53	6	2	2018-04-22 13:12:26
208	57	6	2	2018-04-16 08:17:45
209	6	6	5	2018-04-12 00:51:30
210	56	6	3	2018-04-20 04:47:25
211	8	6	2	2018-04-26 15:58:09
212	59	6	2	2018-04-20 22:18:25
213	54	6	2	2018-04-29 16:08:34
214	55	6	1	2018-04-27 19:02:03
215	62	6	1	2018-04-07 05:51:19
216	32	6	4	2018-04-20 17:57:37
217	31	6	1	2018-04-05 21:50:53
218	36	6	3	2018-04-20 07:50:49
219	38	6	4	2018-04-08 16:02:37
220	37	6	3	2018-04-27 20:42:41
221	35	6	5	2018-04-06 08:53:29
222	34	6	5	2018-04-13 04:55:42
223	115	6	5	2018-04-20 08:03:49
224	33	6	3	2018-04-01 20:17:34
225	45	6	2	2018-04-25 16:54:04
226	49	6	3	2018-04-17 10:04:27
227	40	6	2	2018-04-17 07:23:07
228	61	6	4	2018-04-16 13:15:36
229	47	6	3	2018-04-07 09:10:25
230	51	6	1	2018-04-03 04:27:20
231	46	6	2	2018-04-29 19:24:59
232	42	6	1	2018-04-25 19:25:31
233	50	6	4	2018-04-10 09:37:30
234	41	6	1	2018-04-20 00:27:35
235	63	6	4	2018-04-15 08:03:15
236	64	6	3	2018-04-14 08:11:49
237	65	6	5	2018-04-30 23:13:01
238	43	6	3	2018-04-15 12:21:15
239	66	6	4	2018-04-09 07:57:18
240	67	6	3	2018-04-13 03:32:10
241	68	6	1	2018-04-18 03:32:19
242	69	6	3	2018-04-13 21:45:31
243	70	6	3	2018-04-26 07:00:27
244	71	6	5	2018-04-08 04:43:07
245	72	6	2	2018-04-04 18:51:27
246	73	6	5	2018-04-16 07:00:41
247	74	6	4	2018-04-30 05:50:03
248	75	6	5	2018-04-04 17:35:19
249	76	6	3	2018-04-22 00:51:31
250	77	6	3	2018-04-13 21:15:52
251	78	6	4	2018-04-11 00:51:28
252	79	6	4	2018-04-25 23:11:59
253	80	6	3	2018-04-01 16:09:29
254	81	6	3	2018-04-01 11:36:10
255	82	6	5	2018-04-06 14:54:34
256	83	6	5	2018-04-19 00:35:22
257	84	6	3	2018-04-01 07:50:54
258	85	6	1	2018-04-03 05:43:07
259	86	6	1	2018-04-09 15:37:02
260	87	6	5	2018-04-24 22:49:31
261	88	6	3	2018-04-11 03:39:29
262	7	6	5	2018-04-15 20:23:19
263	89	6	2	2018-04-19 06:29:41
264	52	6	2	2018-04-04 04:22:23
265	90	6	3	2018-04-09 07:13:41
266	91	6	2	2018-05-01 04:14:52
267	92	6	2	2018-04-06 08:03:56
268	93	6	1	2018-04-14 14:31:13
269	94	6	2	2018-04-30 08:40:28
270	95	6	2	2018-04-30 01:37:48
271	103	1	4	2018-04-23 03:46:56
272	107	1	1	2018-04-30 21:56:33
273	101	1	5	2018-04-27 08:49:58
274	105	1	1	2018-04-16 23:07:13
275	109	1	5	2018-04-07 00:24:44
276	102	1	1	2018-04-07 04:01:35
277	4	1	1	2018-04-30 06:09:45
278	99	1	2	2018-04-24 08:06:16
279	100	1	5	2018-04-01 21:40:33
280	104	1	4	2018-04-03 03:48:42
281	106	1	4	2018-04-29 05:41:48
282	110	1	4	2018-04-06 18:55:21
284	98	1	3	2018-04-07 21:27:56
285	2	1	3	2018-04-01 19:07:09
287	48	1	3	2018-04-22 09:17:43
288	3	1	3	2018-04-05 08:49:41
289	30	1	1	2018-04-24 02:13:24
290	58	1	5	2018-04-05 18:53:44
291	5	1	4	2018-04-11 10:19:31
292	44	1	1	2018-04-20 08:26:58
293	114	1	4	2018-04-14 00:35:21
294	39	1	4	2018-04-09 16:34:35
295	1	1	2	2018-04-02 01:36:49
296	60	1	1	2018-04-02 06:06:42
297	53	1	2	2018-04-01 10:34:30
298	57	1	4	2018-04-04 01:14:06
299	6	1	3	2018-04-09 08:31:24
300	56	1	2	2018-04-18 09:26:55
301	8	1	4	2018-04-03 17:13:25
302	59	1	1	2018-04-11 09:56:57
303	54	1	4	2018-04-15 06:05:05
304	55	1	3	2018-04-20 00:56:15
305	62	1	3	2018-04-17 02:28:04
306	32	1	3	2018-04-04 18:07:19
307	31	1	1	2018-04-19 21:39:21
308	36	1	1	2018-04-26 11:42:38
309	38	1	4	2018-04-12 09:32:31
310	37	1	4	2018-04-15 01:09:24
311	35	1	2	2018-04-20 04:40:03
312	34	1	3	2018-04-24 10:26:36
313	115	1	2	2018-04-27 18:25:40
314	33	1	5	2018-04-24 02:57:39
315	45	1	5	2018-04-02 04:41:20
316	49	1	4	2018-04-12 01:44:43
317	40	1	5	2018-04-10 00:16:31
318	61	1	1	2018-04-19 12:11:47
319	47	1	3	2018-04-09 04:31:43
320	51	1	3	2018-04-12 05:02:08
321	46	1	3	2018-04-19 20:52:31
322	42	1	2	2018-04-02 03:32:21
323	50	1	3	2018-04-09 08:33:17
324	41	1	3	2018-04-10 10:13:31
325	63	1	2	2018-04-12 04:18:38
326	64	1	2	2018-04-28 06:27:10
327	65	1	5	2018-04-03 12:07:43
328	43	1	1	2018-04-24 14:27:04
329	66	1	3	2018-04-20 16:04:41
330	67	1	2	2018-04-14 19:33:00
331	68	1	3	2018-04-27 05:40:33
332	69	1	5	2018-04-24 06:33:13
333	70	1	5	2018-04-24 01:14:30
334	71	1	1	2018-04-16 04:00:24
335	72	1	3	2018-04-06 16:58:31
336	73	1	2	2018-04-04 21:47:44
337	74	1	2	2018-04-29 04:41:31
338	75	1	3	2018-04-16 22:10:30
339	76	1	1	2018-04-08 08:09:44
340	77	1	5	2018-04-20 07:52:19
341	78	1	5	2018-04-28 08:44:00
342	79	1	1	2018-04-06 04:11:20
343	80	1	2	2018-04-18 20:22:28
344	81	1	4	2018-04-06 18:11:37
345	82	1	1	2018-04-24 11:31:31
347	84	1	5	2018-04-11 10:30:04
348	85	1	3	2018-04-03 21:26:09
349	86	1	1	2018-04-12 05:29:49
351	88	1	4	2018-04-26 12:08:16
352	7	1	4	2018-04-10 06:51:04
353	89	1	5	2018-04-16 19:14:19
354	52	1	5	2018-04-26 10:59:23
355	90	1	3	2018-04-24 13:15:19
356	91	1	5	2018-04-11 12:41:03
357	92	1	4	2018-04-19 06:22:38
358	93	1	2	2018-04-26 22:59:44
359	94	1	3	2018-04-20 10:29:40
360	95	1	3	2018-04-28 11:54:05
361	103	7	4	2018-04-19 13:04:36
362	107	7	4	2018-04-23 00:40:37
363	101	7	2	2018-04-14 22:49:12
364	105	7	5	2018-04-27 05:46:27
365	109	7	4	2018-04-15 06:44:35
366	102	7	3	2018-04-24 10:07:26
367	4	7	1	2018-04-14 08:56:19
368	99	7	5	2018-04-15 22:28:55
369	100	7	5	2018-04-29 22:40:28
370	104	7	4	2018-04-18 12:09:29
371	106	7	5	2018-04-21 06:55:14
372	110	7	5	2018-04-06 18:10:50
374	98	7	1	2018-04-04 19:21:28
375	2	7	4	2018-04-01 09:07:57
377	48	7	5	2018-04-01 04:43:37
378	3	7	4	2018-04-18 17:42:15
379	30	7	5	2018-04-09 16:58:20
380	58	7	1	2018-04-28 14:28:27
381	5	7	2	2018-04-18 09:21:06
382	44	7	5	2018-04-14 15:32:01
383	114	7	3	2018-04-10 03:12:37
384	39	7	1	2018-04-29 16:41:26
385	1	7	4	2018-04-26 18:49:50
386	60	7	4	2018-04-26 00:51:11
387	53	7	1	2018-04-05 06:24:57
388	57	7	3	2018-04-29 19:41:44
389	6	7	3	2018-04-15 10:47:56
390	56	7	5	2018-04-13 22:07:15
391	8	7	2	2018-04-21 02:39:00
392	59	7	3	2018-04-20 03:22:16
393	54	7	1	2018-04-30 05:45:57
394	55	7	4	2018-04-22 04:24:45
395	62	7	5	2018-04-08 23:12:39
396	32	7	4	2018-04-06 21:33:56
397	31	7	3	2018-04-08 15:59:31
398	36	7	4	2018-04-30 13:19:37
399	38	7	5	2018-04-29 16:57:48
400	37	7	2	2018-04-26 12:34:24
401	35	7	4	2018-04-25 08:52:21
402	34	7	2	2018-04-01 12:54:39
403	115	7	4	2018-04-11 12:11:07
404	33	7	1	2018-04-30 17:13:18
405	45	7	1	2018-04-28 20:13:43
406	49	7	3	2018-04-11 17:31:39
407	40	7	4	2018-04-13 03:29:58
408	61	7	5	2018-04-02 01:44:04
409	47	7	3	2018-04-19 17:08:31
410	51	7	4	2018-04-25 22:50:25
411	46	7	2	2018-04-04 07:41:56
412	42	7	4	2018-04-05 16:31:01
413	50	7	1	2018-04-25 00:13:45
414	41	7	1	2018-04-05 17:35:32
415	63	7	3	2018-04-18 22:12:42
416	64	7	2	2018-04-14 02:03:23
417	65	7	2	2018-04-27 17:15:48
418	43	7	5	2018-04-20 12:50:12
419	66	7	2	2018-04-21 03:14:34
420	67	7	5	2018-04-18 12:32:12
421	68	7	2	2018-04-21 18:50:15
422	69	7	3	2018-04-22 04:09:14
423	70	7	2	2018-04-13 15:27:05
424	71	7	1	2018-04-18 12:06:35
425	72	7	1	2018-04-02 20:44:50
426	73	7	4	2018-04-15 08:47:02
427	74	7	5	2018-04-04 15:33:50
428	75	7	1	2018-04-29 00:18:59
429	76	7	4	2018-04-03 07:06:14
430	77	7	1	2018-04-29 20:36:37
431	78	7	4	2018-04-01 20:47:34
432	79	7	2	2018-04-22 13:22:42
433	80	7	4	2018-04-05 00:28:39
434	81	7	2	2018-04-14 10:58:57
435	82	7	3	2018-04-19 22:01:03
437	84	7	4	2018-04-15 07:00:27
438	85	7	1	2018-04-13 15:34:09
439	86	7	2	2018-04-28 01:11:52
441	88	7	4	2018-04-04 00:14:00
442	7	7	1	2018-04-13 09:17:48
443	89	7	5	2018-04-15 18:36:14
444	52	7	5	2018-04-01 18:59:17
445	90	7	3	2018-04-22 22:10:57
446	91	7	5	2018-04-12 10:17:56
447	92	7	4	2018-04-17 07:25:53
448	93	7	1	2018-04-07 17:47:15
449	94	7	4	2018-04-04 08:31:38
450	95	7	4	2018-04-08 21:03:17
451	103	10	5	2018-04-10 23:18:00
452	107	10	2	2018-04-16 21:15:53
453	101	10	5	2018-04-04 10:11:59
454	105	10	4	2018-04-19 10:16:28
455	109	10	1	2018-04-13 03:48:06
456	102	10	3	2018-04-10 01:01:14
457	4	10	4	2018-04-01 17:21:37
458	99	10	5	2018-04-07 04:25:01
459	100	10	5	2018-04-11 13:18:56
460	104	10	4	2018-04-22 00:11:13
461	106	10	1	2018-04-04 03:40:08
462	110	10	4	2018-04-21 01:57:11
464	98	10	3	2018-04-25 23:12:59
465	2	10	5	2018-04-27 21:41:13
467	48	10	3	2018-04-04 19:40:09
468	3	10	1	2018-04-15 03:11:39
469	30	10	5	2018-04-29 10:17:43
470	58	10	5	2018-04-06 04:58:10
471	5	10	3	2018-04-12 21:43:13
436	83	7	5	2018-04-09 14:53:47
440	87	7	4	2018-04-23 17:04:30
472	44	10	5	2018-04-03 06:25:42
473	114	10	4	2018-04-24 15:14:59
474	39	10	1	2018-04-23 09:45:12
475	1	10	2	2018-04-26 22:50:26
476	60	10	1	2018-04-21 11:56:29
477	53	10	1	2018-04-10 08:17:38
478	57	10	2	2018-04-29 23:27:39
479	6	10	5	2018-04-05 23:35:26
480	56	10	5	2018-04-13 07:37:01
481	8	10	1	2018-04-01 19:47:02
482	59	10	1	2018-04-24 21:23:27
483	54	10	2	2018-04-15 05:13:25
484	55	10	2	2018-04-12 09:46:46
485	62	10	1	2018-04-28 06:15:22
486	32	10	3	2018-04-28 02:56:35
487	31	10	2	2018-04-16 21:29:29
488	36	10	4	2018-04-09 13:29:10
489	38	10	2	2018-04-12 20:02:20
490	37	10	1	2018-04-29 00:27:46
491	35	10	1	2018-04-13 00:21:37
492	34	10	2	2018-04-21 10:23:12
493	115	10	4	2018-04-13 21:33:55
494	33	10	3	2018-04-02 11:22:03
495	45	10	5	2018-04-13 12:13:41
496	49	10	1	2018-04-16 11:08:27
497	40	10	5	2018-04-29 04:53:13
498	61	10	5	2018-04-22 22:44:52
499	47	10	2	2018-04-12 05:49:01
500	51	10	5	2018-04-07 14:50:28
501	46	10	1	2018-04-19 12:17:52
502	42	10	3	2018-04-21 06:56:55
503	50	10	4	2018-04-01 07:15:58
504	41	10	3	2018-04-25 02:31:26
505	63	10	3	2018-04-08 08:57:59
506	64	10	2	2018-04-17 17:58:58
507	65	10	3	2018-04-27 22:28:14
508	43	10	5	2018-04-01 05:48:17
509	66	10	2	2018-04-21 11:41:09
510	67	10	2	2018-04-02 06:50:43
511	68	10	5	2018-04-07 21:51:01
512	69	10	2	2018-04-21 19:16:20
513	70	10	1	2018-04-03 17:31:26
514	71	10	2	2018-04-11 08:11:11
515	72	10	1	2018-04-10 05:27:34
516	73	10	3	2018-04-05 11:18:36
517	74	10	2	2018-04-06 13:09:07
518	75	10	3	2018-04-12 00:57:57
519	76	10	4	2018-04-29 06:01:02
520	77	10	1	2018-04-17 14:17:19
521	78	10	1	2018-04-30 18:42:57
522	79	10	1	2018-04-05 01:09:58
523	80	10	5	2018-04-10 07:53:32
524	81	10	3	2018-04-27 19:48:25
525	82	10	3	2018-04-22 16:35:57
527	84	10	5	2018-04-25 09:10:46
528	85	10	1	2018-04-08 17:36:41
529	86	10	5	2018-04-28 09:49:04
530	87	10	5	2018-04-08 02:21:24
531	88	10	1	2018-04-05 16:19:12
532	7	10	5	2018-04-10 19:29:02
533	89	10	1	2018-04-29 12:16:28
534	52	10	1	2018-04-09 23:00:27
535	90	10	3	2018-04-17 13:41:01
536	91	10	4	2018-04-01 17:36:07
537	92	10	1	2018-05-01 01:24:17
538	93	10	4	2018-04-17 02:49:18
539	94	10	2	2018-04-02 21:37:37
540	95	10	5	2018-04-05 14:21:48
541	103	8	2	2018-04-02 06:38:34
542	107	8	1	2018-04-27 16:52:47
543	101	8	3	2018-04-09 07:30:05
544	105	8	2	2018-04-24 15:29:01
545	109	8	4	2018-04-09 03:29:19
546	102	8	5	2018-04-13 06:07:31
547	4	8	1	2018-04-18 16:31:09
548	99	8	2	2018-04-11 14:34:58
549	100	8	2	2018-04-21 06:50:49
550	104	8	5	2018-04-05 05:30:43
551	106	8	1	2018-04-12 13:39:28
552	110	8	1	2018-04-02 09:09:38
554	98	8	3	2018-04-02 22:01:18
555	2	8	3	2018-04-09 09:33:41
557	48	8	1	2018-04-01 10:47:21
558	3	8	4	2018-04-20 05:56:34
559	30	8	1	2018-04-03 23:46:01
560	58	8	4	2018-04-12 13:03:21
561	5	8	4	2018-04-10 03:39:24
562	44	8	3	2018-04-13 21:45:58
563	114	8	2	2018-04-27 18:12:59
564	39	8	2	2018-04-29 12:05:39
565	1	8	4	2018-04-05 09:16:12
566	60	8	5	2018-04-21 03:15:30
567	53	8	5	2018-04-14 10:26:21
568	57	8	4	2018-04-05 18:30:29
569	6	8	1	2018-04-08 07:37:05
570	56	8	2	2018-04-24 09:25:27
571	8	8	1	2018-04-30 11:20:55
572	59	8	2	2018-04-28 04:21:36
573	54	8	4	2018-04-15 03:55:06
574	55	8	2	2018-04-20 10:15:21
575	62	8	5	2018-04-22 19:02:21
576	32	8	4	2018-04-22 14:53:57
577	31	8	5	2018-04-22 04:22:31
578	36	8	3	2018-04-02 06:52:39
579	38	8	5	2018-04-29 23:31:51
580	37	8	4	2018-04-15 14:08:06
581	35	8	1	2018-04-22 01:12:34
582	34	8	3	2018-04-04 05:55:30
583	115	8	1	2018-04-14 16:02:12
584	33	8	2	2018-04-08 17:03:14
585	45	8	1	2018-04-03 15:20:36
586	49	8	4	2018-04-07 08:06:25
587	40	8	4	2018-04-03 17:49:38
588	61	8	2	2018-04-03 19:01:54
589	47	8	2	2018-04-09 22:56:04
590	51	8	5	2018-04-13 02:38:54
591	46	8	1	2018-04-11 04:00:11
592	42	8	2	2018-04-08 07:08:58
593	50	8	3	2018-04-09 20:40:51
594	41	8	5	2018-04-18 14:59:12
595	63	8	3	2018-04-03 03:36:33
596	64	8	2	2018-04-26 20:05:38
597	65	8	2	2018-04-29 20:39:35
598	43	8	1	2018-04-30 12:37:06
599	66	8	3	2018-04-22 00:47:26
600	67	8	2	2018-04-25 17:33:58
601	68	8	2	2018-04-24 17:12:43
602	69	8	2	2018-04-25 06:12:09
603	70	8	4	2018-04-15 12:09:15
604	71	8	1	2018-04-06 10:56:42
605	72	8	3	2018-04-22 05:27:57
606	73	8	3	2018-04-13 14:44:59
607	74	8	2	2018-04-08 13:48:26
608	75	8	5	2018-04-02 13:18:22
609	76	8	1	2018-04-15 13:21:49
610	77	8	4	2018-04-26 18:39:58
611	78	8	2	2018-04-26 06:58:44
612	79	8	3	2018-04-03 03:26:42
613	80	8	1	2018-04-13 16:40:50
614	81	8	1	2018-04-02 16:39:09
615	82	8	5	2018-04-06 05:12:23
617	84	8	1	2018-04-13 19:28:14
618	85	8	4	2018-04-02 19:27:45
619	86	8	1	2018-04-27 23:52:43
621	88	8	3	2018-04-28 07:48:20
622	7	8	5	2018-04-26 15:27:19
623	89	8	2	2018-04-22 01:35:26
624	52	8	5	2018-04-01 18:28:42
625	90	8	1	2018-04-04 19:37:52
626	91	8	5	2018-04-15 19:12:06
627	92	8	3	2018-04-01 12:42:46
628	93	8	4	2018-04-23 12:55:17
526	83	10	5	2018-04-13 09:52:44
629	94	8	3	2018-04-29 19:08:24
630	95	8	2	2018-04-28 05:01:34
631	103	2	1	2018-04-06 06:29:52
632	107	2	4	2018-04-26 06:11:16
633	101	2	1	2018-04-29 21:22:27
634	105	2	3	2018-04-03 00:16:25
635	109	2	4	2018-04-20 10:35:41
636	102	2	4	2018-04-25 00:54:05
637	4	2	2	2018-04-17 03:53:15
638	99	2	2	2018-04-10 10:05:19
639	100	2	2	2018-04-29 00:41:12
640	104	2	5	2018-04-07 22:09:52
641	106	2	4	2018-04-27 08:06:54
642	110	2	5	2018-04-27 12:11:41
644	98	2	2	2018-04-02 22:18:24
645	2	2	1	2018-04-18 01:38:07
647	48	2	1	2018-04-16 05:43:42
648	3	2	1	2018-04-19 13:13:00
649	30	2	5	2018-04-17 20:48:31
650	58	2	5	2018-04-07 20:31:52
651	5	2	3	2018-04-08 12:34:36
652	44	2	4	2018-04-13 07:02:07
653	114	2	3	2018-04-14 00:57:18
654	39	2	5	2018-04-13 23:52:32
655	1	2	4	2018-04-18 03:52:28
656	60	2	4	2018-04-13 07:30:20
657	53	2	3	2018-04-14 23:49:31
658	57	2	2	2018-04-10 00:13:23
659	6	2	5	2018-04-25 06:34:57
660	56	2	4	2018-04-02 04:37:44
661	8	2	2	2018-04-27 08:51:08
662	59	2	5	2018-04-18 16:17:55
663	54	2	2	2018-04-10 03:58:01
664	55	2	1	2018-04-18 02:07:44
665	62	2	3	2018-04-25 11:23:18
666	32	2	4	2018-04-02 12:51:15
667	31	2	3	2018-04-10 17:26:37
668	36	2	5	2018-04-24 12:44:51
669	38	2	2	2018-04-10 05:51:11
670	37	2	2	2018-04-29 12:27:40
671	35	2	3	2018-04-04 16:10:19
672	34	2	3	2018-04-01 10:35:32
673	115	2	1	2018-04-04 08:13:30
674	33	2	4	2018-04-02 21:33:03
675	45	2	5	2018-04-14 00:17:30
676	49	2	4	2018-04-16 01:07:15
677	40	2	2	2018-04-13 20:12:41
678	61	2	4	2018-04-01 18:06:56
679	47	2	4	2018-04-14 11:43:46
680	51	2	3	2018-04-01 16:09:53
681	46	2	5	2018-04-05 12:39:44
682	42	2	4	2018-04-17 18:29:09
683	50	2	2	2018-04-07 21:53:20
684	41	2	1	2018-04-22 21:56:30
685	63	2	3	2018-04-26 12:00:45
686	64	2	1	2018-04-28 11:03:07
687	65	2	1	2018-04-26 20:28:32
688	43	2	1	2018-04-05 09:38:09
689	66	2	4	2018-04-15 03:22:11
690	67	2	2	2018-04-07 16:15:58
691	68	2	4	2018-05-01 02:34:02
692	69	2	5	2018-04-24 04:18:50
693	70	2	3	2018-04-10 19:24:08
694	71	2	2	2018-04-14 13:28:26
695	72	2	2	2018-04-23 01:12:30
696	73	2	1	2018-04-21 04:01:54
697	74	2	3	2018-04-23 08:26:33
698	75	2	2	2018-04-12 14:37:37
699	76	2	4	2018-04-11 08:57:40
700	77	2	3	2018-04-30 20:21:19
701	78	2	5	2018-04-26 08:07:51
702	79	2	5	2018-04-02 12:56:05
703	80	2	3	2018-04-02 19:26:12
704	81	2	5	2018-04-28 03:52:43
705	82	2	5	2018-04-22 04:37:17
707	84	2	5	2018-04-13 11:00:21
708	85	2	4	2018-04-15 10:05:17
709	86	2	1	2018-04-05 00:27:16
710	87	2	5	2018-04-19 02:45:16
711	88	2	5	2018-04-16 21:35:42
712	7	2	5	2018-04-11 20:38:07
713	89	2	1	2018-04-26 17:01:28
714	52	2	3	2018-04-05 19:38:38
715	90	2	2	2018-04-15 00:56:11
716	91	2	1	2018-04-13 21:29:49
717	92	2	2	2018-04-20 19:59:25
718	93	2	1	2018-04-08 07:29:51
719	94	2	5	2018-04-01 23:08:16
720	95	2	2	2018-04-19 08:08:16
721	103	12	4	2018-04-22 16:10:51
722	107	12	3	2018-04-19 21:14:47
723	101	12	3	2018-04-07 08:57:15
724	105	12	3	2018-04-26 23:16:01
725	109	12	1	2018-04-27 03:54:16
726	102	12	3	2018-04-25 05:00:57
727	4	12	3	2018-04-03 23:39:54
728	99	12	5	2018-04-09 10:28:52
729	100	12	3	2018-04-27 10:25:56
730	104	12	1	2018-04-14 15:27:48
731	106	12	2	2018-04-03 02:58:12
732	110	12	1	2018-04-08 14:31:50
734	98	12	2	2018-04-19 03:29:08
735	2	12	3	2018-04-12 14:21:34
737	48	12	2	2018-04-08 19:22:21
738	3	12	1	2018-04-27 00:55:16
739	30	12	5	2018-04-29 00:30:58
740	58	12	2	2018-04-02 12:52:56
741	5	12	2	2018-04-10 15:50:43
742	44	12	5	2018-04-08 16:07:33
743	114	12	1	2018-04-27 10:54:34
744	39	12	5	2018-04-12 14:21:10
745	1	12	4	2018-04-24 09:11:40
746	60	12	5	2018-04-22 03:30:07
747	53	12	5	2018-04-13 09:03:02
748	57	12	1	2018-04-02 03:52:32
749	6	12	4	2018-04-25 17:27:37
750	56	12	3	2018-04-11 13:38:51
751	8	12	1	2018-04-28 19:30:06
752	59	12	3	2018-04-18 11:19:34
753	54	12	2	2018-04-01 18:19:46
754	55	12	3	2018-04-03 09:23:23
755	62	12	2	2018-04-25 05:06:33
756	32	12	1	2018-04-06 20:46:49
757	31	12	2	2018-04-06 20:47:58
758	36	12	2	2018-04-15 20:45:25
759	38	12	2	2018-04-15 11:40:05
760	37	12	3	2018-04-03 03:34:16
761	35	12	4	2018-04-30 18:29:19
762	34	12	2	2018-04-21 02:56:34
763	115	12	2	2018-04-19 04:41:56
764	33	12	3	2018-04-24 21:22:00
765	45	12	2	2018-04-30 04:08:07
766	49	12	1	2018-04-15 23:24:26
767	40	12	3	2018-04-13 16:46:07
768	61	12	3	2018-04-13 20:25:42
769	47	12	2	2018-04-24 03:35:56
770	51	12	2	2018-04-20 08:13:44
771	46	12	2	2018-04-20 01:14:09
772	42	12	2	2018-04-11 15:53:47
773	50	12	1	2018-04-11 12:18:25
774	41	12	5	2018-04-21 09:37:15
775	63	12	5	2018-04-06 08:51:42
776	64	12	5	2018-04-06 06:03:32
777	65	12	3	2018-04-15 08:09:24
778	43	12	4	2018-04-18 02:46:46
779	66	12	2	2018-04-13 15:03:52
780	67	12	2	2018-04-17 15:49:30
781	68	12	5	2018-04-26 17:36:30
782	69	12	2	2018-04-08 07:22:50
783	70	12	4	2018-04-02 15:12:37
784	71	12	4	2018-04-26 02:01:41
785	72	12	2	2018-04-15 20:15:24
786	73	12	4	2018-04-10 04:36:13
787	74	12	1	2018-04-10 03:51:11
788	75	12	2	2018-04-12 08:31:55
789	76	12	2	2018-04-05 19:51:16
790	77	12	4	2018-04-22 11:39:06
791	78	12	2	2018-04-05 18:02:28
792	79	12	4	2018-04-07 08:17:35
793	80	12	4	2018-04-26 05:03:08
794	81	12	5	2018-04-19 22:53:11
795	82	12	3	2018-04-09 20:40:41
797	84	12	2	2018-04-12 12:07:37
798	85	12	1	2018-04-14 22:32:06
799	86	12	3	2018-04-04 09:00:11
801	88	12	1	2018-04-12 12:55:26
802	7	12	2	2018-04-28 12:17:39
803	89	12	5	2018-04-13 01:02:09
804	52	12	2	2018-04-01 10:30:50
805	90	12	1	2018-04-16 05:19:17
806	91	12	4	2018-04-07 01:54:58
807	92	12	2	2018-04-30 23:41:28
808	93	12	4	2018-04-12 10:45:50
809	94	12	4	2018-04-22 05:04:37
810	95	12	3	2018-04-24 03:42:27
811	103	9	4	2018-04-08 03:43:47
812	107	9	1	2018-04-05 21:02:23
813	101	9	4	2018-04-15 05:02:50
814	105	9	1	2018-04-14 00:26:59
815	109	9	4	2018-04-11 02:25:58
816	102	9	1	2018-04-08 20:17:51
817	4	9	4	2018-04-15 07:04:20
818	99	9	2	2018-04-06 14:14:32
819	100	9	2	2018-04-07 10:21:21
820	104	9	1	2018-04-09 00:57:04
821	106	9	3	2018-05-01 03:52:17
822	110	9	1	2018-04-23 15:23:43
824	98	9	3	2018-04-17 17:57:39
825	2	9	4	2018-04-01 04:33:08
827	48	9	3	2018-04-26 14:24:35
828	3	9	4	2018-04-23 21:43:57
829	30	9	3	2018-04-13 09:08:22
830	58	9	2	2018-04-09 08:23:26
831	5	9	3	2018-04-29 08:11:36
832	44	9	2	2018-04-30 22:59:43
833	114	9	3	2018-04-22 13:43:33
834	39	9	2	2018-04-04 19:55:51
835	1	9	4	2018-04-06 20:18:01
836	60	9	3	2018-04-16 05:21:01
837	53	9	5	2018-04-20 17:33:51
838	57	9	3	2018-04-22 01:14:16
839	6	9	2	2018-04-08 00:52:55
840	56	9	5	2018-04-06 02:03:50
841	8	9	1	2018-04-20 12:28:48
842	59	9	2	2018-04-27 08:43:06
843	54	9	2	2018-04-19 03:35:28
844	55	9	2	2018-04-01 13:28:45
845	62	9	3	2018-04-15 15:03:27
846	32	9	4	2018-04-28 23:51:16
847	31	9	4	2018-04-02 03:31:59
848	36	9	1	2018-04-13 14:11:43
849	38	9	5	2018-04-27 11:38:12
850	37	9	2	2018-04-12 22:25:09
851	35	9	4	2018-04-05 00:20:06
852	34	9	4	2018-04-22 20:17:46
853	115	9	1	2018-04-28 13:15:47
854	33	9	2	2018-04-14 03:41:36
855	45	9	4	2018-04-16 05:54:48
856	49	9	5	2018-04-22 23:04:44
857	40	9	1	2018-04-20 07:24:09
858	61	9	4	2018-04-28 08:50:51
859	47	9	4	2018-04-05 08:41:35
860	51	9	3	2018-04-18 19:39:49
861	46	9	2	2018-04-19 02:02:52
862	42	9	5	2018-04-07 19:14:51
863	50	9	1	2018-04-25 09:22:01
864	41	9	1	2018-04-08 14:39:57
865	63	9	3	2018-04-26 03:02:42
866	64	9	3	2018-04-06 06:10:39
867	65	9	1	2018-04-16 13:37:20
868	43	9	4	2018-04-16 20:24:11
869	66	9	4	2018-04-08 11:28:27
870	67	9	2	2018-05-01 03:03:28
871	68	9	5	2018-04-05 02:39:31
872	69	9	5	2018-04-09 17:06:05
873	70	9	4	2018-04-06 10:39:32
874	71	9	5	2018-04-02 09:37:29
875	72	9	1	2018-04-12 19:11:06
876	73	9	4	2018-04-14 17:16:08
877	74	9	4	2018-04-06 08:39:24
878	75	9	4	2018-04-06 20:09:31
879	76	9	3	2018-04-11 04:06:33
880	77	9	4	2018-04-27 23:19:05
881	78	9	4	2018-04-20 13:35:50
882	79	9	2	2018-04-12 01:02:17
883	80	9	5	2018-04-14 22:13:27
884	81	9	3	2018-04-12 04:00:59
885	82	9	5	2018-04-20 04:52:20
887	84	9	4	2018-04-16 17:27:44
888	85	9	1	2018-04-07 21:17:09
889	86	9	4	2018-04-27 22:43:06
890	87	9	4	2018-04-15 03:03:59
891	88	9	1	2018-04-22 17:42:10
892	7	9	2	2018-04-05 09:30:56
893	89	9	4	2018-04-20 11:41:08
894	52	9	2	2018-04-01 05:06:12
895	90	9	1	2018-04-29 10:01:59
896	91	9	2	2018-04-27 21:29:58
897	92	9	3	2018-04-12 18:29:34
898	93	9	2	2018-04-07 19:41:58
899	94	9	5	2018-04-15 12:46:57
900	95	9	2	2018-04-29 23:26:31
901	103	3	2	2018-04-11 21:40:21
902	107	3	3	2018-04-04 16:24:56
903	101	3	1	2018-04-06 19:34:25
904	105	3	1	2018-04-02 13:22:34
905	109	3	5	2018-04-27 13:15:30
906	102	3	3	2018-04-08 12:53:12
907	4	3	4	2018-04-18 11:23:25
908	99	3	2	2018-04-19 00:12:45
909	100	3	2	2018-04-09 07:21:46
910	104	3	3	2018-04-20 08:51:46
911	106	3	1	2018-04-20 03:41:34
912	110	3	2	2018-04-26 19:15:41
914	98	3	4	2018-04-30 17:15:22
915	2	3	2	2018-04-14 20:26:35
917	48	3	2	2018-04-25 13:50:42
918	3	3	1	2018-04-14 20:48:32
919	30	3	2	2018-04-14 23:34:32
920	58	3	1	2018-04-21 00:09:02
921	5	3	3	2018-04-22 21:04:49
922	44	3	3	2018-04-05 06:00:13
923	114	3	2	2018-04-27 10:45:55
924	39	3	4	2018-04-24 08:54:00
925	1	3	2	2018-04-26 12:04:45
926	60	3	2	2018-04-21 03:14:14
927	53	3	2	2018-04-11 12:24:25
928	57	3	1	2018-04-08 04:34:17
929	6	3	3	2018-04-29 18:34:16
930	56	3	2	2018-04-14 17:53:07
931	8	3	3	2018-05-01 03:35:51
932	59	3	5	2018-04-02 11:55:58
933	54	3	1	2018-04-13 22:38:46
934	55	3	2	2018-04-02 08:58:39
935	62	3	1	2018-04-11 09:50:28
936	32	3	1	2018-04-30 07:53:24
937	31	3	5	2018-04-25 09:23:52
938	36	3	3	2018-04-28 18:21:58
939	38	3	4	2018-04-05 11:47:13
940	37	3	2	2018-04-07 07:36:04
941	35	3	3	2018-04-06 16:59:52
942	34	3	5	2018-04-16 06:06:34
796	83	12	5	2018-04-11 10:04:17
943	115	3	5	2018-04-05 10:39:37
944	33	3	1	2018-04-01 19:50:55
945	45	3	5	2018-04-15 06:16:23
946	49	3	3	2018-04-12 22:37:51
947	40	3	2	2018-04-11 13:56:39
948	61	3	1	2018-04-20 16:36:51
949	47	3	4	2018-04-15 14:39:59
950	51	3	3	2018-04-20 23:27:30
951	46	3	3	2018-04-02 15:49:42
952	42	3	2	2018-04-19 10:31:46
953	50	3	3	2018-04-06 03:11:30
954	41	3	5	2018-04-25 20:56:37
955	63	3	5	2018-04-13 02:23:46
956	64	3	5	2018-04-11 15:21:33
957	65	3	5	2018-04-06 13:15:07
958	43	3	1	2018-04-05 12:08:31
959	66	3	1	2018-04-10 21:11:41
960	67	3	3	2018-04-24 23:35:03
961	68	3	1	2018-04-30 22:53:36
962	69	3	3	2018-04-11 11:00:26
963	70	3	1	2018-04-18 05:20:12
964	71	3	3	2018-04-09 01:57:02
965	72	3	2	2018-04-09 23:12:32
966	73	3	1	2018-04-27 23:24:50
967	74	3	1	2018-04-09 11:32:35
968	75	3	4	2018-04-08 15:44:15
969	76	3	2	2018-04-27 17:15:37
970	77	3	1	2018-04-25 02:30:26
971	78	3	5	2018-04-03 09:57:32
972	79	3	5	2018-04-28 19:21:34
973	80	3	5	2018-04-20 05:29:00
974	81	3	4	2018-04-22 20:37:39
975	82	3	2	2018-04-21 00:24:44
976	83	3	5	2018-04-14 10:46:44
977	84	3	2	2018-04-16 02:27:14
978	85	3	2	2018-04-06 09:22:44
979	86	3	1	2018-04-14 05:14:16
980	87	3	3	2018-04-18 22:15:49
981	88	3	1	2018-04-22 17:24:02
982	7	3	1	2018-04-02 07:08:28
983	89	3	3	2018-04-11 07:22:17
984	52	3	4	2018-04-22 12:56:35
985	90	3	2	2018-04-07 12:22:01
986	91	3	1	2018-04-25 01:50:51
987	92	3	2	2018-04-03 16:36:20
988	93	3	3	2018-04-13 00:21:57
989	94	3	5	2018-04-05 22:55:35
990	95	3	5	2018-04-09 19:51:15
886	83	9	5	2018-04-05 05:52:19
346	83	1	5	2018-04-23 19:49:50
616	83	8	5	2018-04-08 03:53:23
706	83	2	5	2018-04-01 07:29:16
76	83	4	5	2018-04-13 05:28:23
166	83	5	5	2018-04-11 09:56:31
620	87	8	5	2018-04-06 06:02:49
800	87	12	5	2018-04-28 20:31:00
350	87	1	5	2018-04-02 22:08:17
992	123	5	4	2018-05-12 17:58:33.638919
\.


--
-- Data for Name: rt_songs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_songs (id, ordering, is_active, created_at, updated_at, slug) FROM stdin;
116	1	t	2018-05-09 15:04:06.253869	2018-05-17 05:05:51	crazy-in-love
30	3	f	2015-12-06 16:52:20	2018-05-17 05:06:03	perdname
7	88	t	2015-12-19 10:57:46	2018-05-17 04:33:23	cant-hep-falling-in-love
5	4	t	2015-12-27 04:59:32	2018-05-17 04:44:07	thats-life
115	32	t	2018-04-14 14:13:16.313479	2018-05-17 04:41:30	w
8	7	f	2015-11-25 15:55:13	2018-05-17 04:43:15	always-on-my-mind
1	5	t	2015-11-19 09:53:40	2018-05-17 04:43:52	summertime-high-quality-remastered
4	1	f	2016-01-15 11:20:27	2018-05-17 05:04:41	rain-and-tears
3	3	t	2015-11-22 20:15:26	2018-05-17 05:05:17	forget-domani
2	2	f	2015-12-03 16:17:28	2018-05-17 05:05:40	mack-the-knife-in-berlin
6	6	t	2015-12-12 21:21:12	2018-05-17 05:06:11	the-dozens
42	45	t	2015-11-28 20:51:48	2018-05-17 05:06:54	angel-eyes
43	65	f	2015-12-23 10:18:20	2018-05-17 04:37:56	its-a-lonesome-old-town
33	34	t	2015-12-23 18:43:32	2018-05-17 04:39:14	my-only-fascination
40	34	t	2015-12-24 04:37:23	2018-05-17 04:41:17	sailin-home
41	54	t	2015-11-20 06:54:24	2018-05-17 04:38:24	only-the-lonely
35	23	t	2015-12-17 08:41:30	2018-05-17 04:42:22	when-forever-has-gone
39	5	t	2016-01-10 15:44:22	2018-05-17 04:43:46	happy-to-be-on-an-island-in-the-sun
38	23	t	2015-11-24 23:58:36	2018-05-17 04:42:00	someday-somewhere
32	23	t	2015-12-05 22:34:16	2018-05-17 04:42:15	souvenirs
37	23	t	2015-12-18 01:12:39	2018-05-17 04:42:19	lost-in-love
114	4	t	2018-04-11 14:27:16.859596	2018-05-17 04:44:02	4
99	1	t	2018-04-09 07:24:22.476376	2018-05-17 05:07:01	1-8
105	1	t	2018-04-09 07:55:14.138668	2018-05-17 05:04:46	1
34	32	t	2015-12-08 15:03:29	2018-05-17 05:06:35	goodbye-my-love-goodbye
52	89	t	2015-12-06 23:15:46	2018-05-17 04:33:18	a-boy-like-me-a-girl-like-you
68	68	t	2015-12-23 05:35:44	2018-05-17 04:37:43	carmen
50	45	t	2015-12-30 19:08:25	2018-05-17 04:38:32	spring-is-here
47	43	t	2015-11-30 00:16:45	2018-05-17 04:38:54	one-for-my-baby-and-one-more-for-the-road
49	34	t	2015-11-17 22:00:22	2018-05-17 04:39:07	where-or-when
56	7	t	2015-12-06 10:04:38	2018-05-17 04:43:11	amazing-grace
55	8	t	2015-12-28 09:24:26	2018-05-17 04:43:07	america-the-beautiful
53	6	t	2016-01-07 09:32:17	2018-05-17 04:43:30	a-cane-and-a-high-starched-collar
100	1	t	2018-04-09 07:29:39.060919	2018-05-17 05:07:23	1-9
106	1	t	2018-04-09 07:58:07.650547	2018-05-17 05:04:22	1-5
117	2	f	2018-05-09 15:05:39.185359	2018-05-17 05:05:13	baby-boy
44	4	t	2016-01-09 12:20:36	2018-05-17 05:09:34	willow-weep-for-me
48	3	t	2015-11-25 19:09:37	2018-05-17 05:09:41	sleep-warm
67	67	t	2015-11-17 16:52:03	2018-05-17 04:37:46	the-starlit-hour
66	66	t	2015-12-22 07:43:15	2018-05-17 04:37:50	sugar-blues
64	64	t	2015-11-29 06:28:41	2018-05-17 04:38:01	a-tisket-a-tasket
63	63	t	2015-11-19 09:17:50	2018-05-17 04:38:05	undecided
61	34	t	2016-01-12 20:49:26	2018-05-17 04:39:23	jailhouse-rock
60	6	t	2016-01-09 03:23:32	2018-05-17 04:43:39	crying-in-the-chapel
57	6	t	2015-12-15 00:58:38	2018-05-17 04:43:42	and-i-love-you-so
59	8	t	2016-01-13 17:00:38	2018-05-17 05:08:29	blue-christmas
58	3	t	2015-12-06 02:02:23	2018-05-17 05:08:44	are-you-lonesome-tonight
101	1	t	2018-04-09 07:30:38.688732	2018-05-17 05:04:19	1-6
107	1	t	2018-04-09 07:59:47.900032	2018-05-17 05:04:36	1-1
65	65	t	2015-12-07 09:19:21	2018-05-17 05:08:09	dream-a-little-dream-of-me
118	3	t	2018-05-09 15:06:47.973412	2018-05-17 05:09:50	single-ladies-put-a-ring-on-it
71	71	t	2016-01-03 02:44:50	2018-05-17 04:37:27	mussa
45	34	t	2015-12-07 10:14:46	2018-05-17 04:39:18	guess-ill-hang-my-tears-out-to-dry
31	23	f	2015-12-21 02:47:08	2018-05-17 04:42:04	cant-say-how-much-i-loveyou
102	1	t	2018-04-09 07:35:07.136941	2018-05-17 05:04:32	1-2
62	8	t	2015-11-30 23:05:51	2018-05-17 04:43:03	return-to-sender
73	73	t	2015-12-23 18:15:22	2018-05-17 04:37:20	winter-again
72	72	t	2015-12-10 04:31:54	2018-05-17 04:37:23	youve-gone
70	70	t	2015-12-10 04:54:54	2018-05-17 04:37:31	ill-come
69	69	t	2015-12-31 08:10:03	2018-05-17 04:37:34	car
51	45	t	2015-11-18 09:44:38	2018-05-17 04:38:43	a-big-hunk-o-love
46	45	t	2016-01-16 02:22:54	2018-05-17 04:38:48	blues-in-the-night
119	1	t	2018-05-09 15:09:59.377932	2018-05-17 05:04:26	bring-me-to-life
124	22	t	2018-05-15 14:14:49.61213	2018-05-17 04:31:14	zz
89	89	t	2018-04-04 10:05:51	2018-05-17 04:31:39	the-back-seat-of-my-car
103	1	t	2018-04-09 07:36:20.847665	2018-05-17 05:03:51	1-3
76	76	t	2018-04-04 09:54:21	2018-05-17 04:37:08	cocaine
75	75	t	2018-04-04 09:53:13	2018-05-17 04:37:11	badge
74	74	t	2018-04-04 09:52:41	2018-05-17 04:37:15	motherless-children
36	23	t	2015-11-29 13:16:59	2018-05-17 04:42:11	come-waltz-with-me
123	43	t	2018-05-12 17:26:19.213576	2018-05-17 05:07:30	newnew
109	1	t	2018-04-09 08:04:59.510853	2018-05-17 05:07:50	1-4
120	2	f	2018-05-09 15:10:35.251087	2018-05-17 05:09:05	going-under
126	11	t	2018-05-17 08:28:05.752724	2018-05-17 05:28:26	a-s-c-d-fsda-f-dsfzzzzzzzzzz-a-a-a-a-a-a
88	88	t	2018-04-04 10:04:43	2018-05-17 04:33:31	magneto-and-titanium-man
87	87	t	2018-04-04 10:03:53	2018-05-17 04:33:59	say-say-say
86	86	t	2018-04-04 10:02:23	2018-05-17 04:34:06	fourfiveseconds
83	83	t	2018-04-04 09:59:01	2018-05-17 04:36:38	jungleland
82	82	t	2018-04-04 09:58:58	2018-05-17 04:36:43	im-goin-down
81	81	t	2018-04-04 09:57:37	2018-05-17 04:36:47	darkness-on-the-edge-of-town
77	77	t	2018-04-04 09:54:59	2018-05-17 04:37:04	bell-bottom-blues
98	1	f	2018-04-09 07:20:14.746105	2018-05-17 05:04:49	1-7
84	84	t	2018-04-04 10:02:23	2018-05-17 04:36:30	hope-of-deliverance
79	79	t	2018-04-04 09:55:59	2018-05-17 04:36:57	thunder-road
78	78	t	2018-04-04 09:55:20	2018-05-17 04:37:01	i-shot-the-sheriff
121	4	t	2018-05-09 15:11:33.704545	2018-05-17 05:06:24	my-immortal
104	1	t	2018-04-09 07:36:44.375956	2018-05-17 05:07:44	1-10
110	1	t	2018-04-09 09:51:22.721695	2018-05-17 05:08:02	1-11
95	95	t	2018-04-08 14:51:38.628233	2018-05-17 04:29:09	surrender
94	94	t	2018-04-08 14:37:18.080744	2018-05-17 04:29:42	surrender-1
93	93	f	2018-04-08 14:34:56.84118	2018-05-17 04:31:23	hello
91	91	t	2018-04-08 14:12:54.70912	2018-05-17 04:31:28	hello-1
90	90	t	2018-04-04 10:06:21	2018-05-17 04:31:34	ive-had-enough
85	85	t	2018-04-04 10:02:57	2018-05-17 04:34:19	heart-of-the-country
54	8	t	2015-11-17 19:12:39	2018-05-17 04:42:42	elvis-presley-lyrics-elvis-a-z-songdatabase
80	80	t	2018-04-04 09:56:23	2018-05-17 04:36:51	the-river
122	4	t	2018-05-09 15:12:30.832712	2018-05-17 04:43:59	lithium
92	92	f	2018-04-08 14:31:30.292548	2018-05-17 05:08:58	hello-2
\.


--
-- Data for Name: rt_tours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_tours (id, artist_id, tour_name, start_date, end_date, ordering, color, description, created) FROM stdin;
1	5	Small Tour of Vopli Vidopliassova 	2018-12-11	2015-12-22	1	grey	Small Tour of Vopli Vidopliassova during 10 days 	2016-01-16 05:10:55
2	5	Big Tour of Vopli Vidopliassova 	2018-05-12	2016-02-02	2	blue	Big Tour of Vopli Vidopliassova during 20 days 	2015-12-12 11:57:03
\.


--
-- Data for Name: rt_users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_users (id, username, email, password, status, first_name, last_name, phone, website, remember_token, created_at, updated_at, avatar, provider_name, provider_id, verified, verification_token) FROM stdin;
4	adam_lang	adam_lang@site.com	$2y$10$9NsPiH3AEJF2pPXQ.syrquGfsFXqH7JmhfwvWlbboB2BGuU81w2Pm	I	Adam	Lang	\N	\N	\N	2018-03-16 17:38:30	\N	\N	EMAIL	\N	f	\N
2	rod_bodrick	rod_bodrick@site.com	$2y$10$o2k7WaYWM6Oe4MToJx/uk.DF9HoWksHLgt8EoBzPg.cHvUNTghMjS	A	Rod	Bodrick	\N	\N	\N	2018-03-16 17:38:30	\N	\N	EMAIL	\N	f	\N
7	LeadBolton	LeadBolton@zsite.com	$2y$10$OSYesbLuHww776vejEwSB.39pQ4jkjyb.3S51WbufPgXEU.908zcu	N	Lead	Bolton	12343_123	LeadBoltonsite.com_123	\N	2018-04-21 14:05:58	\N	leadbolton.png	EMAIL	\N	f	\N
8	PortoSandro	PortoSandro@zsite.com	$2y$10$w1LEYusyQfNd1bDvUwHJi.mcri9qQ73dQKfckWGpigh0LK6aT77wC	N	Porto	Sandro	12343_123	PortoSandrosite.com_123	\N	2018-04-21 14:09:06	\N	portosandro.png	EMAIL	\N	f	\N
9	SergeLeandro	SergeLeandro@zsite.com	$2y$10$BB8EwFh6qWkoJFGXyAk77.E62b38qCwwDJ2OT8DrLKmmwieryRTDe	N	Serge	Leandro	5436t54657_123	SergeLeandrosite.com_123	\N	2018-04-21 14:15:43	\N	sergeleandro.png	EMAIL	\N	f	\N
12	RodHenrih	RodHenrih@zsite.com	$2y$10$RQV7ehNQbtCQ.KfSPx7RDuXUzuyAUYGAv1Mz5SIGpWKocxW2L07HC	N	Rod	Henrih	5436t54657_123	RodHenrih.com_123	\N	2018-04-21 14:20:20	\N	rodhenrih.png	EMAIL	\N	f	\N
6	Asa Moasa Fesad	asa_moasa_fesad@zdfs.vo	$2y$10$ad9x/bs3Qp.pHhRNUwPBxO4ViV2VjPsNytEIThpgSMSDBSsRmlNdC	A	Defaultfirst	Defaultlast	Default phone_123	Default website_123	\N	2018-04-16 13:57:39	\N	asa-moasa-fesad.png	EMAIL	\N	f	\N
1	JohnGlads	JohnGlads@site.com	$2y$10$RlRIOmu7kpbIP/XrWzY.0OLAAmkSSIa10ZkGGAHs14rJ.XvZZkNb.	A	John	Glads	\N	\N	xlvTCXJT02hIF7xlCZBSNPKrMkzOZSwgJQ73K9KgZWTLjXnCTebAPbS2a1EQ	2018-03-16 17:38:30	\N	\N	EMAIL	\N	f	\N
10	PaulFrenkel	PaulFrenkeld@zsite.com	$2y$10$t/hYAn0LyhP68VmnVuiWBuTt/kiWy4.v6Sq3rQCG1RkxLsBN4Wd8m	N	Paul	Frenkel	5436t54657_123	PaulFrenkelsite.com_123INSERT INTO "rt_m	\N	2018-04-21 14:17:22	\N	2.jpeg	EMAIL	\N	f	\N
3	tony_black	tony_black@site.com	$2y$10$7FNY.qYo4WqoG2nCAgKXZu.VmYswlFy7NKXe.Adk4h3dg9tzSk5vG	A	Tony	Black	\N	\N	vFi7lFF9R7YAoWisL4r7vyHQY5ObSZLBdav6fsDssAqc7Jv1TeIKv6ga4e0O	2018-03-16 17:38:30	\N	\N	EMAIL	\N	f	\N
5	admin	admin@mail.com	$2y$10$IX2Aa56Au0l4rgfzxKJwt.TrEGC/5qe5a.MEqQ62K5FmM.X8hy2ye	A	Sam	Muel	\N	\N	QOzUr9zSNdC58hx3d35aW46S4higiNLfdcKE6LDCMHCiM5LpH9zXh4tK4r2Q	2018-03-16 15:48:56	2018-03-16 15:48:56	\N	EMAIL	\N	f	\N
\.


--
-- Data for Name: rt_users_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_users_groups (id, user_id, group_id, created_at) FROM stdin;
1	5	1	2018-04-01 07:00:41
2	1	1	2018-04-01 07:00:54
10	6	5	2018-04-01 07:00:54
11	7	5	2018-04-01 07:00:54
13	8	5	2018-04-01 07:00:54
14	9	5	2018-04-21 14:15:43
16	10	5	2018-04-21 14:15:43
17	12	5	2018-04-21 14:20:20
\.


--
-- Data for Name: rt_users_logins; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rt_users_logins (id, provider_name, username, user_id, remote_addr, with_success, created_at) FROM stdin;
2	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-18 16:49:48
3	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-18 16:52:28
4	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-18 16:52:43
8	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 07:37:28
9	EMAIL	admin@mail.com	\N	127.0.0.1	f	2018-06-19 08:09:35
10	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 08:09:41
11	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 08:10:56
22	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 09:38:52
24	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 09:46:16
26	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 09:47:24
27	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 09:57:22
28	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-19 13:14:26
29	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-20 06:42:01
31	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-20 10:24:28
32	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-20 10:25:36
33	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-20 13:56:51
34	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-21 06:37:36
35	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-21 13:50:47
36	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-22 07:09:19
37	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-06-22 08:09:53
38	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-23 06:41:49
39	EMAIL	admin@mail.comhgbgb	\N	127.0.0.1	f	2018-06-23 13:00:38
40	EMAIL	admin@mail.comhgbgb	\N	127.0.0.1	f	2018-06-23 13:10:48
41	EMAIL	admin@mail.comhgbgb	\N	127.0.0.1	f	2018-06-23 13:11:02
42	EMAIL	admin@mail.com	\N	127.0.0.1	f	2018-06-23 13:12:27
43	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-23 13:12:48
44	EMAIL	admin@mail.com	\N	127.0.0.1	f	2018-06-23 13:12:57
45	EMAIL	admin@mail.com	\N	127.0.0.1	f	2018-06-23 13:13:03
46	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-23 13:13:17
47	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-06-23 14:05:39
48	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-06-23 16:55:12
49	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-24 06:39:05
51	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-24 07:59:41
62	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-25 07:33:41
63	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-06-25 08:34:08
64	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-02 06:47:57
65	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-02 13:04:09
66	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-02 18:05:46
67	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-02 18:12:59
68	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-03 10:23:25
69	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-03 13:05:17
70	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-03 14:05:02
71	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-09 13:58:49
72	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-09 14:14:31
73	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-09 16:54:53
74	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-10 10:09:22
75	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-11 07:21:19
76	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-13 14:02:41
77	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-07-14 16:48:55
78	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-22 18:17:06
79	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-22 18:25:26
80	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-23 07:32:57
81	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-23 13:24:03
82	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-24 06:35:08
83	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-08-24 07:55:40
84	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-24 13:07:05
85	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-25 06:37:45
86	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-08-25 14:25:55
87	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-26 06:54:09
88	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-08-26 06:57:23
89	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-27 10:37:05
90	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-27 13:17:23
91	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-28 07:40:15
92	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-28 13:00:11
93	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-29 14:22:38
94	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-30 06:24:58
95	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-30 13:26:42
96	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-08-31 06:40:49
97	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-01 06:40:30
98	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-01 12:59:56
99	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-02 06:27:12
100	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-03 06:24:24
101	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-03 15:28:27
102	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-04 06:34:41
103	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-04 13:09:18
104	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-05 07:22:00
105	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-05 13:10:20
106	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 07:04:28
107	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 08:21:19
108	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 08:24:59
109	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 08:25:55
110	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 13:40:29
111	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 13:40:31
112	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-06 13:40:41
113	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-07 07:08:48
114	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-07 13:09:02
115	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-09 15:07:56
116	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-10 07:14:05
117	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-10 09:00:18
118	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-10 12:57:25
119	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-12 06:35:10
120	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-12 10:32:01
121	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-13 07:05:27
122	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-13 13:11:21
123	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 06:52:33
124	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 07:37:15
125	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 07:38:10
126	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 07:56:51
127	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 08:01:00
128	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 08:05:11
129	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 08:06:02
130	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-14 09:13:40
131	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-14 09:14:11
132	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-14 09:14:16
133	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:20:39
134	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:37:27
135	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:38:17
136	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:42:09
137	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:43:27
138	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:43:42
139	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:43:59
140	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:56:44
141	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 09:58:26
142	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 10:15:22
143	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-14 10:17:37
144	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 10:53:03
145	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-14 10:54:24
146	EMAIL	tony_black@site.com	3	127.0.0.1	t	2018-09-14 12:52:23
147	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-15 07:14:46
148	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-15 07:15:18
149	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-15 07:16:31
150	EMAIL	admin@mail.com	5	127.0.0.1	t	2018-09-15 09:33:10
\.


--
-- Name: rt_artist_concert_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_concert_id_seq', 35, true);


--
-- Name: rt_artist_concert_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_concert_translations_id_seq', 49, true);


--
-- Name: rt_artist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_id_seq', 67, true);


--
-- Name: rt_artist_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_image_id_seq', 108, false);


--
-- Name: rt_artist_image_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_image_translations_id_seq', 226, true);


--
-- Name: rt_artist_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_translations_id_seq', 59, true);


--
-- Name: rt_artist_vote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_artist_vote_id_seq', 184, false);


--
-- Name: rt_cms_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_cms_item_id_seq', 51, true);


--
-- Name: rt_cms_item_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_cms_item_translations_id_seq', 66, true);


--
-- Name: rt_genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_genre_id_seq', 11, true);


--
-- Name: rt_genre_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_genre_translations_id_seq', 26, true);


--
-- Name: rt_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_groups_id_seq', 6, false);


--
-- Name: rt_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_migrations_id_seq', 60, true);


--
-- Name: rt_order_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_order_items_id_seq', 3, true);


--
-- Name: rt_orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_orders_id_seq', 3, true);


--
-- Name: rt_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_permissions_id_seq', 7, true);


--
-- Name: rt_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_roles_id_seq', 5, true);


--
-- Name: rt_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_settings_id_seq', 18, true);


--
-- Name: rt_song_artist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_song_artist_id_seq', 165, false);


--
-- Name: rt_song_genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_song_genre_id_seq', 152, true);


--
-- Name: rt_song_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_song_id_seq', 127, false);


--
-- Name: rt_song_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_song_translations_id_seq', 666, true);


--
-- Name: rt_song_vote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_song_vote_id_seq', 993, false);


--
-- Name: rt_tour_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_tour_id_seq', 3, false);


--
-- Name: rt_users_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_users_groups_id_seq', 38, true);


--
-- Name: rt_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_users_id_seq', 68, true);


--
-- Name: rt_users_logins_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rt_users_logins_id_seq', 150, true);


--
-- Name: rt_artist_concert_translations artist_concert_translations_artist_concert_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concert_translations
    ADD CONSTRAINT artist_concert_translations_artist_concert_id_locale_unique UNIQUE (artist_concert_id, locale);


--
-- Name: rt_artist_image_translations artist_image_translations_artist_image_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_image_translations
    ADD CONSTRAINT artist_image_translations_artist_image_id_locale_unique UNIQUE (artist_image_id, locale);


--
-- Name: rt_artist_translations artist_translations_artist_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_translations
    ADD CONSTRAINT artist_translations_artist_id_locale_unique UNIQUE (artist_id, locale);


--
-- Name: rt_cms_item_translations cms_item_translations_cms_item_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_item_translations
    ADD CONSTRAINT cms_item_translations_cms_item_id_locale_unique UNIQUE (cms_item_id, locale);


--
-- Name: rt_cms_item_translations cms_item_translations_title_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_item_translations
    ADD CONSTRAINT cms_item_translations_title_locale_unique UNIQUE (title, locale);


--
-- Name: rt_genre_translations genre_translations_genre_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genre_translations
    ADD CONSTRAINT genre_translations_genre_id_locale_unique UNIQUE (genre_id, locale);


--
-- Name: rt_genres genres_slug_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genres
    ADD CONSTRAINT genres_slug_unique UNIQUE (slug);


--
-- Name: rt_groups groups_description_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_groups
    ADD CONSTRAINT groups_description_unique UNIQUE (description);


--
-- Name: rt_groups groups_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_groups
    ADD CONSTRAINT groups_name_unique UNIQUE (name);


--
-- Name: rt_artist_concert_translations rt_artist_concert_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concert_translations
    ADD CONSTRAINT rt_artist_concert_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_artist_concerts rt_artist_concerts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concerts
    ADD CONSTRAINT rt_artist_concerts_pkey PRIMARY KEY (id);


--
-- Name: rt_artist_image_translations rt_artist_image_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_image_translations
    ADD CONSTRAINT rt_artist_image_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_artist_images rt_artist_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_images
    ADD CONSTRAINT rt_artist_images_pkey PRIMARY KEY (id);


--
-- Name: rt_artist_translations rt_artist_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_translations
    ADD CONSTRAINT rt_artist_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_artist_votes rt_artist_votes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_votes
    ADD CONSTRAINT rt_artist_votes_pkey PRIMARY KEY (id);


--
-- Name: rt_artists rt_artists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artists
    ADD CONSTRAINT rt_artists_pkey PRIMARY KEY (id);


--
-- Name: rt_cms_item_translations rt_cms_item_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_item_translations
    ADD CONSTRAINT rt_cms_item_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_cms_items rt_cms_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_items
    ADD CONSTRAINT rt_cms_items_pkey PRIMARY KEY (id);


--
-- Name: rt_genre_translations rt_genre_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genre_translations
    ADD CONSTRAINT rt_genre_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_genres rt_genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genres
    ADD CONSTRAINT rt_genres_pkey PRIMARY KEY (id);


--
-- Name: rt_groups rt_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_groups
    ADD CONSTRAINT rt_groups_pkey PRIMARY KEY (id);


--
-- Name: rt_model_has_permissions rt_model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_model_has_permissions
    ADD CONSTRAINT rt_model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: rt_model_has_roles rt_model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_model_has_roles
    ADD CONSTRAINT rt_model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: rt_order_items rt_order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_order_items
    ADD CONSTRAINT rt_order_items_pkey PRIMARY KEY (id);


--
-- Name: rt_orders rt_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_orders
    ADD CONSTRAINT rt_orders_pkey PRIMARY KEY (id);


--
-- Name: rt_permissions rt_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_permissions
    ADD CONSTRAINT rt_permissions_pkey PRIMARY KEY (id);


--
-- Name: rt_role_has_permissions rt_role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_role_has_permissions
    ADD CONSTRAINT rt_role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: rt_roles rt_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_roles
    ADD CONSTRAINT rt_roles_pkey PRIMARY KEY (id);


--
-- Name: rt_settings rt_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_settings
    ADD CONSTRAINT rt_settings_pkey PRIMARY KEY (id);


--
-- Name: rt_shoppingcart rt_shoppingcart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_shoppingcart
    ADD CONSTRAINT rt_shoppingcart_pkey PRIMARY KEY (identifier, instance);


--
-- Name: rt_song_artists rt_song_artists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_artists
    ADD CONSTRAINT rt_song_artists_pkey PRIMARY KEY (id);


--
-- Name: rt_song_genres rt_song_genres_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_genres
    ADD CONSTRAINT rt_song_genres_pkey PRIMARY KEY (id);


--
-- Name: rt_song_translations rt_song_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_translations
    ADD CONSTRAINT rt_song_translations_pkey PRIMARY KEY (id);


--
-- Name: rt_song_votes rt_song_votes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_votes
    ADD CONSTRAINT rt_song_votes_pkey PRIMARY KEY (id);


--
-- Name: rt_songs rt_songs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_songs
    ADD CONSTRAINT rt_songs_pkey PRIMARY KEY (id);


--
-- Name: rt_tours rt_tours_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_tours
    ADD CONSTRAINT rt_tours_pkey PRIMARY KEY (id);


--
-- Name: rt_users_groups rt_users_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_groups
    ADD CONSTRAINT rt_users_groups_pkey PRIMARY KEY (id);


--
-- Name: rt_users_logins rt_users_logins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_logins
    ADD CONSTRAINT rt_users_logins_pkey PRIMARY KEY (id);


--
-- Name: rt_users rt_users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users
    ADD CONSTRAINT rt_users_pkey PRIMARY KEY (id);


--
-- Name: rt_settings settings_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_settings
    ADD CONSTRAINT settings_name_unique UNIQUE (name);


--
-- Name: rt_song_translations song_translations_song_id_locale_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_translations
    ADD CONSTRAINT song_translations_song_id_locale_unique UNIQUE (song_id, locale);


--
-- Name: rt_users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: rt_users_groups users_groups_user_id_group_id_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_groups
    ADD CONSTRAINT users_groups_user_id_group_id_unique UNIQUE (user_id, group_id);


--
-- Name: rt_users users_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users
    ADD CONSTRAINT users_username_unique UNIQUE (username);


--
-- Name: artist_concert_translations_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_concert_translations_created_at_index ON public.rt_artist_concert_translations USING btree (created_at);


--
-- Name: artist_concert_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_concert_translations_locale_index ON public.rt_artist_concert_translations USING btree (locale);


--
-- Name: artist_concert_translations_place_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_concert_translations_place_name_index ON public.rt_artist_concert_translations USING btree (place_name);


--
-- Name: artist_image_translations_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_image_translations_created_at_index ON public.rt_artist_image_translations USING btree (created_at);


--
-- Name: artist_image_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_image_translations_locale_index ON public.rt_artist_image_translations USING btree (locale);


--
-- Name: artist_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX artist_translations_locale_index ON public.rt_artist_translations USING btree (locale);


--
-- Name: cms_item_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cms_item_translations_locale_index ON public.rt_cms_item_translations USING btree (locale);


--
-- Name: genre_translations_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX genre_translations_created_at_index ON public.rt_genre_translations USING btree (created_at);


--
-- Name: genre_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX genre_translations_locale_index ON public.rt_genre_translations USING btree (locale);


--
-- Name: genre_translations_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX genre_translations_name_index ON public.rt_genre_translations USING btree (name);


--
-- Name: groups_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX groups_created_at_index ON public.rt_groups USING btree (created_at);


--
-- Name: ind_cms_item_alias; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_cms_item_alias ON public.rt_cms_items USING btree (alias);


--
-- Name: ind_cms_item_page_type_published; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_cms_item_page_type_published ON public.rt_cms_items USING btree (page_type, published);


--
-- Name: ind_genre_published; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_genre_published ON public.rt_genres USING btree (published);


--
-- Name: ind_rt_artist_concerts_artist_id_country_event_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artist_concerts_artist_id_country_event_date ON public.rt_artist_concerts USING btree (artist_id, country, event_date);


--
-- Name: ind_rt_artist_images_artist_id_image_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_rt_artist_images_artist_id_image_name ON public.rt_artist_images USING btree (artist_id, image_name);


--
-- Name: ind_rt_artist_images_artist_id_is_main; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artist_images_artist_id_is_main ON public.rt_artist_images USING btree (artist_id, is_main);


--
-- Name: ind_rt_artist_images_is_home_page; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artist_images_is_home_page ON public.rt_artist_images USING btree (is_home_page);


--
-- Name: ind_rt_artist_votes_artist_id_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artist_votes_artist_id_user_id ON public.rt_artist_votes USING btree (artist_id, user_id);


--
-- Name: ind_rt_artists_created_at; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artists_created_at ON public.rt_artists USING btree (created_at);


--
-- Name: ind_rt_artists_is_active_has_concerts_ordering; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_artists_is_active_has_concerts_ordering ON public.rt_artists USING btree (is_active, has_concerts, ordering);


--
-- Name: ind_rt_artists_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_rt_artists_slug ON public.rt_artists USING btree (slug);


--
-- Name: ind_rt_song_votes_song_id_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_song_votes_song_id_user_id ON public.rt_song_votes USING btree (song_id, user_id);


--
-- Name: ind_rt_songs_is_active_ordering; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_songs_is_active_ordering ON public.rt_songs USING btree (is_active, ordering);


--
-- Name: ind_rt_songs_slug; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ind_rt_songs_slug ON public.rt_songs USING btree (slug);


--
-- Name: ind_rt_tours_artist_id_start_date_end_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_tours_artist_id_start_date_end_date ON public.rt_tours USING btree (artist_id, start_date, end_date);


--
-- Name: ind_rt_users_provider; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_rt_users_provider ON public.rt_users USING btree (provider_name);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.rt_model_has_roles USING btree (model_id, model_type);


--
-- Name: orders_discount_completed_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX orders_discount_completed_index ON public.rt_orders USING btree (discount, completed);


--
-- Name: orders_user_id_completed_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX orders_user_id_completed_index ON public.rt_orders USING btree (user_id, completed);


--
-- Name: settings_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX settings_created_at_index ON public.rt_settings USING btree (created_at);


--
-- Name: song_translations_locale_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX song_translations_locale_index ON public.rt_song_translations USING btree (locale);


--
-- Name: user_logins_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_logins_created_at_index ON public.rt_users_logins USING btree (created_at);


--
-- Name: user_logins_provider_name_user_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_logins_provider_name_user_id_index ON public.rt_users_logins USING btree (provider_name, user_id);


--
-- Name: user_logins_provider_name_username_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_logins_provider_name_username_index ON public.rt_users_logins USING btree (provider_name, username);


--
-- Name: user_logins_with_success_remote_addr_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_logins_with_success_remote_addr_index ON public.rt_users_logins USING btree (with_success, remote_addr);


--
-- Name: users_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_created_at_index ON public.rt_users USING btree (created_at);


--
-- Name: users_groups_created_at_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_groups_created_at_index ON public.rt_users_groups USING btree (created_at);


--
-- Name: users_status_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX users_status_index ON public.rt_users USING btree (status);


--
-- Name: rt_artist_concert_translations artist_concert_translations_artist_concert_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concert_translations
    ADD CONSTRAINT artist_concert_translations_artist_concert_id_foreign FOREIGN KEY (artist_concert_id) REFERENCES public.rt_artist_concerts(id) ON DELETE CASCADE;


--
-- Name: rt_artist_image_translations artist_image_translations_artist_image_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_image_translations
    ADD CONSTRAINT artist_image_translations_artist_image_id_foreign FOREIGN KEY (artist_image_id) REFERENCES public.rt_artist_images(id) ON DELETE CASCADE;


--
-- Name: rt_artist_translations artist_translations_artist_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_translations
    ADD CONSTRAINT artist_translations_artist_id_foreign FOREIGN KEY (artist_id) REFERENCES public.rt_artists(id) ON DELETE CASCADE;


--
-- Name: rt_cms_item_translations cms_item_translations_cms_item_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_item_translations
    ADD CONSTRAINT cms_item_translations_cms_item_id_foreign FOREIGN KEY (cms_item_id) REFERENCES public.rt_cms_items(id) ON DELETE CASCADE;


--
-- Name: rt_artist_concerts fk_rt_artist_concerts_artist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concerts
    ADD CONSTRAINT fk_rt_artist_concerts_artist_id FOREIGN KEY (artist_id) REFERENCES public.rt_artists(id);


--
-- Name: rt_artist_concerts fk_rt_artist_concerts_tour_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_concerts
    ADD CONSTRAINT fk_rt_artist_concerts_tour_id FOREIGN KEY (tour_id) REFERENCES public.rt_tours(id);


--
-- Name: rt_artist_votes fk_rt_artist_votes_artist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_votes
    ADD CONSTRAINT fk_rt_artist_votes_artist_id FOREIGN KEY (artist_id) REFERENCES public.rt_artists(id);


--
-- Name: rt_artist_votes fk_rt_artist_votes_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_artist_votes
    ADD CONSTRAINT fk_rt_artist_votes_user_id FOREIGN KEY (user_id) REFERENCES public.rt_users(id);


--
-- Name: rt_song_artists fk_rt_song_artists_artist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_artists
    ADD CONSTRAINT fk_rt_song_artists_artist_id FOREIGN KEY (artist_id) REFERENCES public.rt_artists(id);


--
-- Name: rt_song_artists fk_rt_song_artists_song_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_artists
    ADD CONSTRAINT fk_rt_song_artists_song_id FOREIGN KEY (song_id) REFERENCES public.rt_songs(id);


--
-- Name: rt_song_genres fk_rt_song_genres_genre_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_genres
    ADD CONSTRAINT fk_rt_song_genres_genre_id FOREIGN KEY (genre_id) REFERENCES public.rt_genres(id);


--
-- Name: rt_song_votes fk_rt_song_votes_song_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_votes
    ADD CONSTRAINT fk_rt_song_votes_song_id FOREIGN KEY (song_id) REFERENCES public.rt_songs(id);


--
-- Name: rt_song_votes fk_rt_song_votes_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_votes
    ADD CONSTRAINT fk_rt_song_votes_user_id FOREIGN KEY (user_id) REFERENCES public.rt_users(id);


--
-- Name: rt_genre_translations genre_translations_genre_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_genre_translations
    ADD CONSTRAINT genre_translations_genre_id_foreign FOREIGN KEY (genre_id) REFERENCES public.rt_genres(id) ON DELETE CASCADE;


--
-- Name: rt_model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.rt_permissions(id) ON DELETE CASCADE;


--
-- Name: rt_model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.rt_roles(id) ON DELETE CASCADE;


--
-- Name: rt_order_items order_items_order_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_order_items
    ADD CONSTRAINT order_items_order_id_foreign FOREIGN KEY (order_id) REFERENCES public.rt_orders(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rt_orders orders_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_orders
    ADD CONSTRAINT orders_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.rt_users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: rt_role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.rt_permissions(id) ON DELETE CASCADE;


--
-- Name: rt_role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.rt_roles(id) ON DELETE CASCADE;


--
-- Name: rt_cms_items rt_cms_items_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_cms_items
    ADD CONSTRAINT rt_cms_items_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.rt_users(id);


--
-- Name: rt_song_translations song_translations_song_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_song_translations
    ADD CONSTRAINT song_translations_song_id_foreign FOREIGN KEY (song_id) REFERENCES public.rt_songs(id) ON DELETE CASCADE;


--
-- Name: rt_users_groups users_groups_group_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_groups
    ADD CONSTRAINT users_groups_group_id_foreign FOREIGN KEY (group_id) REFERENCES public.rt_groups(id) ON DELETE RESTRICT;


--
-- Name: rt_users_groups users_groups_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_groups
    ADD CONSTRAINT users_groups_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.rt_users(id) ON DELETE RESTRICT;


--
-- Name: rt_users_logins users_logins_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rt_users_logins
    ADD CONSTRAINT users_logins_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.rt_users(id);


--
-- PostgreSQL database dump complete
--

