/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

require('jquery-confirm');
import 'jquery-confirm/css/jquery-confirm.css' // file:///_wwwroot/lar/TasksBootstrap41/node_modules/jquery-confirm/css/jquery-confirm.css

window.Vue = require('vue');

import Vue from 'vue'


// import VueI18n from 'vue-i18n' // http://kazupon.github.io/vue-i18n/installation.html#direct-download-cdn
// Vue.use(VueI18n)

import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
Vue.use(VueInternationalization);



import VueRouter from 'vue-router';
window.Vue.use(VueRouter);


import VueLocalStorage from 'vue-localstorage'  //  // https://github.com/pinguinjkeke/vue-local-storage
Vue.use(VueLocalStorage)


import VueI18n from 'vue-i18n'
Vue.use(VueI18n)


import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
// import '~vuetify/src/stylus/main' // Ensure you are using stylus-loader ??


// Vue.use( Vuetify );
import en from 'vuetify/src/locale/en.ts' //  /node_modules/vuetify/src/locale/en.ts English   FROM https://github.com/vuetifyjs/vuetify/blob/dev/src/locale
import es from 'vuetify/src/locale/es.ts' // Has no Spain files
import uk from 'vuetify/src/locale/uk.ts' // Ukrainian
Vue.use( Vuetify, {
    lang: {
        locales: {en, es, uk},
        current: 'en'
    }
});


import Vuelidate from 'vuelidate' // https://github.com/monterail/vuelidate
Vue.use(Vuelidate)


import money from 'v-money' // https://github.com/vuejs-tips/v-money
Vue.use(money, {precision: 2})

// import VuePictureSwipe from 'vue-picture-swipe'; // https://github.com/rap2hpoutre/vue-picture-swipe
// Vue.component('vue-picture-swipe', VuePictureSwipe);


// import codepen{ VueCharts, Line } from 'vue-chartjs'
// import { Line } from 'vue-chartjs'
// Vue.use(VueCharts)
//
// Vue.component('line-chart', {
//     extends: VueChartJs.Line,
//     mounted () {
//         this.renderChart({
//             labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//             datasets: [
//                 {
//                     label: 'Data One',
//                     backgroundColor: '#f87979',
//                     data: [40, 39, 10, 40, 39, 80, 40]
//                 }
//             ]
//         }, {responsive: true, maintainAspectRatio: false})
//     }
// })


// https://laracasts.com/discuss/channels/vue/using-moment-in-vue,
window.moment = require('moment');  // https://momentjs.com/


/*
import * as VueGoogleMaps from 'vue2-google-maps'     ;// https://www.youtube.com/watch?v=ntjhLWH2krY
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBhkNSrpIdRSZlq2oX5nQV0sy5ztMXkLbE',
        libraries: 'places', // This is required if you use the Autocomplete plugin
        v: '3.30',
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)
    }
})
*/


// BACKEND TEMPLATES BLOCK START
import DashboardIndex from './components/Backend/dashboard/DashboardIndex.vue';
import ArtistsIndex from './components/Backend/artists/artistsListing.vue';
import ArtistView from './components/Backend/artists/artistView.vue';
import ArtistEdit from './components/Backend/artists/artistEdit.vue';
import GenresIndex from './components/Backend/genres/genresListing.vue';
import GenreEdit from './components/Backend/genres/genreEdit.vue' ;
import CMSItemsIndex from './components/Backend/cms_items/cMSItemsListing.vue';
import CMSItemEdit from './components/Backend/cms_items/cMSItemEdit.vue';


import UsersIndex from './components/Backend/users/usersListing.vue';
import UserEdit from './components/Backend/users/userEdit.vue';

import BackendMsgPage from './components/Backend/BackendMsgPage.vue';  // http://local-artists-rating.com/home#/admin/msg/sometext111%20222%20333/warning/redirect%20url%20NAH

import SettingsIndex from './components/Backend/settings/settings.vue'; // resources/assets/js/components/Backend/settings/settings.vue
import SongsWithSimilarTitles from './components/Backend/songs/songsWithSimilarTitles.vue'; //getSongsWithSimilarTitlesList

import ChartReports from './components/Backend/charts/chartReports.vue';

import UserProfile from './components/Backend/user/UserProfile.vue';

Vue.component( 'backend-app-layout',  require( './components/layout/backendAppLayout.vue') );
Vue.component('future-concerts-google-maps', require('./components/google-maps/FutureConcertsGoogleMapsBlock.vue'));

// BACKEND TEMPLATES BLOCK END



Vue.component( 'frontend-toolbar',  require( './components/lib/FrontendToolbar.vue') );
Vue.component( 'listing-header',  require( './components/lib/ListingHeader.vue') );
Vue.component( 'breadcrumbs',  require( './components/lib/Breadcrumbs.vue') );


// FRONTEND TEMPLATES BLOCK START

import HomePage from './components/home/HomePage.vue';
import CartPage from './components/cart/CartPage.vue';
import CartCheckoutPage from './components/cart/CartCheckoutPage.vue';
import CmsItemPage from './components/home/CmsItem.vue';
import Contact from './components/Contact.vue';
import ArtistConcertPage from './components/artists/ArtistConcertPage.vue';
import ArtistPage from './components/artists/ArtistPage.vue'; //resources/assets/js/components/artists/ArtistPage.vue
import SongPage from './components/songs/SongPage.vue'; //resources/assets/js/components/artists/SongPage.vue
import SongsByGenrePage from './components/songs/SongsByGenrePage.vue'; //resources/assets/js/components/artists/SongsByGenrePage.vue
import AppFooter from './components/layout/appFooter.vue'; // resources/assets/js/components/layout/appFooter.vue
import NotFound from './components/NotFound.vue'; // resources/assets/js/components/NotFound.vue
import AppLogin from './components/home/appLogin.vue'; // resources/assets/js/components/home/appLogin.vue
import AppRoot from './components/appRoot.vue'; // resources/assets/js/components/home/appLogin.vue
import UserRegister from './components/user/UserRegister.vue';

import MsgPage from './components/home/MsgPage.vue';  // http://local-artists-rating.com/home#/admin/msg/sometext111%20222%20333/warning/redirect%20url%20NAH
// FRONTEND TEMPLATES BLOCK END


import appMixin from './appMixin';

const routes = [
    {
        // path: { component: AppRoot, name: 'appRoot' },
        path: '/',
        // redirect: '/AppRoot',
        components: {
            notFound: NotFound,
            appLogin: AppLogin,
            // appRoot: AppRoot,
            userRegister: UserRegister,
            userProfile: UserProfile,
            homePage: HomePage,
            cartPage: CartPage,
            cartCheckoutPage: CartCheckoutPage,
            // stepperValidationPage: StepperValidationPage,
            cmsItemPage: CmsItemPage,
            dashboardIndex: DashboardIndex,
            artistsIndex: ArtistsIndex,
            artistEditor: ArtistEdit,
            artistView:   ArtistView,

            genresIndex: GenresIndex,
            genreEditor: GenreEdit,

            cMSItemsIndex: CMSItemsIndex,
            cMSItemEditor: CMSItemEdit,

            usersIndex: UsersIndex,
            userEditor: UserEdit,

            backendMsgPage: BackendMsgPage,
            msgPage: MsgPage,

            contactPage: Contact,
            artistConcertPage: ArtistConcertPage,
            artistPage: ArtistPage,
            songPage: SongPage,
            songsByGenrePage: SongsByGenrePage,
            songsWithSimilarTitlesPage: SongsWithSimilarTitles,
            settingsIndex: SettingsIndex,
            chartReportsPage: ChartReports,

        }
    },
    //             cmsItemPage: CmsItemPage,

    {path: '/not-found/:invalid_url?', component: NotFound, name: 'notFound'},
    {path: '/login', component: AppLogin, name: 'appLogin'},
    // {
    //     path: '/',
    //    component: AppRoot, name: 'appRoot'
    // },
    // {path: '/', component: AppRoot, name: 'appRoot'},
    {path: '/register', component: UserRegister, name: 'userRegister'},
    {path: '/profile', component: UserProfile, name: 'userProfile'},
    {path: '/home/:action?/:value?', component: HomePage, name: 'homePage'},
    {path: '/cart', component: CartPage, name: 'cartPage'},
    {path: '/cart/checkout', component: CartCheckoutPage, name: 'cartCheckoutPage'},
    // {path: '/stepper-validation', component: StepperValidationPage, name: 'stepperValidationPage'},

    {path: '/cms-item/:alias', component: CmsItemPage, name: 'cmsItemPage'},
    // {path: '/cms-item/privacy-policy', component: CmsItemPage, name: 'cmsItemPage'},
    //{path: '/cms-item/:alias', component: CmsItemPage, name: 'cmsItemPage'},
    // http://local-artists-rating.com/home#/cms-item/privacy-policy



    // {path: '/admin/dashboard/:default_block?', component: DashboardIndex, name: 'dashboardIndex'},
    {path: '/admin/dashboard/:action?/:value?', component: DashboardIndex, name: 'dashboardIndex'}, // ?
    // http://local-artists-rating.com/home#/admin/dashboard/twitter_new_user/valname

    {path: '/contact', component: Contact, name: 'contact'},
    {path: '/artist-concert-page/:id', component: ArtistConcertPage, name: 'artistConcertPage'},
    {path: '/artist-page/:slug', component: ArtistPage, name: 'artistPage'},
    {path: '/song-page/:slug', component: SongPage, name: 'songPage'},
    {path: '/songs-by-genre-page/:slug', component: SongsByGenrePage, name: 'songsByGenrePage'},


    {path: '/settings', component: SettingsIndex, name: 'settingsIndex'},


    {path: '/songs-with-similar-titles', component: SongsWithSimilarTitles, name: 'songsWithSimilarTitles'},
    {path: '/chart-reports', component: ChartReports, name: 'chartReports'},
    {path: '/admin/artists', component: ArtistsIndex, name: 'artistsIndex'},
    {path: '/admin/artist/view/:id', component: ArtistView, name: 'artistView'},   // resources/assets/js/components/artists/artistView.vue
    {path: '/admin/artist/edit/:id', component: ArtistEdit, name: 'artistEditor'},
    {path: '/admin/genres', component: GenresIndex, name: 'genresIndex'},
    {path: '/admin/genre/edit/:id/:active_song_id?', component: GenreEdit, name: 'genreEditor'},

    {path: '/admin/cms-items', component: CMSItemsIndex, name: 'CMSItemsIndex'},
    {path: '/admin/cms-item/edit/:id', component: CMSItemEdit, name: 'CMSItemEditor'},

    {path: '/admin/users', component: UsersIndex, name: 'usersIndex'},
    {path: '/admin/user/edit/:id', component: UserEdit, name: 'userEditor'},

    {path: '/admin/msg/:msg/:type?/:redirect?', component: BackendMsgPage, name: 'backendMsgPage'},
    {path: '/home/msg/:msg/:type?/:redirect?', component: MsgPage, name: 'msgPage'},

    // {path: '/home/msg/:msg/:type?/:redirect?', component: MsgPage, name: 'msgPage'},
    // http://local-artists-rating.com/#/cms-item/concerts_soon
    // http://local-artists-rating.com/home#/msg/sometext111/warning

    // {path: '/not-found/:invalid_url?', component: NotFound, name: 'notFound'},

    // {path: '/admin/test', component: TestIndex, name: 'testIndex'}, //            testIndex: TestIndex,
    // {path: '/admin/tasks/filter/:filter', component: TasksFilter, name: 'tasksFilter'},

]

// alert("-02")
const router = new VueRouter( {
    mode: 'hash', // default
    routes
})


/*
router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next(  '/not-found/'+encodeURIComponent(to.path)  );
    } else {
        next();
    }
})
*/

// alert( "resources/assets/js/app.js::" )

export const bus = new Vue();
let current_locale = 'en';
let current_locale_label = 'English';

Vue.localStorage.set('backend_locale', 'en')
Vue.localStorage.set('backend_locale_label', 'English')

/*
let lang = 'ru';//document.documentElement.lang.substr(0, 2);
let browserInfo= getBrowserInfo()
if ( browserInfo['name'] =='Chromium' ) { // in Chromium show English
    lang= 'en'
}
*/
// Vue.localStorage.set('someNumber', 123)


// let storage_locale= Vue.localStorage.get('locale')
// alert( "storage_locale::"+(storage_locale)+ "!! typeof storage_locale::"+(typeof storage_locale) )

let storage_locale= Vue.localStorage.get('locale')
let storage_locale_label= Vue.localStorage.get('locale_label')
console.log("storage_locale::")
console.log( storage_locale )

if ( typeof storage_locale!= "undefined" ) {
    current_locale= storage_locale
}
// console.log("current_locale::")
// console.log( current_locale )
if ( current_locale == '' || typeof current_locale == 'undefined' || current_locale == null ) {
    current_locale= 'en'
}
console.log("+++storage_locale::")
console.log( storage_locale )

// if ( lang == '' || typeof lang == 'undefined' ) {
//     lang= 'en'
// }
const i18n = new VueI18n({    // https://github.com/caouecs/Laravel-lang - Additive langs
    locale: current_locale, // set locale
    messages : Locale, // set locale messages
})

console.log("AFTER current_locale::")
console.log( current_locale )

bus.$emit("currentLocaleChanged", current_locale, current_locale_label );
// alert( "bus.$emit(currentLocaleChanged  current_locale::"+current_locale +"  current_locale_label::"+current_locale_label )

// console.log("AFTER i18n::")
// console.log( i18n )


console.log("router::")
console.log( router )
// console.log( router.query ) // this.$route.query.page
// console.log( router.query.page ) // this.$route.query.page

new Vue({ router, i18n,

    data:{
        app_title: '',
        loggedUserProfile: {},   /* LOGGED USER INFO BLOCK */
        loggedUserInGroups: [],
        refsArray:[]
    },

    mixins : [appMixin],

    created() {
    }, // created() {

    mounted() {


        //
        // var app_route_name= this.$route.name
        //
        // console.log("!!here  app_route_name:::");
        // console.log(app_route_name)
        //
    }, // mounted(){

    watch: {
        '$route': function(){
            /*                     Vue.localStorage.set( 'logged_user_id', '' )
                    Vue.localStorage.set( 'logged_user_username', '' )
                    Vue.localStorage.set( 'logged_user_full_name', '' )
 */
            let logged_user_id= Vue.localStorage.get('logged_user_id')
            let logged_user_username= Vue.localStorage.get('logged_user_username')
            let loggedUserProfile= Vue.localStorage.get('loggedUserProfile')

            console.log("+++ watch this.$route  this.$route.path::")
            let is_admin= this.$route.path.includes('/admin/');
            console.log(this.$route.path)

            // alert( "watch  is_admin::"+is_admin+"  logged_user_id::"+logged_user_id+"  logged_user_username::"+logged_user_username+"  loggedUserProfile::"+var_dump(loggedUserProfile)+"  : "+var_dump(this.$route) )
            // || ( typeof loggedUserProfile== "undefined" )    || loggedUserProfile == {}
            if ( is_admin && ( ( typeof logged_user_id == "undefined" ) || ( typeof logged_user_username== "undefined" ) || logged_user_id == "" || logged_user_username == "" ) ) {
                alert( "NOT LOGGED::"+var_dump(-5) )
                this.$router.push({path: '/login'}); 
            }

            // console.log("+++ watchthis.$route::")
            // console.log( this.$route )
            // console.log("+++ watch this.$route  this.$route.params.alias::")
            // console.log(this.$route.params.alias)
            // console.log("+++ watch this.$route  this.$route.name::")
            // console.log(this.$route.name)
        }
    },

    beforeRouteEnter (to, from, next) {
                    alert( " beforeRouteEnter::"+this.var_dump(-86543) )
        console.log("beforeRouteEnter to::")
        console.log( to )

        console.log(to.params.alias)
        next()
    },

    methods: {


    }, // methods: {



} ).$mount('#app')  // new Vue({ router,


// alert( "AFTER::"+var_dump(11) )

function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}


function getBrowserInfo(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chromium|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'IE ',version:(tem[1]||'')};
    }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
    }
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
        name: M[0],
        version: M[1]
    };
}
