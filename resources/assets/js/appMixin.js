export default {


    methods: {

        isNoneEmptyString(val) {
            if ( typeof(val) == "string" && val.length> 0 ) return true;
            return false;
        },

    // takes the form field value and returns true on valid number
        checkValidCreditCard(value) {
            // accept only digits, dashes or spaces
            alert( "checkValidCreditCard value::"+(value) )
            if (/[^0-9-\s]+/.test(value)) return false;

            // The Luhn Algorithm. It's so pretty.
            var nCheck = 0, nDigit = 0, bEven = false;
            value = value.replace(/\D/g, "");

            for (var n = value.length - 1; n >= 0; n--) {
                var cDigit = value.charAt(n),
                    nDigit = parseInt(cDigit, 10);

                if (bEven) {
                    if ((nDigit *= 2) > 9) nDigit -= 9;
                }

                nCheck += nDigit;
                bEven = !bEven;
            }

            return (nCheck % 10) == 0;
        },      // checkValidCreditCard(value) {

        getMoneyOptions() {
            return {
                // decimal: ',',
                // thousands: '.',
                decimal: '.',
                thousands: ' ',
                prefix: '$',
                suffix: '',
                precision: 2,
                masked: false
            };
        },

        moment(...args) {
            return moment(...args);
        },

        getValidationsError(errorResponse) {
            // console.log("getValidationsError errorResponse::")
            // console.log( errorResponse )
            var error_message = '';
            if (typeof errorResponse.data.errors == "undefined") return "";
            var dataErrors = errorResponse.data.errors
            // console.log("dataErrors::")
            // console.log( typeof dataErrors )
            //
            var typeof_data_errors = typeof dataErrors

            for (var property in dataErrors) {
                if (dataErrors.hasOwnProperty(property)) {
                    error_message = error_message + this.Capitalize(property) + " : " + dataErrors[property] + ';'
                }
            }
            return error_message;
        },

        calcSongRating(votes_count, votes_sum, decimals) {
            if (typeof votes_count != "number" || typeof votes_sum != "number") return '';
            if (votes_count <= 0 || votes_sum <= 0) return '';
            if (typeof decimals == "undefined" || decimals == 0) return Math.floor(votes_sum / votes_count);
            return parseFloat(votes_sum / votes_count).toFixed(decimals);
            // return parseFloat( votes_sum/votes_count ).toFixed(decimals);
        },


        calcArtistRating(votes_count, votes_sum, decimals) {
            if (typeof votes_count != "number" || typeof votes_sum != "number") return '';
            if (votes_count <= 0 || votes_sum <= 0) return '';
            if (typeof decimals == "undefined" || decimals == 0) return Math.floor(votes_sum / votes_count);
            return parseFloat(votes_sum / votes_count).toFixed(decimals);
            // return parseFloat( votes_sum/votes_count ).toFixed(decimals);
        },


        markdownToHtml(text) {
            var showdown = require('showdown')
            var converter = new showdown.Converter()
            return converter.makeHtml(text);
        },

        momentDatetime(datetime, datetime_format, default_val) {
            // return moment( datetime ).format(this.settings_js_moment_datetime_format);
            // console.log("momentDatetime datetime::")
            // console.log( datetime )
            // console.log("datetime_format::")
            // console.log( datetime_format )
            // console.log("typeof datetime::")
            // console.log( typeof datetime )
            if (typeof datetime != "string") return ( typeof default_val != "undefined" ? default_val : "" )
            if (typeof datetime_format == "undefined") {
                // alert( "NOT::"+(-3) )
                //this.settings_js_moment_datetime_format= window.settings_js_moment_datetime_format;
            }
            return moment(datetime).format(datetime_format);
        },

        getLabelByKeyFromList(key, listing) {
            // console.log("getLabelByKeyFromList listing::")
            // console.log( listing )
            var ret = ''
            listing.map((nextListingItem, index) => {
                // console.log("nextListingItem::")
                // console.log( nextListingItem )
                // console.log("key::")
                // console.log( key )
                if (nextListingItem.key == key) {
                    // console.log("RETURNED nextListingItem.label::")
                    // console.log( nextListingItem.label )
                    ret = nextListingItem.label;
                    return ret;
                }
            });
            return ret;
        },


        getKeyByLabelFromList(label, listing) {
            // console.log("getKeyByLabelFromList listing::")
            // console.log( listing )
            var ret = ''
            listing.map((nextListingItem, index) => {
                // console.log("+++getKeyByLabelFromList label::")
                // console.log( label )
                if (nextListingItem.label == label) {
                    // alert( "RETURN nextListingItem.key::"+(nextListingItem.key) )
                    // console.log("RETURNED nextListingItem.key::")
                    // console.log( nextListingItem.key )
                    ret = nextListingItem.key;
                    return nextListingItem.key;
                }
            });
            return ret;
        },
        getSplitted(str, splitter, index) {
            if (typeof str == "undefined") return "";
            var value_arr = str.split(splitter);
            if (typeof value_arr[index] != "undefined") {
                return value_arr[index];
            }
            return '';
        },

        preg_quote(str) {	// Quote regular expression characters
            return str.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },

        getVotingColor(vote_value) { //
            // console.log("vote_value::")
            // console.log( vote_value )
            // console.log("window.settings_votingColorsArray::")
            // console.log( window.settings_votingColorsArray )

            if (typeof window.settings_votingColorsArray[vote_value] != 'undefined') {
                // console.log("INSIDE vote_value::")
                // console.log( vote_value )
                //
                // console.log("window.settings_votingColorsArray[vote_value]::")
                // console.log( window.settings_votingColorsArray[vote_value] )
                //
                // console.log("typeof  window.settings_votingColorsArray[vote_value]::")
                // console.log( typeof window.settings_votingColorsArray[vote_value] )
                //
                return window.settings_votingColorsArray[vote_value]
                // return 'backgroundColor:' + window.settings_voting_colors[vote_value]
            }
            return 'teal'
        },

        getFuturePastColor(future_past) { //
            if (typeof window.settings_futurePastColorsArray[future_past] != 'undefined') {
                return 'backgroundColor:' + window.settings_futurePastColorsArray[future_past]
            }
            return '#fff'
        },

        getDictionaryLabel(value, selectionsList, default_value) {
            if (typeof default_value == "undefined") default_value = ''
            // console.log("getDictionaryLabel selectionsList::")
            // console.log( selectionsList )
            //
            // console.log("getDictionaryLabel value::")
            // console.log( value )

            var ret = default_value
            selectionsList.map((nextSelection, index) => {
                // console.log("+++getDictionaryLabel nextSelection::")
                // console.log( nextSelection )

                if (nextSelection.key == value) {
                    // console.log("=== FOUND getDictionaryLabel nextSelection::")
                    // console.log( nextSelection )
                    ret = nextSelection.label;
                }
            });
            // ret= ret.replace(" ",'&nbsp;');
            return ret
        }, //getDictionaryLabel( value, selectionsList, default_value ) {

        goToNamedRef(ref_name) {
            alert("goToNamedRef ref_name::" + (ref_name))
            console.log("Vue::")
            console.log(Vue)

            console.log("Vue.$refs::")
            console.log(Vue.$refs)

            console.log(this.$refs)
            console.log(typeof this.$refs)

            var element = ''
            if (ref_name == 'porto') {

                console.log("+++this::")
                console.log(this)

                console.log("+++!!!this.$refs::") // I show printscreen below
                console.log(this.$refs)

                console.log("++++!!!ref_name::")
                console.log(ref_name)

            }
//                console.log("?????? element::")
//                console.log(element);

            if (typeof element != "undefined") {
                var top = element.offsetTop;
                console.log("INSIDE top::")
                console.log(top)

                window.scrollTo(0, top);
            }
        }, // goToNamedRef(ref_name) {


        addDaysToDate(add_days, current_date) {
            if (typeof current_date == "undefined") {
                current_date = new Date();
            }
            current_date.setDate(current_date.getDate() + add_days);
            return current_date;
        },

        getPaginationParams: function (page, order_by, order_direction, is_question) {
            if (typeof page != "number") page = 1
            var ret = '';
            // debugger;
            if (page > 0) {
                ret = ret + (is_question ? '?' : '&') + 'page=' + page
            }
            if (typeof order_by != "undefined" && this.trim(order_by) != "") {
                ret = ret + (ret == '' ? '' : '&') + 'order_by=' + order_by
            }

            if (typeof order_direction != "undefined" && this.trim(order_direction) != "") {
                ret = ret + (ret == '' ? '' : '&') + 'order_direction=' + order_direction
            }

            return ret;
        }, // getPaginationParams : function(page, order_by, order_direction, is_question) {


        setAppTitle: function (page_title_text, show_only_page_title, return_string) {
            // alert( "setAppTitle page_title_text::"+(page_title_text) )
            // debugger
            if (typeof return_string == "undefined") return_string = false
            var site_name = window.SITE_NAME;

            // window.SITE_NAME= 'FIX ME : ';
            if (typeof show_only_page_title == "undefined" || !show_only_page_title) {
                if (typeof page_title_text != "undefined" && page_title_text != '' && site_name != "undefined" && site_name != '') {
                    // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                    if (return_string) return page_title_text + ' of ' + site_name;
                    if (document.getElementById("app_title")) {
                        // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                        document.getElementById("app_title").innerHTML = page_title_text + ' of ' + site_name
                    }
                    if (document.getElementById("toolbar_app_title")) {
                        // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                        document.getElementById("toolbar_app_title").innerHTML = page_title_text + ' of ' + site_name
                    }
                    return;
                }
                if (site_name != "undefined" && site_name != '') {
                    // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                    if (return_string) return site_name;
                    if (document.getElementById("app_title")) {
                        // alert( "+3+setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                        document.getElementById("app_title").innerHTML = site_name
                    }
                    if (document.getElementById("toolbar_app_title")) {
                        // alert( "+3+setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                        document.getElementById("toolbar_app_title").innerHTML = site_name
                    }
                    return;
                }
            }
            if (typeof page_title_text != "undefined" && page_title_text != '') {
                // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                if (return_string) return page_title_text;
                if (document.getElementById("app_title")) {
                    // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                    document.getElementById("app_title").innerHTML = page_title_text
                }
                if (document.getElementById("toolbar_app_title")) {
                    // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                    document.getElementById("toolbar_app_title").innerHTML = page_title_text
                }
                return;
            }
        }, //Application name : Page Title


        inArray: function (needle, haystack) {
            var length = haystack.length;
            for (var i = 0; i < length; i++) {
                if (haystack[i] == needle) return true;
            }
            return false;
        },

        effectiveDeviceWidth: function (param) {
            var viewport = {
                width: $(window).width(),
                height: $(window).height()
            };
            //alert( "viewport::"+var_dump(viewport) )
            if (typeof param != "undefined") {
                if (param.toLowerCase() == 'width') {
                    return viewport.width;
                }
                if (param.toLowerCase() == 'height') {
                    return viewport.height;
                }
            }
            return viewport;
            //var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
            //// iOS returns available pixels, Android returns pixels / pixel ratio
            //// http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
            //if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            //    deviceWidth = deviceWidth / window.devicePixelRatio;
            //}
            //return deviceWidth;
        },


        getBootstrapPlugins: function () {
            var ret_str = '';
            ret_str += "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "button:" + ( (typeof($.fn.button) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "carousel:" + ( (typeof($.fn.carousel) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "dropdown:" + ( (typeof($.fn.dropdown) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "modal:" + ( (typeof($.fn.modal) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "tooltip:" + ( (typeof($.fn.tooltip) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "popover:" + ( (typeof($.fn.popover) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "tab:" + ( (typeof($.fn.tab) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "affix:" + ( (typeof($.fn.affix) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "collapse:" + ( (typeof($.fn.collapse) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "scrollspy:" + ( (typeof($.fn.scrollspy) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            ret_str += "transition:" + ( (typeof($.fn.transition) != 'undefined') ? " <b>On</b>" : "Off" ) + ". ";
            //ret_str+= "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
            return ret_str;
        },


        getVueVersion: function () {
            return Vue.version;
        },


        formatNumberToHuman: function (number) {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },

        convertMinsToHrsMins: function (minutes) {
            if (minutes < 60) return (minutes + ' minutes');
            var h = Math.floor(minutes / 60);
            var m = minutes % 60;
            h = h < 10 ? h : h;
            m = m < 10 ? m : m;
            return h + ':' + m + ' hour(s)';
        },

        pluralize: function (n, single_str, multi_str) {
            return n === 1 ? single_str : multi_str
        },

        showFullUserName: function (username, first_name, last_name) {
            var ret_str = first_name + " " + last_name + " ( " + username + " ) ";
            return ret_str;
        },

        getHeaderIcon(icon) {
            // alert( "menuIconsArray::"+var_dump(menuIconsArray) )
            if (typeof menuIconsArray != "undefined" && typeof menuIconsArray[icon] != "undefined") return menuIconsArray[icon];
            // if (typeof menuIconsArray != "undefined" && typeof menuIconsArray[icon] != "undefined") return '<i class="' + menuIconsArray[icon] + '"></i>';
        },

        getActiveUserGroupLabel(loggedUserInGroupsArray) {
            return '';
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = '';
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name != "undefined" && nextLoggedUserInGroup.status == 'A') {
                    ret = ret + nextLoggedUserInGroup.group_name + ', ';
                }
            });
            return ret;
        },

        /*checkUserGroupAccess(loggedUserInGroupsArray, group_name) {
            return true;
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = false;
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name == group_name && nextLoggedUserInGroup.status == 'A') {
                    ret = true;
                }
            });
            return ret;
        },
*/
        dateToMySqlFormat(dat) {
            if (typeof dat != 'object') return dat;
            // alert( "dat::"+(dat) +  "   typeof  dat::"+(typeof dat) + "  dat::"+var_dump(dat) )

            var mm = dat.getMonth() + 1; // getMonth() is zero-based
            var dd = dat.getDate();

            return [dat.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
            ].join('-');
        },
        say(row) {
            alert('Open console and looking for response object!')
            console.log(row);
        },
        edit(value) {
            this.action.edit_value = value;
        },
        submit(res) {
            if (res.row[res.k] != this.action.edit_value) {
                let after_update = JSON.parse(JSON.stringify(res.row))
                this.$refs.my_table.set_row_undo(after_update, res.c)
                res.row[res.k] = this.action.edit_value
            }
            this.reset_edit()
        },
        reset_edit() {
            this.$refs.my_table.close()
        },
        set_checkbox(item) {
            this.action.checkbox = item
        },
        edit_row(row) {
            for (let k in row) {
                this.action.edit_row[k] = row[k]
            }
        },
        submit_row(row) {
            let after_update = JSON.parse(JSON.stringify(row))
            for (let key in this.action.edit_row) {
                row[key] = this.action.edit_row[key]
            }
            this.$refs.my_table.set_row_undo(after_update)
            this.$refs.my_table.close(true)
        },

        // timeInAgoFormat : function(value) {
        //     alert( "-2 timeInAgoFormat  value::"+(value) )
        //     return moment(value).fromNow();
        //     // return moment(value).calendar();
        // },
        //


        makeSystemLogout: function (api_version_link, show_message, bus) {
            // alert( "makeSystemLogout api_version_link::"+(api_version_link) )
            axios.post(api_version_link + 'logout').then((response) => {
                if (show_message) {
                    bus.$emit("showSnackbarMessage", {text: "You have no access to this page!", type: 'error'});
                }
                Vue.localStorage.set( 'logged_user_id', '' )
                Vue.localStorage.set( 'logged_user_username', '' )
                Vue.localStorage.set( 'logged_user_full_name', '' )
                Vue.localStorage.set( 'loggedUserProfile', {} )
                this.$router.push({path: '/login'}); 
            }).catch((error) => {
                this.showRunTimeError(error, this);
            });
        }, //makeSystemLogout() {

        getSettingsValue: function (name, default_value, api_version_link, trigger_event_name, bus) {
            axios.get(api_version_link + '/get_settings_value?name=' + name)
                .then(function (response) {
                    // alert( "getSettingsValue response.data::"+(response.data) )
                    if (typeof trigger_event_name != "undefined" && typeof bus != "undefined") {
                        // alert( "INSIDE -1 trigger_event_name"+trigger_event_name )
                        bus.$emit(trigger_event_name, response.data.settings_value);
                    }
                    //                     bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name);

                    return response.data.settings_value;
                })
                .catch(function (error) {
                    // alert( "error::"+var_dump(error) )
//                     app.showRunTimeError(error, app);
                })
        },

        formatColor: function (rgb) {
            var isOk = /^#[0-9A-F]{6}$/i.test(rgb)
            // var isOk  = /^#[0-9A-F]{6}$/i.test('#aabbcc')
            // alert( "isOk::"+isOk )
            if (isOk) return rgb;

            // alert( typeof rgb )
            if (typeof rgb != "string" || this.trim(rgb) == "") return "";
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
        },

        trim: function (str) {
            if (typeof str != "string") return "";
            return str.replace(/^\s+|\s+$/gm, '');
        },

        concatStrings: function (dataArray, splitter) {
            var ret = '';
            // alert( "dataArray::"+(typeof dataArray)+"   "+var_dump(dataArray) )
            const l = dataArray.length;
            dataArray.map((next_string, index) => {
                // next_string = jQuery.trim(next_string);
                next_string = this.trim(next_string);
                // alert( "next_string::"+(next_string) + (typeof next_string) +"  splitter::"+splitter )
                // if ( typeof next_string != "undefined" && typeof next_string != "string" ) {
                if (typeof next_string == "string") {
                    if (next_string) {
                        if (l === index + 1) {
                            ret = ret + next_string;
                        } else {
                            ret = ret + next_string + splitter;
                        }
                    } // if ( next_string ) {
                }
            });
            return ret;
        },

        showRunTimeError: function (error, app) {

            app.message = '';
            app.errorsList = [];
             // alert("  showRunTimeError  ::error"+error)

            // debugger
            if (error == "Unauthenticated." || error == "Request failed with status code 401" ) {
                app.$emit("showSnackbarMessage", {text: 'You are unauthenticated ! Please enter into the system !', type: 'success'});
                this.$router.push({path: '/login'});
            }
            if (typeof error == "string") {
                app.message = this.Capitalize(this.$t( this.commonError(error) ) );
                if ( app.message == "Unauthenticated." ) {
                    app.message = "";
                    app.$emit("showSnackbarMessage", {text: 'You are unauthenticated ! Please enter into the system !', type: 'success'});
                    this.$router.push({path: '/login'});
                }
                return app;
            }
            if (( typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.message != "undefined" ) && ( typeof error.response.data.errors == "undefined" || error.response.data.errors.length == 0)) {
                // alert("::-1   "+ error.response.data.message )
                app.message = this.commonError(error.response.data.message);
                if ( app.message == "Unauthenticated." ) {
                    app.message = "";
                    app.$emit("showSnackbarMessage", {text: 'You are unauthenticated ! Please enter into the system !', type: 'success'});
                    this.$router.push({path: '/login'});
                }
            }
            // alert( "::-02" )
            if (typeof error.response != "undefined" && typeof error.response.data != "undefined" && typeof error.response.data.errors != "undefined" /*&& error.response.data.errors.length > 0*/) {
                // alert( "::-3" )
                app.errorsList = error.response.data.errors;
            }

            return app;
        },

        commonError: function (err_text) {
            return err_text;
        },

        concatStr: function (str, max_str_length_in_listing) {
            if (typeof settings_max_str_length_in_listing == "undefined" && typeof max_str_length_in_listing != "undefined") {
                var settings_max_str_length_in_listing = max_str_length_in_listing
            }
            if (typeof str == "undefined") str = '';
            if (str.length > settings_max_str_length_in_listing) {
                return str.slice(0, settings_max_str_length_in_listing) + '...';
            }
            return str;
        },

        getNowDateTime: function () {
            return Date.now();
        },

        getNowTimestamp: function () {
            return Date.now() / 1000 | 0;
        },


        getFileSizeAsString: function (file_size) {
            if (parseInt(file_size) < 1024) {
                return file_size + 'b';
            }
            if (parseInt(file_size) < 1024 * 1024) {
                return Math.floor(file_size / 1024) + 'kb';
            }
            return Math.floor(file_size / (1024 * 1024)) + 'mb';
        },


        Uppercase: function (string) {
            return string.toUpperCase();
        },

        lowerCase: function (string) {
            return string.toLowerCase(0);
        },

        Capitalize: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },


        confirmMsg: function (question, confirm_function_code, title, icon) {
            // debugger
            if (typeof title == "undefined" || this.trim(title) == "") {
                title = 'Confirm!'
            }
            if (typeof icon == "undefined" || this.trim(icon) == "") {
                // icon = 'glyphicon glyphicon-signal'
                icon = 'confirmation_number'
            }
            $.confirm({
                icon: icon,
                title: title,
                content: question,
                confirmButton: 'YES',
                cancelButton: 'Cancel',
                confirmButtonClass: 'btn-info',
                cancelButtonClass: 'btn-danger',
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    confirm_function_code()
                }
            });
        },

        func_setting_focus: function (focus_field) {
            $('#' + focus_field).focus();
        },

        alertMsg: function (content, title, confirm_button, icon, focus_field) {
            $.alert({
                title: title,
                content: content,
                icon: ( typeof icon != 'undefined' ? icon : 'fa fa-info-circle' ),
                confirmButton: ( typeof confirm_button != 'undefined' ? confirm_button : 'OK' ),
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    setTimeout("func_setting_focus('" + focus_field + "')", 500);
                }
            });
        },

        br2nl: function (str) {
            return str.replace(/<br\s*\/?>/mg, "\n");
        },

        // nl2br: function (str) { // Inserts HTML line breaks before all newlines in a string
        //     return str.replace(/([^>])\n/g, '$1<br/>');
        // },

        nl2br: function (str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        },

        replaceAll: function (str, search, replacement) {
            return str.replace(new RegExp(search, 'g'), replacement);
        },

        prettyJSON: function (json) {
            if (json) {
                json = JSON.stringify(json, undefined, 4);
                json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }
        },


    }, // methods: {

}

function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}
