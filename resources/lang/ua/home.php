<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'home'                              => 'додому',

    'you_are_not_logged'                => 'ви не зареєстровані',
    'login'                             => 'логін',
    'entrance'                          => 'вихід',
    'register'                          => 'реєструйся',
    'start_now'                         => 'розпочати зараз',
    'welcome'                           => 'ласкаво просимо',

	'phone'                             => 'телефон',
	'place'                             => 'місце',
	'email'                             => 'електронна пошта',
	'password'                          => 'пароль',

	'most_rating_artists'               => 'більшість рейтингів співаків',
	'most_rating_songs'                 => 'Найбільш рейтингові пісні',
	'published_on'                      => 'опубліковано',

    'short'                             => [ 'year'=> 'numeric', 'month'=> 'short', 'day'=> 'numeric' ],
    'long'                              => [ 'year'=> 'numeric', 'month'=> 'short', 'day'=> 'numeric', 'weekday'=> 'short', 'hour'=> 'numeric', 'minute'=> 'numeric'],
    'cms_item_not_found'                => 'статтю не знайдено',


];
