<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'submit'                        => 'подати',
    'not_registered'                => 'не зареєстрований',
    'authorize'                     => 'авторизація',
    'with'                          => 'з',
    'about'                         => 'про',
    'invalid_access'                => 'недійсний доступ',
    'published_at'                  => 'опубліковано в',
    'confirm'                       => 'підтвердити',
    'cancel'                        => 'скасувати',
    'OK'                            => 'OK',
    'would_be_implemented_later'    => 'буде реалізована пізніше',
    'you_have_to_login'             => 'вам потрібно ввійти в систему',
    'you_have_already_voted'        => 'ви вже проголосували',
    'cms_item_not_found'            => 'статтю не знайдено',
    'similar_pages'                 => 'подібні сторінки',



];
