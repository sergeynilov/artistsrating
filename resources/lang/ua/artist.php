<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'artist'                      => 'виконавець',
    'birthday'                    => 'день народження',
    'day_of_death'                => 'день смерті',
    'site'                        => 'сайт',
    'group'                       => 'колектив',
    'solo'                        => 'соло',

    //     apple: 'no apples | one apple | {count} apples'

    'has_images'                  => '{related_images_count} зображення | {related_images_count} зображення',
    'votes_with_rating'           => 'голосів {related_votes_count} з рейтингом <b>{calc_artist_rating}</b>',
    'you_can_vote'                => 'ви можете проголосувати',
    'you_have_already_voted'      => 'ви вже проголосували',
    'artist_not_found'            => 'виконавець не знайден',
    'artist_not_active'           => 'виконавець не активний',


];
