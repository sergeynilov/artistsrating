
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'song'                        => 'пісня',
    'has_songs'                   => '{related_songs_count} пісня | пісен {related_songs_count}',
    'votes_with_rating'           => 'голосів {related_votes_count} з рейтингом <b>{calc_song_rating}</b>',
    'you_can_vote'                => 'ти можеш проголосувати',
    'you_have_already_voted'      => 'ти вже проголосував',
    'song_not_found'              => 'пісня не знайдена',
    'song_not_active'             => 'пісня не активна',


];


