<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'cart'                             => 'корзина',
    'add_to_cart'                      => 'додати до кошика',
    'artist_is_in_cart'                => 'цей виконавець вже вибраний у кошику',
    'song_is_in_cart'                  => 'ця пісня вже вибрана в кошику',
    'confirm_remove_product'           => 'підтвердити видалення продукту з кошика',
    'product_removed'                  => 'продукт видалено з кошика успішно',
    'product_to_remove'               =>  'продукт для видалення',

    'your_cart_is_empty'               => 'ваш кошик порожній',
    'search_by_product_name'           => 'пошук по назві продукту',
    'search'                           => 'пошук',

    'clear_cart_content'               => 'очистити вміст кошика',
    'confirm_clear_cart_content'       => 'ви хочете очистити вміст кошика',
    'cart_content_was_cleared'         => 'вміст кошика було успішно очищено',
    'having_coupon'                    => 'має купон / код',
    'continue_shopping'                => 'продовжити покупки',
    'checkout'                         => 'перевірити',
    'return_to_cart'                   => 'повернутися до кошика',
    'complete_order'                   => 'повне замовлення',

    'list_#'                           => '#',
    'list_image'                       => 'зображення',
    'list_product_name'                => 'назва продукту',
    'list_price'                       => 'ціна',
    'list_actions'                     => 'дії',

    'payment_detail'                   => 'платіжні реквізити',
    'your_order'                       => 'ваше замовлення',

    'name_on_card'                     => 'ім\'я на картці',
    'credit_of_debit_card'             => 'кредит дебетової картки',

    'order_completed'                  => 'замовлення успішно завершено',
    'error_order_completing'           => 'завершення виконання помилки',

    'ordered_items_at'                 => 'замовлено {cart_qty_count} пункти на суму',
    'your_search_found_no_results'     => 'у вашому запиті "{search_string}" не знайдено жодних результатів',
    'no_data_found'                    => 'даних не знайдено',


];
