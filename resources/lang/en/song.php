
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'song'                        => 'song',
    'has_songs'                   => '{related_songs_count} song | {related_songs_count} songs',
    'votes_with_rating'           => 'votes {related_votes_count} with rating <b>{calc_song_rating}</b>',
    'you_can_vote'                => 'you can vote',
    'you_have_already_voted'      => 'you have already voted',
    'song_not_found'              => 'song not found',
    'song_not_active'             => 'song is not active',


];


