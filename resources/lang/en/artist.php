<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'artist'                      => 'artist',
    'birthday'                    => 'birthday',
    'day_of_death'                => 'day of death',
    'site'                        => 'site',

    'group'                       => 'group',
    'solo'                        => 'solo',

    'has_images'                  => '{related_images_count} image | {related_images_count} images',
    'votes_with_rating'           => 'votes {related_votes_count} with rating <b>{calc_artist_rating}</b>',
    'you_can_vote'                => 'you can vote',
    'you_have_already_voted'      => 'you have already voted',
    'artist_not_found'            => 'artist not found',
    'artist_not_active'           => 'artist not active',

];
