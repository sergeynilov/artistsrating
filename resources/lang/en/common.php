<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'submit'                        => 'submit',
    'not_registered'                => 'not registered',
    'authorize'                     => 'authorize',
    'with'                          => 'with',
    'about'                         => 'about',
    'invalid_access'                => 'invalid access',
    'published_at'                  => 'published at',
    'confirm'                       => 'confirm',
    'cancel'                        => 'cancel',
    'OK'                            => 'OK',
    'would_be_implemented_later'    => 'would be implemented later',
    'you_have_to_login'             => 'you have to login to the system',
    'you_have_already_voted'        => 'you have already voted',
    'similar_pages'                 => 'similar pages',



];
