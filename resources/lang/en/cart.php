<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'cart'                             => 'shopping cart',
    'add_to_cart'                      => 'add to cart',
    'artist_is_in_cart'                => 'this artist is already selected in cart',
    'song_is_in_cart'                  => 'this song is already selected in cart',
    'confirm_remove_product'           => 'confirm removing product from cart',
    'product_removed'                  => 'product removed from cart successfully',
    'product_to_remove'               =>  'product to remove',

    'your_cart_is_empty'               => 'Your cart is empty',
    'search_by_product_name'           => 'Search by product name',
    'search'                           => 'Search',

    'clear_cart_content'               => 'clear cart content',
    'confirm_clear_cart_content'       => 'do you want to clear cart content',
    'cart_content_was_cleared'         => 'cart content was successfully cleared',
    'having_coupon'                    => 'having a coupon/code',
    'continue_shopping'                => 'continue shopping',
    'checkout'                         => 'checkout',
    'return_to_cart'                   => 'return to cart',
    'complete_order'                   => 'complete order',

    'list_#'                           => '#',
    'list_image'                       => 'image',
    'list_product_name'                => 'product name',
    'list_price'                       => 'price',
    'list_actions'                     => 'actions',

    'payment_detail'                   => 'payment details',
    'your_order'                       => 'your order',

    'name_on_card'                     => 'name on card',
    'credit_of_debit_card'             => 'credit of debit card',

    'order_completed'                  => 'order completed successfully',
    'error_order_completing'           => 'error order completing',
    'ordered_items_at'                 => 'ordered {cart_qty_count} items at ',
    'your_search_found_no_results'     => 'your search for "{search_string}" found no results',
    'no_data_found'                    => 'no data found',


];
