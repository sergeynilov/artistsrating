<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'home'                              => 'home',

    'you_are_not_logged'                => 'you are not logged',
    'login'                             => 'login',
    'entrance'                          => 'entrance',
    'register'                          => 'register',
    'start_now'                         => 'start now',
    'welcome'                           => 'welcome',

	'phone'                             => 'phone',
	'place'                             => 'place',
	'email'                             => 'email',
	'password'                          => 'password',

	'most_rating_artists'               => 'most rating artists',
	'most_rating_songs'                 => 'most rating songs',
	'published_on'                      => 'published on',

    'short'                             => [ 'year'=> 'numeric', 'month'=> 'short', 'day'=> 'numeric' ],
    'long'                              => [ 'year'=> 'numeric', 'month'=> 'short', 'day'=> 'numeric', 'weekday'=> 'short', 'hour'=> 'numeric', 'minute'=> 'numeric'],
    'cms_item_not_found'                => 'article not found',


];
