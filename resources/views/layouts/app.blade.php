<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <?php $current_template= 'artists_rating_light' ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title id="app_title">{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">--}}

    {{--<link href="/fonts/fonts.css" rel="stylesheet" type="text/css">--}}

    {{--<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">--}}
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Inconsolata:300,400,700|Material+Icons' rel="stylesheet">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/docsearch.js/2/docsearch.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">

    
{{--<link href='cdn.materialdesignicons.com/2.1.99/css/materialdesignicons.min.css' rel="stylesheet">--}}

    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">--}}


{{--<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">--}}


    <!-- Styles -->

    <link href="{{ asset('css/'.$current_template.'/app.css') }}" rel="stylesheet">

    {{--iPhone portrait 320 x 480 --}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_xs_320.css') }}" media="only screen and (min-width: 320px) and (max-width: 479px) " />

    {{--iPhone landscape 480 x 320--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_xs_480.css') }}" media="only screen and (min-width: 480px)  and (max-width: 599px) " />

    {{--Kindle portrait 600 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_xs_600.css') }}" media="only screen and (min-width: 600px)  and (max-width: 767px) " />

    {{--iPad portrait 768 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_sm.css') }}" media="only screen and (min-width: 768px)  and (max-width: 1023px) " />

    {{--iPad landscape 1024 x 768--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_md.css') }}" media="only screen and (min-width: 1024px) and (max-width: 1279px) " />

    {{--Macbook 1280 x 800--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_template.'/style_lg.css') }}" media="only screen and (min-width: 1280px)" />

</head>

<body>

<div id="app">

    <backend-app-layout></backend-app-layout>

</div>

    @include('layouts.footer')
    <script src="{{ asset('js/app.js'    ) }}{{  "?dt=".time()  }}"></script>

</body>

</html>
