<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Permission;
use App\Http\Traits\funcsTrait;
use App\User;
use App\library\ListingReturnData;


class PermissionInitData extends Seeder
{
    use funcsTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    use HasRoles;  // https://github.com/spatie/laravel-permission
    public function run()
    {
        // https://www.youtube.com/watch?v=6Hbqoo64u4A

        // Has access to backend pages
        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleUsersManager = Role::create(['name' => 'UsersManager']);
        $roleContentEditor = Role::create(['name' => 'ContentEditor']);

        // Has access to frontend pages
        $roleLoggedUser = Role::create(['name' => 'LoggedUser']);
        $roleAnonymous = Role::create(['name' => 'AnonymousUser']);


        $permissionSystem = Permission::create(['name' => IN_BACKEND_EDIT_SYSTEM_DICTIONARIES]);
        // tables : rt_settings, rt_genres

        $permissionUsersManagement = Permission::create(['name' => IN_BACKEND_EDIT_USERS_DATA]);
        // tables : rt_users->status

        $permissionSetUsersRoleStatus = Permission::create(['name' => IN_BACKEND_SET_USERS_ROLE_STATUS]);
        // tables : rt_users

        $permissionContentEditing = Permission::create(['name' => IN_BACKEND_EDIT_CMS_DATA]);
        // tables : rt_cms_items, rt_cms_item_translations, all rt_artists*,   rt_songs*, rt_tours

        $permissionPublishContent = Permission::create(['name' => IN_BACKEND_PUBLISH_CMS_DATA]);
        // tables : rt_cms_items->published



        $permissionLoggedUser = Permission::create(['name' => IN_FRONTEND_LOGGED_USER_AREA]);


        $permissionAnonymous = Permission::create(['name' => IN_FRONTEND_ALL_PUBLIC_PAGES]);

        $roleAdmin->givePermissionTo($permissionSystem);
        $roleAdmin->givePermissionTo($permissionUsersManagement);
        $roleAdmin->givePermissionTo($permissionSetUsersRoleStatus);
        $roleAdmin->givePermissionTo($permissionContentEditing);
        $roleAdmin->givePermissionTo($permissionPublishContent);


        $roleUsersManager->givePermissionTo($permissionUsersManagement);

        $roleContentEditor->givePermissionTo($permissionContentEditing);
        $roleContentEditor->givePermissionTo($permissionPublishContent);


        $roleLoggedUser->givePermissionTo($permissionLoggedUser);
        $roleAnonymous->givePermissionTo($permissionAnonymous);




        $usersList = User::getUsersList(ListingReturnData::LISTING, []);
        foreach ($usersList as $nextUser) {
//            echo '<pre>00 $nextUser->id::'.print_r($nextUser->id,true).'</pre>'.chr(13).chr(10);
            if ( in_array($nextUser->id, [1,5]) ) {
//                echo '<pre>-1 $nextUser->id::'.print_r($nextUser->id,true).'</pre>'.chr(13).chr(10);
                $nextUser->givePermissionTo(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES);
            }

            
            if ( in_array($nextUser->id, [1, 5]) ) {
                $nextUser->givePermissionTo(IN_BACKEND_SET_USERS_ROLE_STATUS);
            }


            if ( in_array($nextUser->id, [2,7, 5]) ) {
//                echo '<pre>-2 $nextUser->id::'.print_r($nextUser->id,true).'</pre>'.chr(13).chr(10);
                $nextUser->givePermissionTo(IN_BACKEND_EDIT_USERS_DATA);
            }

            if ( in_array($nextUser->id, [3,6]) ) {
//                echo '<pre>-4 $nextUser->id::'.print_r($nextUser->id,true).'</pre>'.chr(13).chr(10);
                $nextUser->givePermissionTo(IN_BACKEND_EDIT_CMS_DATA);
            }

            if ( in_array($nextUser->id, [3]) ) {
                $nextUser->givePermissionTo(IN_BACKEND_PUBLISH_CMS_DATA);
            }

            $nextUser->givePermissionTo(IN_FRONTEND_LOGGED_USER_AREA); // all users have access IN_FRONTEND_LOGGED_USER_AREA
            $nextUser->givePermissionTo(IN_FRONTEND_ALL_PUBLIC_PAGES); // all users have access IN_FRONTEND_ALL_PUBLIC_PAGES
        }



    }

}
