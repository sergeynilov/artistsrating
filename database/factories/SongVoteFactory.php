<?php

use App\SongVote;
use App\Task;
use App\library\ListingReturnData;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\SongVote::class, function (  $faker, $parentData) {
    return [
        'song_id' => $parentData['song_id'],
        'user_id' => $parentData['user_id'],
        'created_at' => $faker->dateTimeBetween($startDate = '-25 days', $endDate = '+5 days', $timezone = null)->format('Y-m-d H:i:s'),
        'vote' => $faker->randomElement( [1, 2, 3, 4, 5]),
    ];
});
