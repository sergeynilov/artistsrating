<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('status', ['N', 'A', 'I'])->default("N")->comment(' N => New(Waiting activation), A=>Active, I=>Inactive');


            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('website', 50)->nullable();

            $table->rememberToken();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->index(['created_at'], 'users_created_at_index');
            $table->index(['status'], 'users_status_index');

        });

        DB::table('users')->insert([
            'id' => 1,
            'username' => 'JohnGlads',
            'first_name' => 'John',
            'last_name' => 'Glads',
            'email' => 'JohnGlads@site.com',
            'status' => 'A',
            'password' => bcrypt('111111'),
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'username' => 'rod_bodrick',
            'first_name' => 'John',
            'last_name' => 'Glads',
            'email' => 'rod_bodrick@site.com',
            'status' => 'A',
            'password' => bcrypt('111111'),
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'username' => 'tony_black',
            'first_name' => 'Tony',
            'last_name' => 'Black',
            'email' => 'tony_black@site.com',
            'status'=> 'A',
            'password' => bcrypt('111111'),
        ]);
        DB::table('users')->insert([
            'id' => 4,
            'username' => 'adam_lang',
            'first_name' => 'Adam',
            'last_name' => 'Lang',
            'email' => 'adam_lang@site.com',
            'status'=> 'I',
            'password' => bcrypt('111111'),
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
