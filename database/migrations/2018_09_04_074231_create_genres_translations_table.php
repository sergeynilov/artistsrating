<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('genre_id')->unsigned();
            $table->string('name',100);
            $table->text('description');
            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'genre_translations_created_at_index');
            $table->index(['name'], 'genre_translations_name_index');

            $table->string('locale',2)->index();

            $table->unique(['genre_id','locale']);
            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_translations');
    }
}
