<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistImagesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_image_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_image_id')->unsigned();
            $table->text('description');
            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'artist_image_translations_created_at_index');

            $table->string('locale',2)->index();

            $table->unique(['artist_image_id','locale']);
            $table->foreign('artist_image_id')->references('id')->on('artist_images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_image_translations');
    }
}
