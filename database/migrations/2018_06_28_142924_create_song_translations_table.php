<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('song_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('song_id')->unsigned();
            $table->string('title', 100);
            $table->string('short_descr', 255);
            $table->text('description');
            $table->string('locale,2')->index();

            $table->unique(['song_id','locale']);
            $table->foreign('song_id')->references('id')->on('songs')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('songs', function (Blueprint $table) {
//            $table->dropColumn('title');
//            $table->dropColumn('short_descr');
//            $table->dropColumn('description');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIndex('cms_item_translations_title_locale_unique');
        Schema::dropIfExists('song_translations');
    }
}
