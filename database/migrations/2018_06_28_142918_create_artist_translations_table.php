<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id')->unsigned();
            $table->string('name', 50);
            $table->text('info');
            $table->string('locale',2)->index();

            $table->unique(['artist_id','locale']);
            $table->foreign('artist_id')->references('id')->on('artists')->onDelete('cascade');
            $table->timestamps();
        });

//        Schema::table('artists', function (Blueprint $table) {
//            $table->dropColumn('name');
//            $table->dropColumn('info');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIndex('cms_item_translations_title_locale_unique');
        Schema::dropIfExists('artist_translations');
    }
}
