<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_item_translations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('cms_item_id')->unsigned();
            $table->string('title', 100);
            $table->string('short_descr',255);
            $table->text('content');
            $table->string('locale',2)->index();

            $table->unique(['cms_item_id','locale']);
            $table->foreign('cms_item_id')->references('id')->on('cms_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_item_translations');
    }
}
