<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_logins', function (Blueprint $table) {
            $table->increments('id');

            $table->string( 'provider_name', 50 )->null();
            $table->string( 'username', 255 )->null();
            
            $table->integer( 'user_id' )->null()->unsigned();
            $table->foreign( 'user_id' )->references('id')->on('users');

            $table->string( 'remote_addr', 50 )->null();
            $table->boolean( 'with_success' );

            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'user_logins_created_at_index');
            $table->index(['with_success', 'remote_addr'], 'user_logins_with_success_remote_addr_index');
            $table->index(['provider_name', 'username'], 'user_logins_provider_name_username_index');
            $table->index(['provider_name', 'user_id'], 'user_logins_provider_name_user_id_index');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_logins');
    }
}
