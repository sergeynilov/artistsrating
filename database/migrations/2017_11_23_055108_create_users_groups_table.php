<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        Schema::create('groups', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 20)->unique();
            $table->string('description', 100)->unique();
            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'groups_created_at_index');
        });

        DB::table('groups')->insert([
            'id' => 1,
            'name' => 'Admin',
            'description' => 'Administrator',
        ]);
        DB::table('groups')->insert([
            'id' => 2,
            'name' => 'Manager',
            'description' => 'Manager description...',
        ]);

        DB::table('groups')->insert([
            'id' => 3,
            'name' => 'Developer',
            'description' => 'Developer description...',
        ]);


        DB::table('groups')->insert([
            'id' => 4,
            'name' => 'Employee',
            'description' => 'Employee description...',
        ]);


        Schema::create('users_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT');

            $table->smallInteger('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('RESTRICT');

            $table->timestamp('created_at')->useCurrent();
            $table->index(['created_at'], 'users_groups_created_at_index');

            $table->unique(['user_id', 'group_id'], 'users_groups_user_id_group_id_unique');
        });



        DB::commit();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::beginTransaction();

        Schema::table('users', function ($table) {
            DB::update( " DELETE FROM ".DB::getTablePrefix()."users WHERE id IN (3,4) " );
        });
        Schema::table('groups', function ($table) {
            $table->dropIndex('groups_created_at_index');
        });
        Schema::table('users_groups', function ($table) {
            $table->dropIndex('users_groups_created_at_index');
        });
        Schema::dropIfExists('users_groups');
        Schema::dropIfExists('groups');

        DB::commit();
    }
}
