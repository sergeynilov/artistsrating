<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCmsItemTranslationsAddUniqueKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_item_translations', function (Blueprint $table) {
            $table->unique(['title', 'locale'], 'cms_item_translations_title_locale_unique');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_item_translations', function (Blueprint $table) {
            $table->dropIndex('cms_item_translations_title_locale_unique');
        });
    }
}
