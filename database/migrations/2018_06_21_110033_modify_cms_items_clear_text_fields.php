<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCmsItemsClearTextFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_items', function (Blueprint $table) {
            $table->dropColumn( [ 'title', 'short_descr', 'content' ] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_items', function (Blueprint $table) {
            $table->string('title', 100);
            $table->string('short_descr',255);
            $table->text('content');
        });
    }
}
