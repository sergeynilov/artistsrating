<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');

            $table->string('card_owner', 100);
            $table->integer('discount')->nullable()->default(0);
            $table->string('discount_code')->nullable();
            $table->integer('qty_count');
            $table->decimal('price_total',10,2);
            $table->string('payment');
            $table->boolean('completed')->default(false);
            $table->string('error_message')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index(['user_id', 'completed'], 'orders_user_id_completed_index');
            $table->index(['discount', 'completed'], 'orders_discount_completed_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
