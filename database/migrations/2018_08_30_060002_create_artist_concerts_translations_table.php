<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistConcertsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_concert_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_concert_id')->unsigned();
            $table->string('place_name',100);
            $table->text('description');
            $table->timestamp('created_at')->useCurrent();

            $table->index(['created_at'], 'artist_concert_translations_created_at_index');
            $table->index(['place_name'], 'artist_concert_translations_place_name_index');

            $table->string('locale',2)->index();

            $table->unique(['artist_concert_id','locale']);
            $table->foreign('artist_concert_id')->references('id')->on('artist_concerts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_concert_translations');
    }
}
