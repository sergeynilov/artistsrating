<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

if ( !defined("IN_BACKEND_EDIT_SYSTEM_DICTIONARIES") ){      // # 1   - including artists, genres, songs, charts, settings(?)
    define("IN_BACKEND_EDIT_SYSTEM_DICTIONARIES", 'In backend edit system dictionaries');
}
if ( !defined("IN_BACKEND_EDIT_USERS_DATA") ) {              // # 2
    define("IN_BACKEND_EDIT_USERS_DATA", 'In backend edit users data');
}
if ( !defined("IN_BACKEND_SET_USERS_ROLE_STATUS") ) {        // # 3
    define("IN_BACKEND_SET_USERS_ROLE_STATUS", 'In backend set users role/status');
}
if ( !defined("IN_BACKEND_EDIT_CMS_DATA") ) {                // # 4
    define("IN_BACKEND_EDIT_CMS_DATA", 'In backend edit content(CMS) data');
}
if ( !defined("IN_BACKEND_PUBLISH_CMS_DATA") ) {            // # 5
    define("IN_BACKEND_PUBLISH_CMS_DATA", 'In backend publish content(CMS) data');
}

if ( !defined("IN_FRONTEND_LOGGED_USER_AREA") ) {           // # 6
    define("IN_FRONTEND_LOGGED_USER_AREA", 'In frontend to have access to pages under logged user(profile) and possibility to vote');
}
if ( !defined("IN_FRONTEND_ALL_PUBLIC_PAGES") ) {           // # 7
    define("IN_FRONTEND_ALL_PUBLIC_PAGES", 'In frontend to have access to all public pages');
}

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
