<?php

namespace App;

use App\MyAppModel;
use App\Http\Traits\funcsTrait;
use DB;

class Settings extends MyAppModel {
    protected $table = 'settings';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $yesNoValueArray = [true => 'Yes', false => 'No'];

    use funcsTrait;

    /* return array of key value/label pairs based on self::$yesNoValueArray - db enum key values/labels implementation */
    public static function getYesNoValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$yesNoValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$yesNoValueArray - db enum key values/labels implementation */
    public static function getYesNoLabel(string $is_active) : string
    {
        if (!empty(self::$yesNoValueArray[$is_active])) {
            return self::$yesNoValueArray[$is_active];
        }
        return '';
    }


    public static function scopeName($query, $name)
    {
        return $query->where('name', '=', $name);
    }

    public static function getValue( $name, $default_value = '' ) {
        $value= Settings::name($name)->get()->first();
        if ( empty($value->value) ) return $default_value;
        return $value->value;
    }


    public static function getSettingsList( array $filtersArray = [], bool $return_keys_array= false )
    {

        $settings_table_name= with(new Settings)->getTableName();
        $quoteModel= Settings::from(  \DB::raw(DB::getTablePrefix().$settings_table_name.' as s' ));

        $additive_fields_for_select= "";
        $fields_for_select= 's.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            $quoteModel->where( DB::raw('s.name'), '=', $filtersArray['name'] );
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of s table */
        $quoteModel->select( \DB::raw($fields_for_select) );
        $userProfilesList = $quoteModel->get();
        if ( $return_keys_array ) {
            $retArray= [];
            foreach( $userProfilesList as $next_key=>$nextUserProfile ) {
//                $retArray[$nextUserProfile->name]= addslashes(strip_tags($nextUserProfile->value));
                $retArray[$nextUserProfile->name]= $nextUserProfile->value;
            }
            return $retArray;
        }
        return $userProfilesList;

    }

}