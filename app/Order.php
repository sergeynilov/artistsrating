<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\SongOrder;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;

class Order extends MyAppModel
{

    protected $table = 'orders';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $orderCompletedValueArray = [ true => 'Is completed', false => 'Not completed' ];

    use funcsTrait;

    protected static function boot() {
        parent::boot();
    }

    protected $fillable = ['user_id', 'card_owner', 'discount', 'discount_code', 'price_total', 'qty_count', 'price_total', 'payment', 'completed', 'error_message'];

    
    /* return array of key value/label pairs based on self::$orderCompletedValueArray - db enum key values/labels implementation */
    public static function getOrderCompletedValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$orderCompletedValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$orderCompletedValueArray - db enum key values/labels implementation */
    public static function getOrderCompletedLabel(string $completed) : string
    {
        if (!empty(self::$orderCompletedValueArray[$completed])) {
            return self::$orderCompletedValueArray[$completed];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getOrdersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'o.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'desc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $order_table_name= with(new Order)->getTableName();
        $quoteModel= Order::from(  \DB::raw(DB::getTablePrefix().$order_table_name.' as o' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'o.*';

        /* Set filter condition for all nonempty values in $filtersArray */
/*        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Order::dbStrLower('name', false, false) . ' like ' . Order::dbStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Order::dbStrLower('name', false, false) . ' like ' . Order::dbStrLower( $filtersArray['name'], true,true ) . ' OR ' . Order::dbStrLower('description', false, false) . ' like ' . Order::dbStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Order::dbStrLower('description', false, false) . ' like ' . Order::dbStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }*/

        if (isset($filtersArray['completed'])) {
            $quoteModel->where( DB::raw('o.completed'), '=', $filtersArray['completed'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("o.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("o.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $song_order_table_name= DB::getTablePrefix().( with(new SongOrder)->getTableName() );
//        if ( !empty($filtersArray['show_related_songs_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $song_order_table_name . ' as sg where so.order_id = ' . 'o.id ) as related_songs_count';
//        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Order)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Order)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $ordersList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $ordersList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $ordersList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $ordersList;

    } // public static function getOrdersList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;

        $order = Order::find($id);

        if (empty($order)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $order= with(new Order)->substituteNullValues($order, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $order;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $order_id ) : array
    {
        $additional_name_validation_rule= 'check_order_unique_by_name:'.( !empty($order_id)?$order_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:100|'.$additional_name_validation_rule,
            'completed'    => 'required|in:'.with( new Order)->getValueLabelKeys( Order::getOrderCompletedValueArray(false) ),
            'description'  => 'required',
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for orders.name field */
    public static function getSimilarOrderByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Order::whereRaw( Order::dbStrLower('name', false, false) .' = '. Order::dbStrLower( Order::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function fillTranslations()
    {
//        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $cmsItemsList = Order::getOrdersList(ListingReturnData::LISTING, []);
        foreach ($cmsItemsList as $nextOrder) {
            $order_id         = $nextOrder->id;
            $name             = $nextOrder->name;
            $description      = $nextOrder->description;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newOrderTranslation= new OrderTranslation();
                $newOrderTranslation->order_id= $order_id;
                if ( $next_lang_key == 'en' ) {
                    $newOrderTranslation->name        = $name;
                    $newOrderTranslation->description = $description;
                } else {
                    $newOrderTranslation->name        = $next_lang_label . ' VERSION : ' . $name;
                    $newOrderTranslation->description = $next_lang_label . ' VERSION : ' . $description;
                }
                $newOrderTranslation->locale= $next_lang_key;

                $newOrderTranslation->created_at= now();
                $newOrderTranslation->save();
            }

        }
    }

}