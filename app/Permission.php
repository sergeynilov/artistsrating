<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\SongPermission;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;

class Permission extends MyAppModel
{

    protected $table = 'sp_pm_permissions';
    protected $primaryKey = 'id';
    public $timestamps = false;

    use funcsTrait;

    protected static function boot() {
        parent::boot();
    }


    protected $fillable = ['name'];

    public function modelHasPermissions()
    {
        return $this->hasMany('App\ModelHasPermission');
    }


    public static function getPermissionsArray($key_return = true): array
    {
        $keysArray = [
            IN_BACKEND_EDIT_SYSTEM_DICTIONARIES,
            IN_BACKEND_EDIT_USERS_DATA,
            IN_BACKEND_SET_USERS_ROLE_STATUS,
            IN_BACKEND_EDIT_CMS_DATA,
            IN_BACKEND_PUBLISH_CMS_DATA,
            IN_FRONTEND_LOGGED_USER_AREA,
            IN_FRONTEND_ALL_PUBLIC_PAGES
        ];
        $retArray  = [];

        foreach ($keysArray as $next_key) {
            $permissionItem = Permission::getSimilarPermissionByName($next_key);
            if (!empty($permissionItem->id)) {
                if ($key_return) {
                    $retArray[] = ['key' => $permissionItem->id, 'label' => $permissionItem->name];
                } else {
                    $retArray[$permissionItem->id] = $permissionItem->name;
                }
            }
        } // foreach( $keysArray as $next_key ) {

        return $retArray;
    }

    /* check if provided name is unique for permissions.name field */
    public static function getSimilarPermissionByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Permission::whereRaw( Permission::dbStrLower('name', false, false) .' = '. Permission::dbStrLower( Permission::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

}