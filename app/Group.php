<?php

namespace App;

use DB;

class Group__DEL extends MyAppModel
{

    protected $table = 'groups';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description',
    ];



    /* check if provided name is unique for groups.name field */
    public static function getSimilarGroupByName( string $name, int $id= null, bool $return_count = false )
    {
        //        $cmsItemRow = CmsItem::whereRaw( CmsItem::dbStrLower('alias', false, false) .' = '. CmsItem::dbStrLower($alias, true,false) )->get();

        $quoteModel = Group::whereRaw( Group::dbStrLower('name', false, false) .' = '. Group::dbStrLower( Group::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get()->first();
        return $retRow;
    }


}