<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use App\Config;

use Auth;
use Carbon\Carbon;

use App\Http\Traits\funcsTrait;
use App\MyAppModel;
use App\Settings;
use App\User;
use App\library\ListingReturnData;
use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class ArtistImageTranslation extends MyAppModel
{
    use funcsTrait;
    protected $table = 'artist_image_translations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['artist_id', 'locale', 'name', 'info', 'created_at'];

    public static function getRowByIdLocale( int $artist_id,  string $locale, $return_count= false )
    {
        $quoteModel = ArtistImageTranslation::where( 'artist_id',  $artist_id )->where( 'locale',  $locale );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get()->first();
        if ( empty($retRow) ) return false;
        return $retRow;
    }

    public static function getSimilarArtistImageTranslationByName( string $name, string $locale= '', int $id= null, $return_count = false )
    {
        $quoteModel = ArtistImageTranslation::whereRaw( ArtistImageTranslation::dbStrLower('name', false, false) .' = '. ArtistImageTranslation::dbStrLower(ArtistImageTranslation::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( !empty( $locale ) ) {
            $quoteModel = $quoteModel->where( 'locale', $locale );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getValidationRulesArray( $artist_id ) : array
    {
        $additional_name_validation_rule= ''; //'check_artist_unique_by_name:'.( !empty($artist_id)?$artist_id:'');
        $validationRulesArray = [
            'name'        => 'required|max:50|'.$additional_name_validation_rule,
            'info'        => '',
        ];
        return $validationRulesArray;

    }


}