<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use App\MyAppModel;
use App\Song;
use App\Genre;
use App\SongTranslation;
use App\Config;
use App\library\ListingReturnData;

class SongGenre extends MyAppModel
{

    protected $table = 'song_genres';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ "song_id", "genre_id" ];
    public static function getSongGenresList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'sg.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $song_genre_table_name= with(new SongGenre)->getTableName();
        $quoteModel= SongGenre::from(  \DB::raw(DB::getTablePrefix().$song_genre_table_name.' as sg' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'sg.*';

        if (!empty($filtersArray['song_id'])) {
            $quoteModel->where(DB::raw('sg.song_id'), '=', $filtersArray['song_id']);
        }

        if (!empty($filtersArray['genre_id'])) {
            $quoteModel->where(DB::raw('sg.genre_id'), '=', $filtersArray['genre_id']);
        }

        if (!empty($filtersArray['song_id'])) {
            $quoteModel->where(DB::raw('sg.song_id'), '=', $filtersArray['song_id']);
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "sg.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "sg.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }
//        $quoteModel->whereRaw( "sg.id <= 10 or sg.id >= 74");

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }



        if ( !empty($filtersArray['show_song_name']) and !empty($filtersArray['locale']) ) { // need to join in select sql username and user_is_active field of author of uc
//            echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
//            die("-1 XXZ=====");
            $songs_table_name= DB::getTablePrefix() . ( with(new Song)->getTableName() );
            $song_translations_table_name=  DB::getTablePrefix().with(new SongTranslation)->getTableName();
            $quoteModel->join(\DB::raw($song_translations_table_name . ' as s_t '), \DB::raw('s_t.song_id'), '=', \DB::raw('sg.song_id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ', s.id as song_id, s_t.title as song_title, s.slug as song_slug, s.is_active as song_is_active, s.ordering as song_ordering, s_t.description as song_description, s_t.short_descr as song_short_descr' ;
            $quoteModel->join( \DB::raw($songs_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('sg.song_id') );
        } // if ( !empty($filtersArray[show_song_name])  ) { // need to join in select sql username and user_is_active field of author of uc


        if ( !empty($filtersArray['show_genre_name']) and !empty($filtersArray['locale']) ) { // need to join in select sql genres fields
            $genres_table_name= DB::getTablePrefix() . ( with(new Genre)->getTableName() );
            $genre_translations_table_name=  DB::getTablePrefix().with(new GenreTranslation)->getTableName();
            $quoteModel->join(\DB::raw($genre_translations_table_name . ' as g_t '), \DB::raw('g_t.genre_id'), '=', \DB::raw('sg.genre_id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ', g.id as genre_id, g.slug as genre_slug, g_t.name as genre_name, g.published as genre_published, g_t.description as genre_description' ;
            $quoteModel->join( \DB::raw($genres_table_name . ' as g '), \DB::raw('g.id'), '=', \DB::raw('sg.genre_id') );
        } // if ( !empty($filtersArray[show_genre_name])  ) {



        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new SongGenre)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new SongGenre)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $songGenresList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $songGenresList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $songGenresList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $songGenresList;

    } // public static function getSongGenresList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $songGenre = SongGenre::find($id);

        if (empty($songGenre)) return false;
        return $songGenre;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function getValidationRulesArray($song_genre_id= null) : array
    {
        $additional_image_name_validation_rule= 'check_song_genre_unique_by_image_name:'.( !empty($song_genre_id)?$song_genre_id:'');
        $additional_image_name_valid_extension_validation_rule= 'check_image_name_valid_extension:'.( !empty($song_genre_id)?$song_genre_id:'');
        $validationRulesArray = [
            'song_genre_id'           => 'required|exists:'.( with(new SongGenre)->getTableName() ).',id',
            'user_id'                => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'image_name'               => 'required|max:255|' . $additional_image_name_validation_rule,
            'extension'              => 'required|max:10|' . $additional_image_name_valid_extension_validation_rule,
            'info'                   => 'nullable',
        ];
        return $validationRulesArray;
    }




}
