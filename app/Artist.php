<?php
namespace App;

use DB;
use Carbon\Carbon;

use App\MyAppModel;
use App\User;
use App\ArtistConcert;
use App\ArtistVote;
use App\SongArtist;
use App\ArtistImage;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;
use Cviebrock\EloquentSluggable\Sluggable;
//use App\Events\ArtistUpdatingEvent;
//use App\Rules\CheckArtistParticipantSelected;

class Artist extends MyAppModel
{
    use funcsTrait;
    use Sluggable;

    protected $table = 'artists';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $artistIsActiveValueArray = [ 'A' => 'Is active', 'I' => 'Is inactive', 'N' => 'New' ];
    private static $artistSingleValueArray = [ 0 => 'Group', 1 => 'Solo' ];
    private static $artistHasConcertsValueArray = [ 0 => 'Has no concerts', 1 => 'Has concerts' ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
              'source' => 'localename'
            ]
        ];
    }

    public function getLocalenameAttribute() {
        $request= request();
        $requestData= $request->all();
        return $requestData['name'];
    }


    public function artistConcerts()
    {
        return $this->hasMany('App\ArtistConcert');
    }

    public function songArtists()
    {
        return $this->hasMany('App\SongArtist');
    }

    public function artistVotes()
    {
        return $this->hasMany('App\ArtistVote');
    }

    public function artistImages()
    {
        return $this->hasMany('App\ArtistImage');
    }


    protected static function boot() {
        parent::boot();


        self::deleting(function($artist) {
            $artist->artistConcerts()->delete();
            $artist->artistImages()->delete();
            $artist->artistVotes()->delete();
            $artist->songArtists()->delete();


        });
    }

    /* return array of key value/label pairs based on self::$artistIsActiveValueArray - db enum key values/labels implementation */
    public static function getArtistIsActiveValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistIsActiveValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistIsActiveValueArray - db enum key values/labels implementation */
    public static function getArtistIsActiveLabel(string $is_active) : string
    {
        if (!empty(self::$artistIsActiveValueArray[$is_active])) {
            return self::$artistIsActiveValueArray[$is_active];
        }
        return '';
    }



    /* return array of key value/label pairs based on self::$artistHasConcertsValueArray - db enum key values/labels implementation */
    public static function getArtistHasConcertsValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistHasConcertsValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistHasConcertsValueArray - db enum key values/labels implementation */
    public static function getArtistHasConcertsLabel(string $has_concerts) : string
    {
        if (isset(self::$artistHasConcertsValueArray[$has_concerts])) {
            return self::$artistHasConcertsValueArray[$has_concerts];
        }
        return 'Has no concerts';
    }


    /* return array of key value/label pairs based on self::$artistSingleValueArray - db enum key values/labels implementation */
    public static function getArtistSingleValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistSingleValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistSingleValueArray - db enum key values/labels implementation */
    public static function getArtistSingleLabel(string $single) : string
    {
        if (!empty(self::$artistSingleValueArray[$single])) {
            return self::$artistSingleValueArray[$single];
        }
        return '';
    }


    protected $fillable = [/*'name', */ 'ordering', 'is_active', 'has_concerts', 'single', 'email', 'birthday', 'day_of_death', 'site', /*'info',*/ 'updated_at'];


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getArtistsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'a.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $artist_table_name= with(new Artist)->getTableName();
        $quoteModel= Artist::from(  \DB::raw(DB::getTablePrefix().$artist_table_name.' as a' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'a.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Artist::dbStrLower('name', false, false) . ' like ' . Artist::dbStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Artist::dbStrLower('name', false, false) . ' like ' . Artist::dbStrLower( $filtersArray['name'], true,true ) . ' OR ' . Artist::dbStrLower('description', false, false) . ' like ' . Artist::dbStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Artist::dbStrLower('description', false, false) . ' like ' . Artist::dbStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

/*        $is_participant_user_id_joined= false;
        if (!empty($filtersArray['participant_user_id'])) {
            $artist_participant_table_name= DB::getTablePrefix().with(new ArtistParticipant)->getTableName();
            $quoteModel->join( \DB::raw($artist_participant_table_name . ' as ucp '), \DB::raw('ucp.artist_id'), '=', \DB::raw('a.id') );
            $quoteModel->where( \DB::raw('ucp.user_id'), '=', $filtersArray['participant_user_id'] );
            $is_participant_user_id_joined= true;
        }
*/
        if (!empty($filtersArray['has_concerts'])) {
            $quoteModel->where( DB::raw('a.has_concerts'), '=', $filtersArray['has_concerts'] );
        }

        if (!empty($filtersArray['is_active'])) {
            $quoteModel->where( DB::raw('a.is_active'), '=', $filtersArray['is_active'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("a.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("a.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }
        if (isset($filtersArray['locale'])) {
            $artist_translations_table_name=  DB::getTablePrefix().with(new ArtistTranslation)->getTableName();
            $quoteModel->join(\DB::raw($artist_translations_table_name . ' as a_t '), \DB::raw('a_t.artist_id'), '=', \DB::raw('a.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", name, info";
        }



        //        $quoteModel->whereRaw( DB::raw("a.id = 9 or a.id = 12 ") );

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $artist_message_table_name= DB::getTablePrefix().( with(new ArtistMessage)->getTableName() );
//        if ( !empty($filtersArray['show_artist_messages_count_by_user_id']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_message_table_name . ' as ucm_1 where ucm_1.artist_id = ' . 'a.id and ucm_1.user_id = '.$filtersArray['show_artist_messages_count_by_user_id'].' ) as artist_messages_count_by_user_id';
//        }

//        if ( !empty($filtersArray['show_artist_messages_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_message_table_name . ' as ucm where ucm.artist_id = ' . 'a.id ) as artist_messages_count';
//        }
        // show_related_votes
        $artist_vote_table_name= DB::getTablePrefix().( with(new ArtistVote)->getTableName() );
        if ( !empty($filtersArray['show_related_votes']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_count' .
                                           ', ( select sum(av.vote) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_sum';
        }

        // show_artist_participants_count
        $artist_concert_table_name= DB::getTablePrefix().( with(new ArtistConcert)->getTableName() );
        if ( !empty($filtersArray['show_future_concerts_count']) ) {
            $today= Carbon::today()->format('Y-m-d');
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_concert_table_name . ' as ac_f where ac_f.artist_id = ' . 'a.id and ac_f.'."event_date >'".$today .
                " 00:00:00'".' ) as related_future_concerts_count';
        }
        if ( !empty($filtersArray['show_related_concerts_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_concert_table_name . ' as ac where ac.artist_id = ' . 'a.id ) as related_concerts_count';
        }

        /*        if (!empty($filtersArray['only_future'])) {
            $today= Carbon::today()->format('Y-m-d');
            $quoteModel->whereRaw( "event_date >'".$today . " 00:00:00'" );
//            $quoteModel->whereRaw( DB::raw("ac.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
*/
        if ( !empty($filtersArray['show_manager_name'])  ) { // need to join in select sql username and user_is_active field of author of uc
//            echo '<pre>-2::</pre>';
            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
            $additive_fields_for_select .= ', u_m.name as manager_name, u_m.is_active as  manager_is_active' ;
            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
        } // if ( !empty($filtersArray[show_manager_name])  ) { // need to join in select sql username and user_is_active field of author of uc


        $artist_image_table_name= DB::getTablePrefix().( with(new ArtistImage)->getTableName() );
        if ( !empty($filtersArray['show_related_images_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_image_table_name . ' as ai where ai.artist_id = ' . 'a.id ) as related_images_count';
        }
        $artist_image_table_name= DB::getTablePrefix().( with(new ArtistImage)->getTableName() );
        if ( !empty($filtersArray['show_related_images_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_image_table_name . ' as ai where ai.artist_id = ' . 'a.id ) as related_images_count';
        }

        $song_artist_table_name= DB::getTablePrefix().( with(new SongArtist)->getTableName() );
        if ( !empty($filtersArray['show_related_songs_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $song_artist_table_name . ' as sa where sa.artist_id = ' . 'a.id ) as related_songs_count';
        }
        /*             $filtersArray['show_related_concerts_count']= 1;
            $filtersArray['show_related_future_concerts_count']= 1;
            $filtersArray['show_related_songs_count']= 1;
 */

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Artist)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Artist)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $artistsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $artistsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $artistsList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $artistsList;

    } // public static function getArtistsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
//        if (!empty($additiveParams['show_manager_name']) ) { // need to join in select sql username and user_is_active field of author of artist
//            $additive_fields_for_select= "";
//            $fields_for_select= 'a.*';
//            $artist_table_name= with(new Artist)->getTableName();
//            $quoteModel= Artist::from(  \DB::raw(DB::getTablePrefix().$artist_table_name.' as uc' ));
//            $quoteModel->where( \DB::raw('a.id'), '=', $id );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );
//            $additive_fields_for_select .= ', u_m.name as manager_name, u_m.is_active as  user_is_active' ;
//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
//            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
//            $quoteModel->select( \DB::raw($fields_for_select) );
//            $artistRows = $quoteModel->get();
//            if (!empty($artistRows[0]) and get_class($artistRows[0]) == 'App\Artist' ) {
//                $is_row_retrieved= true;
//                $artist= $artistRows[0];
//            }
//        } // if (!empty($additiveParams['show_manager_name']) ) { // need to join in select sql username and user_is_active field of author of artist
        if ( !empty($additiveParams['show_related_songs_count']) or  !empty($additiveParams['show_future_concerts_count']) or !empty($additiveParams['show_related_votes'])  or !empty($additiveParams['show_related_images_count']) or !empty($additiveParams['locale']) ) {
            $today= Carbon::today()->format('Y-m-d');
            $fields_for_select= 'a.*';
            $additive_fields_for_select= '';
            $artist_table_name= with(new Artist)->getTableName();
            $quoteModel= Artist::from(  \DB::raw(DB::getTablePrefix().$artist_table_name.' as a' ));
            $quoteModel->where( \DB::raw('a.id'), '=', $id );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );

            if ( !empty($additiveParams['show_future_concerts_count']) ) {
                $artist_concert_table_name  = DB::getTablePrefix() . (with(new ArtistConcert)->getTableName());
                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_concert_table_name . ' as ac where ac.artist_id = ' . 'a.id and ac.' . "event_date >'" . $today . " 00:00:00'" . ' ) as   artist_future_concerts_count';
            } // if ( !empty($additiveParams['show_future_concerts_count']) ) {


            if ( !empty($additiveParams['show_related_images_count']) ) {
                $artist_image_table_name  = DB::getTablePrefix() . (with(new ArtistImage)->getTableName());
                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_image_table_name . ' as ai where ai.artist_id = ' . 'a.id ) as related_images_count';
            } // if ( !empty($additiveParams['show_related_images_count']) ) {



            if ( !empty($additiveParams['show_related_songs_count']) ) {
                $song_artist_table_name  = DB::getTablePrefix() . (with(new SongArtist)->getTableName());
                $additive_fields_for_select .= ', ( select count(*) from ' . $song_artist_table_name . ' as sa where sa.artist_id = ' . 'a.id ) as related_songs_count';
            } // if ( !empty($additiveParams['show_related_songs_count']) ) {


            if ( !empty($additiveParams['show_related_votes']) ) {
                $artist_vote_table_name= DB::getTablePrefix().( with(new ArtistVote)->getTableName() );
                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_count' .
                                               ', ( select sum(av.vote) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_sum';

//                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as ac where ac.artist_id = ' . 'a.id and ac.' . "event_date >'" . $today . " 00:00:00'" . ' ) as   artist_future_concerts_count';
            } // if ( !empty($additiveParams['show_related_votes']) ) {

//            echo '<pre>$artist->$additiveParams::'.print_r($additiveParams,true).'</pre>';
            if (isset($additiveParams['locale'])) {
                $artist_translations_table_name=  DB::getTablePrefix().with(new ArtistTranslation)->getTableName();
                $quoteModel->join(\DB::raw($artist_translations_table_name . ' as a_t '), \DB::raw('a_t.artist_id'), '=', \DB::raw('a.id'));
                $quoteModel->where( 'locale', '=', $additiveParams['locale'] );
                $additive_fields_for_select .= ", a_t.name, a_t.info";
            }

//            echo '<pre>$artist->$additive_fields_for_select::'.print_r($additive_fields_for_select,true).'</pre>';

//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $artistRows = $quoteModel->get();
            if (!empty($artistRows[0]) and get_class($artistRows[0]) == 'App\Artist' ) {
                $is_row_retrieved= true;
                $artist= $artistRows[0];
            }


            // 'show_future_concerts_count'=> 1 , 'show_related_votes'=> 1
        } // if ( !empty($additiveParams['show_related_songs_count']) or  !empty($additiveParams['show_future_concerts_count']) or !empty($additiveParams['show_related_votes'])  or !empty($additiveParams['show_related_images_count']) ) {

        if ( !$is_row_retrieved ) {
            $artist = Artist::find($id);
        }

        if (empty($artist)) return false;
//        echo '<pre>====$additiveParams::'.print_r($additiveParams,true).'</pre>';
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $artist= with(new Artist)->substituteNullValues($artist, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $artist;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $artist_id, $locale ) : array
    {
//        echo '<pre>++ getValidationRulesArray::'.print_r($_POST,true).'</pre>';
        with(new Artist)->debToFile(' Artist ::getValidationRulesArray $artist_id::' . print_r( $artist_id, true ) );

        $additional_name_validation_rule= 'check_artist_unique_by_name:'.( !empty($artist_id)?$artist_id:'') .','. $locale;
        $validationRulesArray = [
            'name'         => 'required|max:100|'.$additional_name_validation_rule,
            'ordering'     => 'required|numeric|min:1',
            'is_active'    => 'required|in:'.with( new Artist)->getValueLabelKeys( Artist::getArtistIsActiveValueArray(false) ),
            'has_concerts' => 'required|in:'.with( new Artist)->getValueLabelKeys( Artist::getArtistHasConcertsValueArray(false) ),
            'birthday'     => '',
            'day_of_death' => '',
            'site'         => '',
            'info'         => 'required',
        ];
        return $validationRulesArray;
    }

    /* check if provided slug is unique for artists.slug field */
    public static function getSimilarArtistBySlug( string $slug, int $id= null, bool $return_count = false )
    {
        $quoteModel = Artist::whereRaw( Artist::dbStrLower('slug', false, false) .' = '. Artist::dbStrLower( Artist::pgEscape($slug), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function fillTranslations()
    {
        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $cmsItemsList = Artist::getArtistsList(ListingReturnData::LISTING, []);
        foreach ($cmsItemsList as $nextArtist) {
            $artist_id       = $nextArtist->id;
            $name             = $nextArtist->name;
            $info       = $nextArtist->info;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newArtistTranslation= new ArtistTranslation();
                $newArtistTranslation->artist_id= $artist_id;
                if ( $next_lang_key == 'en' ) {
                    $newArtistTranslation->name       = $name;
                    $newArtistTranslation->info = $info;
                } else {
                    $newArtistTranslation->name       = $next_lang_label . ' VERSION : ' . $name;
                    $newArtistTranslation->info = $next_lang_label . ' VERSION : ' . $info;
                }
                $newArtistTranslation->locale= $next_lang_key;

                $newArtistTranslation->created_at= now();
                $newArtistTranslation->save();
            }

        }
    }

}

