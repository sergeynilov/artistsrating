<?php namespace App\library {
    use WBoyz\LaravelEnum\BaseEnum;
    class PgDataType extends BaseEnum
    {
        const STRING = 1;
        const NUMERIC = 2;
        const BOOLEAN = 3;
        const ARRAY = 4;
        const DATE = 5;
        const DATETIME = 6;
    }

}