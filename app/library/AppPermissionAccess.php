<?php namespace App\library {

    use WBoyz\LaravelEnum\BaseEnum;

    class AppPermissionAccess extends BaseEnum
    {
        const IN_BACKEND_EDIT_SYSTEM_DICTIONARIES = 1;
        const IN_BACKEND_EDIT_USERS_DATA = 2;
        const IN_BACKEND_SET_USERS_ROLE_STATUS = 3;
        const IN_BACKEND_EDIT_CMS_DATA = 4;
        const IN_BACKEND_PUBLISH_CMS_DATA = 5;
        const IN_FRONTEND_LOGGED_USER_AREA = 6;
        const IN_FRONTEND_ALL_PUBLIC_PAGES = 7;
    }
}