<?php namespace App\library {

    use WBoyz\LaravelEnum\BaseEnum;

    class AppPermissionAccessReturnType extends BaseEnum
    {
        const STRING_RETURN_TYPE = 1;
        const NUMBER_RETURN_TYPE = 2;
        const KEY_LABEL_ARRAY_RETURN_TYPE = 3;
        const ASSOC_ARRAY_RETURN_TYPE = 4;
    }
}