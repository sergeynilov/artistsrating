<?php

namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Config;
use Illuminate\Validation\Rule;

use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use App\library\ListingReturnData;
use App\MyAppModel;
use App\UsersLogins;
//use App\MyAppModel;
use App\Http\Traits\funcsTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use funcsTrait;
    use HasRoles;  // https://github.com/spatie/laravel-permission
    protected $guard_name = 'web'; // or whatever guard you want to use

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $userAvatarPropsArray = [];

    protected static $img_filename_max_length= 50;
    protected static $img_preview_width= 128;
    protected static $img_preview_height= 96;

    public $timestamps = false;

    protected $fillable = ['username', 'email', 'status', 'first_name', 'last_name', 'phone', 'website', 'password', 'provider_name', 'provider_id' , 'avatar' ];

    protected $hidden = ['password', 'remember_token'];

    private static $userStatusLabelValueArray = [ 'A' => 'Active', 'I' => 'Inactive', 'N' => 'New' ];

    public function getTableName(): string
    {
        return $this->table;
    }

    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    public function usersLogins()
    {
        return $this->hasMany('App\UsersLogins');
    }


    // file:///_wwwroot/lar/ArtistsRating/storage/app/public/user-avatars
//    public function usersGroups()
//    {
//        return $this->hasMany('App\UsersGroups');
//    }



    protected static function boot() {
        parent::boot();
        self::deleting(function($user) {
//            $user->usersGroups()->delete();
            $user_user_avatar_filename= User::getUserAvatarPath($user->id,$user->avatar, true);
//            with(new MyAppModel)->deleteFileByPath($user_user_avatar_filename, true);

            foreach ( $user->usersLogins()->get() as $nextUsersLogin ) {
                $nextUsersLogin->delete();
            }

        });
    }


    public static function getUserStatusValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$userStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getUserStatusLabel(string $status): string
    {
        if ( ! empty(self::$userStatusLabelValueArray[$status])) {
            return self::$userStatusLabelValueArray[$status];
        }

        return '';
    }


/*    public static function getUsersGroupsByUserId(int $user_id, $show_group_title = false)
    {
        $usersGroupsList = DB::table('users_groups AS gr')->where('gr.user_id', $user_id)->get();
        if ( ! $show_group_title) {
            return $usersGroupsList;
        }

        return $usersGroupsList;
    }*/


    /* return data array by keys id/name based on filters/ordering... */
    public static function getUsersSelectionList(bool $key_return = true, array $filtersArray = [] , string $order_by = 'name', string $order_direction = 'asc'): array
    {
        $usersList = User::getUsersList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray  = [];
        foreach ($usersList as $nextUser) {
            if ($key_return) {
                $resArray[] = [ 'key' => $nextUser->id, 'label' => $nextUser->name . ' (' . $nextUser->email . ')' ];
            } else {
                $resArray[$nextUser->id] = $nextUser->name . ' (' . $nextUser->email . ')';
            }
        }

        return $resArray;
    }


    public function getFirstNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getLastNameAttribute($value)
    {
        return ucwords($value);
    }

    /* return data array by keys id/title based on filters/ordering... */
    public static function getUsersList(int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param = 0)
    {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) {
            $order_by = 'username';
        }  // set default ordering
        if (empty($order_direction)) {
            $order_direction = 'asc';
        }

        $limit                   = ! empty($filtersArray['limit']) ? $filtersArray['limit'] : '';
        $user_table_name         = DB::getTablePrefix() . with(new User)->getTableName();
        $groups_table_name       = DB::getTablePrefix() . (with(new MyAppModel)->getTableName());
//        $users_groups_table_name = DB::getTablePrefix() . (with(new UsersGroups)->getTableName());
        $quoteModel              = User::from(\DB::raw($user_table_name . ' as u'));
        if ($listingReturnData != ListingReturnData::ROWS_COUNT) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select = "";
        $fields_for_select          = 'u.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if ( ! empty($filtersArray['name'])) {
            $quoteModel->whereRaw(MyAppModel::pgStrLower('name', false, false) . ' like ' . MyAppModel::pgStrLower($filtersArray['name'], true, true));
        }

//        if ( !empty($filtersArray['show_users_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $user_table_name . ' as ci where ci.user_id = ' . \DB::raw('u.id').' ) as users_count';
//        }

/*        $is_user_group_joined = false;
        if ( ! empty($filtersArray['show_user_groups']) and $listingReturnData != ListingReturnData::ROWS_COUNT) {
            $additive_fields_for_select .= ", array_to_string(array_agg(  g.name  ORDER BY g.name   ),',' ) as group_names ";
            if ( ! $is_user_group_joined) {
                $quoteModel->join(\DB::raw($users_groups_table_name . ' as ug '), \DB::raw('ug.user_id'), '=', \DB::raw('u.id'));
                $quoteModel->join(\DB::raw($groups_table_name . ' as g '), \DB::raw('g.id'), '=', \DB::raw('ug.group_id'));
            }
            $is_user_group_joined = true;
        }*/

        // ,       'show_avatar'=> 1, 'show_avatar_info'=> 1


        if ( ! empty($filtersArray['status'])) {
            $quoteModel->where(\DB::raw('u.status'), '=', $filtersArray['status']);
        }


        if ( ! empty($filtersArray['in_groups'])) {
            $groupsArray = [];
            if (is_string($filtersArray['in_groups'])) {
                $groupsArray = with(new User)->pregSplit('/,/', $filtersArray['in_groups']);
            }
            if (is_array($filtersArray['in_groups'])) {
                $groupsArray = $filtersArray['in_groups'];
            }
            if ( ! $is_user_group_joined) {
                $quoteModel->join(\DB::raw($users_groups_table_name . \DB::raw(' as ug ')), \DB::raw('ug.user_id'), '=', \DB::raw('u.id'));
            }
            $quoteModel->whereIn(\DB::raw('ug.group_id'), $groupsArray);
        }

        if ( ! empty($filtersArray['created_at_from'])) {
            $quoteModel->where(\DB::raw('created_at'), '>=', "'" . $filtersArray['created_at_from'] . "'");
        }
        if ( ! empty($filtersArray['created_at_till'])) {
            $quoteModel->where(\DB::raw('created_at'), '<=', "'" . $filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ($listingReturnData != ListingReturnData::ROWS_COUNT) {
            $quoteModel->groupBy(\DB::raw('u.id'));
        }


        if ( ! empty($limit) and (int)$limit > 0) {
            $quoteModel = $quoteModel->take($limit);
        }
        if ($listingReturnData == ListingReturnData::ROWS_COUNT) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $product_user_table_name= DB::getTablePrefix().( with(new ProductUser)->getTableName() );
//        if ( !empty($filtersArray['show_products_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $product_user_table_name . ' as pc where pc.user_id = ' . 'u.id ) as user_products_count';
//        }

        $fields_for_select .= ' ' . $additive_fields_for_select; /* add all custom fields to fields of u table */
        $items_per_page    = with(new MyAppModel)->getItemsPerPage();
        $quoteModel->select(\DB::raw($fields_for_select));
        $data_retrieved = false;
        if ($listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and ( ! empty($page_param) and with(new User)->isPositiveNumeric($page_param))) { /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $usersList      = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved = true;
        }
        if ($listingReturnData == ListingReturnData::PAGINATION_BY_URL and ! $data_retrieved) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $usersList      = $quoteModel->paginate($items_per_page);
            $data_retrieved = true;
        }
        if ( ! $data_retrieved) {
            $usersList      = $quoteModel->get();
            $data_retrieved = true;
        }


        $base_url= with(new User)->getBaseUrl();
        $avatarsExtensionsArray= \Config::get('app.images_extensions');

//        echo '<pre>$base_url::'.print_r($base_url,true).'</pre>';
//        echo '<pre>$avatarsExtensionsArray::'.print_r($avatarsExtensionsArray,true).'</pre>';
        
        foreach ($usersList as $next_key => $nextUser) { // iterate al users in retrieved rows

                if ( ! empty($filtersArray['show_avatar']) and !empty($nextUser->avatar)) {
                    
                    $next_user_avatar           = User::getUserAvatarPath($nextUser->id, $nextUser->avatar, false);
                    
                    $next_user_url              = $base_url . Storage::disk('local')->url($next_user_avatar);
//                    echo '<pre>$next_user_url::'.print_r($next_user_url,true).'</pre>';
                    $next_user_avatar_extension = with(new User)->getFilenameExtension($next_user_avatar);
//                    echo '<pre>$next_user_avatar_extension::'.print_r($next_user_avatar_extension,true).'</pre>';
                    $file_exists                     = Storage::disk('local')->exists('public/' . $next_user_avatar);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                    // file:///_wwwroot/lar/ArtistsRating/storage/app/public/user_avatars/avatar_20/asa-moasa-fesad.png
                    if ($file_exists and in_array($next_user_avatar_extension, $avatarsExtensionsArray)) { // that is avatar - get all props
                        $avatar            = $nextUser->avatar;
                        $avatar_path      = public_path('storage/' . $next_user_avatar);
                        $avatar_url       = $next_user_url;
                        $avatarPropsArray  = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $avatar_url];
                        $previewSizeArray = with(new MyAppModel)->getImageShowSize($avatar_path, self::$img_preview_width, self::$img_preview_height);
//                            echo '<pre>::'.print_r($previewSizeArray,true).'</pre>';
                        if ( ! empty($previewSizeArray['width'])) {
                            $avatarPropsArray['preview_width']  = $previewSizeArray['width'];
                            $avatarPropsArray['preview_height'] = $previewSizeArray['height'];
                        }
                        $nextUserImgPropsArray = MyAppModel::getImageProps($avatar_path, $avatarPropsArray);
//                            echo '<pre>$nextUserImgPropsArray::'.print_r($nextUserImgPropsArray,true).'</pre>';
                        if ( ! empty($nextUserImgPropsArray) and is_array($nextUserImgPropsArray)) {
                            foreach ($nextUserImgPropsArray as $next_user_avatar_key => $next_user_avatar_value) {
                                $usersList[$next_key][$next_user_avatar_key] = $next_user_avatar_value;
                            }
                        }
//                        die("-1 XXZ OINSIDE!");
                    } // if ( $file_exists and in_array($nextUser->extension, $avatarsExtensionsArray) ) { // that is avatar - get all props
/*                    if ($file_exists and ! in_array($next_user_avatar_extension, $avatarsExtensionsArray)) { // that is NOT avatar - get only some props
                        $avatar                                    = $nextUser->avatar;
                        $avatar_path                               = public_path('storage/' . $next_user_avatar);
                        $avatar_url                                = $next_user_url;
                        $avatarPropsArray                          = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $avatar_url];
                        $nextUserImgPropsArray = User::getAvatarProps($avatar_path, $avatarPropsArray);
//                            echo '<pre>$nextUserImgPropsArray::'.print_r($nextUserImgPropsArray,true).'</pre>';
                        if ( ! empty($nextUserImgPropsArray) and is_array($nextUserImgPropsArray)) {
                            foreach ($nextUserImgPropsArray as $next_document_img_key => $next_document_img_value) {
                                $documentCategoriesList[$next_key][$next_document_img_key] = $next_document_img_value;
                            }
                        }
                    }*/
                } // if ( ! empty($filtersArray['show_avatar'])) {
        } // foreach( $usersList as $next_key=>$nextUser ) { // iterate al users in retrieved rows


        return $usersList;

    } // public static function getUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {

    public static function getRowById(int $id, array $additiveParams = [])
    {                                 //show_avatar
        if (empty($id)) return false;
        $user = User::find($id);
        if ($user == null) return false;


        if (!empty($additiveParams['show_avatar']) or !empty($additiveParams['show_avatar_info']) ) {
            $base_url              = with(new User)->getBaseUrl();
            $avatarsExtensionsArray = \Config::get('app.images_extensions');
            if ( !empty($additiveParams['show_avatar_info']) and !empty($user->avatar) ) {
//                echo '<pre>$user->id::'.print_r($user->id,true).'</pre>';
//                echo '<pre>$user->avatar::'.print_r($user->avatar,true).'</pre>';
                $user_avatar_avatar    = User::getUserAvatarPath($user->id, $user->avatar, false);
//                echo '<pre>$user_avatar_avatar::'.print_r($user_avatar_avatar,true).'</pre>';
                $user_avatar_url           = $base_url . Storage::disk('local')->url($user_avatar_avatar);
//                echo '<pre>$user_avatar_url::'.print_r($user_avatar_url,true).'</pre>';
                $file_exists                  = Storage::disk('local')->exists('public/' . $user_avatar_avatar);
                if ($file_exists and in_array($user->extension, $avatarsExtensionsArray)) { // that is avatar - get all props
                    $avatar            = $user->avatar;
                    $avatar_path       = public_path('storage/' . $user_avatar_avatar);
                    $avatar_url        = $user_avatar_url;
                    $avatarPropsArray  = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $avatar_url];
                    $previewSizeArray  = with(new MyAppModel)->getImageShowSize($avatar_path, self::$img_preview_width, self::$img_preview_height);
                    if ( ! empty($previewSizeArray['width'])) {
                        $avatarPropsArray['preview_width']  = $previewSizeArray['width'];
                        $avatarPropsArray['preview_height'] = $previewSizeArray['height'];
                    }
                    $userImgPropsArray = MyAppModel::getImageProps($avatar_path, $avatarPropsArray);
                    if ( ! empty($userImgPropsArray) and is_array($userImgPropsArray)) {
                        foreach ($userImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $user[$next_document_img_key] = $next_document_img_value;
                        }
                    }
                } // if ( $file_exists and in_array($user->extension, $avatarsExtensionsArray) ) { // that is avatar - get all props
                if ($file_exists and ! in_array($user->extension, $avatarsExtensionsArray)) { // that is NOT avatar - get only some props
                    $avatar                    = $user->avatar;
                    $avatar_path               = public_path('storage/' . $user_avatar_avatar);
                    $avatar_url                = $user_avatar_url;
                    $avatarPropsArray          = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $avatar_url];
//                    echo '<pre>$avatarPropsArray::'.print_r($avatarPropsArray,true).'</pre>';
                    $userImgPropsArray = MyAppModel::getImageProps($avatar_path, $avatarPropsArray);
                    if ( ! empty($userImgPropsArray) and is_array($userImgPropsArray)) {
                        foreach ($userImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $user[$next_document_img_key] = $next_document_img_value;
                        }
                    }
                }
            } // if ( ! empty($filtersArray['show_avatar_info'])) {
        } // if (!empty($additiveParams['show_avatar']) or !empty($additiveParams['show_avatar_info']) ) {

        return $user;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function getUserAvatarDir(int $user_id) : string
    {
        $UPLOADS_USERS_AVATAR_DIR= \Config::get('app.UPLOADS_USER_AVATAR_DIR');
        return $UPLOADS_USERS_AVATAR_DIR . '-user-avatar-' . $user_id.'/';
    }

    public static function getUserAvatarPath(int $user_id, $avatar, bool $set_public_subdirectory= false) : string
    {
        if ( empty($avatar) ) return '';
        $UPLOADS_USERS_AVATAR_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_USER_AVATAR_DIR');
        return $UPLOADS_USERS_AVATAR_DIR . '-user-avatar-' . $user_id . '/' . $avatar;
    }


/*    public static function getUserAvatarDir(int $cms_item_id) : string
    {
        $UPLOADS_CMS_ITEMS_IMAGE_DIR= \Config::get('app.UPLOADS_CMS_ITEM_IMAGE_DIR');
        return $UPLOADS_CMS_ITEMS_IMAGE_DIR . 'user-avatar-' . $cms_item_id.'/';
    }

    public static function getUserAvatarPath(int $cms_item_id, $avatar, bool $set_public_subdirectory= false) : string
    {
        if ( empty($avatar) ) return '';
        $UPLOADS_CMS_ITEMS_IMAGE_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_CMS_ITEM_IMAGE_DIR');
        return $UPLOADS_CMS_ITEMS_IMAGE_DIR . 'user-avatar-' . $cms_item_id . '/' . $avatar;
    }*/

    /* get additional properties of cms_item avatar : path, url, size etc... */
    public function getUserAvatarPropsAttribute() : array
    {
        return $this->userAvatarPropsArray;
    }

    /* set additional properties of cms_item avatar : path, url, size etc... */
    public function setUserAvatarPropsAttribute(array $userAvatarPropsArray)
    {
        $this->userAvatarPropsArray = $userAvatarPropsArray;
    }

    public function getContentHintsAttribute($data)
    {
        return !empty($data) ? $data : '';
    }


    public function getContentAttribute($data)
    {
        return !empty($data) ? $data : '';
    }


    /* check if provided name is unique for users.name field */
    public static function getSimilarUserByUsername( string $username, int $id= null, bool $return_count = false )
    {
        $quoteModel = User::whereRaw( (new MyAppModel)->dbStrLower('username', false, false) .' = '. (new MyAppModel)->dbStrLower( (new MyAppModel)->pgEscape($username), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    /* check if provided email is unique for users.email field */
    public static function getSimilarUserByEmail( string $email, int $id= null, bool $return_count = false )
    {
        $quoteModel = User::whereRaw( (new MyAppModel)->dbStrLower('email', false, false) .' = '. (new MyAppModel)->dbStrLower( (new MyAppModel)->pgEscape($email), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getValidationRulesArray( $user_id, $locale ) : array
    {
        $additional_username_validation_rule= 'check_user_unique_by_username:'.( !empty($user_id)?$user_id:'') .','. $locale;
        $additional_email_validation_rule= 'check_user_unique_by_email:'.( !empty($user_id)?$user_id:'') .','. $locale;
        $validationRulesArray = [
            'username'         => 'required|max:255|'.$additional_username_validation_rule,
            'email'            => 'required|max:255|'.$additional_email_validation_rule,
            'status'           => 'required|in:'.with( new MyAppModel)->getValueLabelKeys( User::getUserStatusValueArray(false) ),
            'first_name'       => 'max:50',
            'last_name'        => 'max:50',
            'phone'            => 'max:50',
            'website'          => 'max:50',
            'avatar' => [
//                'string',
                'max:100',
            ],
//            'avatar_url' => [
//                Rule::dimensions()->maxWidth(2896)->maxHeight(2864),
//            ],
        ];
        return $validationRulesArray;
        /*
        -- Drop table

-- DROP TABLE public.rt_users

CREATE TABLE public.rt_users (
	id serial NOT NULL,
        username varchar(255) NOT NULL,
	    email varchar(255) NULL,
	    password varchar(255) NULL,
	    status varchar(255) NOT NULL DEFAULT 'N'::character varying, --  N => New(Waiting activation), A=>Active, I=>Inactive
	    first_name varchar(50) NULL,
	    last_name varchar(50) NULL,
    	phone varchar(50) NULL,
    	website varchar(50) NULL,
	remember_token varchar(100) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NULL,
	avatar varchar(100) NULL,
	provider_name varchar(50) NULL,
	provider_id varchar(255) NULL,
	CONSTRAINT rt_users_pkey PRIMARY KEY (id),
	CONSTRAINT rt_users_status_check CHECK (((status)::text = ANY (ARRAY[('N'::character varying)::text, ('A'::character varying)::text, ('I'::character varying)::text]))),
	CONSTRAINT users_email_unique UNIQUE (email),
	CONSTRAINT users_username_unique UNIQUE (username)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX ind_rt_users_provider ON public.rt_users USING btree (provider_name) ;
CREATE INDEX users_created_at_index ON public.rt_users USING btree (created_at) ;
CREATE INDEX users_status_index ON public.rt_users USING btree (status) ;
         */
    }

}
