<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Config;

use Auth;
use Carbon\Carbon;
use App\library\markdownEmail;
use GrahamCampbell\Markdown\Facades\Markdown;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Http\Traits\funcsTrait;
use App\MyAppModel;
use App\Settings;
use App\CmsItemTranslation;
use App\User;
use App\library\ListingReturnData;
use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class CmsItem extends MyAppModel
{
    use funcsTrait;
    use Sluggable;


    public $translatedAttributes = ['title', 'short_descr', 'content'];
    protected $fillable = [ 'title', 'page_type', 'short_descr', 'content', 'icon', 'image', 'content_hints',  'user_id', 'published', 'updated_at' ];

    protected $table = 'cms_items';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $cmsItemImagePropsArray = [];
    private static $cmsItemContentTypeValueArray = Array('P' => 'Plain', 'M' => 'Markdown', 'H' => 'HTML');
    private static $cmsItemPageTypeValueArray = Array('E' => 'Email Template', 'P' => 'Page', 'B' => 'Blog Article');
    private static $cmsItemPublishedValueArray = [  true => 'Is published', false => 'Not published'  ];

    protected static $img_filename_max_length= 50;
    protected static $img_preview_width= 320;
    protected static $img_preview_height= 240;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'alias' => [
                'source' => 'localetitle'
            ]
        ];
    }

    public function getLocaletitleAttribute() {
        $request= request();
        $requestData= $request->all();
        return $requestData['title'];
    }



    protected static function boot() {
        parent::boot();
/*        self::deleting(function($cmsItem) {
            $artist_image_filename= CmsItem::getCmsItemImagePath($cmsItem->id,$cmsItem->image, true);
//            echo '<pre>$artist_image_filename::'.print_r($artist_image_filename,true).'</pre>';
            CmsItem::deleteFileByPath($artist_image_filename, true);
        });*/
    }

    public function cmsItemTranslations()
    {
        return $this->hasMany('App\CmsItemTranslation');
    }


    /* return array of key value/label pairs based on self::$cmsItemPageTypeValueArray - db enum key values/labels implementation */
    public static function getCmsItemPageTypeValueArray(bool $key_value= true) :array
    {
        $resArray = [];
        foreach (self::$cmsItemPageTypeValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$cmsItemPageTypeValueArray - db enum key values/labels implementation */
    public static function getCmsItemPageTypeLabel(string $page_type) : string
    {
        if (!empty(self::$cmsItemPageTypeValueArray[$page_type])) {
            return self::$cmsItemPageTypeValueArray[$page_type];
        }
        return '';
    }


    /* return array of key value/label pairs based on self::$cmsItemContentTypeValueArray - db enum key values/labels implementation */
    public static function getCmsItemContentTypeValueArray(bool $key_value= true) :array
    {
        $resArray = [];
        foreach (self::$cmsItemContentTypeValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$cmsItemContentTypeValueArray - db enum key values/labels implementation */
    public static function getCmsItemContentTypeLabel(string $page_type) : string
    {
        if (!empty(self::$cmsItemContentTypeValueArray[$page_type])) {
            return self::$cmsItemContentTypeValueArray[$page_type];
        }
        return '';
    }



    /* return array of key value/label pairs based on self::$cmsItemPublisheValueArray - db enum key values/labels implementation */
    public static function getCmsItemPublishedValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$cmsItemPublishedValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$cmsItemPublisheValueArray - db enum key values/labels implementation */
    public static function getCmsItemPublishedLabel(string $published) : string
    {
        if (!empty(self::$cmsItemPublishedValueArray[$published])) {
            return self::$cmsItemPublishedValueArray[$published];
        }
        return '';
    }


    /* return data array by keys id/title based on filters/ordering... */
    public static function getCmsItemsSelectionList( bool $key_return= true,  array $filtersArray = array(), string $order_by = 'cms.title', string $order_direction = 'asc') : array
    {
        $cmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction);
        $resArray = [];
        foreach ($cmsItemsList as $nextCmsItem) {
            if ( $key_return ) {
                $resArray[] = [ 'key'=>$nextCmsItem->title, 'label'=> $nextCmsItem->id ];
            } else {
                $resArray[$nextCmsItem->id] = $nextCmsItem->title;
            }
        }
        return $resArray;
    }

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getCmsItemsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'cms.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $cms_item_table_name= with(new CmsItem)->getTableName();
        $quoteModel= CmsItem::from(  \DB::raw(DB::getTablePrefix().$cms_item_table_name.' as cms' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'cms.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['title'])) {
            if ( empty($filtersArray['in_content']) ) {
                $quoteModel->whereRaw( CmsItem::dbStrLower('title', false, false) . ' like ' . CmsItem::dbStrLower( $filtersArray['title'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.CmsItem::dbStrLower('title', false, false) . ' like ' . CmsItem::dbStrLower( $filtersArray['title'], true,true ) . ' OR ' . CmsItem::dbStrLower('content', false, false) . ' like ' . CmsItem::dbStrLower( $filtersArray['title'], true,true ) .
                ' OR ' . CmsItem::dbStrLower('short_descr', false, false) . ' like ' . CmsItem::dbStrLower( $filtersArray['title'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['alias'])) {
            $quoteModel->where( 'alias', '=', $filtersArray['alias'] );
        }

        if (!empty($filtersArray['page_type'])) {
            $quoteModel->where( 'page_type', '=', $filtersArray['page_type'] );
        }

        if (!empty($filtersArray['content_type'])) {
            $quoteModel->where( 'content_type', '=', $filtersArray['content_type'] );
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( 'user_id', '=', $filtersArray['user_id'] );
        }

        if (isset($filtersArray['published'])) {
            $quoteModel->where( 'published', '=', $filtersArray['published'] );
        }

        if (isset($filtersArray['locale'])) {
            $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
            $quoteModel->join(\DB::raw($cms_item_translations_table_name . ' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', \DB::raw('cms.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", title, short_descr, content ";
        }

//        $quoteModel->whereRaw( 'cms.id=8' );

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "cms.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "cms.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
        $items_per_page= with(new CmsItem)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new CmsItem)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $cmsItemsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $cmsItemsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $cmsItemsList = $quoteModel->get();
            $data_retrieved= true;
        }




        $base_url= with(new CmsItem)->getBaseUrl();
        $imagesExtensionsArray= \Config::get('app.images_extensions');
        foreach( $cmsItemsList as $next_key=>$nextCmsItem ) { /* map all retrieved data when need tof set human readable labels for some fields */

            if (  ( !empty($filtersArray['show_image']) or !empty($filtersArray['show_image_info']) ) and !empty($nextCmsItem->image) ) {
                if ( ! empty($filtersArray['show_image_info']) ) {
                    $next_artist_image_image= CmsItem::getCmsItemImagePath($nextCmsItem->id, $nextCmsItem->image, false);
                    $next_artist_image_url= $base_url.Storage::disk('local')->url($next_artist_image_image );
                    $file_exists = Storage::disk('local')->exists('public/'.$next_artist_image_image);
                    if ( $file_exists and in_array($nextCmsItem->extension, $imagesExtensionsArray) ) { // that is image - get all props
                        $image             = $nextCmsItem->image;
                        $image_path        = public_path('storage/'.$next_artist_image_image);
                        $image_url         = $next_artist_image_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $previewSizeArray= with(new CmsItem)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height );
                        if ( !empty($previewSizeArray['width']) ) {
                            $imagePropsArray['preview_width']= $previewSizeArray['width'];
                            $imagePropsArray['preview_height']= $previewSizeArray['height'];
                        }
                        $nextCmsItemImgPropsArray= CmsItem::getImageProps( $image_path, $imagePropsArray );
                        if ( !empty($nextCmsItemImgPropsArray) and is_array($nextCmsItemImgPropsArray) ) {
                            foreach( $nextCmsItemImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $cmsItemsList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    } // if ( $file_exists and in_array($nextCmsItem->extension, $imagesExtensionsArray) ) { // that is image - get all props
                    if ( $file_exists and !in_array($nextCmsItem->extension, $imagesExtensionsArray) ) { // that is NOT image - get only some props
                        $image             = $nextCmsItem->image;
                        $image_path        = public_path('storage/'.$next_artist_image_image);
                        $image_url         = $next_artist_image_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $nextCmsItemImgPropsArray= CmsItem::getImageProps( $image_path, $imagePropsArray );
                        if ( !empty($nextCmsItemImgPropsArray) and is_array($nextCmsItemImgPropsArray) ) {
                            foreach( $nextCmsItemImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $cmsItemsList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    }
                } // if ( ! empty($filtersArray['show_image_info'])) {
            }

        }

        return $cmsItemsList;

    } // public static function getCmsItemsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;


        if ( !empty($additiveParams['locale']) ) {
            $fields_for_select= 'cms.*';
            $additive_fields_for_select= '';
            $cms_item_table_name= with(new CmsItem)->getTableName();
            $quoteModel= CmsItem::from(  \DB::raw(DB::getTablePrefix().$cms_item_table_name.' as cms' ));
            $quoteModel->where( \DB::raw('cms.id'), '=', $id );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );

//            echo '<pre>$cms_item->$additiveParams::'.print_r($additiveParams,true).'</pre>';
            if (isset($additiveParams['locale'])) {
                $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
                $quoteModel->join(\DB::raw($cms_item_translations_table_name . ' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', \DB::raw('cms.id'));
                $quoteModel->where( 'locale', '=', $additiveParams['locale'] );
                $additive_fields_for_select .= ", cms_t.title, cms_t.short_descr, cms_t.content";
            }

//            echo '<pre>$cms_item->$additive_fields_for_select::'.print_r($additive_fields_for_select,true).'</pre>';

//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $cms_itemRows = $quoteModel->get();
            if (!empty($cms_itemRows[0]) and get_class($cms_itemRows[0]) == 'App\CmsItem' ) {
                $is_row_retrieved= true;
                $cmsItem= $cms_itemRows[0];
            }


            // 'show_future_concerts_count'=> 1 , 'show_related_votes'=> 1
        } // if ( !empty($additiveParams['show_related_songs_count']) or  !empty($additiveParams['show_future_concerts_count']) or !empty($additiveParams['show_related_votes'])  or !empty($additiveParams['show_related_images_count']) ) {


        if ( !$is_row_retrieved ) {
            $cmsItem = CmsItem::find($id);
        }


        if (empty($cmsItem)) return false;

        /*         if (isset($filtersArray['locale'])) {
            $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
            $quoteModel->join(\DB::raw($cms_item_translations_table_name . ' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', \DB::raw('cms.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", title, short_descr, content ";
        }

*/

        if ( !empty($additiveParams['show_image']) or !empty($additiveParams['show_image_info'])  ) {
            $base_url              = with(new CmsItem)->getBaseUrl();
            $imagesExtensionsArray = \Config::get('app.images_extensions');
            if ( !empty($additiveParams['show_image_info']) and !empty($cmsItem->image) ) {
//                echo '<pre>$cmsItem->id::'.print_r($cmsItem->id,true).'</pre>';
//                echo '<pre>$cmsItem->image::'.print_r($cmsItem->image,true).'</pre>';
                $cms_item_image_image    = CmsItem::getCmsItemImagePath($cmsItem->id, $cmsItem->image, false);
//                echo '<pre>$cms_item_image_image::'.print_r($cms_item_image_image,true).'</pre>';
                $cms_item_image_url           = $base_url . Storage::disk('local')->url($cms_item_image_image);
//                echo '<pre>$cms_item_image_url::'.print_r($cms_item_image_url,true).'</pre>';
                $file_exists                  = Storage::disk('local')->exists('public/' . $cms_item_image_image);
                if ($file_exists and in_array($cmsItem->extension, $imagesExtensionsArray)) { // that is image - get all props
                    $image            = $cmsItem->image;
                    $image_path       = public_path('storage/' . $cms_item_image_image);
                    $image_url        = $cms_item_image_url;
                    $imagePropsArray  = ['image' => $image, 'image_path' => $image_path, 'image_url' => $image_url];
                    $previewSizeArray = with(new CmsItem)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height);
                    if ( ! empty($previewSizeArray['width'])) {
                        $imagePropsArray['preview_width']  = $previewSizeArray['width'];
                        $imagePropsArray['preview_height'] = $previewSizeArray['height'];
                    }
                    $cmsItemImgPropsArray = CmsItem::getImageProps($image_path, $imagePropsArray);
                    if ( ! empty($cmsItemImgPropsArray) and is_array($cmsItemImgPropsArray)) {
                        foreach ($cmsItemImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $cmsItem[$next_document_img_key] = $next_document_img_value;
                        }
                    }
                } // if ( $file_exists and in_array($cmsItem->extension, $imagesExtensionsArray) ) { // that is image - get all props
                if ($file_exists and ! in_array($cmsItem->extension, $imagesExtensionsArray)) { // that is NOT image - get only some props
                    $image                    = $cmsItem->image;
                    $image_path               = public_path('storage/' . $cms_item_image_image);
                    $image_url                = $cms_item_image_url;
                    $imagePropsArray          = ['image' => $image, 'image_path' => $image_path, 'image_url' => $image_url];
//                    echo '<pre>$imagePropsArray::'.print_r($imagePropsArray,true).'</pre>';
                    $cmsItemImgPropsArray = CmsItem::getImageProps($image_path, $imagePropsArray);
                    if ( ! empty($cmsItemImgPropsArray) and is_array($cmsItemImgPropsArray)) {
                        foreach ($cmsItemImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $cmsItem[$next_document_img_key] = $next_document_img_value;
                        }
                    }
                }

                
/*                if (isset($additiveParams['locale'])) {
                    $artist_translations_table_name=  DB::getTablePrefix().with(new ArtistTranslation)->getTableName();
                    $quoteModel->join(\DB::raw($artist_translations_table_name . ' as a_t '), \DB::raw('a_t.artist_id'), '=', \DB::raw('a.id'));
                    $quoteModel->where( 'locale', '=', $additiveParams['locale'] );
                    $additive_fields_for_select .= ", a_t.name, a_t.info";
                }*/

                /*                if (isset($filtersArray['locale'])) {
                                    $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
                                    $quoteModel->join(\DB::raw($cms_item_translations_table_name . ' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', \DB::raw('cms.id'));
                                    $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
                                    $additive_fields_for_select .= ", title, short_descr, content ";
                                }*/

            } // if ( ! empty($filtersArray['show_image_info'])) {
        } // if (!empty($additiveParams['show_image']) or !empty($additiveParams['show_image_info']) ) {

//        echo '<pre>$cmsItem::'.print_r($cmsItem,true).'</pre>';
//        die("-1 XXZ");
        return $cmsItem;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function deleteCmsItem(int $cms_item_id, bool $run_validation= true) : array
    {
        if( $run_validation ) { // data must be validated before deleted
            $cmsItem = CmsItem::find( $cms_item_id );
            if ( empty( $cmsItem ) ) {
                return [ 'error_code'      => 1,
                         'errorsList'     => [ "Cms Item # '" . $cms_item_id . "' not found" ],
                         'success_message' => "",
                         'cms_item_id'   => ''
                ];
            }
        } // if( $run_validation ) { // data must be validated before deleted

        $ret= MyAppModel::runPgFunction($cms_item_id, DB::getTablePrefix()."delete_cms_item");
        $cms_item_dir= CmsItem::getCmsItemDir($cms_item_id, false);
        $ret= with(new CmsItem)->deleteDirectory( $cms_item_dir );
        if ( $ret ) { // Deleting was successful
            return [ 'error_code' => 0, 'errorsList' => [], 'success_message' => "CmsItem '".$cmsItem->title."' was deleted ", 'cms_item_id' => $cms_item_id ];
        }
        return [ 'error_code' => 1, 'errorsList' => ["Error deleting cms_item '".$cmsItem->title."' " ], 'success_message' => "", 'cms_item_id' => '' ];  // Deleting was NOT successful
    }


    /* check if provided title is unique for cms_item.title field */
/*    public static function getSimilarCmsItemByTitle( string $title, int $id= null, $return_count = false )
    {
        $quoteModel = CmsItem::whereRaw( CmsItem::dbStrLower('title', false, false) .' = '. CmsItem::dbStrLower(CmsItem::pgEscape($title), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }*/


    public static function getSimilarCmsItemByAlias( string $alias, int $id= null, $return_count = false )
    {
        $quoteModel = CmsItem::whereRaw( CmsItem::dbStrLower('alias', false, false) .' = '. CmsItem::dbStrLower(CmsItem::pgEscape($alias), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    /* get Body of content by title of Content*/
    public static function getBodyContentByAlias(string $alias, array $constantsArray = [])
    {
        $locale= !empty($constantsArray['locale']) ? $constantsArray['locale'] : '';
//        with(new self)->info( $locale,'$locale::' );

//        echo '<pre>$locale::'.print_r($locale,true).'</pre>';
        /*        $made_votes_count             =     VoteItemUsersResult::
            join(\DB::raw('vote_items as vi '), \DB::raw('vi.id'), '=', \DB::raw('vote_item_users_result.vote_item_id'))->
            whereRaw( 'vi.vote_id = ' . $vote_id )->
            count(DB::raw('DISTINCT vote_item_id'));
        $viewParamsArray['made_votes_count']= $made_votes_count;
*/
//        $cms_item_table_name= with(new CmsItem)->getTableName();
//        $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
//        $quoteModel->select( \DB::raw($fields_for_select) );

/*        $cmsItemRow = CmsItem::from(  \DB::raw(DB::getTablePrefix().$cms_item_table_name.' as cms' ) )->

        where(CmsItem::dbStrLower('alias', false, false) . ' = ' . CmsItem::dbStrLower($alias, true, false))->
            join(\DB::raw($cms_item_translations_table_name.' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', ('cms.id'))->
            where( \DB::raw('cms_t.locale'), $locale )->
            select(\DB::raw('cms.*').', '.\DB::raw('cms_t').'.title' )->
//            select('cms_items.*, ' .   str_replace( '"','', \DB::raw("cms_t.title, cms_t.short_descr, cms_t.content")  ) )->
        get()->first();*/
        /* CREATE TABLE public.rt_cms_item_translations (
	id serial NOT NULL,
	cms_item_id int4 NOT NULL,
	title varchar(100) NOT NULL,
	short_descr varchar(255) NOT NULL,
	content */

//        $similarCmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, ['page_type' => 'P', 'published' => true, 'limit' => 6], 'cms.created_at', 'desc');
        $similarCmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, ['alias' => $alias, 'locale' => $locale] );
        if ( $similarCmsItemsList === null or empty($similarCmsItemsList[0]) ) return false;
        $cmsItemRow= $similarCmsItemsList[0];

        if($cmsItemRow==null) {
            return false;
        }
        $title= $cmsItemRow->title;
//                        echo '<pre>$title::'.print_r($title,true).'</pre>';
//                        echo '<pre>$cmsItemRow::'.print_r($cmsItemRow,true).'</pre>';
//                        die("-1 XXZIIIIII");
        foreach ($constantsArray as $key => $value) {
            $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
            $cmsItemRow->short_descr = preg_replace($pattern, $value, $cmsItemRow->short_descr);
        }

        foreach ($constantsArray as $key => $value) {
            $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
            $cmsItemRow->title = preg_replace($pattern, $value, $cmsItemRow->title);
        }

        foreach ($constantsArray as $key => $value) {
            $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
            $cmsItemRow->content = preg_replace($pattern, $value, $cmsItemRow->content);
        }

        return $cmsItemRow;


        /*        $cmsItemRowsList = CmsItem::whereRaw(CmsItem::dbStrLower('alias', false, false) . ' = ' . CmsItem::dbStrLower($alias, true, false))->
                    join(\DB::raw($cms_item_translations_table_name.' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', ('cms_items.id'))->
                    where( \DB::raw('cms_t.locale'), $locale )->
                                get()->first();

                */


//        foreach( $cmsItemRowsList as $next_key=>$nextCmsItemRow ) {
//            echo '<pre>$nextCmsItemRow->id::'.print_r($nextCmsItemRow->id,true).'</pre>';
//            echo '<pre>$nextCmsItemRow->title::'.print_r($nextCmsItemRow->title,true).'</pre>';
//        }
//
//        die("-1 XXZ");
        $fields_for_select= 'cms.*';
        $additive_fields_for_select= '';
        $cms_item_table_name= with(new CmsItem)->getTableName();
        $quoteModel= CmsItem::from(  \DB::raw(DB::getTablePrefix().$cms_item_table_name.' as cms' ));
        $quoteModel->where( 'cms.alias', $alias );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );

//        $cms_item_translations_table_name=  DB::getTablePrefix().with(new CmsItemTranslation)->getTableName();
//        $quoteModel->join(\DB::raw($cms_item_translations_table_name . ' as cms_t '), \DB::raw('cms_t.cms_item_id'), '=', \DB::raw('cms.id'));
//        $additive_fields_for_select .= ", cms_t.title, cms_t.short_descr, cms_t.content";

//            echo '<pre>$cms_item->$additive_fields_for_select::'.print_r($additive_fields_for_select,true).'</pre>';

//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
        $quoteModel->select( \DB::raw($fields_for_select) );
        $cms_itemRows = $quoteModel->get();
        echo '<pre>$cms_itemRows::'.print_r($cms_itemRows,true).'</pre>';
        die("-1 XXZ");
//        if (!empty($cms_itemRows[0]) and get_class($cms_itemRows[0]) == 'App\CmsItem' ) {
//            $cmsItemRow= $cms_itemRows[0];
//        }





        if ( !empty($cmsItemRow) ) {
            foreach ($constantsArray as $key => $value) {
                $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
                $cmsItemRow->content = preg_replace($pattern, $value, $cmsItemRow->content);
            }

            foreach ($constantsArray as $key => $value) {
                $pattern = '/\[([\s]*' . $key . '[\s]*)\]/xsi';
                $cmsItemRow->title = preg_replace($pattern, $value, $cmsItemRow->title);
            }

//            $cmsItemRow->content = Markdown::convertToHtml($cmsItemRow->content);  // TODO
//            $cmsItemRow->title = Markdown::convertToHtml($cmsItemRow->title);
//            $cmsItemRow->short_descr = Markdown::convertToHtml($cmsItemRow->short_descr);

//            echo '<pre>$cmsItemRow::'.print_r($cmsItemRow,true).'</pre>';
//            echo '<pre>$cmsItemRow->translations::'.print_r($cmsItemRow->translations,true).'</pre>';
            if ( !empty($locale) and !empty($cmsItemRow->translations) /*and is_array($cmsItemRow->translations) */ ) {
                foreach( $cmsItemRow->translations as $nextTranslation ) {
                    if ( $nextTranslation->locale == $locale ) {
                        $title= $nextTranslation->title;
//                        echo '<pre>$title::'.print_r($title,true).'</pre>';
//                        die("-1 XXZIIIIII");
                        $short_descr= $nextTranslation->short_descr;
                        $content= $nextTranslation->content;
                        $cmsItemRow->title= $title;
                        $cmsItemRow->short_descr= $short_descr;
                        $cmsItemRow->content= $content;
                        break;
                    }
                }
            }
            return $cmsItemRow;
        }
//        die("-1 XXZ------");
        return false;
    }

    public function getCmsItemIdByAlias($alias)
    {
        $cmsItemRow = CmsItem::whereRaw( CmsItem::dbStrLower('alias', false, false) .' = '. CmsItem::dbStrLower($alias, true,false) )->get();
        if ( !empty($cmsItemRow[0]->id) ) {
            return $cmsItemRow[0]->id;
        }
        return '';
    }

    public static function getCmsItemImageDir(int $cms_item_id) : string
    {
        $UPLOADS_CMS_ITEMS_IMAGE_DIR= \Config::get('app.UPLOADS_CMS_ITEM_IMAGE_DIR');
        return $UPLOADS_CMS_ITEMS_IMAGE_DIR . 'cms-item-image-' . $cms_item_id.'/';
    }

    public static function getCmsItemImagePath(int $cms_item_id, $image, bool $set_public_subdirectory= false) : string
    {
        if ( empty($image) ) return '';
        $UPLOADS_CMS_ITEMS_IMAGE_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_CMS_ITEM_IMAGE_DIR');
        return $UPLOADS_CMS_ITEMS_IMAGE_DIR . 'cms-item-image-' . $cms_item_id . '/' . $image;
    }

    /* get additional properties of cms_item image : path, url, size etc... */
    public function getCmsItemImagePropsAttribute() : array
    {
        return $this->cmsItemImagePropsArray;
    }

    /* set additional properties of cms_item image : path, url, size etc... */
    public function setCmsItemImagePropsAttribute(array $cmsItemImagePropsArray)
    {
        $this->cmsItemImagePropsArray = $cmsItemImagePropsArray;
    }

    public function getContentHintsAttribute($data)
    {
        return !empty($data) ? $data : '';
    }
    

    public function getContentAttribute($data)
    {
        return !empty($data) ? $data : '';
    }

    public static function getValidationRulesArray( $cms_item_id, $locale ) : array
    {
        $additional_title_validation_rule= 'check_cms_item_translations_unique_by_title:'.( !empty($cms_item_id)?$cms_item_id:'') .','. $locale;
        $validationRulesArray = [
            'title'         => 'required|max:100|'.$additional_title_validation_rule,
            'page_type'    => 'required|in:'.with( new CmsItem)->getValueLabelKeys( CmsItem::getCmsItemPageTypeValueArray(false) ),
            'published'    => 'required|in:'.with( new CmsItem)->getValueLabelKeys( CmsItem::getCmsItemPublishedValueArray(false) ),
            'icon'         => '',
        ];
        return $validationRulesArray;
    }


    public static function fillTranslations( )
    {
        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $cmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, []);
        foreach ($cmsItemsList as $nextCmsItem) {
            $cms_item_id       = $nextCmsItem->id;
            $title             = $nextCmsItem->title;
            $short_descr       = $nextCmsItem->short_descr;
            $content           = $nextCmsItem->content;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newCmsItemTranslation= new CmsItemTranslation();
                $newCmsItemTranslation->cms_item_id= $cms_item_id;
                if ( $next_lang_key == 'en' ) {
                    $newCmsItemTranslation->title       = $title;
                    $newCmsItemTranslation->short_descr = $short_descr;
                    $newCmsItemTranslation->content     = $content;
                } else {
                    $newCmsItemTranslation->title       = $next_lang_label . ' VERSION : ' . $title;
                    $newCmsItemTranslation->short_descr = $next_lang_label . ' VERSION : ' . $short_descr;
                    $newCmsItemTranslation->content     = $next_lang_label . ' VERSION : ' . $content;
                }
                $newCmsItemTranslation->locale= $next_lang_key;

                $newCmsItemTranslation->save();
            }

        }
    }
}