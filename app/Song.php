<?php
namespace App;

use DB;
use App\MyAppModel;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Genre;
use App\SongArtist;
use App\SongGenre;
use App\SongVote;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;

class Song extends MyAppModel
{
    protected $table = 'songs';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $songIsActiveValueArray = [ true => 'Is active', false => 'Not active' ];
    use funcsTrait;
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'localetitle'
            ]
        ];
    }

    public function getLocaletitleAttribute() {
        $request= request();
        $requestData= $request->all();
//        echo '<pre>$requestData::'.print_r($requestData,true).'</pre>';
        return $requestData['artistSong']['song_title'];
    }
    public function SongGenres()
    {
        return $this->hasMany('App\SongGenre');
    }

    public function SongArtists()
    {
        return $this->hasMany('App\SongArtist');
    }

    public function SongVotes()
    {
        return $this->hasMany('App\SongVote');
    }


    protected static function boot() {
        parent::boot();
        self::deleting(function($song) {
            $song->SongGenres()->delete();
            $song->SongArtists()->delete();
            $song->SongVotes()->delete();
        });
    }

    protected $fillable = ['title', 'short_descr', 'ordering', 'description', 'is_active', 'updated_at'];

    /* return array of key value/label pairs based on self::$songIsActiveValueArray - db enum key values/labels implementation */
    public static function getSongIsActiveValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$songIsActiveValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$songIsActiveValueArray - db enum key values/labels implementation */
    public static function getSongIsActiveLabel(string $is_active) : string
    {
        if (!empty(self::$songIsActiveValueArray[$is_active])) {
            return self::$songIsActiveValueArray[$is_active];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getSongsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 's.ordering'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';
        $backend_locale= config('app.backend_locale', 'en');

        $song_table_name= with(new Song)->getTableName();
        $quoteModel= Song::from(  \DB::raw(DB::getTablePrefix().$song_table_name.' as s' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 's.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['title'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Song::dbStrLower('title', false, false) . ' like ' . Song::dbStrLower( $filtersArray['title'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Song::dbStrLower('title', false, false) . ' like ' . Song::dbStrLower( $filtersArray['title'], true,true ) . ' OR ' . Song::dbStrLower('description', false, false) . ' like ' . Song::dbStrLower( $filtersArray['title'], true,true ) .
                                       ' OR ' . Song::dbStrLower('description', false, false) . ' like ' . Song::dbStrLower( $filtersArray['title'], true,true ) . ' ) ');
            }
        }

        if (isset($filtersArray['is_active'])) {
            $quoteModel->where( DB::raw('s.is_active'), '=', $filtersArray['is_active'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("s.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("s.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if (isset($filtersArray['locale'])) {
            $song_translations_table_name=  DB::getTablePrefix().with(new SongTranslation)->getTableName();
            $quoteModel->join(\DB::raw($song_translations_table_name . ' as s_t '), \DB::raw('s_t.song_id'), '=', \DB::raw('s.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", s_t.title, s_t.short_descr, s_t.description";
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $quoteModel->whereRaw( "( s.id >=74 and s.id <=80 ) or s.id >= 109");

        /*
                $artist_vote_table_name= DB::getTablePrefix().( with(new ArtistVote)->getTableName() );
        if ( !empty($filtersArray['show_related_votes']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_count' .
                                           ', ( select sum(av.vote) from ' . $artist_vote_table_name . ' as av where av.artist_id = ' . 'a.id ) as related_votes_sum';
        }

 */
        if ( !empty($filtersArray['show_related_votes']) ) {
            $song_vote_table_name= DB::getTablePrefix().( with(new SongVote)->getTableName() );
            $additive_fields_for_select .= ', ( select count(*) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_count' .
                                           ', ( select sum(sv.vote) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_sum';

//                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as ac where ac.artist_id = ' . 'a.id and ac.' . "event_date >'" . $today . " 00:00:00'" . ' ) as   artist_future_concerts_count';
        } // if ( !empty($additiveParams['show_related_votes']) ) {

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Song)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Song)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $songsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $songsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $songsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if (!empty($filtersArray['show_genre_names'])) {
            foreach( $songsList as $next_key=>$nextSong ) {
                $songGenresList= SongGenre::getSongGenresList( ListingReturnData::LISTING, ['song_id'=> $nextSong->id, 'show_genre_name'=> 1, 'locale'=> $backend_locale ] );

                $songsList[$next_key]['songGenresList']= $songGenresList;
            }
        }
        return $songsList;

    } // public static function getSongsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

//        echo '<pre>$additiveParams::'.print_r($additiveParams,true).'</pre>';
        if (  !empty($additiveParams['show_related_votes'])  or !empty($additiveParams['locale']) ) {
            $fields_for_select= 's.*';
            $additive_fields_for_select= '';
            $song_table_name= with(new Song)->getTableName();
            $quoteModel= Song::from(  \DB::raw(DB::getTablePrefix().$song_table_name.' as s' ));
            $quoteModel->where( \DB::raw('s.id'), '=', $id );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );


            if ( !empty($additiveParams['show_related_votes']) or !empty($additiveParams['locale']) ) {
                $song_vote_table_name= DB::getTablePrefix().( with(new SongVote)->getTableName() );
                $additive_fields_for_select .= ', ( select count(*) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_count' .
                                               ', ( select sum(sv.vote) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_sum';
            } // if ( !empty($additiveParams['show_related_votes']) ) {

            /*         if ( !empty($filtersArray['show_related_votes']) ) {
            $song_vote_table_name= DB::getTablePrefix().( with(new SongVote)->getTableName() );
            $additive_fields_for_select .= ', ( select count(*) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_count' .
                                           ', ( select sum(sv.vote) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 's.id ) as related_votes_sum';

//                $additive_fields_for_select .= ', ( select count(*) from ' . $artist_vote_table_name . ' as ac where ac.artist_id = ' . 'a.id and ac.' . "event_date >'" . $today . " 00:00:00'" . ' ) as   artist_future_concerts_count';
        } // if ( !empty($additiveParams['show_related_votes']) ) {
 */
//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );

            if (isset($additiveParams['locale'])) {
                $song_translations_table_name=  DB::getTablePrefix().with(new SongTranslation)->getTableName();
                $quoteModel->join(\DB::raw($song_translations_table_name . ' as s_t '), \DB::raw('s_t.song_id'), '=', \DB::raw('s.id'));
                $quoteModel->where( 'locale', '=', $additiveParams['locale'] );
                $additive_fields_for_select .= ", s_t.title, s_t.description";
            }
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of cms table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $songRows = $quoteModel->get();
            if (!empty($songRows[0]) and get_class($songRows[0]) == 'App\Song' ) {
                $is_row_retrieved= true;
                $song= $songRows[0];
            }


            // 'show_future_concerts_count'=> 1 , 'show_related_votes'=> 1
        }


        if ( !$is_row_retrieved ) {
            $song = Song::find($id);
        }


        if (empty($song)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $song= with(new Song)->substituteNullValues($song, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $song;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $song_id, bool $check_song_participant_selected= false ) : array
    {
//        echo '<pre>++ getValidationRulesArray::'.print_r($_POST,true).'</pre>';
        $additional_name_validation_rule= 'check_song_unique_by_name:'.( !empty($song_id)?$song_id:'');
        $validationRulesArray = [
            'title'        => 'required|max:100|'.$additional_name_validation_rule,
            'short_descr'  => 'required|max:255',
            'ordering'     => 'required|numeric|min:1',
            'is_active'    => 'required|in:'.with( new Song)->getValueLabelKeys( Song::getSongIsActiveValueArray(false) ),
            'description'  => 'required',
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for songs.name field */
    public static function getSimilarSongByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Song::whereRaw( Song::dbStrLower('title', false, false) .' = '. Song::dbStrLower( Song::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    /* check if provided slug is unique for songs.slug field */
    public static function getSimilarSongBySlug( string $slug, int $id= null, bool $return_count = false )
    {
        $quoteModel = Song::whereRaw( Song::dbStrLower('slug', false, false) .' = '. Song::dbStrLower( Song::pgEscape($slug), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getSongsWithSimilarTitlesList()
    {
        $backend_locale= config('app.backend_locale', 'en');
        /* CREATE TABLE public.rt_song_translations (
	id serial NOT NULL,
	song_id int4 NOT NULL,
	title varchar(100) NOT NULL,
	short_descr varchar(255) NOT NULL,
	description text NOT NULL,
	locale varchar(2) NOT NULL,
	created_at timestamp NULL, */

        $songsWithSimilarTitlesList = Song
            ::orderBy('total_songs', 'desc')->orderBy( DB::raw('s_t.title', 'asc') )
            ->join(\DB::raw('rt_song_translations as s_t'), \DB::raw('s_t.song_id'), '=', \DB::raw('rt_songs.id'))
            ->select( \DB::raw('s_t.title'), DB::raw('count(*) as total_songs'))
//            'locale'=> $backend_locale
            ->groupBy( \DB::raw('s_t.title') )
            ->havingRaw('count(*) > 1')
            ->get();

        /*         $tempDetailedVoteItemUsersResults = VoteItemUsersResult// Grouped by vote name
        ::getByCreatedAt($filter_voted_at_from, ' > ')
        ->getByCreatedAt($filter_voted_at_till, ' <= ')
        ->getByUserId($filterSelectedUsers)
        ->getByVoteCategories($filterSelectedVoteCategories)
        ->getByVoteIsQuiz(true)
        ->getByVoteStatus('A')
        ->select(\DB::raw('vote_item_users_result.is_correct, vote_item_users_result.created_at as voted_at, users.id as user_id, users.first_name, users.last_name, users.username, votes.id as vote_id , votes.name as vote_name '))
        ->orderBy('vote_name', 'desc')
        ->orderBy('voted_at', 'asc')
        ->join(\DB::raw('vote_items'), \DB::raw('vote_items.id'), '=', \DB::raw('vote_item_users_result.vote_item_id'))
        ->join(\DB::raw('users '), \DB::raw('users.id'), '=', \DB::raw('vote_item_users_result.user_id'))
        ->join(\DB::raw('votes '), \DB::raw('votes.id'), '=', \DB::raw('vote_items.vote_id'))
        ->get();



                $tempVoteItemUsersResultsCorrect  = VoteItemUsersResult
            ::getByIsCorrect(true)// vote_item_users_result   table
            ->getByCreatedAt($filter_voted_at_from, ' > ')
            ->getByCreatedAt($filter_voted_at_till, '<= ')
            ->getByUserId($filterSelectedUsers)
            ->getByVote($filterSelectedVotes)// VoteItem table
            ->getByVoteCategories($filterSelectedVoteCategories)
            ->getByVoteIsQuiz(true)
            ->getByVoteStatus('A')//   // Vote table
            ->select(\DB::raw(' DATE_FORMAT(vote_item_users_result.created_at, "%Y-%m-%d") as formatted_created_at , count(vote_item_users_result.id) as count'))
            ->orderBy('formatted_created_at', 'asc')
            ->groupBy('formatted_created_at')
            ->join(\DB::raw('vote_items'), \DB::raw('vote_items.id'), '=', \DB::raw('vote_item_users_result.vote_item_id'))
            ->join(\DB::raw('votes '), \DB::raw('votes.id'), '=', \DB::raw('vote_items.vote_id'))
            ->get();
 */

        /*         $songsWithSimilarTitlesList = Song::orderBy('total_songs', 'desc')->orderBy('title', 'asc')
            ->select('title', DB::raw('count(*) as total_songs'))
            ->groupBy('title')
            ->havingRaw('count(*) > 1')
            ->get();
 */
        return $songsWithSimilarTitlesList;
        /*   from rt_artist_votes as av join
      rt_artists as a on a.id = av.artist_id  join
      rt_artist_translations as a_t on a_t.artist_id = av.artist_id AND a_t.locale= p_locale
 */
    }

    public static function fillTranslations()
    {
        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $cmsItemsList = Song::getSongsList(ListingReturnData::LISTING, []);
        foreach ($cmsItemsList as $nextSong) {
            $song_id           = $nextSong->id;
            $title             = $nextSong->title;
            $short_descr       = $nextSong->short_descr;
            $description       = $nextSong->description;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newSongTranslation= new SongTranslation();
                $newSongTranslation->song_id= $song_id;
                if ( $next_lang_key == 'en' ) {
                    $newSongTranslation->short_descr = $short_descr;
                    $newSongTranslation->title       = $title;
                    $newSongTranslation->description = $description;
                } else {
                    $newSongTranslation->short_descr       = $next_lang_label . ' VERSION : ' . substr($short_descr,0,220);
                    $newSongTranslation->title       = $next_lang_label . ' VERSION : ' . $title;
                    $newSongTranslation->description = $next_lang_label . ' VERSION : ' . $description;
                }
                $newSongTranslation->locale= $next_lang_key;

                $newSongTranslation->created_at= now();
                $newSongTranslation->save();
            }

        }
    }

}

