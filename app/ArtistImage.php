<?php
namespace App;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image as Image;

use App\MyAppModel;
use App\User;
use App\Config;
use App\Artist;
use App\ArtistImageTranslation;
use App\library\ListingReturnData;

class ArtistImage extends MyAppModel
{

    protected $table = 'artist_images';
    protected $primaryKey = 'id';
    public $timestamps = false;

    private static $artistIsMainValueArray = [ false => 'Not is main', true => 'Is main' ];
    private static $artistIsHomePageValueArray = [ false => 'Not visible on home page', true => 'Visible on home page' ];

    protected $artistImageImagePropsArray = [];
    protected static $img_image_name_max_length= 50;
    protected static $img_preview_width= 128;
    protected static $img_preview_height= 96;

    protected $fillable = [ "artist_id", "image_name", "is_main", "is_home_page", "description" ];

    protected static function boot() {
        parent::boot();
        self::deleting(function($artistImage) {
            $artist_image_filename= ArtistImage::getArtistImagePath($artistImage->artist_id,$artistImage->image_name, true);
            ArtistImage::deleteFileByPath($artist_image_filename, true);
        });

    }

    /* return array of key value/label pairs based on self::$artistIsMainValueArray - db enum key values/labels implementation */
    public static function getArtistIsMainValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistIsMainValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistIsMainValueArray - db enum key values/labels implementation */
    public static function getArtistIsMainLabel(string $is_main) : string
    {
        if (!empty(self::$artistIsMainValueArray[$is_main])) {
            return self::$artistIsMainValueArray[$is_main];
        }
        return '';
    }

    /* return array of key value/label pairs based on self::$artistIsHomePageValueArray - db enum key values/labels implementation */
    public static function getArtistIsHomePageValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistIsHomePageValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistIsHomePageValueArray - db enum key values/labels implementation */
    public static function getArtistIsHomePageLabel(string $is_home_page) : string
    {
        if (!empty(self::$artistIsHomePageValueArray[$is_home_page])) {
            return self::$artistIsHomePageValueArray[$is_home_page];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getArtistImagesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'ai.image_name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $artist_image_table_name= with(new ArtistImage)->getTableName();
        $quoteModel= ArtistImage::from(  \DB::raw(DB::getTablePrefix().$artist_image_table_name.' as ai' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            if ($order_by == 'is_main_is_home_page_image_name') {
                $quoteModel->orderBy('is_main', 'desc');
                $quoteModel->orderBy('is_home_page', 'desc');
                $quoteModel->orderBy('image_name', 'desc');
            } else {
                $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
            }
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ai.*';

        if (!empty($filtersArray['image_name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( ArtistImage::dbStrLower('image_name', false, false) . ' like ' . ArtistImage::dbStrLower( $filtersArray['image_name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.ArtistImage::dbStrLower('image_name', false, false) . ' like ' . ArtistImage::dbStrLower( $filtersArray['image_name'], true,true ) . ' OR ' . ArtistImage::dbStrLower('description', false, false) . ' like ' . ArtistImage::dbStrLower( $filtersArray['image_name'], true,true ) .
                                       ' OR ' . ArtistImage::dbStrLower('description', false, false) . ' like ' . ArtistImage::dbStrLower( $filtersArray['image_name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['is_home_page'])) {
            $quoteModel->where( DB::raw('ai.is_home_page'), '=', $filtersArray['is_home_page'] );
        }

        if (!empty($filtersArray['artist_id'])) {
            $quoteModel->where(DB::raw('ai.artist_id'), '=', $filtersArray['artist_id']);
        }

        if (!empty($filtersArray['is_main'])) {
            $quoteModel->where(DB::raw('ai.is_main'), '=', $filtersArray['is_main']);
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "ai.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "ai.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }



        if (isset($filtersArray['locale'])) {
            $artist_image_translations_table_name=  DB::getTablePrefix().with(new ArtistImageTranslation)->getTableName();
            $quoteModel->join(\DB::raw($artist_image_translations_table_name . ' as ai_t '), \DB::raw('ai_t.artist_image_id'), '=', \DB::raw('ai.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", description ";
        }



        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new ArtistImage)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new ArtistImage)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $artistImagesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $artistImagesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $artistImagesList = $quoteModel->get();
            $data_retrieved= true;
        }
        $base_url= with(new ArtistImage)->getBaseUrl();
        with(new ArtistImage)->debToFile(print_r($base_url,true),' getArtistImagesList  -1 $base_url::');
        $imagesExtensionsArray= \Config::get('app.images_extensions');
        foreach( $artistImagesList as $next_key=>$nextArtistImage ) { /* map all retrieved data when need to set human readable labels for some fields */

            if (!empty($filtersArray['show_image']) or !empty($filtersArray['show_image_info']) ) {
                if ( ! empty($filtersArray['show_image_info'])) {
                    $next_artist_image_image_name= ArtistImage::getArtistImagePath($nextArtistImage->artist_id,
                        $nextArtistImage->image_name, false);
//                    echo '<pre>$next_artist_image_image_name::'.print_r($next_artist_image_image_name,true).'</pre>';
                    with(new ArtistImage)->debToFile(print_r($base_url,true),' getArtistImagesList  -1 $base_url::');
                    with(new ArtistImage)->debToFile(print_r('public/'.$next_artist_image_image_name,true),' \'public/\'.$next_artist_image_image_name  -10 $base_url::');
                    $next_artist_image_url= $base_url.Storage::disk('local')->url($next_artist_image_image_name );


                    $file_exists = Storage::disk('local')->exists('public/'.$next_artist_image_image_name);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
//                    echo '<pre>$imagesExtensionsArray::'.print_r($imagesExtensionsArray,true).'</pre>';
                    with(new ArtistImage)->debToFile(print_r($file_exists,true),' getArtistImagesList  -2 $file_exists::');
                    with(new ArtistImage)->debToFile(print_r($imagesExtensionsArray,true),' getArtistImagesList  -3 $imagesExtensionsArray::');

                    $next_artist_image_extension= (with(new ArtistImage))->getFilenameExtension($next_artist_image_image_name);
//                    echo '<pre>$next_artist_image_extension::'.print_r($next_artist_image_extension).'</pre>';
                    with(new ArtistImage)->debToFile(print_r($next_artist_image_extension,true),' getArtistImagesList  -4 $next_artist_image_extension::');
                    if ( $file_exists and in_array($next_artist_image_extension, $imagesExtensionsArray) ) { // that is image - get all props
                        $image             = $nextArtistImage->image_name;
                        with(new ArtistImage)->debToFile(print_r($image,true),' getArtistImagesList  -1 $image::');
                        $image_path        = public_path('storage/'.$next_artist_image_image_name);
                        $image_url         = $next_artist_image_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $previewSizeArray= with(new ArtistImage)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height );
                        if ( !empty($previewSizeArray['width']) ) {
                            $imagePropsArray['preview_width']= $previewSizeArray['width'];
                            $imagePropsArray['preview_height']= $previewSizeArray['height'];
                        }
                        $nextArtistImageImgPropsArray= ArtistImage::getImageProps( $image_path, $imagePropsArray );
//                        echo '<pre>$nextArtistImageImgPropsArray::'.print_r($nextArtistImageImgPropsArray,true).'</pre>';
                        with(new ArtistImage)->debToFile(print_r($nextArtistImageImgPropsArray,true),' getArtistImagesList  -5 $nextArtistImageImgPropsArray::');
                        $nextArtistImage->setArtistImageImagePropsAttribute($nextArtistImageImgPropsArray);
                        if ( !empty($nextArtistImageImgPropsArray) and is_array($nextArtistImageImgPropsArray) ) {
                            foreach( $nextArtistImageImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $artistImagesList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    } // if ( $file_exists and in_array($next_artist_image_extension, $imagesExtensionsArray) ) { // that is image - get all props
                    if ( $file_exists and !in_array($next_artist_image_extension, $imagesExtensionsArray) ) { // that is NOT image - get only some props
                        $image             = $nextArtistImage->image_name;
                        $image_path        = public_path('storage/'.$next_artist_image_image_name);
                        $image_url         = $next_artist_image_url;
                        $imagePropsArray   = [ 'image'=> $image, 'image_path'=> $image_path, 'image_url'=> $image_url ];
                        $nextArtistImageImgPropsArray= ArtistImage::getImageProps( $image_path, $imagePropsArray );
//                        die("-1 XXZ+++");
                        $nextArtistImage->setArtistImageImagePropsAttribute($nextArtistImageImgPropsArray);
                        if ( !empty($nextArtistImageImgPropsArray) and is_array($nextArtistImageImgPropsArray) ) {
                            foreach( $nextArtistImageImgPropsArray as $next_document_img_key=>$next_document_img_value ) {
                                $artistImagesList[$next_key][$next_document_img_key]= $next_document_img_value;
                            }
                        }
                    }
                } // if ( ! empty($filtersArray['show_image_info'])) {
            }

        }
//        echo '<pre>$::'.print_r($artistImagesList,true).'</pre>';
//        die("-1 XXZ");
        return $artistImagesList;

    } // public static function getArtistImagesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {


    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !$is_row_retrieved ) {
            $artistImage = ArtistImage::find($id);
        }

//        echo '<pre>$additiveParams::'.print_r($additiveParams,true).'</pre>';
        with(new ArtistImage)->debToFile(print_r($additiveParams,true),' ArtistImage  -0 $additiveParams::');

        if (!empty($additiveParams['show_image']) or !empty($additiveParams['show_image_info']) ) {
            $base_url              = with(new ArtistImage)->getBaseUrl();
            with(new ArtistImage)->debToFile(print_r($base_url,true),' ArtistImage  -1 $base_url::');
            $imagesExtensionsArray = \Config::get('app.images_extensions');
            if ( ! empty($additiveParams['show_image_info'])) {
                $next_artist_image_image_name = ArtistImage::getArtistImagePath($artistImage->artist_id,  $artistImage->image_name, false);
                $next_artist_image_url        = $base_url . Storage::disk('local')->url($next_artist_image_image_name);
                $file_exists                  = Storage::disk('local')->exists('public/' . $next_artist_image_image_name);
//                echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                with(new ArtistImage)->debToFile(print_r($file_exists,true),' ArtistImage  -2 $file_exists::');
                if ($file_exists and in_array($artistImage->extension, $imagesExtensionsArray)) { // that is image - get all props
                    $image            = $artistImage->image_name;
                    $image_path       = public_path('storage/' . $next_artist_image_image_name);
                    $image_url        = $next_artist_image_url;
                    $imagePropsArray  = ['image' => $image, 'image_path' => $image_path, 'image_url' => $image_url];
                    $previewSizeArray = with(new ArtistImage)->getImageShowSize($image_path, self::$img_preview_width, self::$img_preview_height);
                    if ( ! empty($previewSizeArray['width'])) {
                        $imagePropsArray['preview_width']  = $previewSizeArray['width'];
                        $imagePropsArray['preview_height'] = $previewSizeArray['height'];
                    }
                    $artistImageImgPropsArray = ArtistImage::getImageProps($image_path, $imagePropsArray);
//                    echo '<pre>$::'.print_r($artistImageImgPropsArray,true).'</pre>';
                    with(new ArtistImage)->debToFile(print_r($artistImageImgPropsArray,true),' ArtistImage  -3 $artistImageImgPropsArray::');
                    $artistImage->setArtistImageImagePropsAttribute($artistImageImgPropsArray);
                    if ( ! empty($artistImageImgPropsArray) and is_array($artistImageImgPropsArray)) {
                        foreach ($artistImageImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $artistImage[$next_document_img_key] = $next_document_img_value;
                        }
                    }
                } // if ( $file_exists and in_array($artistImage->extension, $imagesExtensionsArray) ) { // that is image - get all props
                if ($file_exists and ! in_array($artistImage->extension, $imagesExtensionsArray)) { // that is NOT image - get only some props
                    $image                    = $artistImage->image_name;
                    $image_path               = public_path('storage/' . $next_artist_image_image_name);
                    with(new ArtistImage)->debToFile(print_r($image_path,true),' ArtistImage  -31 $image_path::');
                    $image_url                = $next_artist_image_url;
                    $imagePropsArray          = ['image' => $image, 'image_path' => $image_path, 'image_url' => $image_url];
                    with(new ArtistImage)->debToFile(print_r($artistImageImgPropsArray,true),' ArtistImage  -4 $artistImageImgPropsArray::');

                    $artistImageImgPropsArray = ArtistImage::getImageProps($image_path, $imagePropsArray);
                    $artistImage->setArtistImageImagePropsAttribute($artistImageImgPropsArray);
//                    public function setArtistImageImagePropsAttribute(array $artistImageImagePropsArray)


                    if ( ! empty($artistImageImgPropsArray) and is_array($artistImageImgPropsArray)) {
                        foreach ($artistImageImgPropsArray as $next_document_img_key => $next_document_img_value) {
                            $artistImage[$next_document_img_key] = $next_document_img_value;
                            // setArtistImageImagePropsAttribute
                        }
                    }
                }
            } // if ( ! empty($filtersArray['show_image_info'])) {
        } // if (!empty($additiveParams['show_image']) or !empty($additiveParams['show_image_info']) ) {

//                              die("-1 XXZartistImage");
        if (empty($artistImage)) return false;
        return $artistImage;
    } // public function getRowById( int $id, array $additiveParams= [] )


    public static function getValidationRulesArray($artist_image_id= null) : array
    {
        $additional_image_name_validation_rule= 'check_artist_image_unique_by_image_name:'.( !empty($artist_image_id)?$artist_image_id:'');
        $additional_image_name_valid_extension_validation_rule= 'check_image_name_valid_extension:'.( !empty($artist_image_id)?$artist_image_id:'');
        $validationRulesArray = [
            'artist_image_id'           => 'required|exists:'.( with(new ArtistImage)->getTableName() ).',id',
            'user_id'                => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'image_name'               => 'required|max:255|' . $additional_image_name_validation_rule,
            'extension'              => 'required|max:10|' . $additional_image_name_valid_extension_validation_rule,
            'info'                   => 'nullable',
        ];
        return $validationRulesArray;
    }


    public static function getSimilarArtistImageByImageName( string $image_name, int $artist_image_id, int $id= null, $return_count= false )
    {
        $quoteModel = ArtistImage::whereRaw( ArtistImage::dbStrLower('image_name', false, false) .' = '. ArtistImage::dbStrLower(ArtistImage::pgEscape($image_name), true,false) );
        $quoteModel = $quoteModel->where( 'artist_image_id', '=' , $artist_image_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getArtistImageDir(int $artist_id) : string
    {
        $UPLOADS_ARTIST_IMAGES_DIR= \Config::get('app.UPLOADS_ARTIST_IMAGES_DIR');
        return $UPLOADS_ARTIST_IMAGES_DIR . 'artist-image-' . $artist_id.'/';
    }

    public static function getArtistImagePath(int $artist_id, $image_name, bool $set_public_subdirectory= false) : string
    {
        if ( empty($image_name) ) return '';
        $UPLOADS_ARTIST_IMAGES_DIR= ($set_public_subdirectory ? "public/" : "") . \Config::get('app.UPLOADS_ARTIST_IMAGES_DIR');
        return $UPLOADS_ARTIST_IMAGES_DIR . 'artist-image-' . $artist_id . '/' . $image_name;
    }

    public function getArtistImageImagePropsAttribute($only_existing_files= false) : array
    {
        return $this->artistImageImagePropsArray;
    }

    public function setArtistImageImagePropsAttribute(array $artistImageImagePropsArray)
    {
        $this->artistImageImagePropsArray = $artistImageImagePropsArray;
    }

    public static function fillTranslations()
    {
        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $artistImagesList = ArtistImage::getArtistImagesList(ListingReturnData::LISTING, []);
        foreach ($artistImagesList as $nextArtistImage) {
            $artist_image_id       = $nextArtistImage->id;
            $description       = $nextArtistImage->description;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newArtistImageTranslation= new ArtistImageTranslation();
                $newArtistImageTranslation->artist_image_id= $artist_image_id;
                if ( $next_lang_key == 'en' ) {
                    $newArtistImageTranslation->description = $description;
                } else {
                    $newArtistImageTranslation->description = $next_lang_label . ' VERSION : ' . $description;
                }
                $newArtistImageTranslation->locale= $next_lang_key;

                $newArtistImageTranslation->created_at= now();
                $newArtistImageTranslation->save();
            }

        }
    }

}

