<?php

namespace App\Http\Controllers;
//namespace Admin;

use Illuminate\Routing\Controller as BaseController;
use Route;
use Config;
use DB;
use JavaScript;
use Illuminate\Http\Request;
use Auth;
//use App\Carbon;
use App\Http\Traits\funcsTrait;
use App\Settings;
use App\UsersGroups;
use App\User;
use App\library\ListingReturnData;


class MyAppController extends BaseController
{
    protected $current_admin_template;
    protected $current_frontend_template;
    protected $commonVarsArray = [];
    protected $loggedUserData = [];
    protected $logged_user_id = null;
    protected $page_title;
    protected $breadcrumbsList = [];
    protected $filtersFieldsArray = [];
    protected $filtersFieldValuesArray = [];
    protected $dashboard_home_url = '';

    protected $data_strip_tags = true;
    protected $clear_doubled_spaces = true;
    protected $stripslashes = true;

    protected $add_page_parameters_with_sort = true;
    protected $add_page_parameters_without_sort = true;

    protected $defaultRequestsArray = [];


    use funcsTrait;

    public function __construct()
    {

        /* define common app vars which would be used in blade templates */
        $this->dashboard_home_url = $this->getDashboardHome();

        $this->commonVarsArray['site_name'] = Settings::getValue('site_name');

        $this->commonVarsArray['route_name'] = Route::getCurrentRoute()->getActionName();

        $action     = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);

        list($controller, $action) = explode('@', $controller);
        $this->commonVarsArray['controller_name'] = $controller;
        $this->commonVarsArray['action_name']     = $action;

/*        $IS_DEMO                          = env('IS_DEMO');
//        echo '<pre>$IS_DEMO::'.print_r($IS_DEMO,true).'</pre>';
        $this->commonVarsArray['is_demo'] = $IS_DEMO;
//                                       die("-1 XXZ");
        $images_dir                          = env('IMAGES_DIR');
        $this->commonVarsArray['images_dir'] = $images_dir;

        $this->current_admin_template                    = env('CURRENT_ADMIN_TEMPLATE');
        $this->commonVarsArray['current_admin_template'] = $this->current_admin_template;

        $this->current_frontend_template                    = env('CURRENT_FRONTEND_TEMPLATE');
        $this->commonVarsArray['current_frontend_template'] = $this->current_frontend_template;

        $this->show_avatar_image                    = env('SHOW_AVATAR_IMAGE');
        $this->commonVarsArray['show_avatar_image'] = $this->show_avatar_image;*/

//        $this->commonVarsArray['dtparam'] = time();
//
//        $this->commonVarsArray['copyright_text'] = Settings::getValue('copyright_text');
//        echo '<pre>$this->commonVarsArray::'.print_r($this->commonVarsArray,true).'</pre>';
//        die("-1 XXZ==");
        /* lang, submit_message_by_enter,  show_online_status */
    }



    /* Render errors list as string */
    protected function addVariablesToJS(array $additiveVariablesList=[]) {
        $PUSHER_APP_KEY= env('PUSHER_APP_KEY');
        $API_VERSION= Config::get('app.API_VERSION',1);
        $GOOGLE_API_KEY= Config::get('app.GOOGLE_API_KEY');
        $API_VERSION_LINK= 'api/';
        $API_ADMIN_VERSION_LINK= 'api/admin/';
        $dashboard_home_url = $this->getDashboardHome();

        $current_locale= \App::getLocale();
        $site_name  = Settings::getValue('site_name', '');
        $main_image_recommended_height = config('app.main_image_recommended_height');
        $main_image_recommended_width = config('app.main_image_recommended_width');


        $this->javaScriptVarsList= [ 'PUSHER_APP_KEY'=>$PUSHER_APP_KEY, 'API_VERSION' => $API_VERSION, 'API_VERSION_LINK' => $API_VERSION_LINK, 'GOOGLE_API_KEY' => $GOOGLE_API_KEY,
                                     'API_ADMIN_VERSION_LINK' => $API_ADMIN_VERSION_LINK, 'DASHBOARD_HOME_URL'=>  $dashboard_home_url, 'SITE_NAME'=> $site_name ,
                                     'MAIN_IMAGE_RECOMMENDED_HEIGHT'=> $main_image_recommended_height , 'MAIN_IMAGE_RECOMMENDED_WIDTH'=> $main_image_recommended_width,
            'CURRENT_LOCALE'=> $current_locale];

        $userProfile = Auth::user();
        if ( !empty($userProfile->email) ) {

            $settingsList = Settings::getSettingsList([], true);
            foreach ($settingsList as $next_key => $next_value) {
                $this->javaScriptVarsList['settings_'.$next_key] = $next_value;
            }


//            $priority_colors= Settings::getValue('priority_colors');
//            $priorityColorsArray= $this->splitKeyValuesTokens($priority_colors, true);
//
//            $is_past_colors= Settings::getValue('is_past_colors');
//            $isPastColorsArray= $this->splitKeyValuesTokens($is_past_colors, true);
        }

        $show_alert_popup = Settings::getValue('show_alert_popup', 'N');

        if (  $show_alert_popup == 'Y' ) { // important information which must be accessible on any page for alerts
            /*         $active_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'A']);
        $pending_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'P']);
        $new_users_count= User::getUsersList(ListingReturnData::ROWS_COUNT, ['active_status'=>'N']);
        $new_invoice_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'I']);
        $processing_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'P']);
 */
//            $active_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'A']);

            $alert_text= '';
//            $new_task_assigned_to_user_count = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::ROWS_COUNT, ['user_id' => $userProfile->id, 'status' => 'N']);
//            echo '<pre>$new_task_assigned_to_user_count::' . print_r($new_task_assigned_to_user_count, true) . '</pre>';

//            if ( $new_task_assigned_to_user_count > 0 ) {
//                $alert_text .= '<a href="' . $this->getDashboardHome() . '/admin/user/index?filter_active_status=N"><b>' . $new_task_assigned_to_user_count . '</b> New not accepted task(s)</a>&nbsp;<br>';
//            }
            /*                if ( $new_product_comments > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/user/new_users_listing"><b>' . $new_product_comments . '</b> New product comment(s)</a><br>';
                            }
                            if ( $is_inappropriate_product_comments > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/product/inappropriate_comments_listing"><b>' . $is_inappropriate_product_comments . '</b> Inappropriate Comment(s)</a><br>';
                            }
                            if ( count($orderItemBackOrdersArray) > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/order/index/page/1/filter_order_item_back_order/1"><b>' . count($orderItemBackOrdersArray) . '</b> Orders(s) with back order items </a><br>';
                            }
                            $twig_Environment->addGlobal( 'backend_alert_text', appUtils::trimRightSubString($alert_text,'<br>') );*/

//            echo '<pre>$alert_text::'.print_r($alert_text,true).'</pre>';
            $this->javaScriptVarsList['app_alert_text']= $alert_text;
//            die("-1 XXZ=====");
        } // if (  $show_alert_popup == 'Y' ) { // important information which must be accessible on any page for alerts

        if ( empty($this->javaScriptVarsList['CURRENT_LANG']) ) $this->javaScriptVarsList['CURRENT_LANG']= 'en';
        if ( empty($this->javaScriptVarsList['API_VERSION']) ) $this->javaScriptVarsList['API_VERSION']= 1;
        if ( empty($this->javaScriptVarsList['API_VERSION_LINK']) ) $this->javaScriptVarsList['API_VERSION_LINK']= 'api/';
        if ( empty($this->javaScriptVarsList['API_ADMIN_VERSION_LINK']) ) $this->javaScriptVarsList['API_ADMIN_VERSION_LINK']= 'api/admin/';
//        if ( empty($this->javaScriptVarsList['user_profile_background_color']) ) $this->javaScriptVarsList['user_profile_background_color']= '#ffffff';
//        if ( empty($this->javaScriptVarsList['user_profile_color']) ) $this->javaScriptVarsList['user_profile_color']= '#000000';
//        if ( empty($this->javaScriptVarsList['user_profile_lang']) ) $this->javaScriptVarsList['user_profile_lang']= 'en';
        if ( empty($this->javaScriptVarsList['settings_items_per_page']) ) $this->javaScriptVarsList['settings_items_per_page']= 20;
        //if ( empty($this->javaScriptVarsList['settings_site_name']) ) $this->javaScriptVarsList['settings_site_name']= '';
        if ( empty($this->javaScriptVarsList['settings_max_str_length_in_listing']) ) $this->javaScriptVarsList['settings_max_str_length_in_listing']= 50;

        $pickdate_format_submit= Config::get('app.pickdate_format_submit','yyyy-mm-dd');
        if ( empty($this->javaScriptVarsList['settings_pickdate_format_submit']) ) $this->javaScriptVarsList['settings_pickdate_format_submit']= $pickdate_format_submit;



        $js_moment_date_format= Config::get('app.js_moment_date_format','yyyy-mm-dd');
        if ( empty($this->javaScriptVarsList['settings_js_moment_date_format']) ) $this->javaScriptVarsList['settings_js_moment_date_format']= $js_moment_date_format;


        $js_moment_datetime_format= Config::get('app.js_moment_datetime_format','Do MMMM, YYYY h:mm:ss A');
        if ( empty($this->javaScriptVarsList['settings_js_moment_datetime_format']) ) $this->javaScriptVarsList['settings_js_moment_datetime_format']= $js_moment_datetime_format;


        $futurePastColorsArray= config('app.futurePastColors', []);
        if (!empty($futurePastColorsArray) and empty($this->javaScriptVarsList['settings_futurePastColorsArray'])) {
            $this->javaScriptVarsList['settings_futurePastColorsArray'] = $futurePastColorsArray;
        }

        $votingColorsArray= config('app.votingColors', []);
        if (!empty($votingColorsArray) and empty($this->javaScriptVarsList['settings_votingColorsArray'])) {
            $this->javaScriptVarsList['settings_votingColorsArray'] = $votingColorsArray;
        }

        $rowsPerPageItemsArray= config('app.rowsPerPageItems', []);
        if (!empty($rowsPerPageItemsArray) and empty($this->javaScriptVarsList['settings_rowsPerPageItemsArray'])) {
            $this->javaScriptVarsList['settings_rowsPerPageItemsArray'] = $rowsPerPageItemsArray;
        }
        $this->loggedUserData= Auth::user();
        if ( $this->loggedUserData != null ) {
            $this->logged_user_id= $this->loggedUserData->id;
            $this->javaScriptVarsList['logged_user_id']= $this->logged_user_id;
            $this->javaScriptVarsList['logged_user_name']= $this->loggedUserData->username;
            $this->javaScriptVarsList['logged_user_first_name']= $this->loggedUserData->first_name;
            $this->javaScriptVarsList['logged_user_last_name']= $this->loggedUserData->last_name;
        }

        $menuIconsArray=  Config::get('app.menuIconsArray',[]);

        if ( empty($this->javaScriptVarsList['menuIconsArray']) ) $this->javaScriptVarsList['menuIconsArray']= $menuIconsArray;

//        echo '<pre>+++3 $this->javaScriptVarsList::'.print_r($this->javaScriptVarsList,true).'</pre>';
//        die("-1 XXZ");
        JavaScript::put($this->javaScriptVarsList);
    }

    /* Render errors list as string */
    protected function getErrorsListAsText( array $errorsList, bool $add_br= false ) : string
    {
        $ret_str= '';
        foreach( $errorsList as $next_key=>$next_error ) {
            $ret_str.= $next_error.', '.($add_br?'<br>':'');
        }
        return with(new MyAppController)->trimRightSubString($ret_str, ', '.($add_br?'<br>':''));
    }



    protected function getRequestParamsArray() : array
    {
        $requestArray= request()->all();
//        echo '<pre>$requestArray::'.print_r($requestArray,true).'</pre>';
//        echo '<pre>$this->defaultRequestsArray::'.print_r($this->defaultRequestsArray,true).'</pre>';
        foreach( $this->defaultRequestsArray as $next_default_request_key=> $next_default_request_key_value ) {
            if ( !empty($next_default_request_key) and !empty($next_default_request_key_value) )
            $requestArray[$next_default_request_key]= $next_default_request_key_value;
        }

        return $requestArray;
    }

    /* Get parameter value from POST/GET request with possible default value */
    protected function getParameter( $param_name, $default_value= null, $flags_array= [] ) {
//        $requestsArray= request()->all();
        $requestsArray= $this->getRequestParamsArray();
//        echo '<pre>$requestsArray::'.print_r($requestsArray,true).'</pre>';
//        die("-1 XXZ777777777");
        if ( isset($requestsArray[$param_name]) ) {
            $ret= $requestsArray[$param_name];
            if (is_array($ret)) {
                $retArray= [];
                foreach ($ret as $item) {
//                    echo '<pre>$item::'.print_r($item,true).'</pre>';
                    $retArray[]= empty($flags_array['skip_urldecode']) ? urldecode($item) : $item;
                }
                return $retArray;
            }
            return empty($flags_array['skip_urldecode']) ? urldecode($ret) : $ret;
        }

        if (is_array($default_value)) {
            $retArray= [];
            foreach ($default_value as $item) {
                $retArray[]= empty($flags_array['skip_urldecode']) ? urldecode($item) : $item;
            }
            return $retArray;
        }

        $default_value= empty($flags_array['skip_urldecode']) ? urldecode($default_value) : $default_value;
        return $default_value;
    }

    /* add variables which must be accessible in blade views */
    protected function addTemplateVars( $varsArray= [], $additiveVarsArray= [] ) {
        $this->commonVarsArray['page_title']= $this->page_title;
        $this->commonVarsArray['page_parameters_with_sort']= '';
        $this->commonVarsArray['page_parameters_without_sort']= '';
        $this->commonVarsArray['filters_hint_text']= '';
        $this->commonVarsArray['filters_hint_count']= '';

        $this->commonVarsArray['dashboard_home_url']= $this->dashboard_home_url;


        if ( $this->add_page_parameters_with_sort ) {
            /* prepare string with filters/order_by parameters for $this->filtersFieldsArray based on current request. It would be used in redirecting to editor/listing to keep current filters/order_by parameters */
            $page_parameters_with_sort = $this->preparePageParameters($this->filtersFieldsArray, $this->getRequestParamsArray() /* request()->all() */, true, true, true);
//            echo '<pre>$page_parameters_with_sort::'.print_r($page_parameters_with_sort,true).'</pre>';
//            die("-1 XXZ");
            $this->commonVarsArray['page_parameters_with_sort']= $page_parameters_with_sort;
        } // if ( !empty($additiveVarsArray['add_page_parameters_with_sort']) {
        if ( $this->add_page_parameters_without_sort ) {
            $page_parameters_without_sort = $this->preparePageParameters($this->filtersFieldsArray, /* request()->all() */ $this->getRequestParamsArray(), false, false, true);
            $this->commonVarsArray['page_parameters_without_sort']= $page_parameters_without_sort;
        } // if ( !empty($additiveVarsArray['add_page_parameters_without_sort']) {

        if ( !empty($additiveVarsArray['add_filters_values']) and is_array($this->filtersFieldsArray) and count($this->filtersFieldsArray)> 0 ) {
            foreach( $this->filtersFieldsArray as $next_filter_field ) {
                $next_filter_value= $this->getParameter( $next_filter_field, '' );
                $this->filtersFieldValuesArray[$next_filter_field]= $next_filter_value;
            }
            $this->commonVarsArray['filtersFieldValuesArray']= $this->filtersFieldValuesArray;
        }

        if ( !empty($additiveVarsArray['add_filters_hint'])) {
            /* prepare filters labels for fields */
            $filters_hint_text= $this->getFiltersLabel( !empty($this->commonVarsArray['filtersFieldValuesArray']) ? $this->commonVarsArray['filtersFieldValuesArray'] : [], ', ' );
            $filters_hint_count= $this->getFiltersCount( !empty($this->commonVarsArray['filtersFieldValuesArray']) ? $this->commonVarsArray['filtersFieldValuesArray'] : [], true );
            $this->commonVarsArray['filters_hint_text']= $filters_hint_text;
            $this->commonVarsArray['filters_hint_count']= $filters_hint_count;
        }

        $show_alert_popup = Settings::getValue('show_alert_popup', 'N');
        echo '<pre>$show_alert_popup::'.print_r($show_alert_popup,true).'</pre>';
        die("-1 XXZ");


        $demo_admin_email= env('DEMO_ADMIN_EMAIL');
        $userProfile = Auth::user();
        if ( !empty($userProfile->email) ) {
            $logged_user_email = $userProfile->email;
            if ($logged_user_email == $demo_admin_email and ! $this->isDeveloperComp()) {
                $show_alert_popup = 'Y';
            }

//            echo '<pre>$userProfile::'.print_r($userProfile,true).'</pre>';
//            foreach ($userProfileList as $next_key => $next_value) {
//                $this->javaScriptVarsList[$next_key] = $next_value;
//            }
//            JavaScript::put($this->javaScriptVarsList);
//            die("-1 XXZ");

        }
        if (  $show_alert_popup == 'Y' ) { // important information which must be accessible on any page for alerts
            /*         $active_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'A']);
        $pending_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'P']);
        $new_users_count= User::getUsersList(ListingReturnData::ROWS_COUNT, ['active_status'=>'N']);
        $new_invoice_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'I']);
        $processing_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'P']);
 */
//            $active_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'A']);

//            $new_task_assigned_to_user_count= TaskAssignedToUser::getTaskAssignedToUsersList( ListingReturnData::ROWS_COUNT, [ 'user_id'=>$userProfile->id, 'status'=='N' ] );
//            echo '<pre>$new_task_assigned_to_user_count::'.print_r($new_task_assigned_to_user_count,true).'</pre>';
//            die("-1 XXZ=====");
//                public static function getTaskAssignedToUsersList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {

                /*            $pending_products_count= Product::getProductsList(ListingReturnData::ROWS_COUNT, ['status'=>'P']);
                            $new_users_count= User::getUsersList(ListingReturnData::ROWS_COUNT, ['active_status'=>'N']);
                            $trade_manager_hasnot_accepted_rows_count  = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['trade_manager_user_id'=>$this->logged_user_id, 'trade_manager_accepted'=>'N']);

                //            echo '<pre>$trade_manager_hasnot_accepted_rows_count::'.print_r($trade_manager_hasnot_accepted_rows_count,true).'</pre>';
                //            die("-1 XXZ");
                            $alert_text = '';

                            $new_invoice_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'I']);
                //            echo '<pre>$new_invoice_orders_count::'.print_r($new_invoice_orders_count,true).'</pre>';

                            $processing_orders_count = Order::getOrdersList(ListingReturnData::ROWS_COUNT, ['status'=> 'P']);*/
//            echo '<pre>$processing_orders_count::'.print_r($processing_orders_count,true).'</pre>';
//
//            die("-1 XXZ");
            /*                if ( $new_invoice_orders_count > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/order/index/page/1/filter_status/I"><b>' . $new_invoice_orders_count . '</b> New Invoice Order(s)</a><br>';
                            }
                            if ( $processing_orders_count > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/order/index/page/1/filter_status/P"><b>' . $processing_orders_count . '</b> Processing Order(s)</a><br>';

                            }*/
            if ( $trade_manager_hasnot_accepted_rows_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/profile/index">You have <b>' . $trade_manager_hasnot_accepted_rows_count . '</b> not accepted orders(s)</a>&nbsp;<br><hr>';
            }

            if ( $active_products_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/product/index?filter_status=A"><b>' . $active_products_count . '</b> Active Product(s)</a>&nbsp;<br>';
            }
            if ( $pending_products_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/product/index?filter_status=P"><b>' . $pending_products_count . '</b> Pending Product(s)</a>&nbsp;<br>';
            }

            if ( $new_invoice_orders_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/order/index?filter_status=I"><b>' . $new_invoice_orders_count . '</b> New invoice orders(s)</a>&nbsp;<br>';
            }
            if ( $processing_orders_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/order/index?filter_status=P"><b>' . $processing_orders_count . '</b> Processing orders(s)</a>&nbsp;<br>';
            }



            if ( $new_users_count > 0 ) {
                $alert_text .= '<a href="' . $this->getBackendHome() . '/admin/user/index?filter_active_status=N"><b>' . $new_users_count . '</b> New User(s)</a>&nbsp;<br>';
            }
            /*                if ( $new_product_comments > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/user/new_users_listing"><b>' . $new_product_comments . '</b> New product comment(s)</a><br>';
                            }
                            if ( $is_inappropriate_product_comments > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/product/inappropriate_comments_listing"><b>' . $is_inappropriate_product_comments . '</b> Inappropriate Comment(s)</a><br>';
                            }
                            if ( count($orderItemBackOrdersArray) > 0 ) {
                                $alert_text .= '<a href="' . base_url() . 'admin/order/index/page/1/filter_order_item_back_order/1"><b>' . count($orderItemBackOrdersArray) . '</b> Orders(s) with back order items </a><br>';
                            }
                            $twig_Environment->addGlobal( 'backend_alert_text', appUtils::trimRightSubString($alert_text,'<br>') );*/



            $this->commonVarsArray['app_alert_text']= $alert_text;
        } // if (  $show_alert_popup == 'Y' ) { // important information which must be accessible on any page for alerts


        return array_merge($this->commonVarsArray, $varsArray);
    }

    public function getCurrentAdminTemplate() {
        return $this->current_admin_template;
    }

    public function getCurrentFrontendTemplate() {
        return $this->current_frontend_template;
    }

    /* if 1st $commonParams = true all app predefined vars are accessible in JS scripts,$paramsArray is additive list of vars are accessible in JS scripts */
    public function prepareAppParamsForJS_DEL($commonParams = true, $paramsArray = [])
    {
        $resArray = [];
        if ($commonParams) { // all app predefined vars are accessible in JS scripts

            $this->commonVarsArray['dashboard_home_url']= $this->dashboard_home_url;
            if (Auth::check()) {
                $this->logged_user_id= Auth::user()->id;
                $this->commonVarsArray['logged_user_id']= Auth::user()->id;
                $this->commonVarsArray['logged_username']= Auth::user()->username;
                $logged_user_image= $this->getLoggedUserImage(Auth::user()->id, Auth::user()->email);
                $this->commonVarsArray['logged_user_image']= $logged_user_image;
            } else {
                $this->logged_user_id= '';
                $this->commonVarsArray['logged_user_id']= '';
                $this->commonVarsArray['logged_username']= '';
                $this->commonVarsArray['logged_user_image']= '';
            }
            foreach( $this->commonVarsArray as $next_var_key=>$next_var_value ) {
                $resArray[$next_var_key]= $next_var_value;
            }
            $resArray['dashboard_home_url'] = !empty($this->commonVarsArray['dashboard_home_url']) ? $this->commonVarsArray['dashboard_home_url'] : '';
            $resArray['site_name'] = !empty($this->commonVarsArray['site_name']) ? $this->commonVarsArray['site_name'] : '';
            $resArray['controller_name'] = !empty($this->commonVarsArray['controller_name']) ? $this->commonVarsArray['controller_name'] : '';
            $resArray['action_name'] = !empty($this->commonVarsArray['action_name']) ? $this->commonVarsArray['action_name'] : '';
            $resArray['pickadate_format_submit'] = !empty($this->commonVarsArray['pickadate_format_submit']) ? $this->commonVarsArray['pickadate_format_submit'] : '';
            $resArray['pickadate_format_view'] = !empty($this->commonVarsArray['pickadate_format_view']) ? $this->commonVarsArray['pickadate_format_view'] : '';
            $resArray['datepicker_min_year'] = !empty($this->commonVarsArray['datepicker_min_year']) ? $this->commonVarsArray['datepicker_min_year'] : '';
            $resArray['datepicker_max_year'] = !empty($this->commonVarsArray['datepicker_max_year']) ? $this->commonVarsArray['datepicker_max_year'] : '';
            $resArray['csrf_token'] = csrf_token();
            $ajax_timeout= env('AJAX_TIMEOUT');
            if ( empty($ajax_timeout) ) $ajax_timeout = 20000;
            $resArray['ajax_timeout'] = $ajax_timeout;
        }
//        echo '<pre>$paramsArray::'.print_r($paramsArray,true).'</pre>';
        if (isset($paramsArray) and is_array($paramsArray)) {
            foreach ($paramsArray as $next_param_key => $next_param_value) {
                $resArray[$next_param_key] = $next_param_value;
            }
        } // if ($commonParams) { // all app predefined vars are accessible in JS scripts

        $ret_code = '';
        foreach ($resArray as $next_param_key => $next_param_value) {
            if ( is_array($next_param_value) ) {
                $ret_code .= '  ' . $next_param_key . ' : ' . "'" . json_encode($next_param_value) . "', ";
            } else {
                $ret_code .= '  ' . $next_param_key . ' : ' . "'" . addslashes(strip_tags($next_param_value)) . "', ";
            }
        }
        return $ret_code;
    }


    protected function setDefaultRequests( array $defaultRequestsArray)
    {
        $this->defaultRequestsArray = $defaultRequestsArray;
//        echo '<pre>++ setDefaultRequests$this->defaultRequestsArray::'.print_r($this->defaultRequestsArray,true).'</pre>';

    }

    protected function setPageTitle(string $page_title)
    {
        $this->page_title = $page_title;
    }

    protected function setBreadcrumbs(array $breadcrumbsList)
    {
        $this->breadcrumbsList = $breadcrumbsList;
        return $this->breadcrumbsList;
    }

    /* prepare string with filters/order_by parameters for $this->filtersFieldsArray based on current request. It would be used in redirecting to editor/listing to keep current filters/order_by parameters */
    protected function preparePageParameters(array $filtersFieldsArray, array $uriParameters, bool $with_page, bool $with_order_by, bool  $ret_string= true)
    {
//        echo '<pre>$with_order_by::'.print_r($with_order_by,true).'</pre>';
        if ( !$with_order_by ) {
            $filtersFieldsArray= $this->unsetArrayByValue('order_direction', $filtersFieldsArray);
            $filtersFieldsArray= $this->unsetArrayByValue('order_by', $filtersFieldsArray);
        }
//        echo '<pre>$filtersFieldsArray::'.print_r($filtersFieldsArray,true).'</pre>';
        $res_str = '';
        $retArray = [];
        if ($with_page) {
            $res_str .= !empty($uriParameters['page']) ? 'page=' . $uriParameters['page'] . '&' : 'page=1&';
            if (!empty($uriParameters['page'])) $retArray['page']= $uriParameters['page'];
        }
        foreach( $filtersFieldsArray as $next_filter_params ) {
            if ( $next_filter_params == 'order_direction' or $next_filter_params == 'order_by' ) continue;
            $res_str .= ! empty($uriParameters[$next_filter_params]) ? $next_filter_params . '=' . $uriParameters[$next_filter_params] . '&' : '';
            if (! empty($uriParameters[$next_filter_params])) $retArray[$next_filter_params]= $uriParameters[$next_filter_params];
        }
        if ($with_order_by) {
            $res_str .= !empty($uriParameters['order_direction']) ? 'order_direction=' . $uriParameters['order_direction'] . '&' : '';
            $res_str .= !empty($uriParameters['order_by']) ? 'order_by=' . $uriParameters['order_by'] . '&' : '';
            if ( !empty($uriParameters['order_direction']) )$retArray['order_direction']= $uriParameters['order_direction'];
            if ( !empty($uriParameters['order_by']) ) $retArray['order_by']= $uriParameters['order_by'];
        }
        if (substr($res_str, strlen($res_str) - 1, 1) == '&') {
            $res_str = substr($res_str, 0, strlen($res_str) - 1);
        }
        return ( $ret_string ? $res_str : $retArray );
    }

    protected function setFlashMessage(string $message_text, string $action_status= 'success')
    {
        \Session::flash('action_text', $message_text);
        \Session::flash('action_status', $action_status);
    }

    /* prepare filters labels for fields, in which key/code must be replaced with label text */
    protected function getFiltersLabel( array $itemsArray, string $splitter ) : string
    {
        $ret_str= '';
        foreach( $itemsArray as $next_key=> $next_value ) {
            if ( !empty($next_value) ) {
                $ret_str.= ucfirst(str_replace( '_',' ', str_replace( ['filter_', '_id'], '',str_replace('_formatted','',$next_key) ) ) ) .
                ' :&nbsp;<strong>'.$next_value.'</strong>' . $splitter;
		    }
	    }
        return with(new MyAppController)->trimRightSubString($ret_str, $splitter );
    }

    /* get filters count to show it in Filter button hint */
    protected function getFiltersCount( array $itemsArray, bool $show_bracket= false ) : string
    {
        $filters_count = 0;
        foreach( $itemsArray as $next_key=> $next_value ) {
            if ( !empty($next_value) or (strlen($next_value) > 0 ) ) {
                $filters_count++;
		    }
	    }
	    if ( $filters_count > 0 and $show_bracket ) {
            return '('.$filters_count.')';
        }
        return '';
    }

    protected function showDebugInfo($data, string $label= '', $indent_code= true, $is_debug= false):string
    {
        if ( !$is_debug and !$is_debug ) return '';
        if ( is_array($data) ) {
            $ret = '<pre>' . count($data) . $label . print_r($this->setArrayAsHtmlSpecialchars($data), true) . '</pre>';
        } else {
            if ( $indent_code ) {
                $ret = '<pre>' . $label . print_r(htmlspecialchars($this->pp($data)), true) . '</pre>';
            } else {
                $ret = '<pre>' . $label . print_r(htmlspecialchars($data), true) . '</pre>';
            }
        }
        echo $ret;
        return $ret;
    }
    protected function pp($html)
    {
        $config = array(
            'indent'       => true,
            'output-xhtml' => false,
            'wrap'         => 300
        );
        $tidy   = new \tidy;
        $tidy->parseString($html, $config, 'utf8');
        $tidy->cleanRepair();

        return $tidy;
    }

    /* Submitting form string value must be worked out according to options of app */
/*    protected function workTextString($str) : string
    {
        if ( empty($str) ) return '';
        if ( $this->clear_doubled_spaces ) {
            $str = $this->makeClearDoubledSpaces($str);
        }
        if ( $this->data_strip_tags ) {
            $str = $this->makeStripTags($str);
        }
        if ( $this->stripslashes ) {
            $str = $this->makeStripslashes($str);
        }
        return trim($str);
    }*/

 
}
