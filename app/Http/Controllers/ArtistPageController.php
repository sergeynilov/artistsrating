<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Input;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Http\Traits\funcsTrait;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\Settings;
use App\User;
use App\Http\Requests\UserRegisterRequest;
use App\ArtistConcert;
use App\Artist;
use App\Song;
use App\ArtistImage;
use App\ArtistVote;
use App\SongVote;
use App\SongArtist;
use App\Tour;
use App\CmsItem;
use Cart;

class ArtistPageController extends MyAppController
{
    use funcsTrait;
    

    ///////// ARTIST PAGE BLOCK START //////////
    public function show()
    {
        
        $loggedUser             = Auth::user();
        $user_has_already_voted = false;
        $artist_is_in_cart= false;
        try {

            $artist_slug = $this->getParameter('artist_slug');    //http://local-artists-rating.com/api/artist_page_show/beyonce
            $typeArray   = $this->getParameter('type');
            $locale = $this->getParameter('locale');

            $artistImagesList   = [];
            $artistMainImage    = [];
            $artistConcertsList = [];
            $songArtistsList    = [];



/*          FOR DEBUG
            $locale= 'en';
            $typeArray= ['main_image'];
            $artist_slug= 'beyonce';*/


            $artist = Artist::getSimilarArtistBySlug($artist_slug);
//            echo '<pre>$artist_slug::'.print_r($artist_slug,true).'</pre>';
//            echo '<pre>$artist::'.print_r($artist,true).'</pre>';
//            die("-1 XXZ======");
            if ($artist == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'artist.artist_not_found',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            if ($artist->is_active != 'A') {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'artist.artist_not_active',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $artist_id = $artist->id;
            $artist    = Artist::getRowById($artist_id, ['show_related_songs_count' => 1, 'show_future_concerts_count' => 1, 'show_related_votes' => 1, 'show_related_images_count' => 1, 'locale'=>$locale]);

            if (in_array('images', $typeArray)) {
                $artistImagesList = ArtistImage::getArtistImagesList(ListingReturnData::LISTING, ['artist_id' => $artist_id, 'show_image_info' => 1, 'locale'=>$locale]);
            }

            if (in_array('main_image', $typeArray)) {
                if (empty($artistImagesList)) {
                    $artistImagesList = ArtistImage::getArtistImagesList(ListingReturnData::LISTING, ['artist_id' => $artist_id, 'show_image_info' => 1, 'is_main'=> 1, 'locale'=>$locale ]);
                }
                foreach( $artistImagesList as $nextArtistImage ) {
//                    echo '<pre>$nextArtistImage->is_main::'.print_r($nextArtistImage->is_main,true).'</pre>';
                    if ( $nextArtistImage->is_main ) {
                        $artistMainImage= $nextArtistImage;
                    }
                }
            }

            if (in_array('concerts', $typeArray)) {
                $artistConcertsList = ArtistConcert::getArtistConcertsList(ListingReturnData::LISTING, [
                    'artist_id'         => $artist_id,
                    'show_tour_name'    => 1,
                    'show_country_name' => 1,
                    'show_future_days'  => 1,

                    'locale'=> $locale
                ], 'event_date', 'asc');
            }

            if (in_array('song_artists', $typeArray)) {
                $songArtistsList = SongArtist::getSongArtistsList(ListingReturnData::LISTING,
                    ['artist_id' => $artist_id, 'show_song_name' => 1, 'show_related_song_votes' => 1, 'song_is_active' => true, 'locale'=>$locale]
                    , 'song_ordering', 'desc');
            }

            if (in_array('artist_voted', $typeArray) and !empty($loggedUser->id)) {
                $artistVotesList = ArtistVote::getArtistVotesList(ListingReturnData::LISTING, ['artist_id' => $artist_id, 'user_id' => $loggedUser->id, 'locale'=>$locale]);
                $user_has_already_voted = $artistVotesList->count() > 0;
            }

            $cartItems = Cart::content();
//            echo '<pre>0000 $artist_id::'.print_r($artist_id,true).'</pre>';
            foreach( $cartItems as $nextCartItem ) {
                $options= $nextCartItem->options;
//                echo '<pre>$nextCartItem::'.print_r($nextCartItem,true).'</pre>';
//                echo '<pre>$options::'.print_r($options,true).'</pre>';

                if ( $options["product_type"] == "artist" and (int)$options["product_id"] == (int)$artist_id ) {
                    $artist_is_in_cart= true;
//                    die("-1 XXZ FOUND");
                    break;
                }
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        sleep(  1 );

//        echo '<pre>$artist_is_in_cart::'.print_r($artist_is_in_cart,true).'</pre>';
        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'artist'                 => $artist,
//            'artistSongsList'       => $artistSongsList,
            'artistImagesList'       => $artistImagesList,
            'artistMainImage'        => $artistMainImage,
            'artistConcertsList'     => $artistConcertsList,
            'songArtistsList'        => $songArtistsList,
            'user_has_already_voted' => $user_has_already_voted,
            'artist_is_in_cart'      => $artist_is_in_cart,
        ], HTTP_RESPONSE_OK);
    }

    public function artist_page_dictionaries()
    {
        try {
            $artistHasConcertsSelectionList = Artist::getArtistHasConcertsValueArray(true);
            $artistIsActiveSelectionList    = Artist::getArtistIsActiveValueArray(true);
            $artistSingleSelectionList      = Artist::getArtistSingleValueArray(true);
            $artistVoteSelectionList        = [];
            $artistVoteSelectionTempList    = ArtistVote::getArtistVoteValueArray(true);
            $countriesList                  = $this->getCountries(true);
            $votingColorsArray              = config('app.votingColors', []);
            foreach ($artistVoteSelectionTempList as $next_key => $nextArtistVoteSelection) {
                $color = '';
                foreach ($votingColorsArray as $next_voting_key => $next_voting_color) {
                    if ($next_voting_key == $nextArtistVoteSelection['key']) {
                        $color = $next_voting_color;
                        break;
                    }
                }
                $artistVoteSelectionList[] = ["key" => $nextArtistVoteSelection['key'], "label" => $nextArtistVoteSelection['label'], 'color' => $color];
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                     => 0,
            'message'                        => '',
            'artistHasConcertsSelectionList' => $artistHasConcertsSelectionList,
            'artistSingleSelectionList'      => $artistSingleSelectionList,
            'artistIsActiveSelectionList'    => $artistIsActiveSelectionList,
            'countriesList'                  => $countriesList,
            'artistVoteSelectionList'        => $artistVoteSelectionList,
        ], HTTP_RESPONSE_OK);
    } // public function artist_page_dictionaries()


    public function make_artist_voting()
    {
        if (!Auth::check()) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'common.you_have_to_login',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $loggedUser = Auth::user();
        $artist_id  = $this->getParameter('artist_id');
        $vote       = $this->getParameter('vote_value');
        try {

            $artistVotesList = ArtistVote::getArtistVotesList(ListingReturnData::LISTING, ['artist_id' => $artist_id, 'user_id' => $loggedUser->id]);
            if (count($artistVotesList) > 0) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'artist.you_have_already_voted',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $newArtistVote            = new ArtistVote();
            $newArtistVote->artist_id = $artist_id;
            $newArtistVote->user_id   = $loggedUser->id;
            $newArtistVote->vote      = $vote;
            DB::beginTransaction();
            $newArtistVote->save();
            $new_artist_vote_id = $newArtistVote->id;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'id'         => $new_artist_vote_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function make_artist_voting()


    public function get_most_rating_artists($locale)
    {
        try {
            $limit                     = Settings::getValue('most_rating_artists_limit');

//            $locale = $this->getParameter('locale');
//            $this->debToFile(' get_most_rating_artists $locale ::' . print_r($locale, true), ' ::');

            $tempMostRatingArtistsList = Artist::getArtistsList(ListingReturnData::LISTING, ['is_active' => 'A', 'show_related_votes' => 1, 'locale'=> $locale]);

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $mostRatingSongsCollection = $tempMostRatingArtistsList->filter(function ($nextMostRatingArtist, $key) {
            return ( ! empty($nextMostRatingArtist['related_votes_sum']) and ! empty($nextMostRatingArtist['related_votes_count']));
        });
        $mostRatingArtistsList     = $mostRatingSongsCollection->toArray();
        usort($mostRatingArtistsList, array($this, 'sortArtistsByRatingAverage'));

        return response()->json([
            'error_code'            => 0,
            'message'               => '',
            'mostRatingArtistsList' => array_slice($mostRatingArtistsList, 0, $limit),
        ], HTTP_RESPONSE_OK);
    } // public function get_most_rating_artists()

    public function sortArtistsByRatingAverage($a, $b)
    {
        if (empty($a['related_votes_count']) or empty($b['related_votes_count'])) {
            return -1;
        }
        $a_rating = round($a['related_votes_sum'] / $a['related_votes_count'], 2);
        $b_rating = round($b['related_votes_sum'] / $b['related_votes_count'], 2);
        if ($a_rating == $b_rating) {
            return 0;
        }

        return ($a_rating < $b_rating) ? 1 : -1;

    }
    ///////// ARTIST PAGE BLOCK END //////////

}

