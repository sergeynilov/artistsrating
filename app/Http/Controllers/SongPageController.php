<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Http\Traits\funcsTrait;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use Cart;

use App\Settings;
use App\User;
use App\Http\Requests\UserRegisterRequest;

use App\UsersGroups;
use App\Artist;
use App\Song;
use App\SongVote;
use App\SongArtist;
use App\Tour;
use App\CmsItem;

class SongPageController extends MyAppController
{
    use funcsTrait;



    ///////// SONG PAGE BLOCK START //////////
    public function show()
    {
        $loggedUser             = Auth::user();
        $user_has_already_voted = false;
        $song_is_in_cart= false;
        try {
            $song_slug = $this->getParameter('song_slug');
            $typeArray = $this->getParameter('type');
            $locale = $this->getParameter('locale');

            $song = Song::getSimilarSongBySlug($song_slug);
            if ($song == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'song.song_not_found',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            if ( !$song->is_active ) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'song.song_not_active',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $songArtistsList    = [];
            $song_id = $song->id;

            $song = Song::getRowById($song_id, ['show_related_songs_count'=> 1, 'show_related_votes' => 1, 'locale'=>$locale]);

            if (in_array('song_artists', $typeArray)) {    // related artists   bof the song
                $songArtistsList = SongArtist::getSongArtistsList(ListingReturnData::LISTING, ['song_id' => $song_id, 'show_artist_name' => 1, 'show_related_song_votes' => 1, 'artist_is_active'=> true, 'locale'=>$locale],
                    'artist_ordering', 'desc');
            }

            if (in_array('song_voted', $typeArray) and !empty($loggedUser->id)) {
                $songVotesList = SongVote::getSongVotesList(ListingReturnData::LISTING, ['song_id' => $song_id, 'user_id' => $loggedUser->id, 'locale'=>$locale]);
                $user_has_already_voted = $songVotesList->count() > 0;
            }

            $cartItems = Cart::content();
//            echo '<pre>0000 $song_id::'.print_r($song_id,true).'</pre>';
            foreach( $cartItems as $nextCartItem ) {
                $options= $nextCartItem->options;
//                echo '<pre>$nextCartItem::'.print_r($nextCartItem,true).'</pre>';
//                echo '<pre>$options::'.print_r($options,true).'</pre>';

                if ( $options["product_type"] == "song" and (int)$options["product_id"] == (int)$song_id ) {
                    $song_is_in_cart= true;
//                    die("-1 XXZ FOUND");
                    break;
                }
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        sleep(  1 );

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'song'                 => $song,
            'songArtistsList'        => $songArtistsList,
            'user_has_already_voted' => $user_has_already_voted,
            'song_is_in_cart'      => $song_is_in_cart,
        ], HTTP_RESPONSE_OK);
    }

    public function song_page_dictionaries()
    {
        try {
            $songIsActiveSelectionList    = Song::getSongIsActiveValueArray(true);
            $songVoteSelectionList        = [];
            $songVoteSelectionTempList    = SongVote::getSongVoteValueArray(true);
            $votingColorsArray            = config('app.votingColors', []);
            foreach ($songVoteSelectionTempList as $next_key => $nextSongVoteSelection) {
                $color = '';
                foreach ($votingColorsArray as $next_voting_key => $next_voting_color) {
                    if ($next_voting_key == $nextSongVoteSelection['key']) {
                        $color = $next_voting_color;
                        break;
                    }
                }
                $songVoteSelectionList[] = ["key" => $nextSongVoteSelection['key'], "label" => $nextSongVoteSelection['label'], 'color' => $color];
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                     => 0,
            'message'                        => '',
            'songIsActiveSelectionList'    => $songIsActiveSelectionList,
            'songVoteSelectionList'        => $songVoteSelectionList,
        ], HTTP_RESPONSE_OK);
    } // public function song_page_dictionaries()


    public function get_most_rating_songs($locale)
    {
        try {
            $limit                   = Settings::getValue('most_rating_songs_limit');
            $tempMostRatingSongsList = Song::getSongsList(ListingReturnData::LISTING, ['is_active' => true, 'show_related_votes' => 1, 'locale'=> $locale]);
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $mostRatingSongsCollection = $tempMostRatingSongsList->filter(function ($nextMostRatingSong, $key) {
            return ( ! empty($nextMostRatingSong['related_votes_sum']) and ! empty($nextMostRatingSong['related_votes_count']));
        });

//        echo '<pre>000::'.print_r($mostRatingSongsList,true).'</pre>';
        $mostRatingSongsList = $mostRatingSongsCollection->toArray();
        usort($mostRatingSongsList, array($this, 'sortSongsByRatingAverage'));

        return response()->json([
            'error_code'          => 0,
            'message'             => '',
            'mostRatingSongsList' => array_slice($mostRatingSongsList, 0, $limit),
        ], HTTP_RESPONSE_OK);
    } // public function get_most_rating_songs()


    public function sortSongsByRatingAverage($a, $b)
    {
        if (empty($a['related_votes_count']) or empty($b['related_votes_count'])) {
            return -1;
        }
        $a_rating = round($a['related_votes_sum'] / $a['related_votes_count'], 2);
        $b_rating = round($b['related_votes_sum'] / $b['related_votes_count'], 2);
        if ($a_rating == $b_rating) {
            return 0;
        }
        return ($a_rating < $b_rating) ? 1 : -1;
    }

    public function make_song_voting()
    {
        if (!Auth::check()) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'common.you_have_to_login',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $loggedUser = Auth::user();
        $song_id  = $this->getParameter('song_id');
        $vote       = $this->getParameter('vote_value');
        try {

            $songVotesList = SongVote::getSongVotesList(ListingReturnData::LISTING, ['song_id' => $song_id, 'user_id' => $loggedUser->id]);
            if (count($songVotesList) > 0) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'common.you_have_to_login',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $newSongVote            = new SongVote();
            $newSongVote->song_id   = $song_id;
            $newSongVote->user_id   = $loggedUser->id;
            $newSongVote->vote      = $vote;
            DB::beginTransaction();
            $newSongVote->save();
            $new_song_vote_id = $newSongVote->id;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'id'         => $new_song_vote_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function make_song_voting()


    ///////// SONG PAGE BLOCK END //////////

}

