<?php

namespace App\Http\Controllers\Auth;

use DB;
use Auth;

use Barryvdh\Debugbar\Facade as Debugbar;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Redirect;
use Socialite;

use App\Http\Controllers\MyAppController;
use App\Http\Traits\funcsTrait;
use App\Events\backendFailOnLoginEvent;
use App\Events\backendSuccessOnLoginEvent;


use App\User;
class LoginController extends MyAppController
{
    use funcsTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use ValidatesRequests;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function redirectToProvider($provider_name)
    {
//        die("-1 XXZ redirectToProvider");
        return Socialite::driver($provider_name)->redirect();
    }


    public function handleSocialiteProviderCallback($provider_name)
    {
        $site_hosting= 'http://local-artists-rating.com';
//        Debugbar::info('<pre>handleSocialiteProviderCallback  $provider_name::'.print_r($provider_name,true).'</pre>');
        $this->debToFile(print_r($provider_name,true),' 000 handleSocialiteProviderCallback  -7 $provider_name::');
        $provider_name= (string)$provider_name;
        $is_debug= true;
/*        $group_name= 'User';
        $userGroup= Group::getSimilarGroupByName($group_name);
        echo '<pre>$userGroup::'.print_r($userGroup,true).'</pre>';*/
//        $this->debToFile(' -2 $userGroup ??? ::' . print_r($userGroup, true), ' ::');


/*        if (  $userGroup === null ) {
//            with(new self)->info( $userGroup,'-1 $userGroup IS NULL ::' );
            $this->debToFile('-1 $userGroup IS NULL ::' . print_r($userGroup, true), ' ::');
            echo '<pre>$::'.print_r($userGroup,true).'</pre>';
            // /home#/home
            //     {path: '/home/msg/:msg/:type?/:redirect?', component: MsgPage, name: 'msgPage'},
            return redirect( $site_hosting."/home#/home/error_message/" . urlencode(" User's group'".$group_name."' is not defined !") );
            // http://local-artists-rating.com/home#/home/msg/some%20error%20text/error/redirect%20url%20NAH
//            return redirect( "/home#/admin/dashboard/error_message/" . urlencode(" User's group'".$group_name."' is not defined !") );

        }*/
//        with(new self)->info( $userGroup,'++ $userGroup IS FOUND ::' );
//        $this->debToFile('++ $userGroup IS FOUND ::' . print_r($userGroup, true), ' ::');

//        die("-1 XXZ");
        try {
            DB::beginTransaction();

            $user = Socialite::driver($provider_name)->user();

            if ( $is_debug ) {
                echo '<pre>-1 $user::' . print_r($user, true) . '</pre>';
                $this->debToFile(print_r($user->id,true),' handleSocialiteProviderCallback  -1 $user::');
            }
//            die("-1 XXZ");
//        $user= Socialite::driver($provider_name)->stateless()->user();
//            $loggedUser = $this->findOrCreateSocialiteUser($user, $provider_name, true);
            $loggedUser = User::where('provider_id', $user->id)->first();
            if ( $is_debug ) {
                echo '<pre>-2 $loggedUser::'.print_r($loggedUser,true).'</pre>';
                $this->debToFile(print_r($loggedUser,true),' handleSocialiteProviderCallback  -2 $loggedUser->id::');
            }
            
            if ($loggedUser !== null ) { // There is already user is system - just login under it's credentials
//                die("-1 XXZ0000");
                Auth::login($loggedUser, true);
                \Event::fire(new backendSuccessOnLoginEvent($loggedUser));
                DB::commit();
                $this->debToFile(print_r($loggedUser,true),' handleSocialiteProviderCallback  -3 $loggedUser::');
                return redirect( $site_hosting . "/home#/admin/dashboard/successful_login/" . urlencode($loggedUser->username) );

            } // if ($loggedUser) { // There is already user is system - just login under it's credentials

            if ( $provider_name == "github" ) {
                $username = !empty($user->name) ? $user->name : $user->nickname;
            } else {
                $username = $user->name;
            }

            $selectedPermissionsArray= [
                ['key' => 6, 'label' => IN_FRONTEND_LOGGED_USER_AREA],
                ['key' => 7, 'label' => IN_FRONTEND_ALL_PUBLIC_PAGES]
            ];
//            echo '<pre>---$selectedPermissionsArray::'.print_r($selectedPermissionsArray,true).'</pre>';
//            die("-1 XXZ==");

            $loggedUser= User::create([ // There NO user is system yet : create it and login under it's credentials
                'username'      => $username,
                'email'         => $user->email,
                'status'        => 'A',
                'provider_name' => $provider_name,
                'provider_id'   => $user->id,
            ]);

            foreach( $selectedPermissionsArray as $next_key=>$nextSelectedPermission ) {
                echo '<pre>$nextSelectedPermission::'.print_r($nextSelectedPermission,true).'</pre>';
                $ret= $loggedUser->givePermissionTo($nextSelectedPermission['label']);
                echo '<pre>==$ret::'.print_r($ret,true).'</pre>';
            }

            if ( $is_debug ) {
                echo '<pre>-4 $loggedUser::'.print_r($loggedUser,true).'</pre>';
                $this->debToFile(print_r($loggedUser->id,true),' handleSocialiteProviderCallback  -4 $loggedUser->id::');
            }
            if ( $loggedUser === null ) {
                echo '<pre>-444 EXIT $loggedUser::'.print_r($loggedUser,true).'</pre>';
                return redirect( $site_hosting . "/home#/admin/dashboard/error_creating_new_user/" . urlencode($username) );
            }

            Auth::login($loggedUser, true);
            \Event::fire(new backendSuccessOnLoginEvent($loggedUser));

            if ( Auth::check() ) {
                $this->debToFile(' -9 handleSocialiteProviderCallback CHECKED OK::');

            }   else {
                $this->debToFile(' -9 handleSocialiteProviderCallback CHECKED NO::');

            }
/*            $newUserGroup           = new UsersGroups();
            $newUserGroup->user_id  = $loggedUser->id;
            $newUserGroup->group_id = $userGroup->id;
            $newUserGroup->save();*/
            $this->debToFile(print_r($loggedUser,true),' handleSocialiteProviderCallback  -5 $loggedUser->id::');
            DB::commit();
//            die("-1 XXZ000");
            sleep(2);

        } catch (Exception $e) {
            DB::rollBack();
            return redirect($site_hosting . "/home#/admin/dashboard/error_creating_new_user/" . urlencode($e->getMessage()));
//            return redirect("/home#/admin/dashboard/error_creating_new_user/" . urlencode($e->getMessage()));
        }

        echo '<pre>$loggedUser->username::'.print_r($loggedUser->username,true).'</pre>';
        echo '<pre>$provider_name::'.print_r($provider_name,true).'</pre>';
//        $this->debToFile(print_r($loggedUser->username,true),' handleSocialiteProviderCallback  -6 $loggedUser->username::');
        $this->debToFile(print_r($provider_name,true),' handleSocialiteProviderCallback  -7 $provider_name::');
        $this->debToFile(print_r($loggedUser->username,true),' handleSocialiteProviderCallback  -71 $loggedUser->username::');

        if ( $provider_name == "google" ) {
//            $url= $site_hosting . "/home#/admin/dashboard/google_new_user/" . urlencode($loggedUser->username);
//            $url= $site_hosting . "/home#/admin/dashboard";
            $url= $site_hosting . "/home";
            $this->debToFile(print_r($url,true),' REDIRECT BY GOOGLE::  -7654 $url::');
            return redirect()->route($url, [])->with([
                'text'   => 'google_new_user',
                'type'   => 'success',
                'action' => 'successful_login'
            ]);

//            return redirect($url);
            //     function redirect($to = null, $status = 302, $headers = [], $secure = null)

            //     function redirect($to = null, $status = 302, $headers = [], $secure = null)

        }

        // http://local-artists-rating.com/home#/admin/dashboard
        if ( $provider_name == "github" ) {
            $url= $site_hosting . "/home";
            $this->debToFile(print_r($url,true),' REDIRECT BY github::  -7654 $url::');
            return redirect()->route($url, [])->with([
                'text'   => 'github_new_user',
                'type'   => 'success',
                'action' => 'successful_login'
            ]);

            //            $this->debToFile( print_r('REDIRECT BY GITHUB::',':') );
//            return redirect("/home#/admin/dashboard/github_new_user/" . urlencode($loggedUser->username));
        }
/*
        if ( $provider_name == "twitter" ) {
            $this->debToFile( print_r('REDIRECT BY twitter::',':') );
            return redirect("/home#/admin/dashboard/twitter_new_user/" . urlencode($loggedUser->username));
        }
        if ( $provider_name == "linkedin" ) {
            echo '<pre>REDIRECT BY LINKEDIN::</pre>';
            $this->debToFile( print_r('REDIRECT BY GOOGLE::',':') );
            return redirect("/home#/admin/dashboard/linkedin_new_user/" . urlencode($loggedUser->username));
        }
        if ( $provider_name == "facebook" ) {
            echo '<pre>REDIRECT BY FACEBOOK::</pre>';
            $this->debToFile( print_r('REDIRECT BY GOOGLE::',':') );
            return redirect("/home#/admin/dashboard/facebook_new_user/" . urlencode($loggedUser->username));
        }*/
    }

    public function findOrCreateSocialiteUser__DEL($user, $provider_name, $create_nonexisting_user = false)
    {
        $loggedUser = User::where('provider_id', $user->id)->first();
        if ($loggedUser) {
            return $loggedUser;
        }

        if ( $provider_name == "github") {
            $username = !empty($user->name) ? $user->name : $user->nickname;
        } else {
            $username = $user->name;
        }
        echo '<pre>$user::'.print_r($user,true).'</pre>';
        echo '<pre>$user->name::'.print_r($user->name,true).'</pre>';
        echo '<pre>$user->email::'.print_r($user->email,true).'</pre>';
        if ( ! $create_nonexisting_user) {
            return;
        }

/*        if ( empty($user->email) ) {
            $user->email= quickRandom(8) ;
        }
        Debugbar::info('<pre>$user::' . print_r($user, true) . '</pre>');
        Debugbar::info('<pre>$user->name::' . print_r($user->name, true) . '</pre>');*/

//        appFuncs::dd( $user, '$user::' );
//        with(new self)->info( $user,'$user::' );
//        echo '<pre>$user->name::'.print_r($user->name,true).'</pre>';
//        die("-1 XXZ");

        return User::create([
            'username'      => $username,
            'email'         => $user->email,
            'status'        => 'A',
            'provider_name' => $provider_name,
            'provider_id'   => $user->id,
        ]);

    }

/*    public function authenticated($request, $user)
    {
        die("-1 XXZ authenticated");
        $retArray = \Event::fire(new backendSuccessOnLoginEvent($user));
//        Redirect::back();
    }*/

    protected function sendFailedLoginResponse()
    {
        die("-1 XXZ sendFailedLoginResponse");
        $loginCreditialsArray= ['email' => request()->input('email'), 'password' => request()->input('password')];
        $retArray = \Event::fire(new backendFailOnLoginEvent($loginCreditialsArray));
/*        echo '<pre>++ $retArray::'.print_r($retArray,true).'</pre>';
        if ( !empty($retArray[0]) and is_array($retArray[0])) {
            $retArray= $retArray[0];
            if ( !empty($retArray['error_code']) ) {
                $this->setFlashMessage( 'Invalid login.' );
                return redirect('/login');
            }
            if ( empty($retArray['error_code']) and !empty($retArray['user_id']) ) {
                return redirect('/admin/dashboard/msg?text='.$retArray['success_message'].'&action=show-login&type=success');
            }
        }*/
    }


}
