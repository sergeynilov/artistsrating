<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Foundation\Console\PackageDiscoverCommand;
use Illuminate\Http\Request;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\ModelHasPermission;
use App\library\AppPermissionAccessReturnType;
use App\User;
use App\MyAppModel;
use App\Http\Requests\ArtistRequest;
use App\Artist;
use App\ArtistImage;
use App\SongTranslation;
use App\ArtistTranslation;
use App\Http\Requests\ArtistTranslationRequest;
use App\ArtistConcert;
use App\SongArtist;
use App\Settings;
use App\Song;
use App\Http\Traits\funcsTrait;

class ArtistsController extends MyAppController
{
    use funcsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    //     Route::get('artists/filter/{filter_type}/{filter_value}', 'Backend\ArtistsController@index');
//Route::resource('artists', 'Backend\ArtistsController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
//                axios.get(this.api_admin_version_link + 'artists/status/N').then((response) => {

    public function index($filter_type = '', $filter_value = '')
    {
        $filtersArray = [];
        if ( ! empty($filter_type) and strtolower($filter_type) == 'is_active') {
            $filtersArray['is_active'] = $filter_value;
        }
//        $this->debToFile(print_r($filtersArray,true),' 000 ArtistsController  -1 $filtersArray::');

        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'created_at' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $all_artists_count             = Artist::getArtistsList(ListingReturnData::ROWS_COUNT, ['']);
            $active_artists_count          = Artist::getArtistsList(ListingReturnData::ROWS_COUNT, ['is_active'=>'A']);
            $related_artist_songs_count    = SongArtist::getSongArtistsList(ListingReturnData::ROWS_COUNT, []);

            $backend_locale= config('app.backend_locale', 'en');
            $new_artists_count    = Artist::getArtistsList(ListingReturnData::ROWS_COUNT, ['is_active'=>'N']);
            $rows_count       = Artist::getArtistsList(ListingReturnData::ROWS_COUNT, $filtersArray);
            $filtersArray['show_related_votes']=           1;
            $filtersArray['show_related_images_count']=    1;
            $filtersArray['show_related_concerts_count']=  1;
            $filtersArray['show_future_concerts_count']=   1;
            $filtersArray['show_related_songs_count']=     1;
            $filtersArray['locale']= $backend_locale;
            $artistsList      = Artist::getArtistsList( ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json([
                'error_code'       => 1,
                'message'          => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page = with(new Artist)->getItemsPerPage();

        return response()->json([
            'error_code'       => 0,
            'message'          => '',
            'rows_count'       => $rows_count,
            'artistsList'                  => $artistsList,
            'all_artists_count'            => $all_artists_count,
            'active_artists_count'         => $active_artists_count,
            'new_artists_count'            => $new_artists_count,
            'related_artist_songs_count'   => $related_artist_songs_count,
            'per_page'         => $per_page,
        ], HTTP_RESPONSE_OK);
    }


    public function show($id, $locale)
    {
        $id = (int)$id;
        try {

            $artist = Artist::getRowById( $id, [ 'show_future_concerts_count'=> 1 , 'show_related_votes'=> 1, 'locale'=>$locale ] );
            $artistImagesList      = ArtistImage::getArtistImagesList( ListingReturnData::LISTING, ['artist_id'=> $id, 'show_image_info'=> 1] );
            if ($artist == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'Artist # "' . $id . '" not found!',
                    'artist'                    => null,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code'         => 1,
                'message'            => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'artist'                 => $artist,
            'artistImagesList'       => $artistImagesList,
        ], HTTP_RESPONSE_OK);
    }

    public function dictionaries()
    {
        $loggedUser                    = Auth::user();
        $loggedUserModelHasPermissions = ModelHasPermission::checkUserHasPermissionsValues( $loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE );
        try {
            $artistHasConcertsSelectionList             = Artist::getArtistHasConcertsValueArray(true);
            $artistIsActiveSelectionList                = Artist::getArtistIsActiveValueArray(true);
            $artistSingleSelectionList                  = Artist::getArtistSingleValueArray(true);
            $countriesList                              = $this->getCountries(true);
            $backend_locale                           = \Config::get('app.backend_locale');
            $langsInSystem                            = \Config::get('app.langsInSystem');
            $langsInSystemList= [];
            foreach( $langsInSystem as $next_key=>$next_value ) {
                $langsInSystemList[]= [ 'key'=> $next_key, 'label'=>$next_value ];
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code'                             => 1,
                'message'                                => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'IN_BACKEND_EDIT_SYSTEM_DICTIONARIES'    => in_array(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES,$loggedUserModelHasPermissions),
            'artistHasConcertsSelectionList'         => $artistHasConcertsSelectionList,
            'artistSingleSelectionList'              => $artistSingleSelectionList,
            'artistIsActiveSelectionList'            => $artistIsActiveSelectionList,
            'countriesList'                          => $countriesList,
            'langsInSystemList'                      => $langsInSystemList,
            'backend_locale'                         => $backend_locale,
        ], HTTP_RESPONSE_OK);
    } // public function dictionaries()



    public function update(ArtistRequest $request)
    {
        $id         = $request->id;
        $artist       = Artist::find($id);

        if ($artist == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Artist # "' . $id . '" not found!',
                'artist'       => (object)[ 'name'        => 'Artist # "' . $id . '" not # found!',  'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();

            
            $updateDataArray          = $request->all();
            $datetime_carbon_format = config('app.datetime_carbon_format','Y-m-d H:i:s');
            $formatted_updated_at = Carbon::createFromFormat($datetime_carbon_format, now());
            $updateDataArray['updated_at'] = $formatted_updated_at;
            $artist->update($updateDataArray);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage() ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'artist' => $artist], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(ArtistRequest $request) 
    {
        try {
            DB::beginTransaction();
            $insertDataArray = $request->all();
            $name= $insertDataArray['name'];
            $info= $insertDataArray['info'];
            $locale= $insertDataArray['locale'];
            unset($insertDataArray['name']);
            unset($insertDataArray['info']);
            unset($insertDataArray['locale']);

            $newArtist       = Artist::create($insertDataArray);
            $newArtistTranslation         = ArtistTranslation::create( [ 'artist_id'=> $newArtist->id, 'name'=>$name, 'info'=> $info, 'locale'=> $locale ] );
//            echo '<pre>$newArtistTranslation->id::'.print_r($newArtistTranslation->id,true).'</pre>';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'artist' => $newArtist], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id) 
    {
        try {
            $artist = Artist::find($id);
            if ($artist == null) {
                return response()->json(['error_code' => 11, 'message' => 'Artist # "' . $id . '" not found!', 'artist' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $artist->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }





    ///////// ARTIST CONCERTS BLOCK START //////// /
    public function get_artist_concert($artist_concert_id= '')
    {
//        echo '<pre>$artist_concert_id::'.print_r($artist_concert_id,true).'</pre>';
        try {
            $artistConcert      = ArtistConcert::getRowById( $artist_concert_id, [  ] );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                      => 0,
            'message'                         => '',
            'artistConcert'                   => $artistConcert,
        ], HTTP_RESPONSE_OK);

    } // public function get_artist_concert($artist_id)

    public function get_artist_concerts($artist_id, $locale)
    {
        try {
//            $songIsActiveSelectionList        = Song::getSongIsActiveValueArray(true);
//            $songsList= [];
//            $songs      = Song::getSongsList( ListingReturnData::LISTING, [], 'ordering', 'asc' );
            $artistConcertsList      = ArtistConcert::getArtistConcertsList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'show_tour_name'=>1,
                                                                                                         'show_country_name'=>1, 'show_future_days'=>1, 'locale'=> $locale ], 'event_date', 'desc' );
            $future_concerts_count= 0;
//            echo '<pre>$artistConcertsList::'.print_r($artistConcertsList,true).'</pre>';
            foreach( $artistConcertsList as $next_key=>$nextArtistConcert ) {
                if ( in_array($nextArtistConcert['event_date_is_past'], [ 'is_today', 'is_tomorrow', 'is_future' ]) ) {
                    $future_concerts_count++;
                }
            }

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'artistConcertsList'                  => $artistConcertsList,
            'future_concerts_count'               => $future_concerts_count,
        ], HTTP_RESPONSE_OK);

    } // public function get_artist_concerts($artist_id)

    public function update_artist_concert(/*artistConcertRequest $request*/)
    {
        $request = request();
//        $artistConcert          = $request->all('artistConcert');
        $artist_id         = $request->artist_id;
        $artistConcertDataArray         = $request->artistConcert;
//        echo '<pre>$artist_id::'.print_r($artist_id,true).'</pre>';
        echo '<pre>$artistConcertDataArray::'.print_r($artistConcertDataArray,true).'</pre>';
        $artist_concert_id= ( !empty($artistConcertDataArray['artist_concert_id']) ? $artistConcertDataArray['artist_concert_id'] : '' );
        $newArtistConcert= null;
        $is_inserted= false;
        echo '<pre>$artist_concert_id::'.print_r($artist_concert_id,true).'</pre>';
        if ( !empty($artist_concert_id) and is_integer($artist_concert_id)) {
            $newArtistConcert       = ArtistConcert::find($artist_concert_id);
        }

        if ($newArtistConcert == null) {
            $newArtistConcert= new ArtistConcert();
//            echo '<pre>$::'.print_r($,true).'</pre>';
            $is_inserted= true;
        }
        echo '<pre>$is_inserted::'.print_r($is_inserted,true).'</pre>';
        try {
            DB::beginTransaction();
            $newArtistConcert->artist_id= $artist_id;
            $newArtistConcert->place_name= $artistConcertDataArray['place_name'];
            $newArtistConcert->participants= $artistConcertDataArray['participants'];
            $newArtistConcert->lat= $artistConcertDataArray['lat'];
            $newArtistConcert->lng= $artistConcertDataArray['lng'];
            $newArtistConcert->event_date= $artistConcertDataArray['event_date'];
            $newArtistConcert->country= $artistConcertDataArray['country'];
            $newArtistConcert->description= ( !empty($artistConcertDataArray['description']) ? $artistConcertDataArray['description'] : ' ' );
            if ( !empty($artistConcertDataArray['tour_id']) ) {
                $newArtistConcert->tour_id = $artistConcertDataArray['tour_id'];
            }
//            echo '<pre>$newArtistConcert::'.print_r($newArtistConcert,true).'</pre>';
            $newArtistConcert->save();
            $artist_concert_id= $newArtistConcert->id;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        echo '<pre>BEFORE $artist_concert_id::'.print_r($artist_concert_id,true).'</pre>';
//        echo '<pre>$is_inserted::'.print_r($is_inserted,true).'</pre>';
//        echo '<pre>HTTP_RESPONSE_OK_RESOURCE_CREATED::'.print_r(HTTP_RESPONSE_OK_RESOURCE_CREATED,true).'</pre>';
        return response()->json(['error_code' => 0, 'message' => '', 'id' => $artist_concert_id], HTTP_RESPONSE_OK_RESOURCE_CREATED );

    } // public function update_artist_concert()

    public function remove_artist_concert($artist_concert_id)
    {
        try {
            $artistConcert = ArtistConcert::find($artist_concert_id);
            if ($artistConcert == null) {
                return response()->json(['error_code' => 11, 'message' => 'artist concert # "' . $artist_concert_id . '" not found!', 'artistConcert' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $artistConcert->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }

    ///////// ARTIST CONCERTS BLOCK END /////////


    ///////// ARTIST SONGS BLOCK START   /////////
    public function get_song_artists_by_artist_id($artist_id, $locale)
    {

        try {
            $songsList= [];
            $checked_artist_songs_rows_count= 0;

            $songs      = Song::getSongsList( ListingReturnData::LISTING, ['locale'=> $locale], 'ordering', 'asc' );
            $songArtistsList      = SongArtist::getSongArtistsList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'show_song_name'=>1, 'locale'=> $locale] );

            foreach( $songs as $next_key=>$nextSongs ) {
//                if ( $nextSongs->id!= 118 ) continue;
                $is_found= false;
                foreach( $songArtistsList as $nextSongArtist ) {
                    if ( $nextSongs->id == $nextSongArtist->song_id ) {
                        $is_found= $nextSongArtist->artist_id;
                        $checked_artist_songs_rows_count++;
                        break;
                    }
                }
//                echo '<pre>$nextSongs::'.print_r($nextSongs,true).'</pre>';
                $songsList[]= [ 'artist_id'=>$is_found, 'song_id'=>$nextSongs->id, 'song_ordering'=>$nextSongs->ordering, 'song_title'=>$nextSongs->title,
                                'song_slug'=> $nextSongs->slug,
                                'song_is_active'=>$nextSongs->is_active, 'song_short_descr'=>$nextSongs->short_descr, 'song_description'=>$nextSongs->description,
                                'song_created_at'=>$nextSongs->created_at, 'checked'=> ($is_found ? true : false) ];
//                echo '<pre>$songsList::'.print_r($songsList,true).'</pre>';
//                die("-1 XXZ");
            }

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'songsList'                           => $songsList,
            'checked_artist_songs_rows_count'     => $checked_artist_songs_rows_count,
        ], HTTP_RESPONSE_OK);

    } // public function get_song_artists_by_artist_id($artist_id)

    public function get_artist_images_by_artist_id($artist_id, $locale)
    {
        try {
            $artistImagesList                       = ArtistImage::getArtistImagesList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'show_image'=>1,
                'show_image_info'=>1, 'locale'=> $locale  ], 'is_main_is_home_page_image_name' );
//            echo '<pre>$::'.print_r($artistImagesList,true).'</pre>';
//            die("-1 XXZ");
            $is_home_page_artist_images_count       = ArtistImage::getArtistImagesList( ListingReturnData::ROWS_COUNT, [ 'artist_id'=>$artist_id, 'is_home_page'=>true] );

            $artistIsMainList            = ArtistImage::getArtistIsMainValueArray( true );
            $artistIsHomePageList        = ArtistImage::getArtistIsHomePageValueArray( true );
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                      => 1,
                'message'                         => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'artistImagesList'                    => $artistImagesList,
            'is_home_page_artist_images_count'    => $is_home_page_artist_images_count,
            'artistIsHomePageList'                => $artistIsHomePageList,
            'artistIsMainList'                    => $artistIsMainList,

        ], HTTP_RESPONSE_OK);

    } // public function get_artist_images_by_artist_id($artist_id)


    public function update_artist_image( /*UserRegisterRequest $request*/)
    {
        $request = request();
        $ArtistImageData          =  $request->all();
//        echo '<pre>$ArtistImageData::'.print_r($ArtistImageData,true).'</pre>';
        try {
            $artist_image_id            = $ArtistImageData['artistImage']['artist_image_id'];
            $artist_image_name          = $ArtistImageData['artistImage']['image_name'];
            $artist_image_name          =  MyAppModel::checkValidImgName($artist_image_name,100,true);
//            echo '<pre>$artist_image_name::'.print_r($artist_image_name,true).'</pre>';
            $artist_id                  = $ArtistImageData['artist_id'];
            $uploaded_file_data         = $ArtistImageData['artistImage']['uploaded_image_url'];
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
//                echo '<pre>$artist_id::'.print_r($artist_id,true).'</pre>';
            $editedArtistImage= null;
            if (!empty($artist_image_id)) {
                $editedArtistImage      = ArtistImage::getRowById( $artist_image_id );
            }
            if ( $editedArtistImage == null) {
                $editedArtistImage       = new ArtistImage();
                $editedArtistImage->artist_id = $artist_id;
            }
            
            $editedArtistImage->image_name = $artist_image_name;
            $editedArtistImage->is_main = $ArtistImageData['artistImage']['is_main'];
            $editedArtistImage->is_home_page = $ArtistImageData['artistImage']['is_home_page'];
            $editedArtistImage->description = $ArtistImageData['artistImage']['description'];
            DB::beginTransaction();
            $editedArtistImage->save();
            $new_artist_image_id= $editedArtistImage->id;
//            echo '<pre>$artist_image_name::'.print_r($artist_image_name,true).'</pre>';
            if ( !empty($artist_image_name) ) {
                //     public static function getArtistImagePath(int $artist_id, $image_name, bool $set_public_subdirectory= false, bool $short_path= false) : string
                $file_name = ArtistImage::getArtistImagePath( $artist_id, $artist_image_name ); //generating unique file name;
//                echo '<pre>$file_name::'.print_r($file_name,true).'</pre>';
                @list($type, $uploaded_file_data) = explode(';', $uploaded_file_data);
                @list(, $uploaded_file_data) = explode(',', $uploaded_file_data);
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
                if($uploaded_file_data!=""){ // storing image in storage/app/public Folder
                    \Storage::disk('public')->put($file_name,base64_decode($uploaded_file_data));
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                   => 0,
            'message'                      => '',
            'id'                           => $new_artist_image_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);

    } // public function update_artist_image()

    public function remove_artist_image($artist_image_id)
    {
        try {
            $artistImage = artistImage::find($artist_image_id);
            if ($artistImage == null) {
                return response()->json(['error_code' => 11, 'message' => 'artist image # "' . $artist_image_id . '" not found!', 'artistImage' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            $artist_image_filename= ArtistImage::getArtistImagePath($artistImage->artist_id,$artistImage->image_name, true);
            $artistImage->delete();
            ArtistImage::deleteFileByPath($artist_image_filename, true);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }

//       Route::delete('remove_artist_image/{artist_image_id}'

    public function get_artist_image($artist_image_id= '')
    {

        try {
            $artistImage      = ArtistImage::getRowById( $artist_image_id, [ 'show_image'=> 1,  'show_image_info'=>1 ] );
        } catch (Exception $e) {
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                      => 0,
            'message'                         => '',
            'artistImage'                     => $artistImage,
        ], HTTP_RESPONSE_OK);

    } // public function get_artist_image($artist_id)


    public function get_artist_songs_dictionaries()
    {
        try {
            $songIsActiveSelectionList        = Song::getSongIsActiveValueArray(true);
//            $backend_locale                           = \Config::get('app.backend_locale');
//            $langsInSystem                            = \Config::get('app.langsInSystem');
//            $langsInSystemList= [];
//            foreach( $langsInSystem as $next_key=>$next_value ) {
//                $langsInSystemList[]= [ 'key'=> $next_key, 'label'=>$next_value ];
//            }
        } catch (Exception $e) {
            return response()->json([
                'error_code'                         => 1,
                'message'                            => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'songIsActiveSelectionList'              => $songIsActiveSelectionList,
//            'langsInSystemList'                      => $langsInSystemList,
//            'backend_locale'                         => $backend_locale,
        ], HTTP_RESPONSE_OK);
    } // public function get_artist_songs_dictionaries($artist_id)

    public function add_song_to_artist($artist_id, $song_id)
    {
        $new_song_artist_id= '';
        try {

            $artistRelatedSongsList      = SongArtist::getSongArtistsList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'song_id'=>$song_id ] );
            if ( $artistRelatedSongsList->count() == 0 ) {
                $newSongArtist           = new SongArtist();
                $newSongArtist->artist_id = $artist_id;
                $newSongArtist->song_id  = $song_id;

                DB::beginTransaction();
                $newSongArtist->save();
                $new_song_artist_id= $newSongArtist->id;
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                   => 0,
            'message'                      => '',
            'id'                           => $new_song_artist_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);

    } // public function add_song_to_artist($artist_id)

    public function remove_song_from_artist($artist_id, $song_id)
    {

        try {
            $artistRelatedSongsList      = SongArtist::getSongArtistsList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'song_id'=>$song_id ] );
            if ( $artistRelatedSongsList->count() == 0 ) {
                return response()->json([
                    'error_code'              => 1,
                    'message'                 => 'Artist # "' . $artist_id . '" for song # "'.$song_id .'" not found!',
                    'artist_id'                => $artist_id,
                    'song_id'                 => $song_id,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $artistRelatedSongsList[0]->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'artist_id'                    => $artist_id,
            'song_id'                     => $song_id,
        ], HTTP_RESPONSE_OK_RESOURCE_DELETED );

    } // public function remove_song_from_artist($artist_id)

    //     Route::get('get_song_artist/{song_id}/{artist_id}', 'ArtistsController@get_song_artist')->name('get_song_artist');
    public function get_song_artist($song_id, $artist_id, $locale)
    {
//        echo '<pre>$song_id::'.print_r($song_id,true).'</pre>';
        try {
            $artistSong= null;
            $artistRelatedSongsList      = SongArtist::getSongArtistsList( ListingReturnData::LISTING, [ 'artist_id'=>$artist_id, 'song_id'=>$song_id, 'show_song_name'=> 1, 'locale'=> $locale
            ] );
            if ( $artistRelatedSongsList->count() == 0 ) {
                $song      = Song::getRowById( $song_id, [ 'locale'=> $locale] );
//                echo '<pre>++ $song::'.print_r($song,true).'</pre>';
                $artistSong= [ 'song_id'=>$song->id, 'song_title'=>$song->song_title, 'song_is_active'=>($song->is_active?1:0), 'song_ordering'=>$song->ordering, 'song_slug'=>$song->slug, 'song_short_descr'=>$song->song_short_descr, 'song_description'=>$song->song_description, 'song_created_at'=>$song->created_at, 'song_updated_at'=>$song->updated_at ];
            } else {
                $artistSong= $artistRelatedSongsList[0];
            }


//            echo '<pre>$artistSong::'.print_r($artistSong,true).'</pre>';
//            die("-1 XXZ");
        } catch (Exception $e) {
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                      => 0,
            'message'                         => '',
            'artistSong'                       => $artistSong,
        ], HTTP_RESPONSE_OK);

    } // public function get_song_artist($artist_id)



    public function update_song_artist()
    {
        $request = request();
        $requestData= $request->all();
        echo '<pre>$requestData::'.print_r($requestData,true).'</pre>';

//        $locale            = $request->locale;
        $artist_id         = $request->artist_id;
        $artistSong        = $request->artistSong;
        $song_id           = ( !empty($artistSong['song_id']) ? $artistSong['song_id'] : '' );
        $locale            =  $artistSong['locale'];
        $song              = null;
        $is_inserted       = false;
        if ( !empty($song_id) and is_integer($song_id)) {
            $song          = Song::find($song_id);
        }

        if ($song == null) {
            $song= new Song();
            $is_inserted= true;
        }
        try {

            /* <pre>$requestData::Array
(
    [artist_id] => 73
    [artistSong] => Array
        (
            [song_id] =>
            [song_title] => 73 en
            [song_ordering] => 73
            [song_short_descr] => 73 en 111111
            [song_description] => 73 en
1111111
22222222
3333
            [attach_song_to_artist] =>
            [locale] => en
            [song_is_active] => 1
        )

) */

            DB::beginTransaction();
            $song_title= $artistSong['song_title'];
            $song_short_descr= $artistSong['song_short_descr'];
            $song_description= $artistSong['song_description'];
            unset($artistSong['song_short_descr']);
            unset($artistSong['song_description']);

            /* -- DROP TABLE public.rt_song_translations

CREATE TABLE public.rt_song_translations (
	id serial NOT NULL,
	song_id int4 NOT NULL,
	title varchar(100) NOT NULL,
	short_descr varchar(255) NOT NULL,
	description text NOT NULL,
	locale varchar(2) NOT NULL,
	created_at timestamp NULL DEFAULT now(),
	updated_at timestamp NULL, */
            $song->is_active= !empty($artistSong['song_is_active'] ) ? $artistSong['song_is_active'] : false;
            $song->ordering= $artistSong['song_ordering'];
//            $song->short_descr= $artistSong['song_short_descr'];
            $attach_song_to_artist= $artistSong['attach_song_to_artist'];


            if (!$is_inserted) {
                $song->updated_at = now();
            }
            $song->save();
            $song_id= $song->id;

            if ($is_inserted) {
                $newSongTranslation         = SongTranslation::create( [ 'song_id'=> $song->id, 'title'=>$song_title, 'short_descr'=> $song_short_descr, 'description'=> $song_description, 'locale'=> $locale ] );
                //     protected $fillable = ['song_id', 'locale', 'title', 'short_descr', 'description', 'created_at'];

            }

            /*     public function store(ArtistRequest $request)
    {
        try {
            DB::beginTransaction();
            $insertDataArray = $request->all();
            $name= $insertDataArray['name'];
            $info= $insertDataArray['info'];
            $locale= $insertDataArray['locale'];
            unset($insertDataArray['name']);
            unset($insertDataArray['info']);
            unset($insertDataArray['locale']);

            $newArtist       = Artist::create($insertDataArray);
            $newArtistTranslation         = ArtistTranslation::create( [ 'artist_id'=> $newArtist->id, 'name'=>$name, 'info'=> $info, 'locale'=> $locale ] );
//            echo '<pre>$newArtistTranslation->id::'.print_r($newArtistTranslation->id,true).'</pre>';
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'artist' => $newArtist], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }
 */
            if ( $attach_song_to_artist ) {
                $songArtist           = new SongArtist();
                $songArtist->song_id  = $song->id;
                $songArtist->artist_id = $artist_id;
                $songArtist->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        echo '<pre>BEFORE $song_id::'.print_r($song_id,true).'</pre>';
//        echo '<pre>$is_inserted::'.print_r($is_inserted,true).'</pre>';
//        echo '<pre>HTTP_RESPONSE_OK_RESOURCE_CREATED::'.print_r(HTTP_RESPONSE_OK_RESOURCE_CREATED,true).'</pre>';
        return response()->json(['error_code' => 0, 'message' => '', 'id' => $song_id], HTTP_RESPONSE_OK_RESOURCE_CREATED );

    } // public function update_song_artist()


    public function remove_song($id)
    {
        try {
            $song = Song::find($id);
            if ($song == null) {
                return response()->json(['error_code' => 11, 'message' => 'Song # "' . $id . '" not found!', 'song' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $song->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }
    ///////// ARTIST SONGS BLOCK END     ///////////

    
    ////////// LOCALE EDITOR BLOCK START //////////////
    public function get_artists_locale($artist_id, $locale= '')
    {
        $artist_id = (int)$artist_id;
        try {

            if ( empty($locale) ) {
                $artistTranslation = Artist::where('id', $artist_id)->first();
                $artistTranslation = $artistTranslation->translate();
            } else {
                $artistTranslation = ArtistTranslation::getRowByIdLocale( $artist_id, $locale );
            }
            if ($artistTranslation == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'ArtistTranslation # "' . $artist_id . '" not found!',
                    'artistTranslation'       => null,
                ], HTTP_RESPONSE_OK);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'artistTranslation'       => $artistTranslation,
        ], HTTP_RESPONSE_OK);
    }


    public function update_artists_locale(ArtistTranslationRequest $request)
    {
        try {
            DB::beginTransaction();

            $updateDataArray             = $request->all();
            $artistTranslation = ArtistTranslation::getRowByIdLocale( $updateDataArray['artist_id'], $updateDataArray['locale'] );
            if ($artistTranslation == null) { // there is no related cms item
                $artistTranslation = ArtistTranslation::create($updateDataArray);
            } else {
                $artistTranslation->update($updateDataArray);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'artist_translation' => $artistTranslation], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }
    ////////// LOCALE EDITOR BLOCK END //////////////



}
