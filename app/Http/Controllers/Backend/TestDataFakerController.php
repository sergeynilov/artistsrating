<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;

use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use Faker;
use App\User;
use App\Artist;
use App\Song;
use App\ArtistConcert;
use App\Settings;
use App\SongVote;
use App\ArtistVote;
use League\HTMLToMarkdown\HtmlConverter;


class TestDataFakerController extends MyAppController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $this->html_to_markdown();
//
//        die("-1 XXZ=====");
//        $this->addVariablesToJS();
//        $site_name  = Settings::getValue('site_name', '');
//        return view('admin.TestDataFaker.index',['site_name'=> $site_name]);
    }


    public function faker_add_votes()
    {
        return ;
        $backend_locale= config('app.backend_locale', 'en');
        $artist_votes_added_count= 0;
        $song_votes_added_count= 0;
        try {
            DB::beginTransaction();


            $usersList = User::getUsersList(ListingReturnData::LISTING, []);


            $artist_votes_to_add= 1;
            $artistsList                   = Artist::getArtistsList( ListingReturnData::LISTING, ['locale'=> $backend_locale] );
            foreach( $usersList as $nextUser ) {
                foreach( $artistsList as $nextArtist ) {
                    factory(\App\ArtistVote::class, $artist_votes_to_add)->create( ['artist_id' => $nextArtist->id, 'user_id' => $nextUser->id] ) ;
                    $artist_votes_added_count++;
                } // foreach( $artistsList as $nextArtist ) {
            } // foreach( $usersList as $nextUser ) {


            
            $song_votes_to_add= 1;
            $songsList                   = Song::getSongsList( ListingReturnData::LISTING, [] );
            foreach( $usersList as $nextUser ) {
                foreach( $songsList as $nextSong ) {
                    factory(\App\SongVote::class, $song_votes_to_add)->create( ['song_id' => $nextSong->id, 'user_id' => $nextUser->id] ) ;
                    $song_votes_added_count++;
                } // foreach( $songsList as $nextSong ) {
            } // foreach( $usersList as $nextUser ) {
            

        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'artist_votes_added_count' => $artist_votes_added_count , 'song_votes_added_count' => $song_votes_added_count ], HTTP_RESPONSE_OK );
    } // public function faker_add_votes()

    protected function createVotes() {

        $backend_locale= config('app.backend_locale', 'en');
        $usersList                   = User::getUsersList( ListingReturnData::LISTING, [] );
        echo '<pre>$usersList::'.print_r($usersList,true).'</pre>';
        foreach( $usersList as $nextUser ) {

            $artistsList                   = Artist::getArtistsList( ListingReturnData::LISTING, ['locale'=> $backend_locale] );
            foreach( $artistsList as $next_key=>$nextArtist ) {
                $vote_value = array_random([1,2,3,4,5]);
                $newArtistVote= new ArtistVote();
                $newArtistVote->artist_id= $nextArtist->id;
                $newArtistVote->user_id= $nextUser->id;
                $newArtistVote->vote= $vote_value;
                $newArtistVote->created_at= $vote_value;   // 2018-04-21 15:19:56
                $newArtistVote->save();
            }

            return;
            $songsList                   = Song::getSongsList( ListingReturnData::LISTING, [] );
            foreach( $songsList as $next_key=>$nextSong ) {
                $vote_value = array_random([1,2,3,4,5]);
                $newSongVote= new SongVote();
                $newSongVote->song_id= $nextSong->id;
                $newSongVote->user_id= $nextUser->id;
                $newSongVote->vote= $vote_value;
                $newSongVote->save();
            }

        }
    }



}