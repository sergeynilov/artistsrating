<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;
use App\library\AppPermissionAccess;
use App\library\AppPermissionAccessReturnType;
use App\Permission;
use App\ModelHasPermission;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use Faker;
use App\User;
use App\Artist;
use App\Genre;
use App\Tour;
use App\ArtistConcert;
use App\Settings;
use App\Song;
use App\CmsItem;
use App\SongVote;
use App\ArtistVote;
use League\HTMLToMarkdown\HtmlConverter;
use App\Http\Traits\funcsTrait;


class DashboardController extends MyAppController
{
    use funcsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $this->html_to_markdown();
//
//        die("-1 XXZ=====");
        $this->addVariablesToJS();
        $site_name = Settings::getValue('site_name', '');

        return view('admin.dashboard.index', ['site_name' => $site_name]);
    }

    protected function createVotes()
    {

        $backend_locale = config('app.backend_locale', 'en');
        $usersList      = User::getUsersList(ListingReturnData::LISTING, []);
        echo '<pre>$usersList::' . print_r($usersList, true) . '</pre>';
        foreach ($usersList as $nextUser) {

            $artistsList = Artist::getArtistsList(ListingReturnData::LISTING, ['locale' => $backend_locale]);
            foreach ($artistsList as $next_key => $nextArtist) {
                $vote_value                = array_random([1, 2, 3, 4, 5]);
                $newArtistVote             = new ArtistVote();
                $newArtistVote->artist_id  = $nextArtist->id;
                $newArtistVote->user_id    = $nextUser->id;
                $newArtistVote->vote       = $vote_value;
                $newArtistVote->created_at = $vote_value;   // 2018-04-21 15:19:56
                $newArtistVote->save();
            }

            return;
            $songsList = Song::getSongsList(ListingReturnData::LISTING, []);
            foreach ($songsList as $next_key => $nextSong) {
                $vote_value           = array_random([1, 2, 3, 4, 5]);
                $newSongVote          = new SongVote();
                $newSongVote->song_id = $nextSong->id;
                $newSongVote->user_id = $nextUser->id;
                $newSongVote->vote    = $vote_value;
                $newSongVote->save();
            }

        }
    }

    public function run_test()
    {
//        require 'vendor/autoload.php';

//        $this->createVotes();
        die("-1 XXZ=== run_test");
        $converter = new HtmlConverter();

        $cmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, []);
        $rows_count   = 0;
        foreach ($cmsItemsList as $nextCmsItem) {
            $content_html         = $nextCmsItem->content;
            $nextCmsItem->content = $converter->convert($content_html);

            $short_descr_html         = $nextCmsItem->short_descr;
            $nextCmsItem->short_descr = $converter->convert($short_descr_html);

            $nextCmsItem->save();
            $rows_count++;

        }
        echo '<pre>WORKED OUT $rows_count::' . print_r($rows_count, true) . '</pre>';
        $html     = "<h3>Quick, to the Batpoles!</h3>";
        $markdown = $converter->convert($html);
        echo '<pre>$markdown::' . print_r($markdown, true) . '</pre>';
        die("-1 XXZ");
    }

    function formatSql($sql, $is_break_line = true, $is_include_html = true)
    {
        $break_line = '';
        $space_char = '  ';
        if ($is_break_line) {
            if ($is_include_html) {
                $break_line = '<br>';
            } else {
                $break_line = PHP_EOL;
            }
        }
        $bold_start = '';
        $bold_end   = '';
        if ($is_include_html) {
            $bold_start = '<B>';
            $bold_end   = '</B>';
        }
        $sql        = ' ' . $sql . ' ';
        $left_cond  = '~\b(?<![%\'])';
        $right_cond = '(?![%\'])\b~i';
        $sql        = preg_replace($left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char . $bold_start . "INSERT INTO" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "and" . $right_cond, $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "or" . $right_cond, $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "as" . $right_cond, $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "exists" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);

        return $sql;
    } // function formatSql($sql, $is_break_line = true, $is_include_html = true)

    public function test()
    {
        $this->addVariablesToJS();

        return view('admin.test.index');

    }


    public function dictionaries()
    {
        $loggedUser                    = Auth::user();
        $loggedUserModelHasPermissions = ModelHasPermission::checkUserHasPermissionsValues($loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE);
        try {
            $genrePublishedSelectionList = Genre::getGenrePublishedValueArray(true);
            $backend_locale              = \Config::get('app.backend_locale');
            $backend_locale_label        = $this->getBackendLocaleLabel();
            $langsInSystemList           = $this->getBackendLangs();
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'IN_BACKEND_EDIT_SYSTEM_DICTIONARIES' => in_array(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES, $loggedUserModelHasPermissions),
            'genrePublishedSelectionList'         => $genrePublishedSelectionList,
            'langsInSystemList'                   => $langsInSystemList,
            'backend_locale'                      => $backend_locale,
            'backend_locale_label'                => $backend_locale_label,
            'is_developer'                        => $this->isDeveloperComp(),
        ], HTTP_RESPONSE_OK);

    } // public function dictionaries($genre_id)

    public function dashboard_profile_data()
    {
        $loggedUser = Auth::user();
        $is_admin   = $this->checkUserGroupAccess("Admin", [], $loggedUser);
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        $order_by                                  = 'task_priority_task_date_end';
        $forAdminAssigningTasksAssignedToUserList  = [];
        $forAdminProcessingTasksAssignedToUserList = [];
        $forAdminCheckingTasksAssignedToUserList   = [];

        $tasksAssignedToUserAsTeamLeaderAssigningList  = [];
        $tasksAssignedToUserAsTeamLeaderProcessingList = [];
        $tasksAssignedToUserAsTeamLeaderCheckingList   = [];

        $activeUserChatsList     = [];
        $yourActiveUserChatsList = [];
        $yourClosedUserChatsList = [];
        $yourEventUsersArray     = [];
        try {


            $tasksAssignedToUserAsTeamLeaderList = TaskAssignedToUser::getTaskAssignedToUsersAsTeamLeaderList($loggedUser->id);   // Get listing of tasks where logged user is admin
            $tasksInResultsArray                 = [];

            if ($is_admin) { // admin is logged

                $yourEventsList      = Event::getEventsList(ListingReturnData::LISTING, ['only_future' => '', 'user_id' => $loggedUser->id, 'show_task_name' => 1], 'e.created_at',
                    'asc');
                $yourEventUsersArray = [];
                foreach ($yourEventsList as $next_key => $nextEventUser) {
                    $end_time    = $this->addMinutesToFormattedDateTime($nextEventUser->at_time, $nextEventUser->duration);
                    $time_format = 'H:i A';
                    if ($this->getFormattedDateTime($nextEventUser->at_time, 'Y-m-d') != $this->getFormattedDateTime($end_time, 'Y-m-d')) {
                        $time_format = 'Y-m-d H:i:s';  // NOT the same day
                    }

                    $event_at_time_is_past = $nextEventUser->getDateTimesCompare($nextEventUser->at_time);
                    $yourEventUsersArray[] = [
                        'id'                    => $nextEventUser->id,
                        'title'                 => $nextEventUser->name,
                        'description'           => $nextEventUser->description,
                        'start'                 => $nextEventUser->at_time,
                        'start_time_label'      => $this->getFormattedDateTime($nextEventUser->at_time, $time_format),
                        'end'                   => $end_time,
                        'end_time_label'        => $this->getFormattedDateTime($end_time, $time_format),
                        'duration'              => $nextEventUser->duration,
                        'access'                => $nextEventUser->access,
                        'event_at_time_is_past' => $event_at_time_is_past,
                        'event_class_name'      => [$event_at_time_is_past],
                        'YOUR_DATA'             => ['val' => 'Let it be.']
                    ];
                }

//                echo '<pre>$yourEventUsersArray::'.print_r($yourEventUsersArray,true).'</pre>';
//                die("-1 XXZ=====");
//                $rows_count= UserChat::getUserChatsList( ListingReturnData::ROWS_COUNT, $filtersArray );

            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }


        return response()->json([
            'error_code'                        => 0,
            'message'                           => '',
            'loggedUser'                        => $loggedUser,
            'assigningTasksAssignedToUserList'  => $assigningTasksAssignedToUserList,
            'processingTasksAssignedToUserList' => $processingTasksAssignedToUserList,
            'checkingTasksAssignedToUserList'   => $checkingTasksAssignedToUserList,

            'tasksAssignedToUserAsTeamLeaderAssigningList'  => $tasksAssignedToUserAsTeamLeaderAssigningList,
            'tasksAssignedToUserAsTeamLeaderProcessingList' => $tasksAssignedToUserAsTeamLeaderProcessingList,
            'tasksAssignedToUserAsTeamLeaderCheckingList'   => $tasksAssignedToUserAsTeamLeaderCheckingList,

            'forAdminAssigningTasksAssignedToUserList'  => $forAdminAssigningTasksAssignedToUserList,
            'forAdminProcessingTasksAssignedToUserList' => $forAdminProcessingTasksAssignedToUserList,
            'forAdminCheckingTasksAssignedToUserList'   => $forAdminCheckingTasksAssignedToUserList,
            'activeUserChatsList'                       => $activeUserChatsList,
            'yourActiveUserChatsList'                   => $yourActiveUserChatsList,
            'yourClosedUserChatsList'                   => $yourClosedUserChatsList,
            'yourEventUsersArray'                       => $yourEventUsersArray,

        ], HTTP_RESPONSE_OK);

    } // public function dashboard_profile_data()


    public function refresh_user_profile_quick_info()
    {
        $loggedUser = Auth::user();
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        try {
            $assigning_tasks_count = TaskAssignedToUser::getTaskAssignedToUsersList(ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id, 'status' => 'A']);


            $user_todos_count = UserTodo::getUserTodosList(ListingReturnData::ROWS_COUNT, ['user_id' => $loggedUser->id]);

            $future_events_count   = Event::getEventsList(ListingReturnData::ROWS_COUNT, ['only_future' => 1, 'user_id' => $loggedUser->id]);
            $today_events_count    = Event::getEventsList(ListingReturnData::ROWS_COUNT, ['only_today' => 1, 'user_id' => $loggedUser->id]);
            $tomorrow_events_count = Event::getEventsList(ListingReturnData::ROWS_COUNT, ['only_tomorrow' => 1, 'user_id' => $loggedUser->id]);


        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'            => 0,
            'message'               => '',
            'loggedUser'            => $loggedUser,
            'assigning_tasks_count' => $assigning_tasks_count,
            'future_events_count'   => $future_events_count,
            'tomorrow_events_count' => $tomorrow_events_count,
            'today_events_count'    => $today_events_count,
        ], HTTP_RESPONSE_OK);

    } // public function refresh_user_profile_quick_info()


    public function get_user_view_data($id)
    {
        try {
            $previewUser       = User::find($id);
            $users_groups_text = UsersGroups::getUsersGroupsTextLabel($previewUser->id, true);

            $profileThumbnailImageDocumentCategory = DocumentCategory::getSimilarDocumentCategoryByAlias('profile_thumbnail_image');
            $user_profile_thumbnail_image_url      = '';
//            echo '<pre>$profileThumbnailImageDocumentCategory::'.print_r($profileThumbnailImageDocumentCategory,true).'</pre>';
            if ($profileThumbnailImageDocumentCategory != null) {
                $userProfileDocument = UserProfileDocument::getUserProfileDocumentsList(ListingReturnData::LISTING,
                    ['document_category_id' => $profileThumbnailImageDocumentCategory->id, 'user_id' => $id]);
//                echo '<pre>$userProfileDocument[0]::'.print_r($userProfileDocument[0],true).'</pre>';
                if ( ! empty($userProfileDocument[0])) {
                    $base_url                            = self::getBaseUrl();
                    $next_user_profile_document_filename = UserProfileDocument::getUserProfileDocumentPath($userProfileDocument[0]->user_id, $userProfileDocument[0]->filename,
                        false);
                    $next_user_profile_document_url      = $base_url . Storage::disk('local')->url($next_user_profile_document_filename);
//                    echo '<pre>$::'.print_r($next_user_profile_document_filename,true).'</pre>';
                    $file_exists = Storage::disk('local')->exists('public/' . $next_user_profile_document_filename);
//                    echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
                    if ($file_exists) {
                        $user_profile_thumbnail_image_url = $next_user_profile_document_url;
                    }
                }
            }
//            echo '<pre>$user_profile_thumbnail_image_url::'.print_r($user_profile_thumbnail_image_url,true).'</pre>';
//            die("-1 XXZ----");


        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                       => 0,
            'message'                          => '',
            'user'                             => $previewUser,
            'users_groups_text'                => $users_groups_text,
            'user_profile_thumbnail_image_url' => $user_profile_thumbnail_image_url

        ], HTTP_RESPONSE_OK);
    } //public function get_user_view_data()


    public function user_events_by_day_user_id($date, $user_id)
    {
//        echo '<pre>user_events_by_day_user_id $date::'.print_r($date,true).'</pre>';
//        echo '<pre>user_events_by_day_user_id $user_id::'.print_r($user_id,true).'</pre>';
//
        $day_is_past  = $this->getDateTimesCompare($date);
        $begin_of_day = $this->getBeginOfDay($date, 'mysql', true);
//        echo '<pre>$begin_of_day::'.print_r($begin_of_day,true).'</pre>';

        $end_of_day = $this->getEndOfDay($date, 'mysql', true);
//        echo '<pre>$end_of_day::'.print_r($end_of_day,true).'</pre>';

        $view_day_formatted          = $this->getFormattedDate($date, 'astext');
        $view_day_formatted_pickdate = $this->getFormattedDateTime($date, 'Y-m-d');

//        echo '<pre>$view_day_formatted::'.print_r($view_day_formatted,true).'</pre>';
//        echo '<pre>$view_day_formatted_pickdate::'.print_r($view_day_formatted_pickdate,true).'</pre>';
//        die("-1 XXZ");
        $filtersArray         = ['user_id' => $user_id, 'at_time_start' => $begin_of_day, 'at_time_end' => $end_of_day];
        $eventUsersByDayArray = [];
        $eventUsersList       = EventUser::getEventUsersList(ListingReturnData::LISTING, $filtersArray);
        foreach ($eventUsersList as $next_key => $nextEventUser) { // "at_time": "2018-02-08 17:30:00",
            $end_time              = $this->addMinutesToFormattedDateTime($nextEventUser->event_at_time, $nextEventUser->event_duration);
            $event_at_time_is_past = $nextEventUser->getDateTimesCompare($nextEventUser->event_at_time);
//            echo '<pre>$event_at_time_is_past::'.print_r($event_at_time_is_past,true).'</pre>';
            $eventUsersByDayArray[] = [
                'event_id'              => $nextEventUser->event_id,
                'title'                 => $nextEventUser->event_name,
                'start'                 => $nextEventUser->event_at_time,
                'start_time_label'      => $this->getFormattedDateTime($nextEventUser->event_at_time, 'H:i A'),
                'end'                   => $end_time,
                'end_time_label'        => $this->getFormattedDateTime($end_time, 'H:i A'),
                'event_duration'        => $nextEventUser->event_duration,
                'event_access'          => $nextEventUser->event_access,
                'event_access_label'    => Event::getEventAccessLabel($nextEventUser->event_access),
//                'at_time_is_past'=> $nextEventUser->event_at_time_is_past,
                'event_at_time_is_past' => $event_at_time_is_past,
                'event_class_name'      => [$event_at_time_is_past],
                'YOUR_DATA'             => ['val' => 'Let it be.']
            ];
        }
//        echo '<pre>$eventUsersByDayArray::'.print_r($eventUsersByDayArray,true).'</pre>';
//        die("-1 XXZ");
        return response()->json([
            'error_code'                  => 0,
            'message'                     => '',
            'eventUsersByDayArray'        => $eventUsersByDayArray,
            'view_day_formatted'          => $view_day_formatted,
            'view_day_formatted_pickdate' => $view_day_formatted_pickdate,
            'day_is_past'                 => $day_is_past
        ], HTTP_RESPONSE_OK);

    } // public function user_events_by_day_user_id($user_id)*/


    public function recalc_user_events()
    {
        $events_added_count      = 0;
        $event_users_added_count = 0;
        try {
            DB::beginTransaction();

            $events_to_add = 5;

            $usersList  = User::getUsersList(ListingReturnData::LISTING, []);
            $usersArray = [];
            foreach ($usersList as $nextUser) {
                $usersArray[] = $nextUser->id;
            }

            factory(\App\Event::class, $events_to_add)->create()->each(function ($newEvent) use ($usersArray, &$events_added_count, &$event_users_added_count) {
                $events_added_count++;
                foreach ($usersArray as $next_key => $next_user_id) {
                    factory(\App\EventUser::class, 1)->create(['event_id' => $newEvent->id, 'user_id' => $next_user_id]);
                    $event_users_added_count++;
                }
//                echo '<pre>$event_users_added_count::'.print_r($event_users_added_count,true).'</pre>';
            });

//            echo '<pre>$events_added_count::'.print_r($events_added_count,true).'</pre>';
//            echo '<pre>$event_users_added_count ::'.print_r($event_users_added_count,true).'</pre>';
        } catch (Exception $e) {

            DB::rollBack();
            throw $e;
        }

        DB::commit();

        return response()->json(['error_code' => 0, 'message' => '', 'events_added_count' => $events_added_count, 'event_users_added_count' => $event_users_added_count],
            HTTP_RESPONSE_OK);
    } // public function recalc_user_events()


    ///////////////// USER EVENTS BLOCK END //////////////////////

    public function get_source_file()
    {
        try {
            $src_file      = $this->getParameter('src_file');
            $file_type     = $this->getParameter('file_type');
            $src_file_path = base_path($src_file);
//            $src_file_path      =   ($src_file);
//            echo '<pre>$src_file_path::'.print_r($src_file_path,true).'</pre>';

            if ( ! File::exists($src_file_path)) {
//                echo "No. No exists.";
                return response()->json(['error_code' => 1, 'message' => "File '" . $src_file_path . "' not found!", 'src_file' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            $file_content = File::get(base_path($src_file));
//            echo '<pre>EXISTS $file_content::'.print_r($file_content,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'src_file' => $src_file, 'file_type' => $file_type, 'file_content' => $file_content],
            HTTP_RESPONSE_OK);
    } //public function get_source_file()

    public function get_system_info()
    {

//        $output = new Process('npm ls'); // http://symfony.com/doc/current/components/process.html

//        echo '<pre>$output::'.print_r($output,true).'</pre>';
//        dump($output);
        $npm_output_listing = '';
//        $output->run(function ($type, $buffer) {
//
//            if (Process::ERR === $type) {
//                ob_start();
//                echo 'ERR > '.$buffer;
//                $npm_output_listing = ob_get_contents();
//                ob_end_clean();
////                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';
////                die("-1 XXZ");
//            } else {
//                ob_start();
//                echo 'OUT > '.$buffer;
//                $npm_output_listing = str_replace(["\n","\r"],"",ob_get_contents());
//                ob_end_clean();
////                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';
////                die("-1 XXZ");
//            }
//            $_SESSION['npm_output_listing'] = str_replace(["\n","\r"],"",$npm_output_listing );
//        });

//        $npm_output_listing= !empty($_SESSION['npm_output_listing']) ? $_SESSION['npm_output_listing']: '';
//                echo '<pre>$npm_output_listing::'.print_r($npm_output_listing,true).'</pre>';

        try {
            $system_info = $this->getSystemInfo();
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'system_info' => $system_info, 'npm_output_listing' => $npm_output_listing], HTTP_RESPONSE_OK);
    } //public function get_system_info()


    public function get_footer_data_info()
    {
        $loggedUser = Auth::user();
//        echo '<pre>$is_admin::'.print_r($is_admin,true).'</pre>';
//        die("-1 XXZ++++");
//        echo '<pre>$loggedUserInfoArray::'.print_r($loggedUserInfoArray,true).'</pre>';
        $client_ip = geoip()->getClientIP();
        $client_ip = '27.974.399.65';
//        echo '<pre>$client_ip::'.print_r($client_ip,true).'</pre>';
        $clientLocation = geoip()->getLocation($client_ip);
//        echo '<pre>$clientLocation::'.print_r($clientLocation,true).'</pre>';

        $new_aregistered_users_count = 0;
        $new_artists_count           = 0;
        $future_tours_count          = 0;
        $site_name                   = Settings::getValue('site_name', '');
        $copyright_text              = Settings::getValue('copyright_text', '');

        $langsInSystemList = [];
        $langsInSystem     = \Config::get('app.langsInSystem');
        foreach ($langsInSystem as $next_key => $next_value) {
            $langsInSystemList[] = ['key' => $next_key, 'label' => $next_value];
        }

        try {
            $new_registered_users_count   = User::getUsersList(ListingReturnData::ROWS_COUNT, ['status' => 'N']);
            $new_artists_count            = Artist::getArtistsList(ListingReturnData::ROWS_COUNT, ['is_active' => 'N']);
            $future_tours_count           = Tour::getToursList(ListingReturnData::ROWS_COUNT, ['only_future' => 1]);
            $future_artist_concerts_count = ArtistConcert::getArtistConcertsList(ListingReturnData::ROWS_COUNT, ['only_future' => 1]);


        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                   => 0,
            'message'                      => '',
            'site_name'                    => $site_name,
            'copyright_text'               => $copyright_text,
            'new_registered_users_count'   => $new_registered_users_count,
            'new_artists_count'            => $new_artists_count,
            'future_tours_count'           => $future_tours_count,
            'future_artist_concerts_count' => $future_artist_concerts_count,
            'langsInSystemList'            => $langsInSystemList,
        ], HTTP_RESPONSE_OK);

    } // public function get_footer_data_info()


    public function get_new_artists_list()
    {
        $backend_locale = config('app.backend_locale', 'en');
        try {
            $newArtistsList                 = Artist::getArtistsList(ListingReturnData::LISTING, ['is_active' => 'N', 'locale' => $backend_locale]);
            $artistSingleSelectionList      = Artist::getArtistSingleValueArray(true);
            $artistHasConcertsSelectionList = Artist::getArtistHasConcertsValueArray(true);
            $artistIsActiveSelectionList    = Artist::getArtistIsActiveValueArray(true);

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                     => 0,
            'message'                        => '',
            'newArtistsList'                 => $newArtistsList,
            'artistSingleSelectionList'      => $artistSingleSelectionList,
            'artistHasConcertsSelectionList' => $artistHasConcertsSelectionList,
            'artistIsActiveSelectionList'    => $artistIsActiveSelectionList,
        ], HTTP_RESPONSE_OK);

    } // public function get_new_artists_list()


    public function get_new_registered_users_list()
    {
        try {
            $newRegisteredUsersList = User::getUsersList(ListingReturnData::LISTING, ['status' => 'N', 'show_avatar' => 1]);

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'newRegisteredUsersList' => $newRegisteredUsersList,
        ], HTTP_RESPONSE_OK);

    } // public function get_new_registered_users_list()


    public function activate_user_status()
    {
        $user_id = $this->getParameter('user_id');
        $status  = $this->getParameter('status');
        try {

            $activatedUser = User::getRowById($user_id);
            if ($activatedUser === null) {
                return response()->json(['error_code' => 11, 'message' => 'User # "' . $user_id . '" not found!', 'genre' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();
            if ($status == "D") {
                $activatedUser->delete();
            } else {
                $activatedUser->status = $status;
                $activatedUser->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'id'         => $user_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function activate_user_status()


    public function get_future_concerts_list()
    {
        $backend_locale = config('app.backend_locale', 'en');
        try {
            $newArtistsList                 = Artist::getArtistsList(ListingReturnData::LISTING, ['is_active' => 'N', 'locale' => $backend_locale]);
            $artistHasConcertsSelectionList = Artist::getArtistHasConcertsValueArray(true);
            $artistIsActiveSelectionList    = Artist::getArtistIsActiveValueArray(true);

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                     => 0,
            'message'                        => '',
            'newArtistsList'                 => $newArtistsList,
            'artistHasConcertsSelectionList' => $artistHasConcertsSelectionList,
            'artistIsActiveSelectionList'    => $artistIsActiveSelectionList,
        ], HTTP_RESPONSE_OK);

    } // public function get_future_concerts_list()


    public function get_songs_with_similar_titles()
    {
        $backend_locale = config('app.backend_locale', 'en');
        try {
            $genrePublishedSelectionList = Genre::getGenrePublishedValueArray(true);
            $songIsActiveSelectionList   = Song::getSongIsActiveValueArray(true);
            $retArray                    = [];
            $songsWithSimilarTitlesList  = Song::getSongsWithSimilarTitlesList();
            foreach ($songsWithSimilarTitlesList as $next_key => $nextSongsWithSimilarTitle) {
                if ($nextSongsWithSimilarTitle->total_songs <= 1) {
                    continue;
                }
                $next_song_title = $nextSongsWithSimilarTitle->title;
                if ($next_song_title == '1') {
                    continue;
                }

                $songsList  = Song::getSongsList(ListingReturnData::LISTING, ['title' => $next_song_title, 'show_genre_names' => 1, 'locale' => $backend_locale]);
                $retArray[] = ['song_title' => $nextSongsWithSimilarTitle->title, 'total_songs' => $nextSongsWithSimilarTitle->total_songs, 'songsList' => $songsList];
            }

//            echo '<pre>$retArray::'.print_r($retArray,true).'</pre>';
//            die("-1 XXZ");
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                  => 0,
            'message'                     => '',
            'songIsActiveSelectionList'   => $songIsActiveSelectionList,
            'songsWithSimilarTitlesList'  => $retArray,
            'genrePublishedSelectionList' => $genrePublishedSelectionList,
        ], HTTP_RESPONSE_OK);
    } // public function get_songs_with_similar_titles()


    public function load_user_permission_groups()
    {
        $backend_locale = config('app.backend_locale', 'en');
        $permissionsArray = Permission::getPermissionsArray();
        try {
//            public static function getModelHasPermissionsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {

//            echo '<pre>$permissionsArray::' . print_r($permissionsArray, true) . '</pre>';
            $modelHasPermissionsList = ModelHasPermission::getModelHasPermissionsList(ListingReturnData::LISTING, ['model_type' => 'App\User'], 'permission_id');


            foreach ($permissionsArray as $next_key => $nextPermission) { // All permissions in turn
                $usersSubitemsArray= [];
                foreach ($modelHasPermissionsList as $nextModelHasPermission) {
//                    echo '<pre>$nextPermission[\'key\']::'.print_r($nextPermission['key'],true).'</pre>';
//                    echo '<pre>$nextModelHasPermission::'.print_r($nextModelHasPermission,true).'</pre>';

                    $next_model_permission_id= '';
                    $next_model_model_id= '';
                    foreach($nextModelHasPermission->getAttributes() as $attribute => $value) {
                        if ( $attribute == 'permission_id' ) {
                            $next_model_permission_id= $value;
                        }
                        if ( $attribute == 'model_id' ) {
                            $next_model_model_id= $value;
                        }
                    }
                    if ( $nextPermission['key'] == $next_model_permission_id) {
                        $nextUser = User::find($next_model_model_id);
                        if ($nextUser == null) {
                            continue;
                        }
                        $usersSubitemsArray[]= [ 'user_id'=>$nextUser->id, 'username'=>$nextUser->username, 'first_name'=>$nextUser->first_name,
                                                 'last_name'=>$nextUser->last_name, 'email'=>$nextUser->email, 'status'=>$nextUser->status, 'created_at'=>$nextUser->created_at  ];
                    }
                }
                $permissionsArray[$next_key]['usersSubitemsArray']= $usersSubitemsArray;
            } // foreach( $permissionsArray as $next_key=>$nextPermission ) { // All permissions in turn
//            echo '<pre>++++$permissionsArray::' . print_r($permissionsArray, true) . '</pre>';
//            die("-1 XXZ");

            /*            $genrePublishedSelectionList      = Genre::getGenrePublishedValueArray(true);
            $songIsActiveSelectionList        = Song::getSongIsActiveValueArray(true);
            $retArray= [];
            $songsWithSimilarTitlesList       = Song::getSongsWithSimilarTitlesList();
            foreach( $songsWithSimilarTitlesList as $next_key=>$nextSongsWithSimilarTitle ) {
                if ( $nextSongsWithSimilarTitle->total_songs <= 1 ) continue;
                $next_song_title= $nextSongsWithSimilarTitle->title;
                if ( $next_song_title == '1' ) continue;

                $songsList= Song::getSongsList( ListingReturnData::LISTING,['title'=>$next_song_title, 'show_genre_names'=> 1, 'locale'=> $backend_locale ] );
                $retArray[]= [ 'song_title'=>$nextSongsWithSimilarTitle->title, 'total_songs'=> $nextSongsWithSimilarTitle->total_songs, 'songsList'=> $songsList ];
            }*/

//            echo '<pre>$retArray::'.print_r($retArray,true).'</pre>';
//            die("-1 XXZ");
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                  => 0,
            'message'                     => '',
            'permissionsArray'            => $permissionsArray,
        ], HTTP_RESPONSE_OK);
    } // public function load_user_permission_groups()


    public function app_description()
    {
        $this->addVariablesToJS();

        return view('admin.app_description.index');

    }

    public function logout()
    {
        Auth::logout();

        return response()->json([
            'error_code' => 0,
            'message'    => '',
        ], HTTP_RESPONSE_OK);
    }

}