<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;

use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\library\PgDataType;
use App\MyAppModel;

use App\User;
use App\Artist;
use App\Song;
use App\ArtistConcert;
use App\Settings;
use App\SongVote;
use App\ArtistVote;
use League\HTMLToMarkdown\HtmlConverter;
use function PHPSTORM_META\map;


class ReportsController extends MyAppController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    /*    public function reports_artists_votes_ratings()
        {
            try {
                $genrePublishedSelectionList      = Genre::getGenrePublishedValueArray(true);
                $songIsActiveSelectionList        = Song::getSongIsActiveValueArray(true);
                $retArray= [];
                $songsWithSimilarTitlesList       = Song::getSongsWithSimilarTitlesList();
                foreach( $songsWithSimilarTitlesList as $next_key=>$nextSongsWithSimilarTitle ) {
                    if ( $nextSongsWithSimilarTitle->total_songs <= 1 ) continue;
                    $next_song_title= $nextSongsWithSimilarTitle->title;
                    if ( $next_song_title == '1' ) continue;
                    $songsList= Song::getSongsList( ListingReturnData::LISTING,['title'=>$next_song_title, 'show_genre_names'=> 1 ] );
                    $retArray[]= [ 'song_title'=>$nextSongsWithSimilarTitle->title, 'total_songs'=> $nextSongsWithSimilarTitle->total_songs, 'songsList'=> $songsList ];
                }

    //            echo '<pre>$retArray::'.print_r($retArray,true).'</pre>';
    //            die("-1 XXZ");
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json([
                    'error_code'                        => 1,
                    'message'                           => $e->getMessage(),
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            return response()->json([
                'error_code'                          => 0,
                'message'                             => '',
                'songIsActiveSelectionList'           => $songIsActiveSelectionList,
                'songsWithSimilarTitlesList'          => $retArray,
                'genrePublishedSelectionList'         => $genrePublishedSelectionList,
            ], HTTP_RESPONSE_OK);
        } // public function reports_artists_votes_ratings()*/


    /* Report of items with filtering, sorting... */
    public function reports_artists_votes_ratings(Request $request)
    {
        /*date_from : filter_date_from, date_till : filter_date_till, artistsList:selectedArtistsList */
        $selected_max_rows   = $this->getParameter('selected_max_rows');
        $filter_date_from   = $this->getParameter('filter_date_from');
        $filter_date_till   = $this->getParameter('filter_date_till');
        $filter_artistsList = $this->getParameter('filter_artistsList');


//        echo '<pre>$selected_max_rows::'.print_r($selected_max_rows,true).'</pre>';
        $is_debug           = false;
//        $filter_date_from   = '2018-04-01';
//        $filter_date_till   = '2018-04-05';
//        $filter_artistsList = [6, 7, 8, 9];


        $backend_locale= config('app.backend_locale', 'en');

        $filter_rows_limit          = $this->getParameter('filter_rows_limit', '');
        $filter_sort_by             = $this->getParameter('filter_sort_by', 'by_sum_asc');
        $filter_date_from_formatted = $this->getParameter('filter_date_from_formatted', '');
        $filter_date_till_formatted = $this->getParameter('filter_date_till_formatted', '');
        $order_by                   = $this->getParameter('order_by', 'name');
        $order_direction            = $this->getParameter('order_direction', 'asc');


//        $reportDataList = [];
        $backgroundColorsArray= config('app.chartColors', [ '#bfbda2', '#005500', '#00007f', '#ffff00', '#ff8e72', '#ff280c', '#00c300', '#070707', '#4d647f', '#fff79e' ] );

        if ($is_debug) {
            echo '<pre>!!!!$filter_date_from::' . print_r($filter_date_from, true) . '</pre>';
        }
        if ($is_debug) {
            echo '<pre>!!!!$filter_date_till::' . print_r($filter_date_till, true) . '</pre>';
        }
        if ($is_debug) {
            echo '<pre>!!!!$filter_artistsList::' . print_r($filter_artistsList, true) . '</pre>';
        }
//            $filter_artists = $this->trimRightSubString($filter_artists, ',');
        $filtersValuesArray = [ /* Array with filters parameters with data and data type */
            ['field_name' => 'p_artist_list', 'value' => (! empty($filter_artistsList) ? $filter_artistsList : 'null'), 'type' => PgDataType::NUMERIC, 'is_array' => true],
            [
                'field_name' => 'p_created_at_from',
                'value'      => (! empty($filter_date_from) ? $filter_date_from : 'null'),
                'type'       => PgDataType::DATE,
                'is_array'   => false
            ],
            [
                'field_name' => 'p_created_at_till',
                'value'      => (! empty($filter_date_till) ? ($filter_date_till . ' 23:59:59') : 'null'),
                'type'       => PgDataType::DATE,
                'is_array'   => false
            ],
            ['field_name' => 'p_sort_type', 'value' => $filter_sort_by, 'type' => PgDataType::STRING, 'is_array' => false],
            ['field_name' => 'p_limit', 'value' => (! empty($filter_rows_limit) ? $filter_rows_limit : 0), 'type' => PgDataType::NUMERIC, 'is_array' => false],
            ['field_name' => 'p_locale', 'value' => $backend_locale, 'type' => PgDataType::STRING, 'is_array' => false],
        ];
        /* run postgresql data retrieving function with provided parameters in $filtersValuesArray  */
//            echo '<pre>$filtersValuesArray::'.print_r($filtersValuesArray,true).'</pre>';
//            die("-1 XXZ");
        $reportDaysList        = [];
        $reportArtistsList     = [];
        $groupedReportDataList = [];
        $chartReportDataList = [];
        $reportDataList        = MyAppModel::runPgFunction($filtersValuesArray, DB::getTablePrefix() . "reports_artists_votes_ratings", true);
//            echo '<pre>$reportDataList::'.print_r($reportDataList,true).'</pre>';
        foreach ($reportDataList as $nextReportDataRow) {  // all rows from sql proc
            if ( ! in_array($nextReportDataRow['created_at'], $reportDaysList)) {
                $reportDaysList[] = $nextReportDataRow['created_at'];    // add new day in days listing
            }
            $is_found = false;

            foreach ($reportArtistsList as $nextReportArtist) {
                if ($nextReportArtist['artist_id'] == $nextReportDataRow['artist_id']) {
                    $is_found = true;
                    break;
                }
            }

            if ( ! $is_found) {
                $reportArtistsList[] = ['artist_id' => $nextReportDataRow['artist_id'], 'artist_name' => $nextReportDataRow['artist_name']];    // add new artist in artists
            }

        } // foreach( $reportDataList as $nextReportDataRow ) {  // all rows from sql proc

        reset($reportDataList);
        $artist_index= 0;
        foreach ($reportArtistsList as $next_report_artist_key => $nextReportArtist) {
//                echo '<pre>NNEXT $nextReportArtist[\'artist_name\']::'.print_r($nextReportArtist['artist_name'],true).'</pre>';
            $tempReportDataList = [];
            $oneArtistChartReportDataList = [];


            reset($reportDaysList);
            foreach ($reportDaysList as $next_day_key => $next_report_day) {//  circle for all days
                $is_day_found = false;
                if ($is_debug) echo '<pre>NEXT $next_report_day::'.print_r($next_report_day,true).'</pre>';

                foreach ($reportDataList as $nextReportDataRow) { // any row in results data
                    if ($nextReportArtist['artist_id'] == $nextReportDataRow['artist_id'] and $next_report_day == $nextReportDataRow['created_at']) { // found data for the
                        // current $next_report_day day
                        $tempReportDataList[] = ['day' => $next_report_day, 'vote_sum' => $nextReportDataRow['vote_sum'], 'vote_qty' => $nextReportDataRow['vote_qty']];

                        if ( $nextReportDataRow['vote_qty'] > 0 ) {
                            $oneArtistChartReportDataList[] = round($nextReportDataRow['vote_sum'] / $nextReportDataRow['vote_qty'], 1);
                        } else {
                            $oneArtistChartReportDataList[] = 0;
                        }

                        
                        if ($is_debug) echo '<pre>INSIDE +++ $is_day_found::'.print_r($is_day_found,true).'</pre>';
                        $is_day_found = true;
                    }
                } // foreach( $reportDataList as $nextReportDataRow ) { // any row in results data

                if ($is_debug) echo '<pre>NEXT $is_day_found::'.print_r($is_day_found,true).'</pre>';
                if ( ! $is_day_found) {
                    $tempReportDataList[] = ['day' => $next_report_day, 'vote_sum' => 0, 'vote_qty' => 0];
                    $oneArtistChartReportDataList[] = 0;
                }

            } // foreach( $reportDaysList as $next_day_key=> $next_report_day ) {//  circle for all days

            $groupedReportDataList[] = [ 'artist_id' => $nextReportArtist['artist_id'], 'artist_name' => $nextReportArtist['artist_name'], 'dataRows' => $tempReportDataList ];
//            $background_color= '';
//            echo '<pre>$artist_index::'.print_r($artist_index,true).'</pre>';
//            $ind= count($backgroundColorsArray) % $artist_index;
//
//            with(new self)->info( $ind,'$ind::' );
//            echo '<pre>$ind::'.print_r($ind,true).'</pre>';
//
//            if ( !in_array($ind, [3,1,2])  ) $ind= 1;
//
            if ( $artist_index >= count($backgroundColorsArray) ) $artist_index= 0;
            $background_color= $backgroundColorsArray[ $artist_index ];
//            $background_color= $backgroundColorsArray[ count($backgroundColorsArray) % $ind ];
//            echo '<pre>$background_color::'.print_r($background_color,true).'</pre>';
            $chartReportDataList[]   = [ 'label' => $nextReportArtist['artist_name'], 'backgroundColor'=> $background_color,
                                        'data' => $oneArtistChartReportDataList ];
            $artist_index++;
        }

//        die("-1 XXZ");

        if ($is_debug) {
            echo '<pre>$reportDaysList::' . print_r($reportDaysList, true) . '</pre>';
            echo '<pre>$reportArtistsList::' . print_r($reportArtistsList, true) . '</pre>';
            echo '<pre>$groupedReportDataList::' . print_r($groupedReportDataList, true) . '</pre>';
            echo '<pre>$chartReportDataList::' . print_r($chartReportDataList, true) . '</pre>';
            die("-1 XXZ reports_artists_votes_ratings");
        }

        /* prepare data from db to charts formatted arrays */

        return response()->json([
            'error_code'            => 0,
            'message'               => '',
//            'groupedReportDataList' => $groupedReportDataList,
            'chartReportDataList'   => $chartReportDataList,
            'reportArtistsList'     => $reportArtistsList,
            'reportDaysList'        => $reportDaysList,

            'selected_max_rows'     => $selected_max_rows,
            'filter_date_from'      => $filter_date_from,
            'filter_date_till'      => $filter_date_till,
            'filter_artistsList'    => $filter_artistsList,
        ], HTTP_RESPONSE_OK);

    } //     public function reports_artists_votes_ratings(Request $request)








    //////
    ///

    
    /* Report of items with filtering, sorting... */
    public function reports_artists_votes_summary_by_period(Request $request) //loadArtistsVotesSummaryByPeriod_BarChart
    {
        /*date_from : filter_date_from, date_till : filter_date_till, artistsList:selectedArtistsList */
        $backend_locale= config('app.backend_locale', 'en');
        $selected_max_rows   = $this->getParameter('selected_max_rows');
        $filter_date_from   = $this->getParameter('filter_date_from');
        $filter_date_till   = $this->getParameter('filter_date_till');
        $filter_artistsList = $this->getParameter('filter_artistsList');


//        echo '<pre>$selected_max_rows::'.print_r($selected_max_rows,true).'</pre>';
        $is_debug           = false;

        $filter_rows_limit          = $this->getParameter('filter_rows_limit', '');
        $filter_sort_by             = $this->getParameter('filter_sort_by', 'by_sum_asc');
        $filter_date_from_formatted = $this->getParameter('filter_date_from_formatted', '');
        $filter_date_till_formatted = $this->getParameter('filter_date_till_formatted', '');
        $order_by                   = $this->getParameter('order_by', 'name');
        $order_direction            = $this->getParameter('order_direction', 'asc');

        $backgroundColorsArray= config('app.chartColors', [ '#bfbda2', '#005500', '#00007f', '#ffff00', '#ff8e72', '#ff280c', '#00c300', '#070707', '#4d647f', '#fff79e' ] );

//        echo '<pre>$backgroundColorsArray::'.print_r($backgroundColorsArray,true).'</pre>';

        if ($is_debug) {
            echo '<pre>!!!!$filter_date_from::' . print_r($filter_date_from, true) . '</pre>';
        }
        if ($is_debug) {
            echo '<pre>!!!!$filter_date_till::' . print_r($filter_date_till, true) . '</pre>';
        }
        if ($is_debug) {
            echo '<pre>!!!!$filter_artistsList::' . print_r($filter_artistsList, true) . '</pre>';
        }
//            $filter_artists = $this->trimRightSubString($filter_artists, ',');
        $filtersValuesArray = [ /* Array with filters parameters with data and data type */
            ['field_name' => 'p_artist_list', 'value' => (! empty($filter_artistsList) ? $filter_artistsList : 'null'), 'type' => PgDataType::NUMERIC, 'is_array' => true],
            [
                'field_name' => 'p_created_at_from',
                'value'      => (! empty($filter_date_from) ? $filter_date_from : 'null'),
                'type'       => PgDataType::DATE,
                'is_array'   => false
            ],
            [
                'field_name' => 'p_created_at_till',
                'value'      => (! empty($filter_date_till) ? ($filter_date_till . ' 23:59:59') : 'null'),
                'type'       => PgDataType::DATE,
                'is_array'   => false
            ],
            ['field_name' => 'p_sort_type', 'value' => $filter_sort_by, 'type' => PgDataType::STRING, 'is_array' => false],
            ['field_name' => 'p_limit', 'value' => (! empty($filter_rows_limit) ? $filter_rows_limit : 0), 'type' => PgDataType::NUMERIC, 'is_array' => false],
            ['field_name' => 'p_locale', 'value' => $backend_locale, 'type' => PgDataType::STRING, 'is_array' => false],

        ];
        /* run postgresql data retrieving function with provided parameters in $filtersValuesArray  */
//            echo '<pre>$filtersValuesArray::'.print_r($filtersValuesArray,true).'</pre>';
//            die("-1 XXZ");
        $chartReportDataList        = MyAppModel::runPgFunction($filtersValuesArray, DB::getTablePrefix() . "reports_artists_votes_summary_by_period", true); // summary_by_period
        $index= 0;
        foreach( $chartReportDataList as $next_key=>$nextChartReportData ) {
            if ( $index >= count($backgroundColorsArray) ) $index= 0;
            $background_color= $backgroundColorsArray[ $index ];
            $chartReportDataList[$next_key]['background_color']= $background_color;
            $index++;
        }

        if ($is_debug) {
            echo '<pre>$chartReportDataList::' . print_r($chartReportDataList, true) . '</pre>';
            die("-1 XXZ reports_artists_votes_summary_by_period");
        }

        /* prepare data from db to charts formatted arrays */

        return response()->json([
            'error_code'            => 0,
            'message'               => '',
            'chartReportDataList'   => $chartReportDataList,

            'selected_max_rows'     => $selected_max_rows,
            'filter_date_from'      => $filter_date_from,
            'filter_date_till'      => $filter_date_till,
            'filter_artistsList'    => $filter_artistsList,
        ], HTTP_RESPONSE_OK);

    } //     public function reports_artists_votes_summary_by_period(Request $request)










    public function reports_dictionaries()
    {
        $backend_locale= config('app.backend_locale', 'en');
        try {
//            $artistsList     = [];
            $tempArtistsList = Artist::getArtistsList(ListingReturnData::LISTING, ['locale'=> $backend_locale]);
//            $artistsList= $tempArtistsList.map()
            $artistsList = collect($tempArtistsList)->map(function ($nextArtist) {
                return [
                    'key'   => $nextArtist->id,
                    'label' => $nextArtist->name . '(' . Artist::getArtistIsActiveLabel($nextArtist->is_active) . ', ' . Artist::getArtistHasConcertsLabel($nextArtist->has_concerts)
                               . ')'
                ];
            });
            /* , { "id": 9, "name": "Beyonce", "ordering": 9, "is_active": "A", "has_concerts": true, "birthday": "1981-09-04", "day_of_death": null, "site": "", "info": "Beyoncé Giselle Knowles-Carter (/biːˈjɒnseɪ/; born September 4, 1981)[2][3][4] is an American singer, songwriter and actress. Born and raised in Houston, Texas, Beyoncé performed in various singing and dancing competitions as a child. Beyoncé rose to fame in the late 1990s as lead singer of the R&B girl-group Destiny's Child. Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups in history. Their hiatus saw Beyoncé's theatrical film debut in Austin Powers in Goldmember (2002) and the release of her debut album, Dangerously in Love (2003). The album established her as a solo artist worldwide, earned five Grammy Awards, and featured the Billboard Hot 100 number one singles \"Crazy in Love\" and \"Baby Boy\". Following the break-up of Destiny's Child in 2006, she released her second solo album, B'Day (2006), which contained the top ten singles \"Déjà Vu\", \"Irreplaceable\", and \"Beautiful Liar\". Beyoncé also continued her acting career, with starring roles in The Pink Panther (2006), Dreamgirls (2006), and Obsessed (2009). Her marriage to rapper Jay-Z and portrayal of Etta James in Cadillac Records (2008) influenced her third album, I Am... Sasha Fierce (2008), which saw the introduction of her alter-ego Sasha Fierce and earned a record-setting six Grammy Awards in 2010, including Song of the Year for \"Single Ladies (Put a Ring on It)\". Beyoncé took a hiatus from music in 2010 and took over management of her career; her fourth album 4 (2011) was subsequently mellower in tone, exploring 1970s funk, 1980s pop, and 1990s soul.[5] Her critically acclaimed fifth album, Beyoncé (2013), was distinguished from previous releases by its experimental production and exploration of darker themes. Her sixth album, Lemonade (2016), also received widespread critical acclaim, and subsequently became the best-selling album of 2016.[6] Throughout her career, Beyoncé has sold 100 million records worldwide,[7] making her one of the world's best-selling music artists.[8][9] She has won 22 Grammy Awards and is the most nominated woman in the award's history. She is also the most awarded artist at the MTV Video Music Awards, with 24 wins.[10][11] The Recording Industry Association of America recognized Beyoncé as the Top Certified Artist in America, during the 2000s decade.[12][13] In 2009, Billboard named her the Top Radio Songs Artist of the Decade, the Top Female Artist of the 2000s decade, and awarded her their Millennium Award in 2011.[17] In 2014, she became the highest-paid black musician in history and was listed among Time's 100 most influential people in the world for a second year in a row.[18] Forbes ranked her as the most powerful female in entertainment on their 2015 and 2017 lists,[19][20] and in 2016 she occupied the sixth place for Time's Person of the Year.[21]", "created_at": "2018-04-08 14:18:14.88945", "updated_at": null, "single": true }, { "id": 6, "name": */

            $songIsActiveSelectionList = Song::getSongIsActiveValueArray(true);
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                => 0,
            'message'                   => '',
            'songIsActiveSelectionList' => $songIsActiveSelectionList,
            'artistsList'               => $artistsList,
        ], HTTP_RESPONSE_OK);
    } // public function reports_dictionaries()


}