<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\library\AppPermissionAccess;
use App\library\AppPermissionAccessReturnType;
use App\User;
use App\MyAppModel;
use App\Http\Requests\CmsItemRequest;
use App\Http\Requests\CmsItemTranslationRequest;
use App\CmsItem;
use App\CmsItemTranslation;
use App\Settings;
use App\ModelHasPermission;
use App\Http\Traits\funcsTrait;

class CmsItemsController extends MyAppController
{
    use funcsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($filter = '')
    {
        $filtersArray = [];

        if ( ! empty($filter) and strtolower($filter) != 'all') {
            $filtersArray['status'] = $filter;
        }

        $backend_locale= config('app.backend_locale', 'en');
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'cms_t.title' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $all_cms_items_count            = CmsItem::getCmsItemsList(ListingReturnData::ROWS_COUNT, []);
            $published_cms_items_count      = CmsItem::getCmsItemsList(ListingReturnData::ROWS_COUNT, ['published'=>true]);
            $not_published_cms_items_count  = CmsItem::getCmsItemsList(ListingReturnData::ROWS_COUNT, ['published'=>false]);
            $rows_count                     = CmsItem::getCmsItemsList(ListingReturnData::ROWS_COUNT, $filtersArray);
            $filtersArray['show_image']     = 1;
            $filtersArray['show_image_info']= 1;
            $filtersArray['locale']= $backend_locale;

            $cmsItemsList                   = CmsItem::getCmsItemsList( ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json([
                'error_code'       => 1,
                'message'          => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page = with(new CmsItem)->getItemsPerPage();

        sleep(  config('app.sleep_in_seconds', 0) );

        return response()->json([
            'error_code'       => 0,
            'message'          => '',
            'rows_count'       => $rows_count,
            'cmsItemsList'            => $cmsItemsList,
            'all_cms_items_count'      => $all_cms_items_count,
            'published_cms_items_count'      => $published_cms_items_count,
            'not_published_cms_items_count'      => $not_published_cms_items_count,
            'per_page'         => $per_page,
        ], HTTP_RESPONSE_OK);
    }


    public function show($id, $locale)
    {
        $id = (int)$id;
        try {

            $cmsItem = CmsItem::getRowById( $id, [ 'show_image'=> 1, 'show_image_info'=> 1, 'locale'=>$locale ] );
            if ($cmsItem == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'CmsItem # "' . $id . '" not found!',
                    'cmsItem'                    => null,
                    'categoriesSelectionList' => null
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'cmsItem'       => $cmsItem,
        ], HTTP_RESPONSE_OK);
    }

    public function dictionaries()
    {
        $loggedUser                    = Auth::user();
        $loggedUserModelHasPermissions = ModelHasPermission::checkUserHasPermissionsValues( $loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE );
//        echo '<pre>$modelHasPermissions::'.print_r($modelHasPermissions,true).'</pre>';
//        die("-1 XXZ");
        try {
            $cmsItemPageTypeSelectionList             = CmsItem::getCmsItemPageTypeValueArray(true);
            $cmsItemContentTypeSelectionList          = CmsItem::getCmsItemContentTypeValueArray(true);
            $cmsItemPublishedSelectionList            = CmsItem::getCmsItemPublishedValueArray(true);

            $backend_locale                           = \Config::get('app.backend_locale');
            $backend_locale_label                     = $this->getBackendLocaleLabel();
            $langsInSystemList= $this->getBackendLangs();
        } catch (Exception $e) {
            return response()->json([
                'error_code'                          => 1,
                'message'                             => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'IN_BACKEND_EDIT_CMS_DATA'               => in_array(IN_BACKEND_EDIT_CMS_DATA,$loggedUserModelHasPermissions),
            'IN_BACKEND_PUBLISH_CMS_DATA'            => in_array(IN_BACKEND_PUBLISH_CMS_DATA,$loggedUserModelHasPermissions),
            'cmsItemPageTypeSelectionList'           => $cmsItemPageTypeSelectionList,
            'cmsItemContentTypeSelectionList'        => $cmsItemContentTypeSelectionList,
            'cmsItemPublishedSelectionList'          => $cmsItemPublishedSelectionList,
            'langsInSystemList'                      => $langsInSystemList,
            'backend_locale'                         => $backend_locale,
            'backend_locale_label'                   => $backend_locale_label,
        ], HTTP_RESPONSE_OK);

    } // public function dictionaries($cms_item_id)



    public function update(CmsItemRequest $request)
    {
        $id            = $request->id;
        $cmsItem       = CmsItem::find($id);
        if ($cmsItem == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'CmsItem # "' . $id . '" not found!',
                'cms_item'      => (object)[ 'name'        => 'CmsItem # "' . $id . '" not # found!',  'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            DB::beginTransaction();
            $updateDataArray               = $request->all();

            $title= $updateDataArray['title'];
            $short_descr= $updateDataArray['short_descr'];
            $content= $updateDataArray['content'];
            $locale= $updateDataArray['locale'];

            unset($updateDataArray['title']);
            unset($updateDataArray['short_descr']);
            unset($updateDataArray['content']);
            unset($updateDataArray['locale']);
            
//            echo '<pre>$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            if (!empty($updateDataArray['image']) and isset($updateDataArray['image_url'])) {
                $image     = MyAppModel::checkValidImgName($updateDataArray['image'], 50,true);
                $image_url = $updateDataArray['image_url'];
                $updateDataArray['image']= $image;
                unset($updateDataArray['image_url']);
            }

            $datetime_carbon_format        = config('app.datetime_carbon_format','Y-m-d H:i:s');
            $formatted_updated_at          = Carbon::createFromFormat($datetime_carbon_format, now());
            $updateDataArray['updated_at'] = $formatted_updated_at;
//            echo '<pre>$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            $cmsItem->update($updateDataArray);


            //     public static function getRowByIdLocale( int $cms_item_id,  string $locale, $return_count= false )
            $relatedCmsItemTranslation= CmsItemTranslation::getRowByIdLocale($id,$locale);
            //    protected $fillable = ['cms_item_id', 'locale', 'title', 'short_descr', 'content'];

            if ( empty($relatedCmsItemTranslation) ) {
                $newCmsItemTranslation = CmsItemTranslation::create([
                    'cms_item_id' => $cmsItem->id,
                    'title'       => $title,
                    'short_descr' => $short_descr,
                    'content'     => $content,
                    'locale'      => $locale,
                ]);
            } else {
                $relatedCmsItemTranslation->title = $title;
                $relatedCmsItemTranslation->short_descr = $short_descr;
                $relatedCmsItemTranslation->content = $content;
                $relatedCmsItemTranslation->update();
            }
//                        echo '<pre>$image::'.print_r($image,true).'</pre>';
            if ( !empty($image) ) {
                //     public static function getArtistImagePath(int $artist_id, $image_name, bool $set_public_subdirectory= false, bool $short_path= false) : string
                $file_name = CmsItem::getCmsItemImagePath( $cmsItem->id, $image ); //generating unique file name;
//                echo '<pre>$file_name::'.print_r($file_name,true).'</pre>';
                @list($type, $uploaded_file_data) = explode(';', $image_url);
                @list(, $uploaded_file_data) = explode(',', $uploaded_file_data);
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
                if($uploaded_file_data!=""){ // storing image in storage/app/public Folder
                    \Storage::disk('public')->put($file_name,base64_decode($uploaded_file_data));
                }
            }

            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
//        die("-1 XXZ");
        return response()->json(['error_code' => 0, 'message' => '', 'cmsItem' => $cmsItem], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    } // public function update(CmsItemRequest $request)


    public function store(CmsItemRequest $request)
    {
        $loggedUser = Auth::user();
        try {
            DB::beginTransaction();
            $cms_item_image_url            = !empty($registeredUserData['cms_item_image_url']) ? $registeredUserData['cms_item_image_url'] : '';
            if ( !empty($cms_item_image_url) ) {
                $image_url                 = $request->input('cms_item_image_url');
            }

            $insertDataArray = $request->all();
//            echo '<pre>++ $insertDataArray::'.print_r($insertDataArray,true).'</pre>';
            $title= $insertDataArray['title'];
            $short_descr= $insertDataArray['short_descr'];
            $content= $insertDataArray['content'];
            $locale= $insertDataArray['locale'];

            unset($insertDataArray['title']);
            unset($insertDataArray['short_descr']);
            unset($insertDataArray['content']);
            unset($insertDataArray['locale']);

            if (!empty($insertDataArray['image']) and isset($insertDataArray['image_url'])) {
                $image     = MyAppModel::checkValidImgName($insertDataArray['image'], 50 ,true);
                $insertDataArray['image']= $image;
                $image_url = $insertDataArray['image_url'];
                unset($insertDataArray['image_url']);
            }
            $insertDataArray['user_id']    = $loggedUser->id;
            $newCmsItem                    = CmsItem::create($insertDataArray);

            $newCmsItemTranslation         = CmsItemTranslation::create( [ 'cms_item_id'=> $newCmsItem->id, 'title'=>$title, 'short_descr'=> $short_descr, 'content'=> $content , 'locale'=> $locale ] );

            if ( !empty($image) ) {
                //     public static function getArtistImagePath(int $artist_id, $image_name, bool $set_public_subdirectory= false, bool $short_path= false) : string
                $file_name = CmsItem::getCmsItemImagePath( $newCmsItem->id, $image ); //generating unique file name;
//                echo '<pre>$file_name::'.print_r($file_name,true).'</pre>';
                @list($type, $uploaded_file_data) = explode(';', $image_url);
                @list(, $uploaded_file_data) = explode(',', $uploaded_file_data);
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
                if($uploaded_file_data!=""){ // storing image in storage/app/public Folder
                    \Storage::disk('public')->put($file_name,base64_decode($uploaded_file_data));
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'cmsItem' => $newCmsItem], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function store(CmsItemRequest $request)


    public function destroy($id)
    {
        try {
            $cmsItem = CmsItem::find($id);
            if ($cmsItem == null) {
                return response()->json(['error_code' => 11, 'message' => 'CmsItem # "' . $id . '" not found!', 'cms_item' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $artist_image_filename= CmsItem::getCmsItemImagePath($id,$cmsItem->image, true);
            $cmsItem->delete();
            CmsItem::deleteFileByPath($artist_image_filename, true);

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }



    ////////// LOCALE EDITOR BLOCK START //////////////
    public function get_cms_items_locale($cms_item_id, $locale= '')
    {
        $cms_item_id = (int)$cms_item_id;
        try {

            if ( empty($locale) ) {
                $cmsItemTranslation = CmsItem::where('id', $cms_item_id)->first();
                $cmsItemTranslation = $cmsItemTranslation->translate();
            } else {
                $cmsItemTranslation = CmsItemTranslation::getRowByIdLocale( $cms_item_id, $locale );
            }
            if ($cmsItemTranslation == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'CmsItemTranslation # "' . $cms_item_id . '" not found!',
                    'cmsItemTranslation'       => null,
                ], HTTP_RESPONSE_OK);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'cmsItemTranslation'       => $cmsItemTranslation,
        ], HTTP_RESPONSE_OK);
    }


    public function update_cms_items_locale(CmsItemTranslationRequest $request)
    {
        try {
            DB::beginTransaction();

            $updateDataArray     = $request->all();
            $cmsItem             = CmsItem::find($updateDataArray['cms_item_id']);
            if ($cmsItem == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'CmsItem # "' . $updateDataArray['cms_item_id'] . '" not found!',
                    'cms_item'      => (object)[ 'name'        => 'CmsItem # "' . $updateDataArray['cms_item_id'] . '" not # found!',  'description' => ''
                    ]
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $cmsItem->updated_at = now();
            $cmsItem->save();

            $cmsItemTranslation = CmsItemTranslation::getRowByIdLocale( $updateDataArray['cms_item_id'], $updateDataArray['locale'] );
            if ($cmsItemTranslation == null) { // there is no related cms item
                $cmsItemTranslation = CmsItemTranslation::create($updateDataArray);
            } else {
                $cmsItemTranslation->update($updateDataArray);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'cms_item_translation' => $cmsItemTranslation], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }
    ////////// LOCALE EDITOR BLOCK END //////////////


}