<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\library\AppPermissionAccess;
use App\library\AppPermissionAccessReturnType;
use App\User;
use App\ModelHasPermission;
use App\MyAppModel;
use App\Permission;
use App\Http\Requests\UserRequest;
use App\Settings;
use App\Http\Traits\funcsTrait;

class UsersController extends MyAppController
{
    use funcsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    //     Route::get('users/filter/{filter_type}/{filter_value}', 'Backend\UsersController@index');

    public function index($filter_type = '', $filter_value = '')
    {
        $filtersArray = [];
        if ( ! empty($filter_type) and strtolower($filter_type) == 'status') {
            $filtersArray['status'] = $filter_value;
        }
        $backend_locale= config('app.backend_locale', 'en');
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'u.username' );  
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        try {
            $all_users_count            = User::getUsersList(ListingReturnData::ROWS_COUNT, []);
            $new_users_count      = User::getUsersList(ListingReturnData::ROWS_COUNT, ['status'=>'N']);
            $active_users_count  = User::getUsersList(ListingReturnData::ROWS_COUNT, ['status'=>'A']);
            $inactive_users_count  = User::getUsersList(ListingReturnData::ROWS_COUNT, ['status'=>'I']);
            $rows_count                     = User::getUsersList(ListingReturnData::ROWS_COUNT, $filtersArray);
            $filtersArray['show_avatar']     = 1;
            $filtersArray['show_avatar_info']= 1;
            $filtersArray['locale']= $backend_locale;

            $usersList                   = User::getUsersList( ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json([
                'error_code'       => 1,
                'message'          => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page = with(new MyAppModel)->getItemsPerPage();

        sleep(  config('app.sleep_in_seconds', 0) );

        return response()->json([
            'error_code'                 => 0,
            'message'                   => '',
            'rows_count'                => $rows_count,
            'usersList'                 => $usersList,
            'all_users_count'           => $all_users_count,
            'new_users_count'           => $new_users_count,
            'active_users_count'        => $active_users_count,
            'inactive_users_count'      => $inactive_users_count,
            'per_page'                  => $per_page,
        ], HTTP_RESPONSE_OK);
    }


    public function show($id)
    {
        $id = (int)$id;
        try {


            $user = User::getRowById( $id, [ 'show_avatar'=> 1, 'show_avatar_info'=> 1 ] );
            if ($user == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'User # "' . $id . '" not found!',
                    'user'                    => null,
                    'categoriesSelectionList' => null
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'user'       => $user,
        ], HTTP_RESPONSE_OK);
    }

    public function dictionaries($edited_user_id= null)
    {
        $loggedUser                         = Auth::user();
        $editedUserSelectedPermissionsArray = [];
        $loggedUserModelHasPermissions      = ModelHasPermission::checkUserHasPermissionsValues( $loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE );
//        $IN_BACKEND_EDIT_USERS_DATA= false;
//        $IN_BACKEND_SET_USERS_ROLE_STATUS= false;
//        echo '<pre>$$loggedUser->id::'.print_r($loggedUser->id,true).'</pre>';
        $userStatusSelectionList            = User::getUserStatusValueArray(true);
//        try {

//                    $this->debToFile(print_r($edited_user_id,true),' 000   -1 $edited_user_id::');

            if ( !empty($edited_user_id) and $edited_user_id != "new" ) {
                $editedUser= User::find($edited_user_id);
//                $this->debToFile(print_r($editedUser,true),' 000   -2 $editedUser::');
            }
            if( !empty($editedUser) ) {
                $editedUserSelectedPermissionsArray = ModelHasPermission::checkUserHasPermissionsValues($editedUser, AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE);
//                $this->debToFile(print_r($editedUserSelectedPermissionsArray,true),' 000   -3 $editedUserSelectedPermissionsArray::');
            }
            $permissionsArray                       = Permission::getPermissionsArray(true);
        return response()->json([
            'error_code'                              => 0,
            'message'                                 => '',
            'loggedUser'                              => $loggedUser,
//            'IN_BACKEND_EDIT_USERS_DATA'              => $IN_BACKEND_EDIT_USERS_DATA,
//            'IN_BACKEND_SET_USERS_ROLE_STATUS'        => $IN_BACKEND_SET_USERS_ROLE_STATUS,
            'IN_BACKEND_EDIT_USERS_DATA'              => in_array(IN_BACKEND_EDIT_USERS_DATA,$loggedUserModelHasPermissions),
            'IN_BACKEND_SET_USERS_ROLE_STATUS'        => in_array(IN_BACKEND_SET_USERS_ROLE_STATUS,$loggedUserModelHasPermissions),
            'userStatusSelectionList'                 => $userStatusSelectionList,
            'permissionsArray'                        => $permissionsArray,
            'selectedPermissionsArray'                => $editedUserSelectedPermissionsArray,
        ], HTTP_RESPONSE_OK);

    } // public function dictionaries($user_id)



    public function load_user_permission_groups_list($edited_user_id= null)
    {
        $loggedUser                         = Auth::user();
        $editedUserSelectedPermissionsArray = [];
        $loggedUserModelHasPermissions      = ModelHasPermission::checkUserHasPermissionsValues( $loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE );
//        $IN_BACKEND_EDIT_USERS_DATA= false;
//        $IN_BACKEND_SET_USERS_ROLE_STATUS= false;
//        echo '<pre>$$loggedUser->id::'.print_r($loggedUser->id,true).'</pre>';
        $userStatusSelectionList            = User::getUserStatusValueArray(true);
//        try {

//                    $this->debToFile(print_r($edited_user_id,true),' 000   -1 $edited_user_id::');

            if ( !empty($edited_user_id) and $edited_user_id != "new" ) {
                $editedUser= User::find($edited_user_id);
//                $this->debToFile(print_r($editedUser,true),' 000   -2 $editedUser::');
            }
            if( !empty($editedUser) ) {
                $editedUserSelectedPermissionsArray = ModelHasPermission::checkUserHasPermissionsValues($editedUser, AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE);
//                $this->debToFile(print_r($editedUserSelectedPermissionsArray,true),' 000   -3 $editedUserSelectedPermissionsArray::');
            }
            $permissionsArray                       = Permission::getPermissionsArray(true);
        return response()->json([
            'error_code'                              => 0,
            'message'                                 => '',
            'loggedUser'                              => $loggedUser,
//            'IN_BACKEND_EDIT_USERS_DATA'              => $IN_BACKEND_EDIT_USERS_DATA,
//            'IN_BACKEND_SET_USERS_ROLE_STATUS'        => $IN_BACKEND_SET_USERS_ROLE_STATUS,
            'IN_BACKEND_EDIT_USERS_DATA'              => in_array(IN_BACKEND_EDIT_USERS_DATA,$loggedUserModelHasPermissions),
            'IN_BACKEND_SET_USERS_ROLE_STATUS'        => in_array(IN_BACKEND_SET_USERS_ROLE_STATUS,$loggedUserModelHasPermissions),
            'userStatusSelectionList'                 => $userStatusSelectionList,
            'permissionsArray'                        => $permissionsArray,
            'selectedPermissionsArray'                => $editedUserSelectedPermissionsArray,
        ], HTTP_RESPONSE_OK);

    } // public function load_user_permission_groups_list($user_id)



    public function update(UserRequest $request)
    {
        $id            = $request->id;
        $user       = User::find($id);
        if ($user == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'User # "' . $id . '" not found!',
                'user'      => (object)[ 'name'        => 'User # "' . $id . '" not # found!',  'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            $updateDataArray               = $request->all();
//            echo '<pre> $updateDataArray::'.print_r($updateDataArray,true).'</pre>';
//            die("-1 XXZ");
            DB::beginTransaction();
            $selectedPermissionsArray= [];
            if ( !empty( $updateDataArray['selectedPermissionsArray'] ) ) {

                $selectedPermissionsArray= $updateDataArray['selectedPermissionsArray'];
                unset($updateDataArray['selectedPermissionsArray']);
            }
//            echo '<pre>$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            /*                     data: {
                        id: this.user_id, username: this.user_username, first_name: this.user_first_name, last_name: this.user_last_name,
                        phone: this.user_phone, website: this.user_website,
                        status: this.user_SelectStatus.key,
                        avatar_url: this.user_avatar_url, avatar: this.user_avatar
                    },
 */
            if (!empty($updateDataArray['image']) and isset($updateDataArray['image_url'])) {
                $image     = MyAppModel::checkValidImgName($updateDataArray['image'], 50,true);
                $image_url = $updateDataArray['image_url'];
                $updateDataArray['image']= $image;
                unset($updateDataArray['image_url']);
            }

            $datetime_carbon_format        = config('app.datetime_carbon_format','Y-m-d H:i:s');
            $formatted_updated_at          = Carbon::createFromFormat($datetime_carbon_format, now());
            $updateDataArray['updated_at'] = $formatted_updated_at;
//            echo '<pre>$updateDataArray::'.print_r($updateDataArray,true).'</pre>';
            $user->update($updateDataArray);

//                        echo '<pre>$image::'.print_r($image,true).'</pre>';
            if ( !empty($image) ) {
                //     public static function getArtistImagePath(int $artist_id, $image_name, bool $set_public_subdirectory= false, bool $short_path= false) : string
                $file_name = User::getUserImagePath( $user->id, $image ); //generating unique file name;
//                echo '<pre>$file_name::'.print_r($file_name,true).'</pre>';
                @list($type, $uploaded_file_data) = explode(';', $image_url);
                @list(, $uploaded_file_data) = explode(',', $uploaded_file_data);
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
                if($uploaded_file_data!=""){ // storing image in storage/app/public Folder
                    \Storage::disk('public')->put($file_name,base64_decode($uploaded_file_data));
                }
            }

            $user->revokePermissionTo(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES);
            $user->revokePermissionTo(IN_BACKEND_EDIT_USERS_DATA);
            $user->revokePermissionTo(IN_BACKEND_SET_USERS_ROLE_STATUS);
            $user->revokePermissionTo(IN_BACKEND_EDIT_CMS_DATA);
            $user->revokePermissionTo(IN_BACKEND_PUBLISH_CMS_DATA);
            $user->revokePermissionTo(IN_FRONTEND_LOGGED_USER_AREA);
            $user->revokePermissionTo(IN_FRONTEND_ALL_PUBLIC_PAGES);

            foreach( $selectedPermissionsArray as $next_key=>$nextSelectedPermission ) {
                echo '<pre>UPDATE $nextSelectedPermission[\'label\']::'.print_r($nextSelectedPermission['label'],true).'</pre>';
                $user->givePermissionTo($nextSelectedPermission['label']);
            }


            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
//        die("-1 XXZ");
        return response()->json(['error_code' => 0, 'message' => '', '$user' => $user], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(UserRequest $request)
    {
        $loggedUser = Auth::user();
        try {
            DB::beginTransaction();
            $user_avatar_url            = !empty($registeredUserData['user_avatar_url']) ? $registeredUserData['user_avatar_url'] : '';
            if ( !empty($user_avatar_url) ) {
                $avatar_url                 = $request->input('user_avatar_url');
            }

            $insertDataArray = $request->all();
            $selectedPermissionsArray= [];
//            echo '<pre> $insertDataArray::'.print_r($insertDataArray,true).'</pre>';
            if ( !empty( $insertDataArray['selectedPermissionsArray'] ) ) {

                $selectedPermissionsArray= $insertDataArray['selectedPermissionsArray'];
                unset($insertDataArray['selectedPermissionsArray']);
            }
//            echo '<pre>++ $insertDataArray::'.print_r($insertDataArray,true).'</pre>';
            if (!empty($insertDataArray['avatar']) and isset($insertDataArray['avatar_url'])) {
                $avatar     = MyAppModel::checkValidImgName($insertDataArray['avatar'], 100 ,true);
                echo '<pre>$avatar::'.print_r($avatar,true).'</pre>';
                $insertDataArray['avatar']= $avatar;
                $avatar_url = $insertDataArray['avatar_url'];
                unset($insertDataArray['avatar_url']);
            }
            $insertDataArray['user_id']    = $loggedUser->id;
            echo '<pre>++ $insertDataArray::'.print_r($insertDataArray,true).'</pre>';
            $newUser                       = User::create($insertDataArray);

            if ( !empty($avatar) ) {
                //     public static function getArtistAvatarPath(int $artist_id, $avatar_name, bool $set_public_subdirectory= false, bool $short_path= false) : string
                $file_name = User::getUserAvatarPath( $newUser->id, $avatar ); //generating unique file name;
//                echo '<pre>$file_name::'.print_r($file_name,true).'</pre>';
                @list($type, $uploaded_file_data) = explode(';', $avatar_url);
                @list(, $uploaded_file_data) = explode(',', $uploaded_file_data);
//                echo '<pre>$uploaded_file_data::'.print_r($uploaded_file_data,true).'</pre>';
                if($uploaded_file_data!=""){ // storing avatar in storage/app/public Folder
                    \Storage::disk('public')->put($file_name,base64_decode($uploaded_file_data));
                }
            }

            foreach( $selectedPermissionsArray as $next_key=>$nextSelectedPermission ) {
                $newUser->givePermissionTo($nextSelectedPermission['label']);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'user' => $newUser], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)
    {
        try {
            $user = User::find($id);
            if ($user == null) {
                return response()->json(['error_code' => 11, 'message' => 'User # "' . $id . '" not found!', 'user' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $artist_avatar_filename= User::getUserAvatarPath($id,$user->avatar, true);
            $user->delete();
            MyAppModel::deleteFileByPath($artist_avatar_filename, true);

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }


}