<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\library\AppPermissionAccess;
use App\library\AppPermissionAccessReturnType;
use App\ModelHasPermission;
use App\User;
use App\Http\Requests\GenreRequest;
use App\Http\Requests\SongGenreRequest;
use App\Http\Requests\GenreTranslationRequest;
use App\Genre;
use App\GenreTranslation;
use App\Song;
use App\SongGenre;
use App\Settings;
use App\Http\Traits\funcsTrait;

class GenresController extends MyAppController
{
    use funcsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($filter = '')
    {
        $filtersArray = [];
        if ( ! empty($filter) and strtolower($filter) != 'all') {
            $filtersArray['status'] = $filter;
        }
        $page= (int)$this->getParameter( 'page', 1 );
        $order_by= $this->getParameter( 'order_by', 'name' );
        $order_direction= $this->getParameter( 'order_direction', 'asc' );
        $backend_locale= config('app.backend_locale', 'en');

        try {
            $all_genres_count            = Genre::getGenresList(ListingReturnData::ROWS_COUNT, ['']);
            $published_genres_count      = Genre::getGenresList(ListingReturnData::ROWS_COUNT, ['published'=>true]);
            $not_published_genres_count  = Genre::getGenresList(ListingReturnData::ROWS_COUNT, ['published'=>false]);
            $related_genre_songs_count   = SongGenre::getSongGenresList(ListingReturnData::ROWS_COUNT, []);
            $rows_count       = Genre::getGenresList(ListingReturnData::ROWS_COUNT, $filtersArray);
            $filtersArray['show_related_songs_count']= 1;
            $filtersArray['locale']= $backend_locale;
            $genresList      = Genre::getGenresList( ListingReturnData::LISTING, $filtersArray, $order_by, $order_direction, $page );
        } catch (Exception $e) {
            return response()->json([
                'error_code'       => 1,
                'message'          => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $per_page = with(new Genre)->getItemsPerPage();

        sleep(  config('app.sleep_in_seconds', 0) );

        return response()->json([
            'error_code'       => 0,
            'message'          => '',
            'rows_count'       => $rows_count,
            'genresList'            => $genresList,
            'all_genres_count'      => $all_genres_count,
            'related_genre_songs_count'      => $related_genre_songs_count,
            'published_genres_count'      => $published_genres_count,
            'not_published_genres_count'      => $not_published_genres_count,
            'per_page'         => $per_page,
        ], HTTP_RESPONSE_OK);
    }


    public function show($id, $locale)
    {
        $id = (int)$id;
        try {

            $genre = Genre::getRowById( $id, ['show_genre_songs_count'=> 1, 'locale'=>$locale ] );
            if ($genre == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'Genre # "' . $id . '" not found!',
                    'genre'                    => null,
                    'categoriesSelectionList' => null
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'genre'       => $genre,
        ], HTTP_RESPONSE_OK);
    }

    public function dictionaries()
    {
        $loggedUser                    = Auth::user();
        $loggedUserModelHasPermissions = ModelHasPermission::checkUserHasPermissionsValues( $loggedUser, AppPermissionAccessReturnType::STRING_RETURN_TYPE );
        try {
            $genrePublishedSelectionList            = Genre::getGenrePublishedValueArray(true);
            $backend_locale                         = \Config::get('app.backend_locale');
            $backend_locale_label                   = $this->getBackendLocaleLabel();
            $langsInSystemList= $this->getBackendLangs();
        } catch (Exception $e) {
            return response()->json([
                'error_code'                             => 1,
                'message'                                => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'IN_BACKEND_EDIT_SYSTEM_DICTIONARIES'    => in_array(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES,$loggedUserModelHasPermissions),
            'genrePublishedSelectionList'            => $genrePublishedSelectionList,
            'langsInSystemList'                      => $langsInSystemList,
            'backend_locale'                         => $backend_locale,
            'backend_locale_label'                   => $backend_locale_label,
        ], HTTP_RESPONSE_OK);

    } // public function dictionaries($genre_id)

    public function update(GenreRequest $request)
    {
        $id         = $request->id;
        $genre       = Genre::find($id);
        if ($genre == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Genre # "' . $id . '" not found!',
                'genre'      => (object)[ 'name'        => 'Genre # "' . $id . '" not # found!',  'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $updateDataArray               = $request->all();

            $name= $updateDataArray['name'];
            $description= $updateDataArray['description'];
            $locale= $updateDataArray['locale'];

            unset($updateDataArray['name']);
            unset($updateDataArray['description']);
            unset($updateDataArray['locale']);


            $datetime_carbon_format        = config('app.datetime_carbon_format','Y-m-d H:i:s');
            $formatted_updated_at          = Carbon::createFromFormat($datetime_carbon_format, now());
            $updateDataArray['updated_at'] = $formatted_updated_at;
            $genre->update($updateDataArray);

            //     public static function getRowByIdLocale( int $genre_id,  string $locale, $return_count= false )
            $relatedGenreTranslation= GenreTranslation::getRowByIdLocale($id,$locale);
            //    protected $fillable = ['genre_id', 'locale', 'title', 'short_descr', 'content'];

            if ( empty($relatedGenreTranslation) ) {
                $newGenreTranslation = GenreTranslation::create([
                    'genre_id' => $genre->id,
                    'name'       => $name,
                    'description' => $description,
                    'locale'      => $locale,
                ]);
            } else {
                $relatedGenreTranslation->name = $name;
                $relatedGenreTranslation->description = $description;
                $relatedGenreTranslation->update();
            }


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'genre' => $genre], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function store(GenreRequest $request)
    {
        try {
            DB::beginTransaction();
            $insertDataArray = $request->all();

            $name= $insertDataArray['name'];
            $description= $insertDataArray['description'];
            $locale= $insertDataArray['locale'];

            unset($insertDataArray['name']);
            unset($insertDataArray['description']);
            unset($insertDataArray['locale']);

            $newGenre        = Genre::create($insertDataArray);
            $newGenreTranslation         = GenreTranslation::create( [ 'genre_id'=> $newGenre->id, 'name'=>$name, 'description'=> $description, 'locale'=> $locale ] );
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'genre' => $newGenre], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }


    public function destroy($id)
    {
        try {
            $genre = Genre::find($id);
            if ($genre == null) {
                return response()->json(['error_code' => 11, 'message' => 'Genre # "' . $id . '" not found!', 'genre' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $song_genres_count= SongGenre::getSongGenresList( ListingReturnData::ROWS_COUNT, [ 'genre_id'=>$id ] );
            if ( $song_genres_count > 0 ) {
                return response()->json(['error_code'=> 1, 'message'=> 'Genre # "'.$genre->name.'" can not be deleted, as it is assigned to 
                '.$song_genres_count . ' song(s). ', 'genre'=>null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $genre->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }


    //// GENRE RELATED SONGS BLOCK START /////////
    public function get_genre_song($song_id, $genre_id= '')
    {
        try {
            $genreSong= null;
            $genreRelatedSongsList      = SongGenre::getSongGenresList( ListingReturnData::LISTING, [ 'genre_id'=>$genre_id, 'song_id'=>$song_id, 'show_song_name'=> 1
            ] );
            if ( $genreRelatedSongsList->count() == 0 ) {
                $song      = Song::getRowById( $song_id);
                $genreSong= [ 'song_id'=>$song->id, 'song_title'=>$song->title, 'song_is_active'=>($song->is_active?1:0), 'song_ordering'=>$song->ordering, 'song_short_descr'=>$song->short_descr, 'song_description'=>$song->description, 'song_created_at'=>$song->created_at, 'song_updated_at'=>$song->updated_at ];
            } else {
                $genreSong= $genreRelatedSongsList[0];
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                      => 0,
            'message'                         => '',
            'genreSong'                       => $genreSong,
        ], HTTP_RESPONSE_OK);

    } // public function get_genre_song($genre_id)


    public function get_genre_related_songs($genre_id)
    {
        $backend_locale= config('app.backend_locale', 'en');
        try {
            $songIsActiveSelectionList        = Song::getSongIsActiveValueArray(true);
            $songsList= [];
            $checked_genre_songs_rows_count= 0;
            $songs                 = Song::getSongsList( ListingReturnData::LISTING, ['locale'=> $backend_locale], 'ordering', 'asc' );
            $genreRelatedSongsList = [];
            if ( strtolower($genre_id)!= 'new' ) {
                $genreRelatedSongsList = SongGenre::getSongGenresList(ListingReturnData::LISTING, ['genre_id' => $genre_id, 'show_song_name' => 1, 'locale'=> $backend_locale], 'song_ordering', 'asc');
            }
            foreach( $songs as $next_key=>$nextSongs ) {
//                echo '<pre>$nextSongs::'.print_r($nextSongs,true).'</pre>';
//                die("-1 XXZ");
                $is_found= false;
                foreach( $genreRelatedSongsList as $nextGenreRelatedSong ) {
                    if ( $nextSongs->id == $nextGenreRelatedSong->song_id ) {
                        $is_found= $nextGenreRelatedSong->genre_id;
                        $checked_genre_songs_rows_count++;
                        break;
                    }
                }
                $songsList[]= [ 'genre_id'=>$is_found, 'song_id'=>$nextSongs->id, 'song_ordering'=>$nextSongs->ordering, 'song_title'=>$nextSongs->title,
                                'song_slug'=>$nextSongs->slug,
                                'song_is_active'=>$nextSongs->is_active, 'song_short_descr'=>$nextSongs->short_descr, 'song_created_at'=>$nextSongs->created_at, 'song_description'=>$nextSongs->description, 'checked'=> ($is_found ? true : false) ];
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'songsList'                           => $songsList,
            'songIsActiveSelectionList'           => $songIsActiveSelectionList,
            'checked_genre_songs_rows_count'      => $checked_genre_songs_rows_count,
        ], HTTP_RESPONSE_OK);
    } // public function get_genre_related_songs($genre_id)

    public function add_song_to_genre($genre_id, $song_id)
    {
        $new_song_genre_id= '';
        try {

            $genreRelatedSongsList      = SongGenre::getSongGenresList( ListingReturnData::LISTING, [ 'genre_id'=>$genre_id, 'song_id'=>$song_id ] );
            if ( $genreRelatedSongsList->count() == 0 ) {
                $newSongGenre           = new SongGenre();
                $newSongGenre->genre_id = $genre_id;
                $newSongGenre->song_id  = $song_id;
                
                DB::beginTransaction();
                $newSongGenre->save();
                $new_song_genre_id= $newSongGenre->id;
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                   => 0,
            'message'                      => '',
            'id'                           => $new_song_genre_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    } // public function add_song_to_genre($genre_id)

    public function remove_song_from_genre($genre_id, $song_id)
    {

        try {
            $genreRelatedSongsList      = SongGenre::getSongGenresList( ListingReturnData::LISTING, [ 'genre_id'=>$genre_id, 'song_id'=>$song_id ] );
            if ( $genreRelatedSongsList->count() == 0 ) {
                return response()->json([
                    'error_code'              => 1,
                    'message'                 => 'Genre # "' . $genre_id . '" for song # "'.$song_id .'" not found!',
                    'genre_id'                => $genre_id,
                    'song_id'                 => $song_id,
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $genreRelatedSongsList[0]->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'error_code'                  => 1,
                'message'                     => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
            'genre_id'                    => $genre_id,
            'song_id'                     => $song_id,
        ], HTTP_RESPONSE_OK_RESOURCE_DELETED );
    } // public function remove_song_from_genre($genre_id)



    public function update_song_genre(/*SongGenreRequest $request*/)
    {
        $request = request();

        $genre_id         = $request->genre_id;
        $genreSong         = $request->genreSong;
        $song_id= ( !empty($genreSong['song_id']) ? $genreSong['song_id'] : '' );
        $song= null;
        $is_inserted= false;
        if ( !empty($song_id) and is_integer($song_id)) {
            $song       = Song::find($song_id);
        }

        if ($song == null) {
            $song= new Song();
            $is_inserted= true;
        }
        try {
            DB::beginTransaction();
            $song->title= $genreSong['song_title'];
            $song->is_active= !empty($genreSong['song_is_active'] ) ? $genreSong['song_is_active'] : false;
            $song->ordering= $genreSong['song_ordering'];
            $song->short_descr= $genreSong['song_short_descr'];
            $song->description= $genreSong['song_description'];
            $attach_song_to_genre= $genreSong['attach_song_to_genre'];

            if (!$is_inserted) {
                $song->updated_at = now();
            }
            $song->save();
            $song_id= $song->id;

            if ( $attach_song_to_genre ) {
                $songGenre           = new SongGenre();
                $songGenre->song_id  = $song->id;
                $songGenre->genre_id = $genre_id;
                $songGenre->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'id' => $song_id], HTTP_RESPONSE_OK_RESOURCE_CREATED );
    } // public function update_song_genre()


    public function remove_song($id)
    {
        try {
            $song = Song::find($id);
            if ($song == null) {
                return response()->json(['error_code' => 11, 'message' => 'Song # "' . $id . '" not found!', 'song' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $song->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }

    //// GENRE RELATED SONGS BLOCK END /////////


    ////////// LOCALE EDITOR BLOCK START //////////////
    public function get_genres_locale($genre_id, $locale= '')
    {
        $genre_id = (int)$genre_id;
        try {

            if ( empty($locale) ) {
                $genreTranslation = Genre::where('id', $genre_id)->first();
                $genreTranslation = $genreTranslation->translate();
            } else {
                $genreTranslation = GenreTranslation::getRowByIdLocale( $genre_id, $locale );
            }
            if ($genreTranslation == null) {
                return response()->json([
                    'error_code'              => 11,
                    'message'                 => 'GenreTranslation # "' . $genre_id . '" not found!',
                    'genreTranslation'        => null,
                ], HTTP_RESPONSE_OK);
            }

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(  config('app.sleep_in_seconds', 0) );
        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'genreTranslation'       => $genreTranslation,
        ], HTTP_RESPONSE_OK);
    }


    public function update_genre_locale(GenreTranslationRequest $request)
    {
        try {
            DB::beginTransaction();

            $updateDataArray             = $request->all();
            $genre                       = Genre::find($updateDataArray['genre_id']);
            if ($genre == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Genre # "' . $updateDataArray['genre_id'] . '" not found!',
                    'genre'      => (object)[ 'name'        => 'Genre # "' . $updateDataArray['genre_id'] . '" not # found!',  'description' => ''
                    ]
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }


            $genre->updated_at = now();
            $genre->save();

            $genreTranslation = GenreTranslation::getRowByIdLocale( $updateDataArray['genre_id'], $updateDataArray['locale'] );
            if ($genreTranslation == null) { // there is no related genre
                $genreTranslation = GenreTranslation::create($updateDataArray);
            } else {
                $genreTranslation->update($updateDataArray);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => '', 'genre_translation' => $genreTranslation], HTTP_RESPONSE_OK_RESOURCE_CREATED);
    }
    ////////// LOCALE EDITOR BLOCK END //////////////



}