<?php

namespace App\Http\Controllers\Backend;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Config;

use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;
use App\library\PgDataType;
use App\MyAppModel;

use App\User;
//use App\Artist;
//use App\Song;
//use App\ArtistConcert;
use App\Settings;
//use App\SongVote;
//use App\ArtistVote;
//use League\HTMLToMarkdown\HtmlConverter;
use function PHPSTORM_META\map;


class SettingsController extends MyAppController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $settingsList                           = Settings::getSettingsList([], true);
//            echo '<pre>$settingsList::'.print_r($settingsList,true).'</pre>';
        } catch (Exception $e) {
            return response()->json([
                'error_code'                        => 1,
                'message'                           => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                            => 0,
            'message'                               => '',
            'settingsList'                          => $settingsList,
        ], HTTP_RESPONSE_OK);
    }


    public function settings_dictionaries()
    {
        try {
            $countriesList                          = $this->getCountries(true);
            $yesNoValueList                         = Settings::getYesNoValueArray(true);
        } catch (Exception $e) {
            return response()->json([
                'error_code'                        => 1,
                'message'                           => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json([
            'error_code'                             => 0,
            'message'                                => '',
            'countriesList'                          => $countriesList,
            'yesNoValueList'                         => $yesNoValueList,
        ], HTTP_RESPONSE_OK);

    } // public function settings_dictionaries()


}