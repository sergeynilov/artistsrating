<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Input;

use Auth;
use DB;
use Config;

use App\Http\Traits\funcsTrait;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\Settings;
use App\User;
//use App\ArtistConcert;
//use App\UsersGroups;
use App\Artist;
use App\Song;
use App\ArtistImage;
//use App\ArtistVote;
//use App\SongVote;
//use App\SongArtist;
//use App\Tour;
use App\CmsItem;
use App\SongGenre;
use App\Order;
use App\OrderItem;
use Cart;

class CartController extends MyAppController
{
    use funcsTrait;


    public function index()
    {
//        echo '<pre>app/Http/Controllers/HomeController.php$_GET ::'.print_r($_GET,true).'</pre>';
//        die("-1 XXZ");
        $this->addVariablesToJS();
        $site_name = Settings::getValue('site_name', '');
//        with(new self)->info( $site_name,'++ HOME index $site_name ::' );

        return view('frontend.home.index', ['site_name' => $site_name]);
//        return view('frontend.home.index', ['site_name' => $site_name]);
    }



    ///////// ARTIST CART BLOCK START //////////
    public function add_artist_to_cart($artist_id, $locale)
    {
        if (!Auth::check()) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'common.you_have_to_login',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $loggedUser = Auth::user();
        try {
            $artist = Artist::getRowById($artist_id, ['locale'=> $locale]);
            if ($artist == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Artist "' . $artist_id . '" not found!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            if ($artist->is_active != 'A') {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Artist "' . $artist_id . '" is not active!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $duplicatedArtists = Cart::search(function ($nextCartItem, $rowId) use ($artist) {
                $options= $nextCartItem->options;
                return $nextCartItem->model->id === $artist->id and $options['product_type'] == 'artist';
            });

            if ($duplicatedArtists->isNotEmpty()) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Artist is already in your cart!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $productsOptions= ['product_type' => 'artist', 'product_name' => $artist->name, 'product_id' => $artist_id];
            $artistImagesList = ArtistImage::getArtistImagesList(ListingReturnData::LISTING, ['artist_id' => $artist_id, 'show_image_info' => 1, 'show_image' => 1, 'is_main'=> 1 ]);
            foreach( $artistImagesList as $nextArtistImage ) {
//                    echo '<pre>$nextArtistImage::'.print_r($nextArtistImage,true).'</pre>';
                if ( $nextArtistImage->is_main ) {

                    $artistImageImagePropsAttribute= $nextArtistImage->getArtistImageImagePropsAttribute();
                    $productsOptions= array_merge($productsOptions,$artistImageImagePropsAttribute);
//                    echo '<pre>$artistImageImagePropsAttribute::'.print_r($artistImageImagePropsAttribute,true).'</pre>';
//                    echo '<pre>$productsOptions::'.print_r($productsOptions,true).'</pre>';
//                    die("-1 XXZ");
//                    echo '<pre>$nextArtistImage::'.print_r($nextArtistImage,true).'</pre>';
//                    $artistMainImage= $nextArtistImage;
                }
            }

            $artist_price = \Config::get('app.1_artist_price');
            Cart::add(['id' => $artist->id, 'name' => $artist->name, 'qty' => 1, 'price' => $artist_price, 'options' => $productsOptions])->associate('App\Artist');
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        sleep(  1 );

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'artist'                 => $artist,
        ], HTTP_RESPONSE_OK);
    } // public function add_artist_to_cart()

    ///////// ARTIST CART BLOCK END //////////


    ///////// SONG CART BLOCK START //////////
    public function add_song_to_cart($song_id, $locale)
    {
        if (!Auth::check()) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'common.you_have_to_login',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $loggedUser = Auth::user();
        try {
            $song = Song::getRowById($song_id, ['locale'=> $locale]);

            if ($song == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Song "' . $song_id . '" not found!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            if ($song->is_active != 'A') {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Song "' . $song_id . '" is not active!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $duplicatedSongs = Cart::search(function ($nextCartItem, $rowId) use ($song) {
                $options= $nextCartItem->options;
                return $nextCartItem->model->id === $song->id and $options['product_type'] == 'song';
            });

            if ($duplicatedSongs->isNotEmpty()) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'Song is already in your cart!',
                ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $productsOptions= ['product_type' => 'song', 'product_name' => $song->title, 'product_id' => $song_id];
            $relatedGenresArray= [];
            $relatedGenresList = SongGenre::getSongGenresList(ListingReturnData::LISTING, ['song_id' => $song_id, 'show_genre_name'=>1, 'locale'=> $locale]/*, 'g_t.name', 'asc'*/);
            foreach( $relatedGenresList as $nextRelatedGenre ) {
                $relatedGenresArray[]= ['genre_id'=>$nextRelatedGenre->genre_id, 'genre_name'=>$nextRelatedGenre->genre_name, 'genre_slug'=>$nextRelatedGenre->genre_slug];
            }
//            echo '<pre>$relatedGenresArray::'.print_r($relatedGenresArray,true).'</pre>';
//            die("-1 XXZ");
            $productsOptions['relatedGenresArray']= $relatedGenresArray;

            $song_price = \Config::get('app.1_song_price');
            Cart::add(['id' => $song->id, 'name' => $song->title, 'qty' => 1, 'price' => $song_price, 'options' => $productsOptions])->associate('App\Song');
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        sleep(  1 );

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'song'                 => $song,
        ], HTTP_RESPONSE_OK);
    } // public function add_song_to_cart()

    ///////// SONG CART BLOCK END //////////



    public function get_cart_data($locale)
    {
        $loggedUser             = Auth::user();
        try {
            $nextCartItemsList= Cart::content();
            $cartContentsArray = $nextCartItemsList->map(function ($item) {
                $options= $item->options;
//                    echo '<pre>$item::'.print_r($item,true).'</pre>';
//                    echo '<pre>$options::'.print_r($options,true).'</pre>';
//                $options= $item->options->product_type;
//                echo '<pre>$options->product_type::'.print_r($options->product_type,true).'</pre>';
                return [
                    'product_row_id'=>$item->rowId,
                    'product_name'=>$item->name,
                    'product_id'=>$item->model->id,
                    'price'=>$item->price,
                    'slug'=>$item->model->slug,
                    'qty'=>$item->qty,
                    'product_type'=>$options->product_type,
                    'relatedGenresArray'=>$options->relatedGenresArray,
                    'image'=>$options->image,
                    'image_path'=>$options->image_path,
                    'image_url'=>$options->image_url,
                    'image_preview_width'=>$options->image_preview_width,
                    'image_preview_height'=>$options->image_preview_height,
                ];
            })->values();

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

//        sleep(  1 );

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'cartContentsArray'      => $cartContentsArray,
            'cart_price_total'       => Cart::instance('default')->subtotal(),
            'cart_qty_count'         => Cart::instance('default')->count()
        ], HTTP_RESPONSE_OK);
    } // public function get_cart_data()

    public function get_cart_info()
    {
        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'cart_price_total'       => Cart::instance('default')->subtotal(),
            'cart_qty_count'         => Cart::instance('default')->count()
        ], HTTP_RESPONSE_OK);
    } // public function get_cart_info()

    public function cart_remove_product($product_row_id, $product_id)
    {
        try {
            Cart::remove($product_row_id);
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }

    public function clear_cart_content()
    {
        try {
            Cart::destroy();
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK_RESOURCE_DELETED);
    }


    public function complete_order()
    {
        $loggedUser             = Auth::user();
        $card_owner= $this->getParameter( 'card_owner' );
        $card_number= $this->getParameter( 'card_number' );
        $discount= $this->getParameter( 'discount', 0 );
        $discount_code= $this->getParameter( 'discount_code', '' );

//        $locale= $this->getParameter( 'locale' );
        try {
            DB::beginTransaction();
            $insertOrderData= [
                'user_id'=> $loggedUser->id,
                'card_owner'=> $card_owner,
                'qty_count'=> Cart::instance('default')->count(),
                'price_total'=> Cart::instance('default')->subtotal(),
                'payment'=> 'stripe',
                'completed'=> true
            ];

            if ( $discount ) {
                $insertOrderData['discount'] = $discount;
            }
            if ( !empty($discount_code) ) {
                $insertOrderData['discount_code'] = $discount_code;
            }
            
            $newOrder                    = Order::create($insertOrderData);

            $new_order_id= $newOrder->id;
            $nextCartItemsList= Cart::content();
            foreach( $nextCartItemsList as $nextCartItem ) {
                $options= $nextCartItem->options;
                $insertOrderItemData= [
                    'order_id'=> $new_order_id,
                    'product_id'=> $nextCartItem->model->id,
                    'product_type'=> $options->product_type,
                    'qty'=> $nextCartItem->qty,
                    'price'=> $nextCartItem->price
                ];
                $newOrderItem                    = OrderItem::create($insertOrderItemData);

            }
            $cartContentsArray = $nextCartItemsList->map(function ($item) {
                $options= $item->options;
                return [
                    'product_row_id'=>$item->rowId,
                    'product_name'=>$item->name,
                    'product_id'=>$item->model->id,
                    'price'=>$item->price,
                    'slug'=>$item->model->slug,
                    'qty'=>$item->qty,
                    'product_type'=>$options->product_type,
                    'relatedGenresArray'=>$options->relatedGenresArray,
                    'image'=>$options->image,
                    'image_path'=>$options->image_path,
                    'image_url'=>$options->image_url,
                    'image_preview_width'=>$options->image_preview_width,
                    'image_preview_height'=>$options->image_preview_height,
                ];
            })->values();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage() ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'             => 0,
            'message'                => '',
            'order_id'               => $new_order_id,
        ], HTTP_RESPONSE_OK);
    } // public function complete_order()


}

