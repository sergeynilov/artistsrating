<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Input;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Config;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Http\Traits\funcsTrait;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\MyAppController;
use App\library\ListingReturnData;

use App\Settings;
use App\User;
use App\MyAppModel;
use App\Http\Requests\UserRegisterRequest;
use App\ArtistConcert;
use App\Artist;
use App\ArtistImage;
use App\Genre;
use App\Tour;
use App\CmsItem;
use App\ModelHasPermission;
use App\Song;
use App\Events\backendFailOnLoginEvent;
use App\Events\backendSuccessOnLoginEvent;


class HomeController extends MyAppController
{
    use funcsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

//        Genre::fillTranslations();
//        ArtistConcert::fillTranslations();
//        ArtistImage::fillTranslations();
//        CmsItem::fillTranslations();
//        Song::fillTranslations();
//        Artist::fillTranslations();
//        echo '<pre>app/Http/Controllers/HomeController.php$_GET ::'.print_r($_GET,true).'</pre>';
//        die("-1 XXZ");
//        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        echo '<pre>app/Http/Controllers/HomeController.php$_GET ::'.print_r($_GET,true).'</pre>';
//        die("-1 XXZ");
        $this->addVariablesToJS();
        $site_name = Settings::getValue('site_name', '');
        with(new self)->info( $site_name,'++ HOME index $site_name ::' );

        return view('frontend.home.index', ['site_name' => $site_name]);
    }

    public function cms_item_index()
    {
        echo '<pre>app/Http/Controllers/HomeController.php$ cms_item_index_GET ::'.print_r($_GET,true).'</pre>';
        $action     = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);

        list($controller, $action) = explode('@', $controller);
        echo '<pre>$controller::'.print_r($controller,true).'</pre>';
        echo '<pre>$action::'.print_r($action,true).'</pre>';

        die("-1 XXZ");
        $this->addVariablesToJS();
        $site_name = Settings::getValue('site_name', '');

        return view('frontend.home.index', ['site_name' => $site_name]);
    }


    public function get_cms_item($alias, $locale)
    {
        try {
            $cmsItem = CmsItem::getBodyContentByAlias($alias, ['show_song_name' => 1, 'locale'=> $locale]);
//            echo '<pre>+++$cmsItem::'.print_r($cmsItem,true).'</pre>';
//            die("-1 XXZ");
//            die("-1 XXZ");
            if ($cmsItem == null) {
                return response()->json([
                    'error_code' => 11,
                    'message'    => 'home.cms_item_not_found',
                    'cmsItem' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            $similarCmsItemsList = CmsItem::getCmsItemsList(ListingReturnData::LISTING, ['page_type' => 'P', 'published' => true, 'limit' => 6, 'locale'=> $locale], 'cms.created_at', 'desc');
            foreach ($similarCmsItemsList as $key => $nextCmsItem) {
                if ($nextCmsItem->alias == $alias) {
                    unset($similarCmsItemsList[$key]);
                }
            }

//            echo '<pre>$cmsItem::'.print_r($cmsItem,true).'</pre>';
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $cmsItem = CmsItem::getRowById($cmsItem->id, ['show_image' => 1, 'show_image_info' => 1, 'locale'=> $locale]);
//        echo '<pre>$cmsItem::'.print_r($cmsItem,true).'</pre>';
//        die("-1 XXZ");
        return response()->json(['error_code' => 0, 'message' => '', 'cmsItem' => $cmsItem, 'similarCmsItemsList' => $similarCmsItemsList], HTTP_RESPONSE_OK);
    } // public function get_cms_item()

    public function get_cms_items_by_alias()
    {
        try {
            $aliasArray = $this->getParameter('alias');
            $locale = $this->getParameter('locale');

//            $locale= 'ru'; // DEBUGGING UNCOMMENT
//            $aliasArray        = ['contact_us','tdes_quiz', null, 'NONE', '', 'NO'];
//            echo '<pre>$aliasArray::'.print_r($aliasArray,true).'</pre>';
//            echo '<pre>get_cms_items_by_alias $locale::'.print_r($locale,true).'</pre>';


            $retArray = ['error_code' => 0, 'message' => ''];
            if (is_array($aliasArray)) {

                /*                $collection = collect($aliasArray)->filter(function ($next_alias) {
                //                    echo '<pre>$next_alias::'.print_r($next_alias,true).'</pre>';
                                    if (empty($next_alias)) {
                                        return false;
                                    }
                                    $cmsItemContent = CmsItem::getBodyContentByAlias($next_alias, []);
                                    echo '<pre>$cmsItemContent::'.print_r($cmsItemContent,true).'</pre>';
                                    die("-1 XXZ");
                                    if ($cmsItemContent === null) {
                                        return false;
                                    }

                                    return $cmsItemContent;
                                });*/
//                $collection = $collection->where('ID IS SET', 100);
//                $collection->all();
                // $next_alias

                foreach ($aliasArray as $next_alias) {
                    if (empty($next_alias)) {
                        continue;
                    }
                    $cmsItemContent = CmsItem::getBodyContentByAlias( $next_alias, ['locale'=> $locale ] );
                    if ($cmsItemContent !== null) {
                        $retArray[$next_alias] = $cmsItemContent;
                    }
                }
            }
        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage()], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
//        echo '<pre>++++$collection::'.print_r($collection,true).'</pre>';
//        die("-1 XXZ");
        return response()->json($retArray, HTTP_RESPONSE_OK);
//        return response()->json($collection->toJson(), HTTP_RESPONSE_OK );
    } // public function get_cms_items_by_alias()

    public function get_config_values()
    {
        try {
            $nameArray = $this->getParameter('name');
            $retArray  = ['error_code' => 0, 'message' => ''];
            if (is_array($nameArray)) {
                foreach ($nameArray as $next_name) {
                    if (empty($next_name)) {
                        continue;
                    }
                    $value= config('app.'.$next_name, '');
                    if ($value !== null) {
                        $retArray[$next_name] = $value;
                    }
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function get_config_values()


    public function get_settings_values()
    {
        try {
//            $locale = $this->getParameter('locale');
//            $this->debToFile(' get_settings_values $locale ::' . print_r($locale, true), ' ::');

//                 echo '<pre>$locale::'.print_r($locale,true).'</pre>';
//            $nameArray= ['site_heading'];
//            $locale= 'ru';

            $nameArray = $this->getParameter('name');
            $current_locale = $this->getParameter('current_locale');
            $retArray  = ['error_code' => 0, 'message' => ''];
            if (is_array($nameArray)) {
                foreach ($nameArray as $next_name) {
                    $is_param_defined= false;
                    if (empty($next_name)) {
                        continue;
                    }
                    if ( $next_name== 'is_developer' ) {
                        $retArray['is_developer'] = $this->isDeveloperComp();
                        $is_param_defined= true;
                    }

                    if ( !$is_param_defined ) {
                        $value = Settings::getValue($next_name/* . (! empty($locale) ? '_' . $locale : '' ) */);
                        if ($value !== null) {
                            if (!empty($current_locale)) {
                                $next_name= $this->trimRightSubString( $next_name, '_'.$current_locale );
                            }
                            $retArray[$next_name] = $value;
                        }
                    } // if ( !$is_param_defined ) {
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function get_settings_values()

    public function get_logged_user_info()
    {
        try {
            $loggedUser = '';
            if (Auth::check()) {
                $loggedUser = Auth::user();
            } else {
                return response()->json(['error_code' => 1, 'message' => 'Invalid access', 'loggedUser' => '', 'site_name' => null],
                    HTTP_RESPONSE_OK);
            }
            $site_name = Settings::getValue('site_name', '');

//            $usersGroupsList    = UsersGroups::getUsersGroupsList(ListingReturnData::LISTING, ['user_id' => $loggedUser->id, 'show_group_name' => 1]);


        } catch (Exception $e) {
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'loggedUser' => '', 'site_name' => null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'loggedUser' => $loggedUser, 'site_name' => $site_name],
            HTTP_RESPONSE_OK);
    } //public function get_logged_user_info()

    public function get_artist_images($type = '')
    {
        $filtersArray = [];
        if ($type == 'home') {
            $filtersArray = ['is_home_page' => 1, 'show_image' => 1, 'show_image_info' => 1];
        }
        try {
            $artistImagesList = ArtistImage::getArtistImagesList(ListingReturnData::LISTING, $filtersArray, 'created_at', 'asc');
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        sleep(config('app.sleep_in_seconds', 0));

        return response()->json([
            'error_code'       => 0,
            'message'          => '',
            'artistImagesList' => $artistImagesList,
        ], HTTP_RESPONSE_OK);

    } // public function get_artist_images()


    
    public function login()
    {
        $request   = request();
        $loginData = $request->all();
//        echo '<pre>$loginData::'.print_r($loginData,true).'</pre>';
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

// run the validation rules on the inputs from the form
        $validator = Validator::make($loginData, $rules);

// if the validator fails, redirect back to the form
        if ($validator->fails()) {
            /*     public function handle(backendFailOnLoginEvent $event)
    {                          //  https://modzone.ru/blog/2017/05/15/user-events/
        if ( !empty($event->loginCreditialsArray['email']) and !empty($event->loginCreditialsArray['password'])  ) {
 */
            return response()->json(['error_code' => 1, 'message' => 'common.invalid_access'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        } else {
            // create our user data for the authentication
            $userdata = [
                'email'    => $loginData['email'],
                'password' => $loginData['password']
            ];

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                $loggedUser = Auth::user();
                \Event::fire(new backendSuccessOnLoginEvent($loggedUser));
//                $loggedUserHasPermissions= [];
//                $loggedUserHasPermissionsList = ModelHasPermission::getModelHasPermissionsList(ListingReturnData::LISTING, ['model_type' => 'App\User', 'model_id'=> $loggedUser->id]);
//                foreach( $loggedUserHasPermissionsList as $next_key=>$nextLoggedUserHasPermission ) {
////                    $loggedUserHasPermissions[]= [ $nextLoggedUserHasPermission-> ];
//
//                }

                return response()->json(['error_code' => 0, 'message' => '', 'loggedUser' => $loggedUser/*, 'loggedUserHasPermissions'=> $loggedUserHasPermissions */ ],
                    HTTP_RESPONSE_OK);
            } else {
                //                 $this->setFlashMessage( $this->_('dashboard_messages.tmp_csvp_row_was_set_to_is_applied', 1, ['project_name'=>$tmpCsvp->project_name ] ), 'success' );

                \Event::fire(new backendFailOnLoginEvent( [ 'email'=> $loginData['email'], 'password'=> $loginData['password'] ] ) );
                return response()->json(['error_code' => 1, 'message' => 'common.invalid_access'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

        }

    } // public function login()


    public function logout()
    {
        Auth::logout();
        return response()->json([
            'error_code'                          => 0,
            'message'                             => '',
        ], HTTP_RESPONSE_OK);
    }

    public function user_register(UserRegisterRequest $request)
    {
//        $request            = request();
        $registeredUserData = $request->all();
        $group_name= 'User';
        $userGroup= Group::getSimilarGroupByName($group_name);
        try {
            $avatar_image_url = ! empty($registeredUserData['avatar_image_url']) ? $registeredUserData['avatar_image_url'] : '';
            if ( ! empty($avatar_image_url)) {
                $file_data     = $request->input('avatar_image_url');
                $username_slug = Str::slug($registeredUserData['username']);
            }
            $newUser                 = new User();
            $newUser->username       = $registeredUserData['username'];
            $newUser->email          = $registeredUserData['email'];
            $newUser->password       = Hash::make($registeredUserData['password']);
            $newUser->first_name     = $registeredUserData['first_name'];
            $newUser->last_name      = $registeredUserData['last_name'];
            $newUser->phone          = $registeredUserData['phone'];
            $newUser->website        = $registeredUserData['website'];
            $newUser->status         = $registeredUserData['status'];
            $newUser->provider_name  = 'EMAIL';
            if ( ! empty($username_slug)) {
                $user_filename = $username_slug . '.png';
                $user_filename =  MyAppModel::checkValidImgName($user_filename,100,true);
                $newUser->avatar = $user_filename;
            }
            DB::beginTransaction();
            $newUser->save();
            $new_user_id              = $newUser->id;
            $newUsersGroups           = new UsersGroups();
            $newUsersGroups->user_id  = $new_user_id;
            $newUsersGroups->group_id = $userGroup->id;
            $newUsersGroups->save();
            if ( ! empty($avatar_image_url)) {
                $file_name = User::getUserAvatarPath($new_user_id, $user_filename );
                @list($type, $file_data) = explode(';', $file_data);
                @list(, $file_data) = explode(',', $file_data);
                if ($file_data != "") { // storing image in storage/app/public Folder
                    \Storage::disk('public')->put($file_name, base64_decode($file_data));
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'id'         => $new_user_id,
        ], HTTP_RESPONSE_OK_RESOURCE_CREATED);

    } // public function user_register()


    // Route::post('switch_locale/{lang}/{lang_label}', 'HomeController@switch_locale')->name('switch_locale');
    public function switch_locale()
    {
        $to_locale        = $this->getParameter('to_locale');
        \App::setLocale($to_locale);
        $current_locale= \Lang::getLocale();
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'locale'=> $to_locale ], HTTP_RESPONSE_OK );
    } // public function switch_locale()

    public function get_dictionaries_by_name($dictionary_name)
    {
        try {
//            $artistHasConcertsSelectionList             = Artist::getArtistHasConcertsValueArray(true);
//            $artistIsActiveSelectionList                = Artist::getArtistIsActiveValueArray(true);
            $dataList = [];
            $frontend_locale       = '';
//            $current_locale       = '';
            $locale = $this->getParameter('locale');
            $langsInSystemList     = [];

            if ($dictionary_name == 'countries') {
                $dataList = $this->getCountries(true);
            }
            if ($dictionary_name == 'locals') {
                $current_locale        = $locale;
                $frontend_locale       = \Config::get('app.frontend_locale');
                $langsInSystem         = \Config::get('app.langsInSystem');
                foreach( $langsInSystem as $next_key=>$next_value ) {
                    $langsInSystemList[]= [ 'key'=> $next_key, 'label'=>$next_value ];
                }

            }
        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'          => 0,
            'message'             => '',
            'dataList'            => $dataList,
            'locale'              => $locale,
            'frontend_locale'     => $frontend_locale,
            'langsInSystemList'   => $langsInSystemList,
        ], HTTP_RESPONSE_OK);
    } // public function dictionaries($artist_id)


    public function get_future_tours_list()
    {
        try {
            $newArtistsList                 = Artist::getArtistsList(ListingReturnData::LISTING, ['is_active' => 'N']);
            $artistHasConcertsSelectionList = Artist::getArtistHasConcertsValueArray(true);
            $artistIsActiveSelectionList    = Artist::getArtistIsActiveValueArray(true);

        } catch (Exception $e) {
            return response()->json([
                'error_code' => 1,
                'message'    => $e->getMessage(),
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'error_code'                     => 0,
            'message'                        => '',
            'newArtistsList'                 => $newArtistsList,
            'artistHasConcertsSelectionList' => $artistHasConcertsSelectionList,
            'artistIsActiveSelectionList'    => $artistIsActiveSelectionList,
        ], HTTP_RESPONSE_OK);

    } // public function get_future_tours_list()



    public function future_concerts_dictionaries()
    {
        $tmpArtistsList = Artist::getArtistsList(ListingReturnData::LISTING, ['is_active' => 'A', 'has_concerts' => true, 'show_future_concerts_count' => 1]);
        $artistsList = collect($tmpArtistsList)->map(function ($nextArtist) {
            if ( $nextArtist->related_future_concerts_count > 0 ) {
                return [
                    'key'   => $nextArtist->id,
                    'label' => $nextArtist->name . ' with ' . $nextArtist->related_future_concerts_count . ' future concerts'
                ];
            }
        });

        return response()->json([
            'error_code'                    => 0,
            'message'                       => '',
            'artistsList'                   => array_values( $artistsList->filter()->toArray() ),
        ], HTTP_RESPONSE_OK);

    } // public function future_concerts_dictionaries()

    public function get_future_concerts_list()
    {

        $filter_date_from    = $this->getParameter('filter_date_from');
        $filter_date_till    = $this->getParameter('filter_date_till');
        $selectedArtistsList = $this->getParameter('selectedArtistsList', [], ['skip_urldecode' => 1]);

        $only_future          = (empty($filter_date_from) and empty($filter_date_till)) ? 1 : '';
        $selectedArtistsArray = [];
        foreach ($selectedArtistsList as $nextSelectedArtist) {
            $selectedArtistsArray[] = $nextSelectedArtist['key'];
        }

        $futureArtistConcertsList = ArtistConcert::getArtistConcertsList(ListingReturnData::LISTING, [
            'event_date_from'   => $filter_date_from,
            'event_date_till'   => $filter_date_till,
            'artistsArray'      => $selectedArtistsArray,
            'only_future'       => $only_future,
            'artist_is_active'  => 'A',
            'artist_main_image' => true
        ]);

        $markersList = collect($futureArtistConcertsList)->map(function ($nextFutureArtistConcert) {
            $base_url                = with(new ArtistImage)->getBaseUrl();
            $artist_image_image_name = ArtistImage::getArtistImagePath($nextFutureArtistConcert->artist_id, $nextFutureArtistConcert->artist_image_name, false);
            $artist_image_url        = $base_url . Storage::disk('local')->url($artist_image_image_name);

            return [
                'id'                => $nextFutureArtistConcert->id,
                'description'       => $nextFutureArtistConcert->description,
                'artist_image_name' => $nextFutureArtistConcert->artist_image_name,
                'artist_image_url'  => $artist_image_url,
                'artist_id'         => $nextFutureArtistConcert->artist_id,
                'position'          => ['lat' => (float)$nextFutureArtistConcert->lat, 'lng' => (float)$nextFutureArtistConcert->lng],
                'name'              => $nextFutureArtistConcert->artist_name . ' at ' .
                                       $this->getFormattedDateTime($nextFutureArtistConcert->event_date) . ' : ' . $nextFutureArtistConcert->place_name
            ];
        });

        return response()->json([
            'error_code' => 0,
            'message'    => '',
            'markers'    => $markersList,
        ], HTTP_RESPONSE_OK);

    } // public function get_future_tours_list()

}

