<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\funcsTrait;
use function PHPSTORM_META\type;

class WorkTextString
{

    use funcsTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $inputDataArray = $request->all();
        foreach( $inputDataArray as $next_key=>$next_value ) {
            if ( !empty($next_value) and is_string($next_value) ) {
                $request[$next_key] = $this->workTextString($next_value);
            }
        }
        return $next($request);
    }



}
