<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

use App\User;
use App\Artist;


class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $valid_nice_name_format= config('app.valid_nice_name_format');
        $additional_check_nice_name_validation_rule= 'regex:'.$valid_nice_name_format;
        $validationRulesArray = [
            'username'        => 'required|max:255',
            'email'           => 'required|email|max:255',
            'password'        => 'required|min:6',
            'status'          => 'required|in:N,A,I',
            'first_name'      => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'last_name'       => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'phone'           => 'required|max:50',
            'website'         => 'required|max:50',
        ];
        return $validationRulesArray;

    }
}
