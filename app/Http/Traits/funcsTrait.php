<?php

namespace App\Http\Traits;

use Barryvdh\Debugbar\Facade as Debugbar;
use Excel;
use File;
use Carbon\Carbon;
use Config;
use URL;
use App\Permission;
use Intervention\Image\Facades\Image as Image;

use App\MyAppModel;

define("HTTP_RESPONSE_OK", 200); // https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
define("HTTP_RESPONSE_OK_RESOURCE_CREATED", 201);
define("HTTP_RESPONSE_OK_RESOURCE_DELETED", 204);
define("HTTP_RESPONSE_OK_RESOURCE_UPDATED", 205);

define("HTTP_RESPONSE_NOT_FOUND", 404);
define("HTTP_RESPONSE_BAD_REQUEST", 400);
define("HTTP_RESPONSE_INTERNAL_SERVER_ERROR", 500);
define("HTTP_RESPONSE_NOT_IMPLEMENTED", 501);


trait funcsTrait
{
    private $m_concat_str_max_length = 30;
    private $m_concat_str_add_chars = '...';

    public function isDeveloperComp($check_debug= false) {
        if ( $check_debug ) {
            $debug = config('app.debug');
            if ( ! $debug) {
                return false;
            }
        }

        $url = config('app.url');
        $pos = strpos($url, 'local-artists-rating.com');  //http://local-artists-rating.com
        if ( !($pos === false)) {
            return true;
        }
        return false;
    }

    
    public function getBackendLangs(bool $key_value= true) : array
    {
        $langsInSystem              = \Config::get('app.langsInSystem');
        $langsInSystemList = [];
        foreach ($langsInSystem as $next_key => $next_value) {
            if ( $key_value ) {
                $langsInSystemList[] = ['key' => $next_key, 'label' => $next_value];
            } else {
                $langsInSystemList[$next_key] =  $next_value;
            }
        }
        return $langsInSystemList;
    }

    public function getBackendLocaleLabel(): string
    {
        $backend_locale             = \Config::get('app.backend_locale');
        $langsInSystem              = \Config::get('app.langsInSystem');
        $langsInSystemList = [];
        foreach ($langsInSystem as $next_key => $next_value) {
            $langsInSystemList[] = ['key' => $next_key, 'label' => $next_value];
            if ($backend_locale == $next_key) {
                return $next_value;
            }
        }
        return $backend_locale;
    }

    public function splitKeyValuesTokens(string $str, bool $return_key_values = false): array
    {
//        $str= "1:' #f8f876  ', 2 :      '#c5c5c5', 3:'#a5a5a5', 4:'#e097f8'  , 5:'#f87063'    , 6:'  #ff280c' ";
        $pattern = "~
        (?<key_name>[0-9a-z_]+)[\s]*:[\s]*'[\s]*(?<key_value>[0-9a-f#]*)[\s]*'
        ~isUx";
        $ret     = preg_match_all($pattern, $str, $tokensArray, PREG_SET_ORDER);
//        echo '<pre>$tokensArray::'.print_r($tokensArray,true).'</pre>';
        if ( ! $ret) {
            return [];
        }

        if ( ! $return_key_values) {
            return $tokensArray;
        }

        $retArray = [];
        foreach ($tokensArray as $next_key => $nextToken) {
            $retArray[$nextToken['key_name']] = $nextToken['key_value'];
        }

        return $retArray;
    }

    public function substituteNullValues($modelObject, array $dataArray)
    {
//        echo '<pre>$dataArray::'.print_r($dataArray,true).'</pre>';
        foreach ($dataArray as $next_field_name) {
            $val = $modelObject->{$next_field_name};
            if (is_null($val)) {
                $modelObject->{$next_field_name} = '';
            }
        }
//        echo '<pre>$modelObject::'.print_r($modelObject,true).'</pre>';
//        die("-1 XXZ substituteNullValues");
        return $modelObject;
    }


    public function writeArrayToCsvFile(array $dataArray, string $filename, array $directoriesArray): int
    {
        self::createDir($directoriesArray);
        $path = $directoriesArray[count($directoriesArray) - 1];
        Excel::create($filename, function ($excel) use ($dataArray) {
            $excel->sheet('file', function ($sheet) use ($dataArray) {
                $sheet->fromArray($dataArray);
            });
        })->store('csv', $path);//->export('csv');
        return 1;
    }


    public static function getKeyOfName(string $name): string
    {
        return strtolower(str_replace(' ', '_', $name));
    }

//    public static function setFieldErrorTag(array $errorsList, string $fieldname, string $attr) : string
//    {
//        if (empty($attr)) $attr = ' class=" has-danger " ';
//        if ( empty($errorsList[$fieldname]) ) return '';
//        return $attr;
//    }


    public static function showListHeaderItem(string $url, string $filters_str, string $field_title, string $fieldname, string $order_by, string $order_direction): string
    {
        if (empty($field_title)) {
            $field_title = ucwords($fieldname);
        }
//            $field_title = str_replace(' ', '&nbsp;', $field_title);
        $filters_str .= '&order_by=' . urlencode($fieldname);
        $filters_str .= '&order_direction=' . self::getOrderDirection($fieldname, $order_by, $order_direction);
//            echo '<pre>$filters_str::'.print_r($filters_str,true).'</pre>';
//            echo '<pre>$fieldname::'.print_r($fieldname,true).'</pre>';
//            die("-1 XXZ");
//            $filters_str .= '&fieldname=' . urlencode($fieldname);
        $img_html = self::getListOrderDirectionImage($fieldname, $order_by, $order_direction);
        if ($fieldname == "tRRitle") {
            echo '<pre>$field_title::' . print_r(htmlspecialchars($field_title), true) . '</pre>';
            echo '<pre>$img_html::' . print_r(htmlspecialchars($img_html), true) . '</pre>';
            echo '<pre>$filters_str::' . print_r(htmlspecialchars($filters_str), true) . '</pre>';
        }
        $res_url = '<a href="' . $url . $filters_str . '"  style="white-space:nowrap;" ><span >' . $field_title . $img_html . '</span></a>';
        if ($fieldname == "titleWWW") {
            echo '<pre>$res_url::' . print_r(htmlspecialchars($res_url), true) . '</pre>';
        }

//        die("-1 XXZ===");
        return $res_url;
    }

    public static function getOrderDirection($fieldname, $order_by, $order_direction)
    {
//            echo '<pre>$fieldname::'.print_r($fieldname,true).'</pre>';
//            echo '<pre>$order_by::'.print_r($order_by,true).'</pre>';
//            echo '<pre>$order_direction::'.print_r($order_direction,true).'</pre>';
        return (($order_by == $fieldname and $order_direction == 'asc') ? "desc" : "asc");
    }

    public static function getListOrderDirectionImage($fieldname, $order_by, $order_direction)
    {
        if ($order_by == $fieldname and strtolower($order_direction) == 'asc') {
            return '&nbsp;<i class="material-icons small" style="padding-top: 20px" >thumb_down</i>'; /* fa-2x */
        }
        if ($order_by == $fieldname and strtolower($order_direction) == 'desc') {
            return '&nbsp;<i class="material-icons small" style="top: 20px" >thumb_up</i>';
        }

        return '';//'<i class="fa fa-bluetooth" style="display: inline-block;" >';
    }

    public static function concatStr(string $str, int $max_length = 0, string $add_str = ' ...', $show_help = false, $strip_tags = true, $additive_code = ''): string
    {
        if ($strip_tags) {
            $str = strip_tags($str);
        }
        $ret_html = self::limitChars($str, (! empty($max_length) ? $max_length : self::$m_concat_str_max_length), $add_str);
        if ($show_help and strlen($str) > $max_length) {
//			$str = appFuncs::nl2br2($str);
//            $str = addslashes(appFuncs::nl2br2($str));
            $ret_html .= '<i class=" a_link fa fa-object-group" style="font-size:larger;" hidden ' . $additive_code . ' ></i>';
//            $ret_html .= '<i class=" a_link fa fa-life-bouy" style="font-size:larger;" onclick="javascript:showDownloadableDescription(9);"></i>';
//            $ret_html .= '&nbsp;<button type="button" class="btn"  style="padding:1px; border:1px dotted grey;" data-toggle="tooltip" data-placement="bottom" title="'.$str.'">+</button>';
//            echo '<pre>=========$ret_html::'.print_r($ret_html,true).'</pre>';
        }
//        echo '<pre>$ret_html::'.print_r($ret_html,true).'</pre>';
//        die("-1 XXZ");
        return $ret_html;
    }


    public static function limitChars($str, $limit = 100, $end_char = null, $preserve_words = false)
    {
        $end_char = ($end_char === null) ? '&#8230;' : $end_char;

        $limit = (int)$limit;

        if (trim($str) === '' OR strlen($str) <= $limit) {
            return $str;
        }

        if ($limit <= 0) {
            return $end_char;
        }

        if ($preserve_words == false) {
            return rtrim(substr($str, 0, $limit)) . $end_char;
        }
        // TO FIX AND DELETE SPACE BELOW
        preg_match('/^.{' . ($limit - 1) . '}\S* /us', $str, $matches);

        return rtrim($matches[0]) . (strlen($matches[0]) == strlen($str) ? '' : $end_char);
    }

    /**
     * Limits a phrase to a given number of words.
     *
     * @param   string   phrase to limit words of
     * @param   integer  number of words to limit to
     * @param   string   end character or entity
     *
     * @return  string
     */
    public static function limitWords($str, $limit = 100, $end_char = null)
    {
        $limit    = (int)$limit;
        $end_char = ($end_char === null) ? '&#8230;' : $end_char;

        if (trim($str) === '') {
            return $str;
        }

        if ($limit <= 0) {
            return $end_char;
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

        // Only attach the end character if the matched string is shorter
        // than the starting string.
        return rtrim($matches[0]) . (strlen($matches[0]) === strlen($str) ? '' : $end_char);
    }


    public static function setArrayHeader(array $headersArray, array $dataArray): array
    {
//	    echo '<pre>setArrayHeader  $headersArray::'.print_r($headersArray,true).'</pre>';
        if (empty($headersArray) or ! is_array(($headersArray))) {
            return $dataArray;
        }
        $retArray = [];
        foreach ($headersArray as $next_header_key => $next_header_text) {
//		    echo '<pre>$next_header_text::'.print_r($next_header_text,true).'</pre>';
            $retArray[] = ["key" => $next_header_key, "label" => $next_header_text];
        }
//        echo '<pre>++-$retArray::'.print_r($retArray,true).'</pre>';
        if (is_array($dataArray)) {
            foreach ($dataArray as $next_data_key => $nextDataSubarray) {
//                echo '<pre>$next_data_key::'.print_r($next_data_key,true).'</pre>';
//                echo '<pre>$nextDataSubarray::'.print_r($nextDataSubarray,true).'</pre>';
//                die("-1 XXZ");
                $retArray[] = $nextDataSubarray;
            }
        }

//        echo '<pre>--------$retArray::'.print_r($retArray,true).'</pre>';
        return $retArray;
        /* "0": {
"key": 3,
"label": "Joomla Development"
}, */
    }

    public static function showYesNoLabel($value): string // showYesNoLabel($value)
    {
        if ($value) {
            return MyAppModel::$pgYesNoValueArray[1];
        }
        if ( ! $value) {
            return MyAppModel::$pgYesNoValueArray[0];
        }

        return '';
    }

    public static function getSortDirectionValueArray($db = 'pgsql')
    {
        if ($db == 'pgsql') {
            return MyAppModel::$pgSortDirectionValueArray;
        }
        if ($db == 'mysql') {
            return MyAppModel::$mysqlSortDirectionValueArray;
        }
    }


//    public static function getYesNoValueArray($db='pgsql', $return_key_value= false)
//    {
//        if ( $db =='pgsql' ) {
//            if ($return_key_value) {
//                $retArray = [];
//                foreach( MyAppModel::$pgYesNoValueArray as $next_key=>$next_value ) {
//                    $retArray[]= [ 'key'=>$next_key, 'label'=>$next_value ];
//                }
//                return $retArray;
//            }
//            return MyAppModel::$pgYesNoValueArray;
//        }
//        if ( $db =='mysql' ) {
//            if ($return_key_value) {
//                $retArray = [];
//                foreach( MyAppModel::$mysqlYesNoValueArray as $next_key=>$next_value ) {
//                    $retArray[]= [ 'key'=>$next_key, 'label'=>$next_value ];
//                }
//                return $retArray;
//            }
//            return MyAppModel::$mysqlYesNoValueArray;
//        }
//    }

    public function sendEmail($to, $email_title, $email_content, $save_email_history = true, $cms_item_template_id = null)
    {   // https://laravel.com/docs/5.5/mail  !!

        return true; // Will be implemented later
        $ret = Mail::raw($email_content, function ($message) use ($to, $email_title) {
//            echo '<pre>$to::'.print_r($to,true).'</pre>';
            $message->from('us@example.com', 'Laravel');
            $message->to($to)->cc('nilovsergey@yahoo.com');
            $message->subject($email_title);
        });

//        echo '<pre>$ret::'.print_r($ret,true).'</pre>';
        return true;
        Mail::to($request->user())->send(new OrderShipped($order));


        self::debToFile(' sendEmail $to::' . print_r($to, true) . ' $subject::' . print_r($subject, true) . ' $message::' . print_r($message, true), ' sendEmail::');
        //appUtils::debToFile( ' sendEmail $to::' . print_r( $to, true ) .' $subject::' . print_r( $subject, true ) .' $message::' . print_r( $message, true ), false );
        $ci          = &get_instance();
        $configArray = $ci->config->config;
        //$ci->load->model('msent_emails_history', '', true);
        $ci->load->library('email');
        $ci->email->from($configArray['NoAnswer_Email'], 'No Reply');
        $ci->email->to($to);
//        $ci->email->cc( array('nilov@softreactor.com') );
//        $ci->email->bcc( array('nilov@softreactor.com') );
        $ci->email->subject($subject);
        $ci->email->message(strip_tags($message));

        /*        if ($save_email_history) {
                    $LoggedUserData = [];//AppAuth::getLoggedUserData();
                    //echo '<pre> $LoggedUserData ::'.print_r( $LoggedUserData, true ).'</pre><br>';
                    if (!empty($LoggedUserData['logged_user_id'])) {
                        $dataArray = [ 'author_id' => $LoggedUserData['logged_user_id'], 'to_email' => $to, 'message' => $message, 'subject' => $subject ];
                    } else {
                        $dataArray = [ 'to_email' => $to, 'message' => $message, 'subject' => $subject ];
                    }
                    // echo '<pre> $dataArray ::'.print_r( $dataArray, true ).'</pre><br>';
                    if (!empty($cms_item_template_id)) {
                        $dataArray['cms_item_id'] = $cms_item_template_id;
                    }
                    //$ci->msent_emails_history->UpdateSent_Emails_History('', $dataArray);
                }*/

        return $ci->email->send();
    }


    public function getBaseUrl()
    {
        $base_url = \Config::get('app.base_url');

        return $base_url;

        return URL::to($url);
        /*     'base_url' => $base_url,
    'url' => env('APP_URL', $base_url),
 */
    }

    public function getPublicPath()
    {
        return public_path();
    }


    function getFilenameExtension($file)
    {
        return File::extension($file);
    }


    public function info($info, $descr_text = '')
    { // TODO
        if (class_exists("Debugbar")) {
            if ( ! empty($descr_text)) {
                Debugbar::info($descr_text . print_r($info, true));
            } else {
                Debugbar::info($info);
            }
        }
    }


    protected function pp($html)
    {
        $level = 0;

        return preg_replace_callback('~<(/?)([^>]+)>~', function ($m) use (&$level) {
            $m[1] and --$level;
            $indent = str_repeat('  ', $level);
            $res    = "\n$indent$m[0]\n$indent";
            $m[1] or ++$level;

            return $res;
        }, $html);
    }

    public function loremIpsumText($number = 1, $p_wrapping = false, $initial_text = '')
    {
        if ($number <= 1 or ! is_numeric($number)) {
            return ($p_wrapping ? '<p>' : '') . $initial_text . 'Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.';
        }

        $ret_str = '';
        if (is_numeric($number) and $number > 1) {
            for ($i = 0; $i < $number; $i++) {
                $ret_str .= ($p_wrapping ? '<p>' : '') . ($i == 0 ? $initial_text : '') . 'Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.' . ($p_wrapping ? '</p>' : '');
            }
        }

        return $ret_str;
    }

    /*
    public function showFormattedSql(string $sql, bool $is_break_line = true) : string
    {
        $BreakLine = '';
        if ($is_break_line) {
            $BreakLine = '<br>';
        }
        $sql = ' ' . $sql . ' ';
        $sql = preg_replace("/insert into/", "&nbsp;&nbsp;<B>INSERT INTO</B>", $sql);
        $sql = preg_replace("/insert/", "&nbsp;<B>INSERT</B>", $sql);
        $sql = preg_replace("/delete/", "&nbsp;<B>DELETE</B>", $sql);
        $sql = preg_replace("/values/", $BreakLine . "&nbsp;&nbsp;<B>VALUES</B>", $sql);
        $sql = preg_replace("/update/", "&nbsp;<B>UPDATE</B>", $sql);
        $sql = preg_replace("/straight_join/", $BreakLine . "<B>&nbsp;&nbsp;STRAIGHT_JOIN</B>", $sql);
        $sql = preg_replace("/left join/", $BreakLine . "&nbsp;&nbsp;<B>LEFT JOIN</B>", $sql);
//    $sql = preg_replace("join", $BreakLine."&nbsp;&nbsp;<B>JOIN</B>", $sql)  ;
        $sql = preg_replace("/select/", "&nbsp;<B>SELECT</B>", $sql);
        $sql = preg_replace("/from/", $BreakLine . "&nbsp;&nbsp;<B>FROM</B>", $sql);
        $sql = preg_replace("/where/", $BreakLine . "&nbsp;&nbsp;<B>WHERE</B>", $sql);
        $sql = preg_replace("/group by/", $BreakLine . "&nbsp;&nbsp;<B>GROUP BY</B>", $sql);
        $sql = preg_replace("/having/", $BreakLine . "&nbsp;&nbsp;<B>HAVING</B>", $sql);
        $sql = preg_replace("/order by/", $BreakLine . "&nbsp;&nbsp;<B>ORDER BY</B>", $sql);
        return $sql;
    } */
    function formatSql($sql, $is_break_line = true, $is_include_html = true)
    {
        $break_line = '';
        $space_char = '  ';
        if ($is_break_line) {
            if ($is_include_html) {
                $break_line = '<br>';
            } else {
                $break_line = PHP_EOL;
            }
        }
        $bold_start = '';
        $bold_end   = '';
        if ($is_include_html) {
            $bold_start = '<B>';
            $bold_end   = '</B>';
        }
        $sql        = ' ' . $sql . ' ';
        $left_cond  = '~\b(?<![%\'])';
        $right_cond = '(?![%\'])\b~i';
        $sql        = preg_replace($left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char . $bold_start . "INSERT INTO" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "and" . $right_cond, $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "or" . $right_cond, $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "as" . $right_cond, $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
        $sql        = preg_replace($left_cond . "exists" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);

        return $sql;
    }


    public function debToFile($contents, string $descr_text = '', bool $is_sql = false, string $file_name = '')
    {
//        echo '<pre>'.$descr_text.'  $contents::'.print_r($contents,true).'</pre>';
//        return;
        try {
            if (empty($file_name)) {
                $file_name = storage_path() . '/logs/logging_deb.txt';
            }
//                $file_name = '/_wwwroot/lar/lprods/storage/logs/logging_deb.txt';
//                $fd = fopen($file_name, ($is_clear_text ? "w+" : "a+"));
            $fd = fopen($file_name, "a+");
            if (is_array($contents) == 'array') {
                $contents = print_r($contents, true);
            }
            if ($is_sql) {
                fwrite($fd, '<pre>' . $descr_text . '::' . self::showFormattedSql($contents, false) . '<pre>' . chr(13));
            } else {
                if (is_scalar($contents)) {
                    fwrite($fd, '<pre>' . $descr_text . '::' . $contents . '<pre>' . chr(13));
                } else {
                    fwrite($fd, '<pre>' . $descr_text . '::' . print_r($contents, true) . '<pre>' . chr(13));
                }
            }
            //                     echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . AppUtil::showFormattedSql($Var) . "</font></b></h5>";

            fclose($fd);

            return true;
        } catch (Exception $lException) {
            return false;
        }
    }

    /**
     * Prepare Password with given length($length)
     *
     */
    public function generatePassword($length = 8)
    {
        $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz!~@#$%^&*()_+|";
        $Res      = '';
        for ($I = 0; $I < $length; $I++) {
            $Index = random_int(0, strlen($alphabet) - 1);
            $Res   .= substr($alphabet, $Index, 1);
        }

        return $Res;
    }

    public function getFileSizeAsString(string $file_size): string
    {
        if ((int)$file_size < 1024) {
            return $file_size . 'b';
        }
        if ((int)$file_size < 1024 * 1024) {
            return floor($file_size / 1024) . 'kb';
        }

        return floor($file_size / (1024 * 1024)) . 'mb';
    }


    public function createDir(array $directoriesList = [], $mode = 0777)
    {
        foreach ($directoriesList as $dir) {
            if ( ! file_exists($dir)) {
                mkdir($dir, $mode);
            }
        }
    }

    public function isPositiveNumeric(int $str): bool
    {
        if (empty($str)) {
            return false;
        }

        return (is_numeric($str) && $str > 0 && $str == round($str));
    }


    public function replaceSpaces($S)
    {
        $Pattern = '/([\s])/xsi';
        $S       = preg_replace($Pattern, '&nbsp;', $S);

        return $S;
    }


    public function pregSplit(string $splitter, string $string_items, bool $skip_empty = true, $to_lower = false): array
    {
        $retArray = [];
        $a        = preg_split($splitter, $string_items);
//        echo '<pre>------$a::'.print_r($a,true).'</pre>';
        foreach ($a as $next_key => $next_value) {
            if ($skip_empty and empty($next_value)) {
                continue;
            }
            $retArray[] = ($to_lower ? strtolower(trim($next_value)) : trim($next_value));
        }

        return $retArray;
    }


    public function fromPgToTimestamp($pg_datetime)
    {
        try {
            if (empty($pg_datetime)) {
                return '';
            }
            $datetime_carbon_format = config('app.datetime_carbon_format', 'Y-m-d H:i:s');
            $a                      = Carbon::createFromFormat($datetime_carbon_format, $pg_datetime);
            $ts                     = $a->timestamp;

            return (int)$ts;
        } catch (exception $e) {
            return '';
        }
    }

    public function getBeginOfDay($timestamp, $output_format = '', $with_zero = false)
    {
        $begin_of_day = strtotime("midnight", $timestamp);
        if ($output_format == 'mysql') {
            return Carbon::createFromTimestampMs($begin_of_day, Config::get('app.timezone'))->format('Y-m-d') . ($with_zero ? " 00:00:00" : "");
        }
        if ($output_format == 'timestamp' or empty($output_format)) {
            return $begin_of_day;
        }
    } // public function getBeginOfDay($timestamp, $output_format= '')

    public function getEndOfDay($timestamp, $output_format = '', $with_zero = false)
    {
        $begin_of_day = strtotime("midnight", $timestamp);
        $end_of_day   = strtotime("tomorrow", $begin_of_day) - 1;
        if ($output_format == 'mysql') {
            return Carbon::createFromTimestampMs($end_of_day, Config::get('app.timezone'))->format('Y-m-d') . ($with_zero ? " 23:59:59" : "");
        }
        if ($output_format == 'timestamp' or empty($output_format)) {
            return $end_of_day;
        }
    } // public function getEndOfDay($timestamp, $output_format= '')

    public function getDateTimesCompare($dt_1): string
    {
//        echo '<pre>$dt_1::'.print_r($dt_1,true).'</pre>';
        $datetime_carbon_format = config('app.datetime_carbon_format', 'Y-m-d H:i:s');
        if (empty($dt_1)) {
            return '';
        }
        if (self::isValidTimeStamp($dt_1)) {
            $a_1 = Carbon::createFromTimestampMs($dt_1, Config::get('app.timezone'));
        } else {
            $a_1 = Carbon::createFromFormat($datetime_carbon_format, $dt_1);
        }
        if ($a_1->isToday()) {
            return 'is_today';
        }
        if ($a_1->isTomorrow()) {
            return 'is_tomorrow';
        }
        if ($a_1->isFuture()) {
            return 'is_future';
        }
        if ($a_1->isPast()) {
            return 'is_past';
        }

        return '';
    }

    public function getDayNumberByTimestamp(int $datetime): int
    {
        $day_0 = mktime(0, 0, 0, date('n'), date('j'));
        $day_1 = mktime(0, 0, 0, date('n'), date('j') + 1);
        $day_2 = mktime(0, 0, 0, date('n'), date('j') + 2);
        $day_3 = mktime(0, 0, 0, date('n'), date('j') + 3);
        $day_4 = mktime(0, 0, 0, date('n'), date('j') + 4);
        $day_5 = mktime(0, 0, 0, date('n'), date('j') + 5);
        $day_6 = mktime(0, 0, 0, date('n'), date('j') + 6);
        $day_7 = mktime(0, 0, 0, date('n'), date('j') + 7);
        if ($datetime >= $day_0 and $datetime < $day_1) {
            return 0;
        }
        if ($datetime >= $day_1 and $datetime < $day_2) {
            return 1;
        }
        if ($datetime >= $day_2 and $datetime < $day_3) {
            return 2;
        }
        if ($datetime >= $day_3 and $datetime < $day_4) {
            return 3;
        }
        if ($datetime >= $day_4 and $datetime < $day_5) {
            return 4;
        }
        if ($datetime >= $day_5 and $datetime < $day_6) {
            return 5;
        }
        if ($datetime >= $day_6 and $datetime < $day_7) {
            return 6;
        }

        return -1;
    }


    public function addMinutesToFormattedDateTime($datetime, int $minutes, string $output_format = ''): string
    {
        if (self::isValidTimeStamp($datetime)) {
            return Carbon::createFromTimestampMs($datetime, Config::get('app.timezone'))->format($output_format);
        }

        try {
            if (empty($datetime)) {
                return '';
            }
            $datetime_carbon_format = config('app.datetime_carbon_format', 'Y-m-d H:i:s');
            $a                      = Carbon::createFromFormat($datetime_carbon_format, $datetime);

            return $a->addMinutes($minutes);
        } catch (exception $e) {
            return '';
        }
    }

    public function getFormattedDateTime($datetime, $output_format = ''): string
    {
//        echo '<pre>getFormattedDateTime $datetime::'.print_r($datetime,true).'</pre>';
//        with(new self)->info( $datetime,'$datetime::' );
        if (empty($datetime)) {
            return '';
        }
//        die("-1 XXZ==");
        if (self::isValidTimeStamp($datetime)) {
//            echo '<pre>$output_format::'.print_r($output_format,true).'</pre>';
            if ( ! empty($output_format)) {
                return Carbon::createFromTimestampMs($datetime, Config::get('app.timezone'))->format($output_format);
            }
            $datetime_carbon_format = config('app.datetime_carbon_format');

            return Carbon::createFromTimestampMs($datetime, Config::get('app.timezone'))->format($datetime_carbon_format);
        }
        try {
            $datetime_carbon_format = config('app.datetime_carbon_format', 'Y-m-d H:i:s');
            if (strpos($datetime, '.') !== false) {
                $datetime_carbon_format = config('app.datetime_carbon_format_miliseconds', 'Y-m-d H:i:s.u');
            }
            $a = Carbon::createFromFormat($datetime_carbon_format, $datetime);
            if ( ! empty($output_format)) {
                return $a->format($output_format);
                /*         if (empty($date)) return '';
                        if (strpos($date, '.') === false) {
                            $postgresql_format = env('POSTGRESQL_FORMAT', 'Y-m-d H:i:s');
                        } else {
                            $postgresql_format = env('POSTGRESQL_FORMAT_MILISECONDS', 'Y-m-d H:i:s.u');
                        }
                        return Carbon::createFromFormat($postgresql_format, $date)->format(self::getDateTimeFormat("astext"));
                 */
            }
//            $a = Carbon::createFromFormat($datetime_carbon_format, $datetime);
//            echo '<pre>$::' . print_r($a, true) . '</pre>';
//            $b = $a->format(self::getDateTimeFormat("astext"));
//        echo '<pre>$::'.print_r($b,true).'</pre>';
//        die("-1 XXZ");
//        return $b;
            return $a->format(self::getDateTimeFormat("astext"));
        } catch (exception $e) {
            return '';
        }
    }

    // https://gist.github.com/mattytemple/3804571  => similar https://www.sitepoint.com/community/t/show-nice-time-formats-now-15-minutes-2-hours-ago-etc/266991
    public function time2str($ts)
    {
        if (empty($ts)) {
            return $ts;
        }
//        echo '<pre>time2str $ts::'.print_r($ts,true).'</pre>';
        $datetime_carbon_format = config('app.datetime_carbon_format', 'Y-m-d H:i:s');

        // dt->timestamp
//        echo '<pre>$postgresql_format::'.print_r($postgresql_format,true).'</pre>';
//        die("-1 XXZ");
        $a = Carbon::createFromFormat($datetime_carbon_format, $ts);
//        echo '<pre>=====::'.print_r($a,true).'</pre>';
        $ts = $a->timestamp;
        if ( ! ctype_digit($ts)) {
            echo '<pre>-1</pre>';
            $ts = strtotime($ts);
        }

//        echo '<pre>++++= $ts::'.print_r($ts,true).'</pre>';
        $diff = time() - $ts;
//        echo '<pre>$diff::'.print_r($diff,true).'</pre>';
//        die("-1 XXZ");
        if ($diff == 0) {
            return 'now';
        } elseif ($diff > 0) {
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0) {
                if ($diff < 60) {
                    return 'just now';
                }
                if ($diff < 120) {
                    return '1 minute ago';
                }
                if ($diff < 3600) {
                    return floor($diff / 60) . ' minutes ago';
                }
                if ($diff < 7200) {
                    return '1 hour ago';
                }
                if ($diff < 86400) {
                    return floor($diff / 3600) . ' hours ago';
                }
            }
            if ($day_diff == 1) {
                return 'Yesterday';
            }
            if ($day_diff < 7) {
                return $day_diff . ' days ago';
            }
            if ($day_diff < 31) {
                return ceil($day_diff / 7) . ' weeks ago';
            }
            if ($day_diff < 60) {
                return 'last month';
            }

            return date('F Y', $ts);
        } else {
            $diff     = abs($diff);
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0) {
                if ($diff < 120) {
                    return 'in a minute';
                }
                if ($diff < 3600) {
                    return 'in ' . floor($diff / 60) . ' minutes';
                }
                if ($diff < 7200) {
                    return 'in an hour';
                }
                if ($diff < 86400) {
                    return 'in ' . floor($diff / 3600) . ' hours';
                }
            }
            if ($day_diff == 1) {
                return 'Tomorrow';
            }
            if ($day_diff < 4) {
                return date('l', $ts);
            }
            if ($day_diff < 7 + (7 - date('w'))) {
                return 'next week';
            }
            if (ceil($day_diff / 7) < 4) {
                return 'in ' . ceil($day_diff / 7) . ' weeks';
            }
            if (date('n', $ts) == date('n') + 1) {
                return 'next month';
            }

            return date('F Y', $ts);
        }
    }

    public function getFormattedDate($date, $output_format = ''): string
    {
        if (empty($date)) {
            return '';
        }
        $date_carbon_format = config('app.date_carbon_format');
        if (self::isValidTimeStamp($date)) {
            if (strtolower($output_format) == 'astext') {
                $date_carbon_format_as_text = config('app.date_carbon_format_as_text', '%d %B, %Y');

                return Carbon::createFromTimestampMs($date, Config::get('app.timezone'))->formatLocalized($date_carbon_format_as_text);
            }
            if (strtolower($output_format) == 'pickdate') {
                $date_carbon_format_as_pickdate = config('app.pickdate_format_submit');

                return Carbon::createFromTimestamp($date, Config::get('app.timezone'))->format($date_carbon_format_as_pickdate);
            }

            return Carbon::createFromTimestampMs($date, Config::get('app.timezone'))->format($date_carbon_format);
// // 30 January, 2018 7:55 AM;
            // formatLocalized('%A %d %B %Y');
        }


//        echo '<pre>$::'.print_r($date,true).'</pre>';
//        echo '<pre>getFormattedDate $postgresql_format::'.print_r($postgresql_format,true).'</pre>';
//        die("-1 XXZ getFormattedDate");
        $A = preg_split("/ /", $date);
        if (count($A) == 2) {
            $date = $A[0];
        }


        $a = Carbon::createFromFormat($date_carbon_format, $date);
//        echo '<pre>$::'.print_r($a,true).'</pre>';
        $b = $a->format(self::getDateFormat("astext"));
//        echo '<pre>$::'.print_r($b,true).'</pre>';
//        die("-1 XXZ");
        return $a->format(self::getDateFormat("astext"));
    }

    public function getDateTimeFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d H:i';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y g:i A';
        }

        return 'Y-m-d H:i';
    }

    public function getDateFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y';
        }

        return 'Y-m-d';
    }


    public function getLoggedUserImage(int $user_id, $return_url = true): string
    { //
        $base_url                = self::getBaseUrl();
        $public_path             = self::getPublicPath();
        $UPLOADS_USER_IMAGES_URL = env('UPLOADS_USER_IMAGES_URL');
        $UPLOADS_USER_IMAGES_DIR = env('UPLOADS_USER_IMAGES_DIR');
        $images_dir              = $public_path . '/' . $UPLOADS_USER_IMAGES_DIR . '-user-' . $user_id . '/';
//        echo '<pre>$images_dir::'.print_r($images_dir,true).'</pre>';
        $files_of_directory = self::getFilesOfDirectory($images_dir);
//        echo '<pre>$files_of_directory::'.print_r($files_of_directory,true).'</pre>';
        if ( ! empty($files_of_directory[0]['file_path'])) {
            if ($return_url) {
                return $base_url . '/' . $UPLOADS_USER_IMAGES_URL . '-user-' . $user_id . '/' . $files_of_directory[0]['filename'];
            } else {
                return $files_of_directory[0]['file_path'];
            }
        }
        $DEFAULT_USER_IMAGE = env('DEFAULT_USER_IMAGE');
//        echo '<pre>$::'.print_r(public_path() . '/' . $UPLOADS_USER_IMAGES_DIR . $DEFAULT_USER_IMAGE,true).'</pre>';
        if (file_exists($public_path . '/' . $UPLOADS_USER_IMAGES_DIR . $DEFAULT_USER_IMAGE)) {
            if ($return_url) {
                return $base_url . '/' . $UPLOADS_USER_IMAGES_URL . $DEFAULT_USER_IMAGE;
            } else {
                return $public_path . '/' . $UPLOADS_USER_IMAGES_DIR . $DEFAULT_USER_IMAGE;
            }
        }

        return '';
    }

    public function getImageShowSize(string $image_filename, int $orig_width, int $orig_height)
    {
        try {
            $height = Image::make($image_filename)->height();
            $width  = Image::make($image_filename)->width();
        } catch (Exception $e) {
            return false;
        }
        $retArray                    = ['width' => 0, 'height' => 0, 'original_width' => 0, 'original_height' => 0];
        $retArray['original_width']  = $width;
        $retArray['original_height'] = $height;
        $retArray['width']           = $width;
        $retArray['height']          = $height;

        $ratio = round($width / $height, 3);

        if ($width > $orig_width) {
            $retArray['width']  = (int)($orig_width);
            $retArray['height'] = (int)($orig_width / $ratio);
            if ($retArray['width'] <= (int)$orig_width and $retArray['height'] <= (int)$orig_height) {
                //appUtils::deb(-1);
                return $retArray;
            }
            $width  = $retArray['width'];
            $height = $retArray['height'];
        }
        if ($height > $orig_height and ((int)($orig_height / $ratio)) <= $orig_width) {
            $retArray['width']  = (int)($orig_height * $ratio);
            $retArray['height'] = (int)($orig_height);

            return $retArray;
        }
        if ($height > $orig_height and ((int)($orig_height / $ratio)) > $orig_width) {
            $retArray['width']  = (int)($orig_height * $ratio);
            $retArray['height'] = (int)($retArray['width'] / $ratio);

            return $retArray;
        }

        return $retArray;
    }


    public function deleteDirectory(string $directory_name)
    {
        if ( ! file_exists($directory_name) or ! is_dir($directory_name)) {
            return true;
        }
        $H = OpenDir($directory_name);
        while ($nextFile = readdir($H)) { // All files in dir
            if ($nextFile == "." or $nextFile == "..") {
                continue;
            }
            //appUtils::deb($directory_name . DIRECTORY_SEPARATOR . $nextFile, '$directory_name . DIRECTORY_SEPARATOR . $nextFile::');
            unlink($directory_name . DIRECTORY_SEPARATOR . $nextFile);
        }
        closedir($H);

        return rmdir($directory_name);
    }

    public function getFilesOfDirectory(string $directory_name): array
    {
        if ( ! file_exists($directory_name) or ! is_dir($directory_name)) {
            return [];
        }
        $retArray = [];
        $H        = OpenDir($directory_name);
        while ($next_file = readdir($H)) { // All files in dir
            if ($next_file == "." or $next_file == "..") {
                continue;
            }
            //appUtils::deb($directory_name . DIRECTORY_SEPARATOR . $next_file, '$directory_name . DIRECTORY_SEPARATOR . $next_file::');
            $retArray[] = ['filename' => $next_file, 'file_path' => $directory_name . $next_file];
        }
        closedir($H);

        return $retArray;
    }


    public function getCountryName(string $country): string
    {
        $countriesList = self::getCountries(false);
        if ( ! empty($countriesList[$country])) {
            return $countriesList[$country];
        }

        return $country;
    }

    public function getCountries(bool $return_keys = false): array
    {

        $arr = [
            'ad' => 'Andorra',
            'ae' => 'United Arab Emirates',
            'af' => 'Afghanistan',
            'ag' => 'Antigua and Barbuda',
            'ai' => 'Anguilla',
            'al' => 'Albania',
            'am' => 'Armenia',
            'an' => 'Netherlands Antilles',
            'ao' => 'Angola',
            'aq' => 'Antarctica',
            'ar' => 'Argentina',
            'as' => 'American Samoa',
            'at' => 'Austria',
            'au' => 'Australia',
            'aw' => 'Aruba',
            'az' => 'Azerbaijan',
            'ba' => 'Bosnia Hercegovina',
            'bb' => 'Barbados',
            'bd' => 'Bangladesh',
            'be' => 'Belgium',
            'bf' => 'Burkina Faso',
            'bg' => 'Bulgaria',
            'bh' => 'Bahrain',
            'bi' => 'Burundi',
            'bj' => 'Benin',
            'bm' => 'Bermuda',
            'bn' => 'Brunei Darussalam',
            'bo' => 'Bolivia',
            'br' => 'Brazil',
            'bs' => 'Bahamas',
            'bt' => 'Bhutan',
            'bv' => 'Bouvet Island',
            'bw' => 'Botswana',
            'by' => 'Belarus (Byelorussia)',
            'bz' => 'Belize',
            'ca' => 'Canada',
            'cc' => 'Cocos Islands',
            'cd' => 'Congo, The Democratic Republic of the',
            'cf' => 'Central African Republic',
            'cg' => 'Congo',
            'ch' => 'Switzerland',
            'ci' => 'Ivory Coast',
            'ck' => 'Cook Islands',
            'cl' => 'Chile',
            'cm' => 'Cameroon',
            'cn' => 'China',
            'co' => 'Colombia',
            'cr' => 'Costa Rica',
            'cs' => 'Czechoslovakia',
            'cu' => 'Cuba',
            'cv' => 'Cape Verde',
            'cx' => 'Christmas Island',
            'cy' => 'Cyprus',
            'cz' => 'Czech Republic',
            'de' => 'Germany',
            'dj' => 'Djibouti',
            'dk' => 'Denmark',
            'dm' => 'Dominica',
            'do' => 'Dominican Republic',
            'dz' => 'Algeria',
            'ec' => 'Ecuador',
            'ee' => 'Estonia',
            'eg' => 'Egypt',
            'eh' => 'Western Sahara',
            'er' => 'Eritrea',
            'es' => 'Spain',
            'et' => 'Ethiopia',
            'fi' => 'Finland',
            'fj' => 'Fiji',
            'fk' => 'Falkland Islands',
            'fm' => 'Micronesia',
            'fo' => 'Faroe Islands',
            'fr' => 'France',
            'fx' => 'France, Metropolitan FX',
            'ga' => 'Gabon',
            'gb' => 'United Kingdom',
            'gd' => 'Grenada',
            'ge' => 'Georgia',
            'gf' => 'French Guiana',
            'gh' => 'Ghana',
            'gi' => 'Gibraltar',
            'gl' => 'Greenland',
            'gm' => 'Gambia',
            'gn' => 'Guinea',
            'gp' => 'Guadeloupe',
            'gq' => 'Equatorial Guinea',
            'gr' => 'Greece',
            'gs' => 'South Georgia and the South Sandwich Islands',
            'gt' => 'Guatemala',
            'gu' => 'Guam',
            'gw' => 'Guinea-bissau',
            'gy' => 'Guyana',
            'hk' => 'Hong Kong',
            'hm' => 'Heard and McDonald Islands',
            'hn' => 'Honduras',
            'hr' => 'Croatia',
            'ht' => 'Haiti',
            'hu' => 'Hungary',
            'id' => 'Indonesia',
            'ie' => 'Ireland',
            'il' => 'Israel',
            'in' => 'India',
            'io' => 'British Indian Ocean Territory',
            'iq' => 'Iraq',
            'ir' => 'Iran',
            'is' => 'Iceland',
            'it' => 'Italy',
            'jm' => 'Jamaica',
            'jo' => 'Jordan',
            'jp' => 'Japan',
            'ke' => 'Kenya',
            'kg' => 'Kyrgyzstan',
            'kh' => 'Cambodia',
            'ki' => 'Kiribati',
            'km' => 'Comoros',
            'kn' => 'Saint Kitts and Nevis',
            'kp' => 'North Korea',
            'kr' => 'South Korea',
            'kw' => 'Kuwait',
            'ky' => 'Cayman Islands',
            'kz' => 'Kazakhstan',
            'la' => 'Laos',
            'lb' => 'Lebanon',
            'lc' => 'Saint Lucia',
            'li' => 'Lichtenstein',
            'lk' => 'Sri Lanka',
            'lr' => 'Liberia',
            'ls' => 'Lesotho',
            'lt' => 'Lithuania',
            'lu' => 'Luxembourg',
            'lv' => 'Latvia',
            'ly' => 'Libya',
            'ma' => 'Morocco',
            'mc' => 'Monaco',
            'md' => 'Moldova Republic',
            'mg' => 'Madagascar',
            'mh' => 'Marshall Islands',
            'mk' => 'Macedonia, The Former Yugoslav Republic of',
            'ml' => 'Mali',
            'mm' => 'Myanmar',
            'mn' => 'Mongolia',
            'mo' => 'Macau',
            'mp' => 'Northern Mariana Islands',
            'mq' => 'Martinique',
            'mr' => 'Mauritania',
            'ms' => 'Montserrat',
            'mt' => 'Malta',
            'mu' => 'Mauritius',
            'mv' => 'Maldives',
            'mw' => 'Malawi',
            'mx' => 'Mexico',
            'my' => 'Malaysia',
            'mz' => 'Mozambique',
            'na' => 'Namibia',
            'nc' => 'New Caledonia',
            'ne' => 'Niger',
            'nf' => 'Norfolk Island',
            'ng' => 'Nigeria',
            'ni' => 'Nicaragua',
            'nl' => 'Netherlands',
            'no' => 'Norway',
            'np' => 'Nepal',
            'nr' => 'Nauru',
            'nt' => 'Neutral Zone',
            'nu' => 'Niue',
            'nz' => 'New Zealand',
            'om' => 'Oman',
            'pa' => 'Panama',
            'pe' => 'Peru',
            'pf' => 'French Polynesia',
            'pg' => 'Papua New Guinea',
            'ph' => 'Philippines',
            'pk' => 'Pakistan',
            'pl' => 'Poland',
            'pm' => 'St. Pierre and Miquelon',
            'pn' => 'Pitcairn',
            'pr' => 'Puerto Rico',
            'pt' => 'Portugal',
            'pw' => 'Palau',
            'py' => 'Paraguay',
            'qa' => 'Qatar',
            're' => 'Reunion',
            'ro' => 'Romania',
            'ru' => 'Russia',
            'rw' => 'Rwanda',
            'sa' => 'Saudi Arabia',
            'sb' => 'Solomon Islands',
            'sc' => 'Seychelles',
            'sd' => 'Sudan',
            'se' => 'Sweden',
            'sg' => 'Singapore',
            'sh' => 'St. Helena',
            'si' => 'Slovenia',
            'sj' => 'Svalbard and Jan Mayen Islands',
            'sk' => 'Slovakia (Slovak Republic)',
            'sl' => 'Sierra Leone',
            'sm' => 'San Marino',
            'sn' => 'Senegal',
            'so' => 'Somalia',
            'sr' => 'Suriname',
            'st' => 'Sao Tome and Principe',
            'sv' => 'El Salvador',
            'sy' => 'Syria',
            'sz' => 'Swaziland',
            'tc' => 'Turks and Caicos Islands',
            'td' => 'Chad',
            'tf' => 'French Southern Territories',
            'tg' => 'Togo',
            'th' => 'Thailand',
            'tj' => 'Tajikistan',
            'tk' => 'Tokelau',
            'tm' => 'Turkmenistan',
            'tn' => 'Tunisia',
            'to' => 'Tonga',
            'tp' => 'East Timor',
            'tr' => 'Turkey',
            'tt' => 'Trinidad, Tobago',
            'tv' => 'Tuvalu',
            'tw' => 'Taiwan',
            'tz' => 'Tanzania',
            'ua' => 'Ukraine',
            'ug' => 'Uganda',
            'uk' => 'United Kingdom',
            'um' => 'United States Minor Islands',
            'us' => 'United States',
            'uy' => 'Uruguay',
            'uz' => 'Uzbekistan',
            'va' => 'Vatican City',
            'vc' => 'Saint Vincent, Grenadines',
            've' => 'Venezuela',
            'vg' => 'Virgin Islands (British)',
            'vi' => 'Virgin Islands (USA)',
            'vn' => 'Viet Nam',
            'vu' => 'Vanuatu',
            'wf' => 'Wallis and Futuna Islands',
            'ws' => 'Samoa',
            'ye' => 'Yemen',
            'yt' => 'Mayotte',
            'yu' => 'Yugoslavia',
            'za' => 'South Africa',
            'zm' => 'Zambia',
            'zr' => 'Zaire',
            'zw' => 'Zimbabwe'
        ];
        if ($return_keys) {
            $retArray = [];
            foreach ($arr as $next_key => $next_value) {
                $retArray[] = ['key' => $next_key, 'label' => $next_value];
            }

            return $retArray;
        }

        return $arr;
    }


    public function setArrayAsHtmlSpecialchars($arr)
    {
        if (is_string($arr)) {
            return htmlspecialchars($arr);
        }
        if (is_array($arr) == 'array') {
            foreach ($arr as $next_key => $next_value) {
                if (gettype($next_value) == "string") {
                    $arr[$next_key] = htmlspecialchars($next_value);
//                    $arr[$next_key] = addslashes(htmlspecialchars($next_value));
                }
                if (gettype($next_value) == "array") {
                    $arr[$next_key] = self::setArrayAsHtmlSpecialchars($next_value);
                }
            }
        }

        return $arr;
    }


    public function getRightSubstring(string $S, $count): string
    {
        return substr($S, strlen($S) - $count, $count);
    }

    public function trimRightSubString(string $s, string $substr): string
    {
        $res = preg_match('/(.*?)(' . preg_quote($substr, "/") . ')$/si', $s, $A);
        if ( ! empty($A[1])) {
            return $A[1];
        }

        return $s;
    }

    public function convertFromMySqlToCalendarFormat(string $string_date): string
    { // 	2012-12-28      2016-09-05 -> 5 September, 2016
        if (empty($string_date)) {
            return '';
        }                      //2016-09-05 -> 5 September, 2016
        $A = preg_split("/-/", $string_date);
        if (count($A) != 3) {
            return false;
        }
        $year     = $A[0];
        $month    = $A[1];
        $day      = $A[2];
        $tmp_date = mktime(null, null, null, $month, $day, $year);

        return Carbon::createFromTimestampMs($tmp_date, Config::get('app.timezone'))->format(\Config::get('app.date_carbon_format_as_text', '%d %B, %Y'));
    }

    //             $url_by_code= $this->getCharsOfString($url);
    public function getCharsOfString($str)
    {
        $res_str = '';
        $l       = mb_strlen($str);
        $res_str = '';
        for ($i = 0; $i < $l; $i++) {
//            $res_str= '';
// string mb_substr ( string $str , int $start [, int $length = NULL [, string $encoding = mb_internal_encoding() ]] )
            $res_str .= '  ' . $i . ':  :' . mb_substr($str, $i, 1) . '' . "<br>";
        }

        return $res_str;
//        Debugbar::addMessage($s, 'appFuncs::'.$src);
    }

    public function dd($s, $src = '')
    {
        Debugbar::addMessage($s, 'appFuncs::' . $src);
    }

    public static function getSystemInfo()
    {
        $string = ' Laravel:<b>' . app()::VERSION . '</b><br>' .
                  'DEBUG:<b>' . env('APP_DEBUG') . '</b><br>' .
                  'ENV:<b> ' . env('APP_ENV') . '</b><br>' .
                  'DB_HOST:<b> ' . env('DB_HOST') . '</b><br>' .
                  'APP_LOG_LEVEL:<b> ' . env('APP_LOG_LEVEL') . '</b><br>' .
                  'DB_CONNECTION:<b> ' . env('DB_CONNECTION') . '</b><br>' .
                  'DB_DATABASE:<b> ' . env('DB_DATABASE') . '</b><br>' .
                  'Prefix:<b>' . \DB::getTablePrefix() . '</b>';

        return $string;
    }

    // http://stackoverflow.com/questions/36657629/how-to-use-traits-laravel-5-2

    protected function _(string $message, int $number = 1, array $varsArray = [], $to_lower = false): string
    {
        /* $number = 2;
throw_unless($number === 3, new NotThreeException('Number is not three')); */
        return trans_choice($to_lower ? strtolower($message) : $message, $number, $varsArray);
    }

    protected function getDashboardHome(): string
    {
        return \App::make('url')->to('/');
    }

    public function makeStripTags(string $str)
    {
        return strip_tags($str);
    }

    protected function makeClearDoubledSpaces(string $str): string
    {
        return preg_replace("/(\s{2,})/ms", " ", $str);
    }

    protected function makeStripslashes(string $str): string
    {
        return stripslashes($str);
    }


    /* Submitting form string value must be worked out according to options of app */
    protected function workTextString($str)
    {
        if (is_string($str)) {
            $str = $this->makeStripTags($str);
        }
        if (is_string($str)) {
            $str = $this->makeStripslashes($str);
        }
        if (is_string($str)) {
            $str = $this->makeClearDoubledSpaces($str);
        }

        return is_string($str) ? trim($str) : '';
    }

    function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
               && ($timestamp <= PHP_INT_MAX)
               && ($timestamp >= ~PHP_INT_MAX);
    }

    function getFileNameExt($FileName)
    {
        $K   = strrpos($FileName, ".");
        $Ext = '';
        if ($K > 0) { // File has an extension
            $Ext = substr($FileName, $K + 1);
        }

        return trim($Ext);
    }

    function getFileNameBase($FileName)
    {
        $K   = strrpos($FileName, ".");
        $Ext = '';
        if ($K > 0) { // File has an extension
            $Ext = substr($FileName, 0, $K - 1);
        }

        return trim($Ext);
    }
}
