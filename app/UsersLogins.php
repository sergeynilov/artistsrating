<?php

namespace App;

use DB;
use App\MyAppModel;
use App\library\ListingReturnData;
use App\Login;

class UsersLogins extends MyAppModel
{

    protected $table = 'users_logins';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'provider_name', 'username', 'user_id', 'remote_addr', 'with_success' ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getUsersLoginsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 )
    {

        if (empty($order_by)) $order_by = 'ul.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';

        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $users_logins_table_name= with(new UsersLogins)->getTableName();
        $quoteModel= UsersLogins::from(  \DB::raw(DB::getTablePrefix().$users_logins_table_name.' as ul' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT ) {
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ul.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( \DB::raw('ul.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['login_id'])) {
            $quoteModel->where( \DB::raw('ul.login_id'), '=', $filtersArray['login_id'] );
        }

        if (!empty($filtersArray['with_success'])) {
            $quoteModel->where( \DB::raw('ul.with_success'), '=', $filtersArray['with_success'] );
        }

        if (!empty($filtersArray['provider_name'])) {
            $quoteModel->where( \DB::raw('ul.provider_name'), '=', $filtersArray['provider_name'] );
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of ul table */
        $items_per_page= with(new UsersLogins)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UsersLogins)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $usersLoginsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $usersLoginsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $usersLoginsList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $usersLoginsList;

    } // public static function getUsersLoginsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {


}