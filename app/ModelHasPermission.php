<?php
namespace App;

use DB;
use App\MyAppModel;
use App\User;
use App\library\AppPermissionAccess;
use App\library\AppPermissionAccessReturnType;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;

class ModelHasPermission extends MyAppModel
{

    protected $table = 'sp_pm_model_has_permissions';
//    protected $primaryKey = null;
    protected $primaryKey = ['permission_id', 'model_id', 'model_type']; // PRIMARY KEY (permission_id, model_id, model_type)

    public $timestamps = false;

    use funcsTrait;

    protected static function boot() {
        parent::boot();
    }

    public function permission()
    {
        return $this->belongsTo('App\Permission');
    }


    protected $fillable = ['model_type', 'model_id', 'permission_id'];
    public static function checkUserHasPermissionsValues( User $checkUser, int $ret_type ) : array
    {
        $retArray= [];
        if ( $checkUser === null ) return [];

        $inBackendEditSystemDictionaries= $checkUser->can(IN_BACKEND_EDIT_SYSTEM_DICTIONARIES); // #1
        if ( $inBackendEditSystemDictionaries ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_BACKEND_EDIT_SYSTEM_DICTIONARIES;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_BACKEND_EDIT_SYSTEM_DICTIONARIES;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_BACKEND_EDIT_SYSTEM_DICTIONARIES, 'label'=> IN_BACKEND_EDIT_SYSTEM_DICTIONARIES ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_BACKEND_EDIT_SYSTEM_DICTIONARIES] = IN_BACKEND_EDIT_SYSTEM_DICTIONARIES;
            }
        }


        $InBackendEditUsersData= $checkUser->can(IN_BACKEND_EDIT_USERS_DATA);  // #2
        if ( $InBackendEditUsersData ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_BACKEND_EDIT_USERS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_BACKEND_EDIT_USERS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_BACKEND_EDIT_USERS_DATA, 'label'=> IN_BACKEND_EDIT_USERS_DATA ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_BACKEND_EDIT_USERS_DATA] = IN_BACKEND_EDIT_USERS_DATA;
            }
        }


        $InBackendSetUsersRoleStatus= $checkUser->can(IN_BACKEND_SET_USERS_ROLE_STATUS); // #3
        if ( $InBackendSetUsersRoleStatus ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_BACKEND_SET_USERS_ROLE_STATUS;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_BACKEND_SET_USERS_ROLE_STATUS;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_BACKEND_SET_USERS_ROLE_STATUS, 'label'=> IN_BACKEND_SET_USERS_ROLE_STATUS ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_BACKEND_SET_USERS_ROLE_STATUS] = IN_BACKEND_SET_USERS_ROLE_STATUS;
            }
        }


        $inBackendEditCmsData= $checkUser->can(IN_BACKEND_EDIT_CMS_DATA);             // #4
        if ( $inBackendEditCmsData ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_BACKEND_EDIT_CMS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_BACKEND_EDIT_CMS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_BACKEND_EDIT_CMS_DATA, 'label'=> IN_BACKEND_EDIT_CMS_DATA ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_BACKEND_EDIT_CMS_DATA] = IN_BACKEND_EDIT_CMS_DATA;
            }
        }


        $InBackendPublishCmsData= $checkUser->can(IN_BACKEND_PUBLISH_CMS_DATA);       // #5
        if ( $InBackendPublishCmsData ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_BACKEND_PUBLISH_CMS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_BACKEND_PUBLISH_CMS_DATA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_BACKEND_PUBLISH_CMS_DATA, 'label'=> IN_BACKEND_PUBLISH_CMS_DATA ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_BACKEND_PUBLISH_CMS_DATA] = IN_BACKEND_PUBLISH_CMS_DATA;
            }
        }


        $InFrontendLoggedUserArea= $checkUser->can(IN_FRONTEND_LOGGED_USER_AREA);     // #6
        if ( $InFrontendLoggedUserArea ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_FRONTEND_LOGGED_USER_AREA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_FRONTEND_LOGGED_USER_AREA;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_FRONTEND_LOGGED_USER_AREA, 'label'=> IN_FRONTEND_LOGGED_USER_AREA ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_FRONTEND_LOGGED_USER_AREA] = IN_FRONTEND_LOGGED_USER_AREA;
            }
        }


        $InFrontendAllPublicPages= $checkUser->can(IN_FRONTEND_ALL_PUBLIC_PAGES);     // #7
        if ( $InFrontendAllPublicPages ) {
            if ( $ret_type== AppPermissionAccessReturnType::STRING_RETURN_TYPE ) {
                $retArray[] = IN_FRONTEND_ALL_PUBLIC_PAGES;
            }
            if ( $ret_type== AppPermissionAccessReturnType::NUMBER_RETURN_TYPE ) {
                $retArray[] = AppPermissionAccess::IN_FRONTEND_ALL_PUBLIC_PAGES;
            }
            if ( $ret_type== AppPermissionAccessReturnType::KEY_LABEL_ARRAY_RETURN_TYPE ) {
                $retArray[] = [ 'key'=> AppPermissionAccess::IN_FRONTEND_ALL_PUBLIC_PAGES, 'label'=> IN_FRONTEND_ALL_PUBLIC_PAGES ];
            }
            if ( $ret_type== AppPermissionAccessReturnType::ASSOC_ARRAY_RETURN_TYPE ) {
                $retArray[AppPermissionAccess::IN_FRONTEND_ALL_PUBLIC_PAGES] = IN_FRONTEND_ALL_PUBLIC_PAGES;
            }
        }



/*        $loggedUserHasPermissionsList = ModelHasPermission::getModelHasPermissionsList(ListingReturnData::LISTING, ['model_type' => 'App\User', 'model_id'=> $model_id]);
        foreach( $loggedUserHasPermissionsList as $nextLoggedUserHasPermission ) {
            with(new self)->info( $nextLoggedUserHasPermission,'$nextLoggedUserHasPermission->permission()::' );
            $permission_name= $nextLoggedUserHasPermission->permission();
            echo '<pre>$permission_name::'.print_r($permission_name,true).'</pre>';
            with(new self)->info( $permission_name,'$permission_name::' );
                    if ( $permission_name == IN_BACKEND_EDIT_SYSTEM_DICTIONARIES ) { // # 1
                        $retArray[]= $ret_string ? AppPermissionAccess::IN_BACKEND_EDIT_SYSTEM_DICTIONARIES : '';
                    }
                    if ( $permission_name == IN_BACKEND_EDIT_USERS_DATA ) {          // # 2
                        $retArray[]= $ret_string ? AppPermissionAccess::IN_BACKEND_EDIT_USERS_DATA : '';
                    }
                      if ( $permission_name == IN_BACKEND_SET_USERS_ROLE_STATUS ) {   // # 3
                            $retArray[]= $ret_string ? AppPermissionAccess::IN_BACKEND_SET_USERS_ROLE_STATUS : '';
                    }
                   if ( $permission_name == IN_BACKEND_EDIT_CMS_DATA ) {           // # 4
                        $retArray[]= $ret_string ? AppPermissionAccess::IN_BACKEND_EDIT_CMS_DATA : '';
                    }
                    if ( $permission_name == IN_BACKEND_PUBLISH_CMS_DATA ) {        // # 5
                        $retArray[]= $ret_string ? AppPermissionAccess::IN_BACKEND_PUBLISH_CMS_DATA : '';
                    }
                    if ( $permission_name == IN_FRONTEND_LOGGED_USER_AREA ) {      // # 6
                        $retArray[]= $ret_string ? AppPermissionAccess::IN_FRONTEND_LOGGED_USER_AREA : '';
                    }

            if ( $permission_name == IN_FRONTEND_ALL_PUBLIC_PAGES ) {      // # 7
                $retArray[]= $ret_string ? AppPermissionAccess::IN_FRONTEND_ALL_PUBLIC_PAGES : '';
            }
        }*/
        return $retArray;
    } // public static function checkUserHasPermissionsValues( User $checkUser, bool $ret_string= true ) : array

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getModelHasPermissionsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'mhp.model_type'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $model_has_permission_table_name= with(new ModelHasPermission)->getTableName();
        $quoteModel= ModelHasPermission::from(  \DB::raw(DB::getTablePrefix().$model_has_permission_table_name.' as mhp' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'mhp.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (isset($filtersArray['model_type'])) {
            $quoteModel->where( DB::raw('mhp.model_type'), '=', $filtersArray['model_type'] );
        }

        if (isset($filtersArray['model_id'])) {
            $quoteModel->where( DB::raw('mhp.model_id'), '=', $filtersArray['model_id'] );
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $song_model_has_permission_table_name= DB::getTablePrefix().( with(new SongModelHasPermission)->getTableName() );
//        if ( !empty($filtersArray['show_related_songs_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $song_model_has_permission_table_name . ' as sg where smhp.model_has_permission_id = ' . 'mhp.id ) as related_songs_count';
//        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new ModelHasPermission)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new ModelHasPermission)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $model_has_permissionsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $model_has_permissionsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $model_has_permissionsList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $model_has_permissionsList;

    } // public static function getModelHasPermissionsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;

        $model_has_permission = ModelHasPermission::find($id);

        if (empty($model_has_permission)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $model_has_permission= with(new ModelHasPermission)->substituteNullValues($model_has_permission, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $model_has_permission;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $model_has_permission_id ) : array
    {
        $additional_name_validation_rule= 'check_model_has_permission_unique_by_name:'.( !empty($model_has_permission_id)?$model_has_permission_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:100|'.$additional_name_validation_rule,
            'published'    => 'required|in:'.with( new ModelHasPermission)->getValueLabelKeys( ModelHasPermission::getModelHasPermissionPublishedValueArray(false) ),
            'description'  => 'required',
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for model_has_permissions.name field */
    public static function getSimilarModelHasPermissionByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = ModelHasPermission::whereRaw( ModelHasPermission::dbStrLower('name', false, false) .' = '. ModelHasPermission::dbStrLower( ModelHasPermission::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

}
