<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use App\Config;

use Auth;
use Carbon\Carbon;

use App\Http\Traits\funcsTrait;
use App\MyAppModel;
use App\Settings;
use App\User;
use App\CmsItem;
use App\library\ListingReturnData;
use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class CmsItemTranslation extends MyAppModel
{
    use funcsTrait;
    protected $table = 'cms_item_translations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['cms_item_id', 'locale', 'title', 'short_descr', 'content'];


    public function cmsItem()
    {
        return $this->belongsTo('App\CmsItem');
    }

    public static function getRowByIdLocale( int $cms_item_id,  string $locale, $return_count= false )
    {
        $quoteModel = CmsItemTranslation::where( 'cms_item_id',  $cms_item_id )->where( 'locale',  $locale );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get()->first();
        if ( empty($retRow) ) return false;
        return $retRow;
    }

    public static function getSimilarCmsItemTranslationByTitle( string $title, string $locale= '', int $cms_item_id= null, $return_count = false )
    {
        $quoteModel = CmsItemTranslation::whereRaw( CmsItemTranslation::dbStrLower('title', false, false) .' = '. CmsItemTranslation::dbStrLower(CmsItemTranslation::pgEscape($title), true,false) );
        if ( !empty( $cms_item_id ) ) {
            $quoteModel = $quoteModel->where( 'cms_item_id', '!=' , $cms_item_id );
        }
        if ( !empty( $locale ) ) {
            $quoteModel = $quoteModel->where( 'locale', $locale );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getValidationRulesArray( $cms_item_id ) : array
    {
        $additional_title_validation_rule= '';//'check_cms_item_unique_by_title:'.( !empty($cms_item_id)?$cms_item_id:'');
        $validationRulesArray = [
            'title'        => 'required|max:100|'.$additional_title_validation_rule,
            'short_descr'  => 'required|max:255',
            'content'      => 'required',
        ];
        return $validationRulesArray;
    }


}