<?php
namespace App;

use DB;

use App\MyAppModel;
use App\User;
use App\Config;
use App\Artist;
use App\library\ListingReturnData;

class ArtistVote extends MyAppModel
{

    protected $table = 'artist_votes';
    protected $primaryKey = 'id';
    public $timestamps = false;

    private static $artistVoteValueArray = [ 1=> 'Poor', 2=> 'Not good', 3=> 'So-so', 4=> 'Good', 5=> 'Excellent' ];

    protected $artistVoteImagePropsArray = [];

    protected $fillable = [ "artist_id", "user_id", "vote" ];


    protected static function boot() {
        parent::boot();
    }

    /* return array of key value/label pairs based on self::$artistVoteValueArray - db enum key values/labels implementation */
    public static function getArtistVoteValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$artistVoteValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$artistVoteValueArray - db enum key values/labels implementation */
    public static function getArtistVoteLabel(string $is_main) : string
    {
        if (!empty(self::$artistVoteValueArray[$is_main])) {
            return self::$artistVoteValueArray[$is_main];
        }
        return '';
    }



    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getArtistVotesList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0
    ) {
        if (empty($order_by)) $order_by = 'av.vote'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $artist_vote_table_name= with(new ArtistVote)->getTableName();
        $quoteModel= ArtistVote::from(  \DB::raw(DB::getTablePrefix().$artist_vote_table_name.' as av' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'av.*';


        if (!empty($filtersArray['artist_id'])) {
            $quoteModel->where(DB::raw('av.artist_id'), '=', $filtersArray['artist_id']);
        }

        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where(DB::raw('av.user_id'), '=', $filtersArray['user_id']);
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( "av.created_at >='".$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( "av.created_at <='".$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new ArtistVote)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new ArtistVote)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $artistVotesList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $artistVotesList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $artistVotesList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $artistVotesList;

    } // public static function getArtistVotesList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param=
    // 0 ) {

    public static function getValidationRulesArray($artist_vote_id= null) : array
    {
        $validationRulesArray = [
            'artist_vote_id'           => 'required|exists:'.( with(new ArtistVote)->getTableName() ).',id',
            'artist_id'                => 'required|exists:'.( with(new Artist)->getTableName() ).',id',
            'user_id'                  => 'required|exists:'.( with(new User)->getTableName() ).',id',
            'vote'                     => 'required|in:'.with( new ArtistVote)->getValueLabelKeys( ArtistVote::getArtistIsMainValueArray(false) ),
        ];
        return $validationRulesArray;
    }

}