<?php
namespace App;

use Carbon\Carbon;
use DB;
use App\MyAppModel;
use App\Artist;
use App\ArtistConcertTranslation;
use App\library\ListingReturnData;

class ArtistConcert extends MyAppModel
{

    protected $table = 'artist_concerts';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function boot() {
        parent::boot();
    }

    protected $fillable = [ 'artist_id', 'place_name', 'lat', 'lng', 'participants',  'event_date', 'country', 'description' ];

/*
            $artistConcert->lat= $artistConcert['lat'];
            $artistConcert->lng= $artistConcert['lng'];
            $artistConcert->event_date= $artistConcert['event_date'];
            $artistConcert->country= $artistConcert['country'];
            $artistConcert->description= $artistConcert['description'];
            if ( !empty($artistConcert['tour_id']) ) {
                $artistConcert->tour_id = $artistConcert['tour_id'];
            }

            echo '<pre>$artistConcert::'.print_r($artistConcert,true).'</pre>';

            $artistConcert->save();                                                                                                              f
*/
    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getArtistConcertsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'ac.event_date'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $artist_concert_table_name= with(new ArtistConcert)->getTableName();
        $quoteModel= ArtistConcert::from(  \DB::raw(DB::getTablePrefix().$artist_concert_table_name.' as ac' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ac.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['place_name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( ArtistConcert::dbStrLower('place_name', false, false) . ' like ' . ArtistConcert::dbStrLower( $filtersArray['place_name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.ArtistConcert::dbStrLower('place_name', false, false) . ' like ' . ArtistConcert::dbStrLower( $filtersArray['place_name'], true,true ) . ' OR ' . ArtistConcert::dbStrLower('description', false, false) . ' like ' . ArtistConcert::dbStrLower( $filtersArray['place_name'], true,true ) .
                                       ' OR ' . ArtistConcert::dbStrLower('description', false, false) . ' like ' . ArtistConcert::dbStrLower( $filtersArray['place_name'], true,true ) . ' ) ');
            }
        }

        if (!empty($filtersArray['artist_id'])) {
            $quoteModel->where( DB::raw('ac.artist_id'), '=', $filtersArray['artist_id'] );
        }

        if (!empty($filtersArray['tour_id'])) {
            $quoteModel->where( DB::raw('ac.tour_id'), '=', $filtersArray['tour_id'] );
        }

        if (!empty($filtersArray['country'])) {
            $quoteModel->where( DB::raw('ac.country'), '=', $filtersArray['country'] );
        }

        if (!empty($filtersArray['only_future'])) {
            $today= Carbon::today()->format('Y-m-d');
            $quoteModel->whereRaw( "event_date >'".$today . " 00:00:00'" );
        }
        if (!empty($filtersArray['artistsArray'])) {
            $quoteModel->whereIn( DB::raw("ac.artist_id"), $filtersArray['artistsArray'] );
        }

        if (!empty($filtersArray['event_date_from'])) {
            $quoteModel->whereRaw( DB::raw("ac.event_date >='").$filtersArray['event_date_from'] . "'" );
        }
        if (!empty($filtersArray['event_date_till'])) {
            $quoteModel->whereRaw( DB::raw("ac.event_date <='").$filtersArray['event_date_till'] . " 23:59:59'" );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("ac.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("ac.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }


        // artist_main_image
        $artist_image_table_name= DB::getTablePrefix().( with(new ArtistConcert)->getTableName() );
        if ( !empty($filtersArray['artist_main_image']) ) {
            $additive_fields_for_select .= ', ( select image_name from ' . $artist_image_table_name . ' as ai where ai.artist_id = ' . 'ac.artist_id AND ai.is_main limit 1) as artist_image_name';
        }

        if ( !empty($filtersArray['artist_is_active'])  ) { // need to join in select sql username and user_is_active field of author of uc
            with(new self)->info( $filtersArray['artist_is_active'],'+++ $filtersArray[\'artist_is_active\']::' );
            $artists_table_name= DB::getTablePrefix() . ( with(new Artist)->getTableName() );
            $additive_fields_for_select .= ', a.id as artist_id, a.name as artist_name, a.has_concerts as artist_has_concerts' ;
            $quoteModel->join( \DB::raw($artists_table_name . ' as a '), \DB::raw('a.id'), '=', \DB::raw('ac.artist_id AND '.
                                                              \DB::raw('a.is_active') . "='". $filtersArray['artist_is_active'] ."' ") );
        } // if ( !empty($filtersArray[show_song_name])  ) { // need to join in select sql username and user_is_active field of author of uc



        if ( !empty($filtersArray['locale'])  ) { // need to join in select sql username and user_is_active field of author
            $artist_concert_translations_table_name= DB::getTablePrefix().( with(new ArtistConcertTranslation)->getTableName() );
//                $quoteModel->join( \DB::raw($songs_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('sa.song_id') );
                $quoteModel->join( \DB::raw($artist_concert_translations_table_name . ' as ac_t '), \DB::raw('ac_t.artist_concert_id'), '=', \DB::raw('ac.id') );
                $quoteModel->where( \DB::raw('ac_t.locale'), '=', $filtersArray['locale'] );
        } // if ( !empty($filtersArray[show_song_name])  ) { // need to join in select sql username and user_is_active field of author of uc


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new ArtistConcert)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new ArtistConcert)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $artistConcertsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $artistConcertsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $artistConcertsList = $quoteModel->get();
            $data_retrieved= true;
        }

        foreach( $artistConcertsList as $next_key=>$nextArtistConcert ) { /* map all retrieved data when need to set human readable labels for some fields */
            if (!empty($filtersArray['show_future_days'])) {
                $nextArtistConcert['event_date_is_past']= with(new ArtistConcert)->getDateTimesCompare( $nextArtistConcert->event_date, time() );
            }
        }
        return $artistConcertsList;

    } // public static function getArtistConcertsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if ( !$is_row_retrieved ) {
            $artist_concert = ArtistConcert::find($id);
        }

        if (empty($artist_concert)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $artist_concert= with(new ArtistConcert)->substituteNullValues($artist_concert, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $artist_concert;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $artist_concert_id, bool $check_artist_concert_participant_selected= false ) : array
    {
        $additional_name_validation_rule= 'check_artist_concert_unique_by_name:'.( !empty($artist_concert_id)?$artist_concert_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:255|'.$additional_name_validation_rule,
            'description'  => 'required',
            'creator_id'   => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'task_id'      => 'nullable|exists:'.( with(new Task)->getTableName() ).',id',
            'artist_concertParticipantsArray'=> [ 'required',new CheckArtistConcertParticipantSelected() ]
        ];

        return $validationRulesArray;
    }

    /* check if provided name is unique for artist_concerts.name field */
    public static function getSimilarArtistConcertByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = ArtistConcert::whereRaw( ArtistConcert::dbStrLower('name', false, false) .' = '. ArtistConcert::dbStrLower( ArtistConcert::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function fillTranslations()
    {
        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $artistImagesList = ArtistConcert::getArtistConcertsList(ListingReturnData::LISTING, []);
        foreach ($artistImagesList as $nextArtistConcert) {
            $artist_concert_id       = $nextArtistConcert->id;
            $place_name       = $nextArtistConcert->place_name;
            $description       = $nextArtistConcert->description;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newArtistConcertTranslation= new ArtistConcertTranslation();
                $newArtistConcertTranslation->artist_concert_id= $artist_concert_id;
                if ( $next_lang_key == 'en' ) {
                    $newArtistConcertTranslation->place_name = $place_name;
                    $newArtistConcertTranslation->description = $description;
                } else {
                    $newArtistConcertTranslation->place_name = $next_lang_label . ' VERSION : ' . $place_name;
                    $newArtistConcertTranslation->description = $next_lang_label . ' VERSION : ' . $description;
                }
                $newArtistConcertTranslation->locale= $next_lang_key;

                $newArtistConcertTranslation->created_at= now();
                $newArtistConcertTranslation->save();
            }

        }
    }


}

