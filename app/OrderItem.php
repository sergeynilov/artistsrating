<?php
namespace App;

use Carbon\Carbon;
use DB;
use App\MyAppModel;
use App\User;
use App\Order;
use App\library\ListingReturnData;

class OrderItem extends MyAppModel
{

    protected $table = 'order_items';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function boot() {
        parent::boot();
    }

    protected $fillable = [ 'order_id', 'product_id', 'product_type', 'qty', 'price' ];


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getOrderItemsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'oi.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $order_item_table_name= with(new OrderItem)->getTableName();
        $quoteModel= OrderItem::from(  \DB::raw(DB::getTablePrefix().$order_item_table_name.' as oi' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'oi.*';

        if (!empty($filtersArray['order_id'])) {
            $quoteModel->where( DB::raw('oi.order_id'), '=', $filtersArray['order_id'] );
        }

        // 'order_id' => $order_id, 'show_order_name' => 1, 'show_related_order_votes' => 1, 'order_is_active'=> true],       'order_ordering', 'desc');
        if (!empty($filtersArray['order_id'])) {
            $quoteModel->where( DB::raw('oi.order_id'), '=', $filtersArray['order_id'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("oi.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("oi.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $orders_table_name= DB::getTablePrefix() . ( with(new Order)->getTableName() );
        $order_translations_table_name=  DB::getTablePrefix().with(new OrderTranslation)->getTableName();
        $order_table_joined= false;
        if ( !empty($filtersArray['show_order_name']) and !empty($filtersArray['locale'])  ) { // need to join in select sql username and user_is_active field of author
            if ( !$order_table_joined ) {
                $quoteModel->join( \DB::raw($orders_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('oi.order_id') );
                $quoteModel->join( \DB::raw($order_translations_table_name . ' as s_t '), \DB::raw('s_t.order_id'), '=', \DB::raw('s.id') );
                $quoteModel->where( \DB::raw('s_t.locale'), '=', $filtersArray['locale'] );
                $order_table_joined= true;
            }
            $additive_fields_for_select .= ', s.id as order_id, s_t.title as order_title, s.slug as order_slug, s.slug as order_slug, s_t.short_descr as order_short_descr, s_t.description as order_description, s.is_active as order_is_active, s.ordering as order_ordering';
        } // if ( !empty($filtersArray[show_order_name])  ) { // need to join in select sql username and user_is_active field of author of uc


        if (isset($filtersArray['order_is_active'])) {
            if ( !$order_table_joined ) {
                $order_table_joined= true;
                $quoteModel->join( \DB::raw($orders_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('oi.order_id') );
                $quoteModel->join( \DB::raw($order_translations_table_name . ' as s_t '), \DB::raw('s_t.order_id'), '=', \DB::raw('s.id') );
                $quoteModel->where( \DB::raw('s_t.locale'), '=', $filtersArray['locale'] );
            }
            $quoteModel->where(DB::raw('s.is_active'), '=', $filtersArray['order_is_active']);
        }

//        show_related_order_votes
        $order_vote_table_name= DB::getTablePrefix().( with(new OrderVote)->getTableName() );
        if ( !empty($filtersArray['show_related_order_votes']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $order_vote_table_name . ' as sv where sv.order_id = ' . 'oi.order_id ) as related_order_votes_count' .
                                           ', ( select sum(sv.vote) from ' . $order_vote_table_name . ' as sv where sv.order_id = ' . 'oi.order_id ) as related_order_votes_sum';
        }


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new OrderItem)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new OrderItem)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $orderItemsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $orderItemsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $orderItemsList = $quoteModel->get();
            $data_retrieved= true;
        }

        return $orderItemsList;

    } // public static function getOrderItemsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

}