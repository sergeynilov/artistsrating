<?php

namespace App;

use DB;
use App\MyAppModel;
use App\library\ListingReturnData;

class UsersGroups__DEL extends MyAppModel
{

    protected $table = 'users_groups';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'group_id', 'status'];

    public static function getUsersGroupsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 )
    {

        if (empty($order_by)) $order_by = 'ug.user_id'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';

        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $users_groups_table_name= with(new UsersGroups)->getTableName();
        $quoteModel= UsersGroups::from(  \DB::raw(DB::getTablePrefix().$users_groups_table_name.' as ug' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT ) {
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'ug.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['user_id'])) {
            $quoteModel->where( \DB::raw('ug.user_id'), '=', $filtersArray['user_id'] );
        }

        if (!empty($filtersArray['group_id'])) {
            $quoteModel->where( \DB::raw('ug.group_id'), '=', $filtersArray['group_id'] );
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }
//                                 echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if ( !empty($filtersArray['show_group_name'])  ) {
//            echo '<pre>INSIDE::::</pre>';
            $groups_table_name= DB::getTablePrefix() . ( with(new Group())->getTableName() );
            $additive_fields_for_select .= ', g.name as group_name' ;
            $quoteModel->join( \DB::raw($groups_table_name . ' as g '), \DB::raw('g.id'), '=', \DB::raw('ug.group_id') );
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of ug table */
        $items_per_page= with(new UsersGroups)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new UsersGroups)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $usersGroupsList = $quoteModel->paginate($items_per_page, null, null, $page_param);
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $usersGroupsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $usersGroupsList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $usersGroupsList;

    } // public static function getUsersGroupsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getUsersGroupsTextLabel( int $user_id, bool $show_no_active_labels= false)
    {

        $usersGroupsList   = UsersGroups::getUsersGroupsList(ListingReturnData::LISTING, ['user_id' => $user_id, 'show_group_name' => 1]);
        $users_groups_text = '';
        foreach ($usersGroupsList as $next_key => $nextUserGroup) {
            $status_label = '';
            if ($nextUserGroup->status == 'N' and $show_no_active_labels) {
                $status_label = ' ( <span class="text-warning">New(Waiting for confirmation)</span> )';
            }
            if ($nextUserGroup->status == 'I' and $show_no_active_labels ) {
                $status_label = ' ( <span class="text-warning">Inactive</span> )';
            }
            $users_groups_text .= $nextUserGroup->group_name . $status_label . ', ';
        }
        $users_groups_text= with(new UsersGroups)->trimRightSubString($users_groups_text, ', ');
        return $users_groups_text;
    }

}