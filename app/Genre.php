<?php
namespace App;

use DB;
use Cviebrock\EloquentSluggable\Sluggable;
use App\MyAppModel;
use App\User;
use App\SongGenre;
use App\library\ListingReturnData;
use App\Http\Traits\funcsTrait;

class Genre extends MyAppModel
{

    protected $table = 'genres';
    protected $primaryKey = 'id';
    public $timestamps = false;
    private static $genrePublishedValueArray = [ true => 'Is published', false => 'Not published' ];

    use funcsTrait;
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'localename'
            ]
        ];
    }

    public function getLocalenameAttribute() {
        $request= request();
        $requestData= $request->all();
        return $requestData['name'];
    }

    protected static function boot() {
        parent::boot();
    }


    protected $fillable = ['name', 'description', 'published', 'updated_at'];


    /* return array of key value/label pairs based on self::$genrePublishedValueArray - db enum key values/labels implementation */
    public static function getGenrePublishedValueArray(bool $key_value= true) : array
    {
        $resArray = [];
        foreach (self::$genrePublishedValueArray as $key => $value) {
            if ( $key_value ) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    /* return label of key value/label pairs by key value  based on self::$genrePublishedValueArray - db enum key values/labels implementation */
    public static function getGenrePublishedLabel(string $published) : string
    {
        if (!empty(self::$genrePublishedValueArray[$published])) {
            return self::$genrePublishedValueArray[$published];
        }
        return '';
    }


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getGenresList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
        if (empty($order_by)) $order_by = 'g.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $genre_table_name= with(new Genre)->getTableName();
        $quoteModel= Genre::from(  \DB::raw(DB::getTablePrefix().$genre_table_name.' as g' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'g.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Genre::dbStrLower('name', false, false) . ' like ' . Genre::dbStrLower( $filtersArray['name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Genre::dbStrLower('name', false, false) . ' like ' . Genre::dbStrLower( $filtersArray['name'], true,true ) . ' OR ' . Genre::dbStrLower('description', false, false) . ' like ' . Genre::dbStrLower( $filtersArray['name'], true,true ) .
                                       ' OR ' . Genre::dbStrLower('description', false, false) . ' like ' . Genre::dbStrLower( $filtersArray['name'], true,true ) . ' ) ');
            }
        }

        if (isset($filtersArray['published'])) {
            $quoteModel->where( DB::raw('g.published'), '=', $filtersArray['published'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("g.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("g.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if (isset($filtersArray['locale'])) {
            $genre_translations_table_name=  DB::getTablePrefix().with(new GenreTranslation)->getTableName();
            $quoteModel->join(\DB::raw($genre_translations_table_name . ' as g_t '), \DB::raw('g_t.genre_id'), '=', \DB::raw('g.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", name, description ";
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $song_genre_table_name= DB::getTablePrefix().( with(new SongGenre)->getTableName() );
        if ( !empty($filtersArray['show_related_songs_count']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $song_genre_table_name . ' as sg where sg.genre_id = ' . 'g.id ) as related_songs_count';
        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Genre)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Genre)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $genresList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $genresList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $genresList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $genresList;

    } // public static function getGenresList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;

        if ( !empty($additiveParams['locale']) ) {
            $fields_for_select= 'g.*';
            $additive_fields_for_select= '';
            $genre_table_name= with(new Genre)->getTableName();
            $quoteModel= Genre::from(  \DB::raw(DB::getTablePrefix().$genre_table_name.' as g' ));
            $quoteModel->where( \DB::raw('g.id'), '=', $id );
//            $users_table_name= DB::getTablePrefix() . ( with(new User)->getTableName() );

//            echo '<pre>$genre->$additiveParams::'.print_r($additiveParams,true).'</pre>';
            if (isset($additiveParams['locale'])) {
                $genre_translations_table_name=  DB::getTablePrefix().with(new GenreTranslation)->getTableName();
                $quoteModel->join(\DB::raw($genre_translations_table_name . ' as g_t '), \DB::raw('g_t.genre_id'), '=', \DB::raw('g.id'));
                $quoteModel->where( 'locale', '=', $additiveParams['locale'] );
                $additive_fields_for_select .= ", g_t.name, g_t.description";
            }

//            echo '<pre>$genre->$additive_fields_for_select::'.print_r($additive_fields_for_select,true).'</pre>';

//            $quoteModel->join( \DB::raw($users_table_name . ' as u_m '), \DB::raw('u_m.id'), '=', \DB::raw('a.manager_id') );
            $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of g table */
            $quoteModel->select( \DB::raw($fields_for_select) );
            $genreRows = $quoteModel->get();
            if (!empty($genreRows[0]) and get_class($genreRows[0]) == 'App\Genre' ) {
                $is_row_retrieved= true;
                $genre= $genreRows[0];
            }


            // 'show_future_concerts_count'=> 1 , 'show_related_votes'=> 1
        } // if ( !empty($additiveParams['show_related_songs_count']) or  !empty($additiveParams['show_future_concerts_count']) or !empty($additiveParams['show_related_votes'])  or !empty
        if ( !$is_row_retrieved ) {
            $genre = Genre::find($id);
        }

        if (empty($genre)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $genre= with(new Genre)->substituteNullValues($genre, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $genre;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $genre_id ) : array
    {
        $validationRulesArray = [
            'published'    => 'required|in:'.with( new Genre)->getValueLabelKeys( Genre::getGenrePublishedValueArray(false) ),
        ];
        return $validationRulesArray;
    }

    /* check if provided name is unique for genres.name field */
/*    public static function getSimilarGenreByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Genre::whereRaw( Genre::dbStrLower('name', false, false) .' = '. Genre::dbStrLower( Genre::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }*/


    public static function fillTranslations()
    {
//        return;
        $langsInSystem = \Config::get('app.langsInSystem');
        $GenreItemsList = Genre::getGenresList(ListingReturnData::LISTING, []);
        foreach ($GenreItemsList as $nextGenre) {
            $genre_id         = $nextGenre->id;
            $name             = $nextGenre->name;
            $description      = $nextGenre->description;
            foreach( $langsInSystem as $next_lang_key=>$next_lang_label ) {
                $newGenreTranslation= new GenreTranslation();
                $newGenreTranslation->genre_id= $genre_id;
                if ( $next_lang_key == 'en' ) {
                    $newGenreTranslation->name        = $name;
                    $newGenreTranslation->description = $description;
                } else {
                    $newGenreTranslation->name        = $next_lang_label . ' VERSION : ' . $name;
                    $newGenreTranslation->description = $next_lang_label . ' VERSION : ' . $description;
                }
                $newGenreTranslation->locale= $next_lang_key;

                $newGenreTranslation->created_at= now();
                $newGenreTranslation->save();
            }

        }
    }

}