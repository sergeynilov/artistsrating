<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use App\Config;

use Auth;
use Carbon\Carbon;

use App\Http\Traits\funcsTrait;
use App\MyAppModel;
use App\Settings;
use App\User;
use App\library\ListingReturnData;
use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class GenreTranslation extends MyAppModel
{
    use funcsTrait;
    protected $table = 'genre_translations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['genre_id', 'locale', 'name', 'description', 'created_at'];

    public static function getRowByIdLocale( int $genre_id,  string $locale, $return_count= false )
    {
        $quoteModel = GenreTranslation::where( 'genre_id',  $genre_id )->where( 'locale',  $locale );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get()->first();
        if ( empty($retRow) ) return false;
        return $retRow;
    }

    public static function getSimilarGenreTranslationByName( string $name, string $locale= '', int $id= null, $return_count = false )
    {
        $quoteModel = GenreTranslation::whereRaw( GenreTranslation::dbStrLower('name', false, false) .' = '. GenreTranslation::dbStrLower(GenreTranslation::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( !empty( $locale ) ) {
            $quoteModel = $quoteModel->where( 'locale', $locale );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getValidationRulesArray( $genre_id, $locale ) : array
    {
        $additional_name_validation_rule= 'check_genre_translations_unique_by_name:'.( !empty($genre_id)?$genre_id:'') .','. $locale;
        $validationRulesArray = [
            'name'               => 'required|max:100|'.$additional_name_validation_rule,
            'description'        => 'required',
        ];
        return $validationRulesArray;

    }


}