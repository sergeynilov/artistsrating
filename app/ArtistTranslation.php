<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;
use DB;
use App\Config;

use Auth;
use Carbon\Carbon;

use App\Http\Traits\funcsTrait;
use App\MyAppModel;
use App\Settings;
use App\User;
use App\library\ListingReturnData;
use App\library\PgDataType;


/* Content Manage System rows, used for email templates', pages of content and blog articles */
class ArtistTranslation extends MyAppModel
{
    use funcsTrait;
    protected $table = 'artist_translations';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['artist_id', 'locale', 'name', 'info', 'created_at'];

    public static function getRowByIdLocale( int $artist_id,  string $locale, $return_count= false )
    {
        $quoteModel = ArtistTranslation::where( 'artist_id',  $artist_id )->where( 'locale',  $locale );
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get()->first();
        if ( empty($retRow) ) return false;
        return $retRow;
    }

    public static function getSimilarArtistTranslationByName( string $name, string $locale= '', int $id= null, $return_count = false )
    {
//        with(new Artist)->debToFile(print_r($name,true),' 000 getSimilarArtistTranslationByName  -1 $name::');
//        with(new Artist)->debToFile(print_r($id,true),' 000 getSimilarArtistTranslationByName  -2 $id::');
        $quoteModel = ArtistTranslation::whereRaw( ArtistTranslation::dbStrLower('name', false, false) .' = '. ArtistTranslation::dbStrLower(ArtistTranslation::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'artist_id', '!=' , $id );
        }
        if ( !empty( $locale ) ) {
            $quoteModel = $quoteModel->where( 'locale', $locale );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    public static function getValidationRulesArray( $artist_id ) : array
    {
        $additional_name_validation_rule= ''; //'check_artist_unique_by_name:'.( !empty($artist_id)?$artist_id:'');
        $validationRulesArray = [
            'name'        => 'required|max:50|'.$additional_name_validation_rule,
            'info'        => '',
        ];
        return $validationRulesArray;

    }


}