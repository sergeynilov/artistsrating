<?php
namespace App;

use Carbon\Carbon;
use DB;
use App\MyAppModel;
use App\User;
use App\Song;
use App\library\ListingReturnData;

class SongArtist extends MyAppModel
{

    protected $table = 'song_artists';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function boot() {
        parent::boot();
    }

    protected $fillable = [ 'artist_id', 'song_id' ];


    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getSongArtistsList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 'sa.created_at'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $song_artist_table_name= with(new SongArtist)->getTableName();
        $quoteModel= SongArtist::from(  \DB::raw(DB::getTablePrefix().$song_artist_table_name.' as sa' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 'sa.*';

        if (!empty($filtersArray['artist_id'])) {
            $quoteModel->where( DB::raw('sa.artist_id'), '=', $filtersArray['artist_id'] );
        }

        // 'song_id' => $song_id, 'show_artist_name' => 1, 'show_related_song_votes' => 1, 'artist_is_active'=> true],       'song_ordering', 'desc');
        if (!empty($filtersArray['song_id'])) {
            $quoteModel->where( DB::raw('sa.song_id'), '=', $filtersArray['song_id'] );
        }

        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("sa.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("sa.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

        $artists_table_name= DB::getTablePrefix() . ( with(new Artist)->getTableName() );
        $artist_translations_table_name=  DB::getTablePrefix().with(new ArtistTranslation)->getTableName();
        $artist_table_joined= false;
        /*        if (isset($filtersArray['locale'])) {
            $quoteModel->join(\DB::raw($artist_translations_table_name . ' as a_t '), \DB::raw('a_t.artist_id'), '=', \DB::raw('a.id'));
            $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
            $additive_fields_for_select .= ", name, info";
        }
*/
        if ( !empty($filtersArray['show_artist_name']) and !empty($filtersArray['locale'])  ) { // need to join in select sql username and user_is_active field of author
            // of uc
            if ( !$artist_table_joined ) {
//                echo '<pre>-1 $artist_table_joined::'.print_r($artist_table_joined,true).'</pre>';
                $quoteModel->join( \DB::raw($artists_table_name . ' as a '), \DB::raw('a.id'), '=', \DB::raw('sa.artist_id') );
                $quoteModel->join(\DB::raw($artist_translations_table_name . ' as a_t '), \DB::raw('a_t.artist_id'), '=', \DB::raw('a.id'));
                $quoteModel->where( 'locale', '=', $filtersArray['locale'] );
                $artist_table_joined= true;
            }
//            echo '<pre>-2 $artist_table_joined::'.print_r($artist_table_joined,true).'</pre>';

            $additive_fields_for_select .= ', a.id as artist_id, a_t.name as artist_name, a.slug as artist_slug, a.site as artist_site, a.is_active as artist_is_active, a.ordering as artist_ordering, a_t.info as artist_info';
        } // if ( !empty($filtersArray[show_artist_name])  ) { // need to join in select sql username and user_is_active field of author of uc
        if (isset($filtersArray['song_is_active'])) {
//            echo '<pre>-3 $artist_table_joined::'.print_r($artist_table_joined,true).'</pre>';
            if ( !$artist_table_joined ) {
                $artist_table_joined= true;
//                echo '<pre>-4 $artist_table_joined::'.print_r($artist_table_joined,true).'</pre>';
                $quoteModel->join( \DB::raw($artists_table_name . ' as a '), \DB::raw('a.id'), '=', \DB::raw('sa.artist_id') );
            }
            $quoteModel->where(DB::raw('s.is_active'), '=', $filtersArray['song_is_active']);
        }

//        echo '<pre>$additive_fields_for_select::'.print_r($additive_fields_for_select,true).'</pre>';
//        die("-1 XXZ");


        $songs_table_name= DB::getTablePrefix() . ( with(new Song)->getTableName() );
        $song_translations_table_name=  DB::getTablePrefix().with(new SongTranslation)->getTableName();
        $song_table_joined= false;
        if ( !empty($filtersArray['show_song_name']) and !empty($filtersArray['locale'])  ) { // need to join in select sql username and user_is_active field of author
            if ( !$song_table_joined ) {
                $quoteModel->join( \DB::raw($songs_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('sa.song_id') );
                $quoteModel->join( \DB::raw($song_translations_table_name . ' as s_t '), \DB::raw('s_t.song_id'), '=', \DB::raw('s.id') );
                $quoteModel->where( \DB::raw('s_t.locale'), '=', $filtersArray['locale'] );
                $song_table_joined= true;
            }
            $additive_fields_for_select .= ', s.id as song_id, s_t.title as song_title, s.slug as song_slug, s.slug as song_slug, s_t.short_descr as song_short_descr, s_t.description as song_description, s.is_active as song_is_active, s.ordering as song_ordering';
        } // if ( !empty($filtersArray[show_song_name])  ) { // need to join in select sql username and user_is_active field of author of uc


        if (isset($filtersArray['song_is_active'])) {
            if ( !$song_table_joined ) {
                $song_table_joined= true;
                $quoteModel->join( \DB::raw($songs_table_name . ' as s '), \DB::raw('s.id'), '=', \DB::raw('sa.song_id') );
                $quoteModel->join( \DB::raw($song_translations_table_name . ' as s_t '), \DB::raw('s_t.song_id'), '=', \DB::raw('s.id') );
                $quoteModel->where( \DB::raw('s_t.locale'), '=', $filtersArray['locale'] );
            }
            $quoteModel->where(DB::raw('s.is_active'), '=', $filtersArray['song_is_active']);
        }

//        show_related_song_votes
        $song_vote_table_name= DB::getTablePrefix().( with(new SongVote)->getTableName() );
        if ( !empty($filtersArray['show_related_song_votes']) ) {
            $additive_fields_for_select .= ', ( select count(*) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 'sa.song_id ) as related_song_votes_count' .
                                           ', ( select sum(sv.vote) from ' . $song_vote_table_name . ' as sv where sv.song_id = ' . 'sa.song_id ) as related_song_votes_sum';
        }


        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new SongArtist)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new SongArtist)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $songArtistsList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $songArtistsList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $songArtistsList = $quoteModel->get();
            $data_retrieved= true;
        }

        return $songArtistsList;

    } // public static function getSongArtistsList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {


}

