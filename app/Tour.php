<?php
namespace App;

use Carbon\Carbon;
use DB;
use App\MyAppModel;
use App\User;
use App\library\ListingReturnData;
//use App\Events\TourUpdatingEvent;

class Tour extends MyAppModel
{

    protected $table = 'tours';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected static function boot() {
        parent::boot();
    }


    protected $fillable = [ 'artist_id', 'tour_name', 'start_date', 'end_date', 'ordering',  'color',  'description' ];

    /* get list of data with filter parameters given in $filtersArray and $listingReturnData as data returned type */
    public static function getToursList( int $listingReturnData, array $filtersArray = [], string $order_by = '', string $order_direction = '', int $page_param= 0 ) {
//        echo '<pre>$filtersArray::'.print_r($filtersArray,true).'</pre>';
        if (empty($order_by)) $order_by = 't.name'; // set default ordering
        if (empty($order_direction)) $order_direction = 'asc';
        $limit = ! empty( $filtersArray['limit'] ) ? $filtersArray['limit'] : '';

        $tour_table_name= with(new Tour)->getTableName();
        $quoteModel= Tour::from(  \DB::raw(DB::getTablePrefix().$tour_table_name.' as t' ));
        if ( $listingReturnData != ListingReturnData::ROWS_COUNT  ) { // getting rows numbers do not need $order_by/$order_direction parameters
            $quoteModel->orderBy(\DB::raw($order_by), ((strtolower($order_direction) == 'desc' or strtolower($order_direction) == 'asc') ? $order_direction : ''));
        }

        $additive_fields_for_select= "";
        $fields_for_select= 't.*';

        /* Set filter condition for all nonempty values in $filtersArray */
        if (!empty($filtersArray['tour_name'])) {
            if ( empty($filtersArray['in_description']) ) {
                $quoteModel->whereRaw( Tour::dbStrLower('tour_name', false, false) . ' like ' . Tour::dbStrLower( $filtersArray['tour_name'], true,true ));
            } else {
                $quoteModel->whereRaw( ' ( '.Tour::dbStrLower('tour_name', false, false) . ' like ' . Tour::dbStrLower( $filtersArray['tour_name'], true,true ) . ' OR ' . Tour::dbStrLower('description', false, false) . ' like ' . Tour::dbStrLower( $filtersArray['tour_name'], true,true ) .
                                       ' OR ' . Tour::dbStrLower('description', false, false) . ' like ' . Tour::dbStrLower( $filtersArray['tour_name'], true,true ) . ' ) ');
            }
        }

        $is_participant_user_id_joined= false;
        if (!empty($filtersArray['participant_user_id'])) {
            $tour_participant_table_name= DB::getTablePrefix().with(new TourParticipant)->getTableName();
            $quoteModel->join( \DB::raw($tour_participant_table_name . ' as ucp '), \DB::raw('ucp.tour_id'), '=', \DB::raw('t.id') );
            $quoteModel->where( \DB::raw('ucp.user_id'), '=', $filtersArray['participant_user_id'] );
            $is_participant_user_id_joined= true;
        }


        if (!empty($filtersArray['artist_id'])) {
            $quoteModel->where( DB::raw('t.artist_id'), '=', $filtersArray['artist_id'] );
        }


        /*CREATE TABLE public.rt_tours (
	id int4 NOT NULL,
	artist_id int4 NOT NULL,
	tour_name varchar(100) NOT NULL,
	start_date date NOT NULL,
	end_date date NOT NULL,
	"ordering" int4 NULL,
	color varchar(20) NOT NULL,
	description text NOT NULL,
	created timestamp NOT NULL DEFAULT now(),*/
        // 'only_future'

        if (!empty($filtersArray['only_future'])) {
            $today= Carbon::today()->format('Y-m-d');
            $quoteModel->whereRaw( "start_date >'".$today . " 00:00:00'" );
//            $quoteModel->whereRaw( DB::raw("t.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_from'])) {
            $quoteModel->whereRaw( DB::raw("t.created_at >='").$filtersArray['created_at_from'] . "'" );
        }
        if (!empty($filtersArray['created_at_till'])) {
            $quoteModel->whereRaw( DB::raw("t.created_at <='").$filtersArray['created_at_till'] . " 23:59:59'");
        }

        if ( ! empty( $limit ) and (int) $limit > 0 ) {
            $quoteModel = $quoteModel->take( $limit );
        }
        if ( $listingReturnData == ListingReturnData::ROWS_COUNT ) { /* return number of rows with filter parameters given in $filtersArray */
            return $quoteModel->get()->count();
        }

//        $tour_message_table_name= DB::getTablePrefix().( with(new TourMessage)->getTableName() );
//        if ( !empty($filtersArray['show_tour_messages_count_by_user_id']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $tour_message_table_name . ' as ucm_1 where ucm_1.tour_id = ' . 't.id and ucm_1.user_id = '.$filtersArray['show_tour_messages_count_by_user_id'].' ) as tour_messages_count_by_user_id';
//        }

//        if ( !empty($filtersArray['show_tour_messages_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $tour_message_table_name . ' as ucm where ucm.tour_id = ' . 't.id ) as tour_messages_count';
//        }

        // show_tour_participants_count
//        $tour_participant_table_name= DB::getTablePrefix().( with(new TourParticipant)->getTableName() );
//        if ( !empty($filtersArray['show_tour_participants_count']) ) {
//            $additive_fields_for_select .= ', ( select count(*) from ' . $tour_participant_table_name . ' as ucm where ucm.tour_id = ' . 't.id ) as tour_participants_count';
//        }

        $fields_for_select.= ' ' . $additive_fields_for_select; /* add all custom fields to fields of t table */
        $items_per_page= with(new Tour)->getItemsPerPage();
        $quoteModel->select( \DB::raw($fields_for_select) );
        $data_retrieved= false;
        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_PARAM and (!empty($page_param) and with(new Tour)->isPositiveNumeric($page_param) ) and $items_per_page > 0 ) {  /* get list of data with filter parameters given in $filtersArray and page number = $page_param  */
            $limit_start= ($page_param - 1) * $items_per_page ;
            $quoteModel->offset( $limit_start );
            $quoteModel->take( $items_per_page );
            $toursList = $quoteModel->get();
            $data_retrieved= true;
        }

        if ( $listingReturnData == ListingReturnData::PAGINATION_BY_URL and !$data_retrieved ) { /* get list of data with filter parameters given in $filtersArray and page number is page parameter (default 1) in url */
            $toursList = $quoteModel->paginate($items_per_page);
            $data_retrieved= true;
        }

        if ( !$data_retrieved ) {
            $toursList = $quoteModel->get();
            $data_retrieved= true;
        }
        return $toursList;

    } // public static function getToursList( int $listingReturnData, array $filtersArray = [], string $order_by = , string $order_direction = , int $page_param= 0 ) {

    public static function getRowById( int $id, array $additiveParams= [] )
    {
        if (empty($id)) return false;
        $is_row_retrieved= false;
        if ( !$is_row_retrieved ) {
            $tour = Tour::find($id);
        }

        if (empty($tour)) return false;
        if (!empty($additiveParams['set_null_fields_space']) and is_array($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space
            $tour= with(new Tour)->substituteNullValues($tour, $additiveParams['set_null_fields_space']);
        } // if (!empty($additiveParams['set_null_fields_space']) ) { // for fields in this option substitute null into empty space

        return $tour;
    } // public function getRowById( int $id, array $additiveParams= [] )

    public static function getValidationRulesArray( $tour_id, bool $check_tour_participant_selected= false ) : array
    {
//        echo '<pre>++ getValidationRulesArray::'.print_r($_POST,true).'</pre>';
        $additional_name_validation_rule= 'check_tour_unique_by_name:'.( !empty($tour_id)?$tour_id:'');
        $validationRulesArray = [
            'name'         => 'required|max:255|'.$additional_name_validation_rule,
            'description'  => 'required',
            'creator_id'   => 'nullable|exists:'.( with(new User)->getTableName() ).',id',
            'task_id'      => 'nullable|exists:'.( with(new Task)->getTableName() ).',id',
            'tourParticipantsArray'=> [ 'required',new CheckTourParticipantSelected() ]
        ];

        return $validationRulesArray;
    }

    /* check if provided name is unique for tours.name field */
    public static function getSimilarTourByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Tour::whereRaw( Tour::dbStrLower('name', false, false) .' = '. Tour::dbStrLower( Tour::pgEscape($name), true,false) );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}

