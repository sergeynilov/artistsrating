<?php

// php artisan config:cache
$base_url= '';
/*$base_url = 'http://local-artists-rating.com';
if ( !empty($_SERVER['SERVER_NAME']) ) {
    $pos      = strpos($_SERVER['SERVER_NAME'], 'ec2-34-212-140-26.us-west-2.compute.amazonaws.com');
    if ($pos !== false) {
        $base_url = 'http://ec2-34-212-140-26.us-west-2.compute.amazonaws.com/tasks/public';
    }
}*/
//echo '<pre> false)::'.print_r(env('APP_DEBUG', false),true).'</pre>';
//                  die("-1 XXZ=======");
return [

    'valid_color_format'=> '~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~',
//    'valid_color_format'=> '/^(#[a-f0-9]{6}|black|green|silver|gray|olive|white|yellow|maroon|navy|red|blue|purple|teal|fuchsia|aqua)$/i',
    'valid_nice_name_format'     => '~^[a-zA-Z]*$~',
    'ff_valid_phone_format'      =>  '(01)[0-9]{9}',
    'valid_percent_format'       =>  '^\d*(\.\d{1,2})?$',
    'valid_geographic_coordinate_format' =>  '^[\-]?\d*(\.\d{1,7})?$',
    'valid_money_format'         =>  '^\d+(\.\d{1,2})?$',
    'valid_shipping_decimal_format' =>  '^\d*(\.\d{1,2})?$',
    'valid_phone_format'         =>  '(01)[0-9]{9}',
    'js_moment_date_format'      => 'Do MMMM, YYYY',
//    'js_moment_datetime_format'  => 'Do MMMM, YYYY h:mm:ss A',
    'js_moment_datetime_format'  => 'Do MMMM, YYYY h:mm A',
    'pickdate_format_submit'     => 'yyyy-mm-dd',
    'pickdate_format_view'       => 'd mmmm, yyyy',
    'datetime_carbon_format'     => 'Y-m-d H:i:s',
    'datetime_carbon_format_miliseconds'     => 'Y-m-d H:i:s.u',
    'date_carbon_format'         => 'Y-m-d',
    'date_carbon_format_as_text' => '%d %B, %Y',

    'main_image_recommended_height'=>"240px",
    'main_image_recommended_width'=>"320px",

    'datepicker_min_year'        => 1980,
    'datepicker_max_year'        => 2099,

    '1_song_price'               => 2000.25,
    '1_artist_price'             => 1500.50,
    '5_songs_discount'           => 10,
    '5_artists_discount'         => 10,
//    '1_song_price'               => 0.25,
//    '1_artist_price'             => 0.56,
//    '5_songs_discount'           => 10,
//    '5_artists_discount'         => 10,


    'currency'                   => 'USD dollar',
    'currency_short'             => '$',
    'currency_left'              => true,

/*    'avatar_dimension_label'     => 'Must be selected image max width 96px max height 64px, ratio 3 / 2',
    'avatar_max_width'           => 96,
    'avatar_max_height'          => 64,
    'avatar_max_ratio'           => 3 / 2,*/


    /*            'avatar' => [
                'string',
                'max:100',
//                Rule::dimensions()->maxWidth(96)->maxHeight(64)->ratio(3 / 2),
                Rule::dimensions()->maxWidth(6)->maxHeight(4)->ratio(3 / 2),
*/
    'images_extensions'=> ['png', 'webp', 'jpg', 'jpeg', 'gif'],
    'uploaded_documents_extensions'=> ['png', 'webp', 'jpg', 'jpeg', 'gif', 'doc', 'xls', 'zip', 'rar'],
    'UPLOADS_ARTIST_IMAGES_DIR'=> 'artist-images/',
    'UPLOADS_ARTIST_IMAGES_URL'=> 'artist-images/',

    'UPLOADS_USER_AVATAR_DIR'=> 'user-avatars/',
    'UPLOADS_USER_AVATAR_URL'=> 'user-avatars/',

    'UPLOADS_CMS_ITEM_IMAGE_DIR'=> 'cms-item-images/',
    'UPLOADS_CMS_ITEM_IMAGE_URL'=> 'cms-item-images/',
    'default_song_image_url'=> 'images/default_song.png',

    'API_VERSION' => env('API_VERSION', '1'),
    'GOOGLE_API_KEY' => env('GOOGLE_API_KEY'),
    'sleep_in_seconds' => 0.5,

    'futurePastColors' => [ 'is_past'=>'#bfbda2', 'is_today'=>'#ff280c', 'is_tomorrow'=>'#fc8865', 'is_future'=>'#ffffff' ],


    'chartColors' => [ '#bfbda2', '#005500', '#00007f', '#ffff00', '#ff8e72', '#ff280c', '#00c300', '#070707', '#4d647f', '#fff79e' ],

    
    'votingColors' => [ 1=>'lime lighten-4', 2=>'brown lighten-4', 3=>'red lighten-4', 4=>'red lighten-2', 5=>'red darken-2' ],
    'rowsPerPageItems' => [ ["text"=>"2 rows","value"=>2], ["text"=>"5 rows","value"=>5], ["text"=>"10 rows","value"=>10], ["text"=>"20 rows","value"=>20], ["text"=>"50 rows","value"=>50], ["text"=>"All rows","value"=>-1] ],


    
    'menuIconsArray'=> [ 'genre' => 'queue_music', 'artist' => 'accessible', 'user' => 'face', 'home' => 'home',  'exit' => 'exit_to_app', 'concert' => 'music_note',
        'song' => 'library_music', 'image' => 'add_a_photo', 'country' => 'map', 'cms_item' => 'content_paste',   'not_found' => 'not_interested', 'vote'  => 'highlight',
        'charts' => 'report', 'settings' => 'gamepad', 'msg' => 'info', 'shopping_cart' => 'shopping_cart', 'checkout' => 'check_circle_outline', 'coupon' => 'aspect_ratio',
        'users_role_group'=> 'group', 'languages'=> 'language'],


/*
|--------------------------------------------------------------------------
| Application Name
|--------------------------------------------------------------------------
|
| This value is the name of your application. This value is used when the
| framework needs to place the application's name in a notification or
| any other location as required by the application or its packages.
|
*/

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

//    'base_url' => $base_url,
    'base_url' => env('APP_URL', $base_url),
    'url' => env('APP_URL', $base_url),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'UTC',

    'langsInSystem' => [  'en'=>'English', 'ua'=>'Ukrainian' /*'es'=>'Spain', */],

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'frontend_locale' => 'en',
    'backend_locale' => 'en',
    'locale' => 'en',

    'SHOW_DEMO_ADMIN_ON_LOGIN' => 1,
    'DEMO_ADMIN_EMAIL'         => 'admin@mail.com',
    'DEMO_ADMIN_PASSWORD'      => '111111',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        'Laracasts\Utilities\JavaScript\JavaScriptServiceProvider',
        Barryvdh\Debugbar\ServiceProvider::class,
        'Intervention\Image\ImageServiceProvider',
        Illuminate\View\ViewServiceProvider::class,
        Laravel\Socialite\SocialiteServiceProvider::class,
        



        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

//        'Markdown' => GrahamCampbell\Markdown\Facades\Markdown::class,
        MartinLindhe\VueInternationalizationGenerator\GeneratorProvider::class,

        \Torann\GeoIP\GeoIPServiceProvider::class,
        Spatie\Permission\PermissionServiceProvider::class,
        Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class,
        Gloudemans\Shoppingcart\ShoppingcartServiceProvider::class,
        Clockwork\Support\Laravel\ClockworkServiceProvider::class,

        \SocialiteProviders\Manager\ServiceProvider::class
//        'PDF' => Barryvdh\DomPDF\Facade::class,


    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Debugbar' => Barryvdh\Debugbar\Facade::class,
        'Image' => 'Intervention\Image\Facades\Image',

        // Replace that with the path to the service provider
        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,

        'Socialite' => Laravel\Socialite\Facades\Socialite::class,


        'GeoIP' => \Torann\GeoIP\Facades\GeoIP::class,
        'Cart' => Gloudemans\Shoppingcart\Facades\Cart::class,

    ],

];
