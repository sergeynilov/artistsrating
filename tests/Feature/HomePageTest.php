<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomePageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/#/home')
            ->assertSee('Home of Artists Rating')
            ->assertDontSee('Rails')
        ;

//        $response->assertStatus(200);
        $this->assertEquals(200, $response->status());
    }
}
