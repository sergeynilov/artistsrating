<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/auth/{provider}/callback', 'Auth\LoginController@handleSocialiteProviderCallback');

// http://local-artists-rating.com/auth/google/callback?state=lwXJEcbLNxnzXE8R0QjJRjsFANKBocAMIo7pzEFu&code=4/AAA57h6DD0UTTMHlclm9309nrOctAOfXHKdLMnEH8aRpE86UjQjwccVsFUpr75F0HJ0jendCDsFkfLn2aBUWO0Q&authuser=0&hd=softreactor.com&session_state=b719716a66d6b2cadba6d4f3b29346599d23daa6..2d6d&prompt=none#
// http://local-artists-rating.com/auth/google/callback?state=X9wJypzb8Ci0Z2kFUNjyPsgbk2FW1aApiNS3EBYZ&code=4/AACWMFv6bDGDmUba5r9UmP0Pbl5AKxopTcp4TF0QDUKdWkEWVK7im8FryO-Gr8kjIKViA8nnxcMTbAIolx764Hg&authuser=0&hd=softreactor.com&session_state=b719716a66d6b2cadba6d4f3b29346599d23daa6..2d6d&prompt=none#
// auth/google/callback
//Route::get('/auth/{provider}/twitter', 'Auth\LoginController@handleProviderCallback');


// app/Http/Controllers/Auth/RegisterController.php
//Route::get('auth_register', 'Auth\RegisterController@index')->name('auth_register');
Route::get('auth_register', 'Auth\RegisterController@listing')->name('auth_register');

Route::get('home', 'HomeController@index')->name('home');
Route::get('privacy-policy', 'HomeController@cms_item_index')->name('cms_item_index');
Route::get('privacy-policy', 'HomeController@cms_item_index')->name('cms_item_index');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


//Route::post('update_cms_items_locale/{cms_item_id}', 'Backend\CmsItemsController@update_cms_items_locale')->name('update_cms_items_locale')->middleware('WorkTextString');

Route::group(['middleware' => [], 'prefix' => 'api', 'as' => ''], function () {

    // http://local-artists-rating.com/privacy-policy

    //                 return redirect("/home#/admin/dashboard/successful_login/" . urlencode($loggedUser->name));
    Route::post('switch_locale', 'HomeController@switch_locale')->name('switch_locale');




    Route::get('song_page_dictionaries', 'SongPageController@song_page_dictionaries')->name('song_page_dictionaries');
    Route::post('song_page_show', 'SongPageController@show')->name('song_page_show');
    Route::post('make_song_voting', 'SongPageController@make_song_voting')->name('make_song_voting');
    Route::get('get_most_rating_songs/{locale}', 'SongPageController@get_most_rating_songs')->name('get_most_rating_songs');


    Route::get('artist_page_dictionaries', 'ArtistPageController@artist_page_dictionaries')->name('artist_page_dictionaries');

    //http://local-artists-rating.com/api/artist_page_show/beyonce
//    Route::get('artist_page_show/{artist_slug}', 'ArtistPageController@show')->name('artist_page_show');


    Route::post('artist_page_show', 'ArtistPageController@show')->name('artist_page_show');
    Route::post('make_artist_voting', 'ArtistPageController@make_artist_voting')->name('make_artist_voting');
    Route::get('get_most_rating_artists/{locale}', 'ArtistPageController@get_most_rating_artists')->name('get_most_rating_artists');

    Route::post('login', 'HomeController@login');
    Route::get('logout', 'HomeController@logout');
    Route::post('logout', 'HomeController@logout');
    Route::post('user_register', 'HomeController@user_register');
    Route::post('/{locale}s_by_alias', 'HomeController@get_cms_items_by_alias');
    Route::get('get_cms_item/{alias}/{locale}', 'HomeController@get_cms_item');
    Route::post('get_settings_values', 'HomeController@get_settings_values');
    Route::post('get_config_values', 'HomeController@get_config_values');

    Route::get('get_artist_images/{type}', 'HomeController@get_artist_images');

    Route::get('get_logged_user_info', 'HomeController@get_logged_user_info');
    Route::get('get_dictionaries_by_name/{dictionary_name}', 'HomeController@get_dictionaries_by_name'); //     public function get_dictionaries_by_name($dictionary_name)
    Route::get('get_future_tours_list', 'HomeController@get_future_tours_list')->name('get_future_tours_list');
    Route::get('future_concerts_dictionaries', 'HomeController@future_concerts_dictionaries')->name('future_concerts_dictionaries');
    Route::post('get_future_concerts_list', 'HomeController@get_future_concerts_list')->name('get_future_concerts_list');


    //                 axios.post(this.api_version_link + 'add_artist_to_cart', {'artist_id': this.artist_id }).then((response) => {

    // CART BLOCK BEGIN
    Route::get('add_artist_to_cart/{artist_id}/{locale}', 'CartController@add_artist_to_cart')->name('add_artist_to_cart');
    Route::get('add_song_to_cart/{song_id}/{locale}', 'CartController@add_song_to_cart')->name('add_song_to_cart');
    Route::get('get_cart_data/{locale}', 'CartController@get_cart_data')->name('get_cart_data');
    Route::get('get_cart_info', 'CartController@get_cart_info')->name('get_cart_info');
    Route::post('clear_cart_content', 'CartController@clear_cart_content')->name('clear_cart_content');
    Route::delete('cart_remove_product/{product_row_id}/{product_id}', 'CartController@cart_remove_product')->name('cart_remove_product');
    Route::post('complete_order', 'CartController@complete_order')->name('complete_order');
    // CART BLOCK END


});


Route::group(['middleware' => ['auth'], 'prefix' => 'api/admin', 'as' => 'admin.'], function () {
    Route::get('logout', 'HomeController@logout');
    Route::post('logout', 'HomeController@logout');

    Route::get('settings_dictionaries', 'Backend\SettingsController@settings_dictionaries')->name('settings_dictionaries');
    Route::get('settings', 'Backend\SettingsController@index')->name('settings');

    Route::get('reports_dictionaries', 'Backend\ReportsController@reports_dictionaries')->name('reports_dictionaries');
    Route::post('reports_artists_votes_ratings', 'Backend\ReportsController@reports_artists_votes_ratings')->name('reports_artists_votes_ratings');
    Route::post('reports_artists_votes_summary_by_period', 'Backend\ReportsController@reports_artists_votes_summary_by_period')->name('reports_artists_votes_summary_by_period');

    Route::post('faker_add_votes', 'Backend\TestDataFakerController@faker_add_votes')->name('faker_add_votes');
    Route::post('run_test', 'Backend\DashboardController@run_test')->name('run_test')->middleware('WorkTextString');

    Route::get('get_user_view_data/{id}', 'Backend\DashboardController@get_user_view_data')->name('get_user_view_data');

    Route::get('load_user_permission_groups', 'Backend\DashboardController@load_user_permission_groups')->name('load_user_permission_groups');
    Route::get('dashboard_dictionaries', 'Backend\DashboardController@dictionaries')->name('dashboard_dictionaries');
    Route::get('dashboard', 'Backend\DashboardController@index')->name('dashboard');
//    Route::get('app_description', 'Backend\DashboardController@app_description')->name('app_description');
    Route::get('get_system_info', 'Backend\DashboardController@get_system_info')->name('get_system_info');


    Route::get('get_artist_by_locale/{artist_id}/{locale}', 'Backend\ArtistsController@show')->name('get_artist_by_locale');

    Route::get('artists/filter/{filter_type}/{filter_value}', 'Backend\ArtistsController@index');
    Route::resource('artists', 'Backend\ArtistsController', ['except' => ['create', 'edit']])->middleware('WorkTextString');

    Route::get('get_artists_locale/{artist_id}/{locale}', 'Backend\ArtistsController@get_artists_locale')->name('get_artists_locale');
    Route::post('update_artists_locale/{artist_id}', 'Backend\ArtistsController@update_artists_locale')->name('update_artists_locale')->middleware('WorkTextString');

    Route::get('artist_dictionaries', 'Backend\ArtistsController@dictionaries')->name('artist_dictionaries');
    Route::get('get_artist_concerts/{artist_id}/{locale}', 'Backend\ArtistsController@get_artist_concerts')->name('get_artist_concerts');
    Route::get('get_artist_concert/{artist_concert_id}', 'Backend\ArtistsController@get_artist_concert')->name('get_artist_concert');
    Route::post('update_artist_concert', 'Backend\ArtistsController@update_artist_concert')->name('update_artist_concert')->middleware('WorkTextString');

    Route::delete('remove_artist_concert/{artist_concert_id}', 'Backend\ArtistsController@remove_artist_concert')->name('remove_artist_concert');

    Route::get('get_artist_songs_dictionaries', 'Backend\ArtistsController@get_artist_songs_dictionaries')->name('get_artist_songs_dictionaries');
    Route::get('get_song_artists_by_artist_id/{artist_id}/{locale}', 'Backend\ArtistsController@get_song_artists_by_artist_id')->name('get_song_artists_by_artist_id');
    Route::get('get_artist_images_by_artist_id/{artist_id}/{locale}', 'Backend\ArtistsController@get_artist_images_by_artist_id')->name('get_artist_images_by_artist_id');
    Route::get('get_artist_image/{artist_image_id}', 'Backend\ArtistsController@get_artist_image')->name('get_artist_image');
    Route::delete('remove_artist_image/{artist_image_id}', 'Backend\ArtistsController@remove_artist_image')->name('remove_artist_image');


    Route::post('update_artist_image', 'Backend\ArtistsController@update_artist_image')->name('update_artist_image')->middleware('WorkTextString');

    Route::post('add_song_to_artist/{artist_id}/{song_id}', 'Backend\ArtistsController@add_song_to_artist')->name('add_song_to_artist')->middleware('WorkTextString');
    Route::post('remove_song_from_artist/{artist_sid}/{song_id}', 'Backend\ArtistsController@remove_song_from_artist')->name('remove_song_from_genre')->middleware('WorkTextString');
    Route::get('get_song_artist/{song_id}/{artist_id}/{locale}', 'Backend\ArtistsController@get_song_artist')->name('get_song_artist');
    Route::post('update_song_artist', 'Backend\ArtistsController@update_song_artist')->name('update_song_artist')->middleware('WorkTextString');


    Route::get('get_genre_by_locale/{genre_id}/{locale}', 'Backend\GenresController@show')->name('get_genre_by_locale');
    Route::resource('genres', 'Backend\GenresController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::get('genre_dictionaries', 'Backend\GenresController@dictionaries')->name('genre_dictionaries');
    Route::post('update_genre_locale/{genre_id}', 'Backend\GenresController@update_genre_locale')->name('update_genre_locale')->middleware('WorkTextString');


    Route::get('get_cms_item_by_locale/{cms_item_id}/{locale}', 'Backend\CmsItemsController@show')->name('get_cms_item_by_locale');
    Route::resource('cms-items', 'Backend\CmsItemsController', ['except' => ['create', 'edit']])->middleware('WorkTextString');

    Route::get('get_cms_items_locale/{cms_item_id}/{locale}', 'Backend\CmsItemsController@get_cms_items_locale')->name('get_cms_items_locale');
    Route::post('update_cms_items_locale/{cms_item_id}', 'Backend\CmsItemsController@update_cms_items_locale')->name('update_cms_items_locale')->middleware('WorkTextString');
    Route::get('cms_item_dictionaries', 'Backend\CmsItemsController@dictionaries')->name('cms_item_dictionaries');


    ////////////// UsersController
    Route::get('users/filter/{filter_type}/{filter_value}', 'Backend\UsersController@index');
    Route::resource('users', 'Backend\UsersController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::get('user_dictionaries', 'Backend\UsersController@dictionaries')->name('user_dictionaries');
    Route::get('user_dictionaries/{user_id}', 'Backend\UsersController@dictionaries')->name('user_dictionaries');
    Route::get('load_user_permission_groups_list', 'Backend\UsersController@dictionaries')->name('load_user_permission_groups_list');

//

//    get_songs_with_similar_titles
    Route::get('get_songs_with_similar_titles', 'Backend\DashboardController@get_songs_with_similar_titles')->name('get_songs_with_similar_titles');

    Route::get('get_genre_related_songs/{genre_sid}/{locale}', 'Backend\GenresController@get_genre_related_songs')->name('get_genre_related_songs');
    Route::post('add_song_to_genre/{genre_id}/{song_id}', 'Backend\GenresController@add_song_to_genre')->name('add_song_to_genre')->middleware('WorkTextString');
    Route::post('remove_song_from_genre/{genre_sid}/{song_id}', 'Backend\GenresController@remove_song_from_genre')->name('remove_song_from_genre')->middleware('WorkTextString');
    Route::get('get_genre_song/{song_id}/{genre_id}', 'Backend\GenresController@get_genre_song')->name('get_genre_song');
    Route::get('get_genre_song/{song_id}', 'Backend\GenresController@get_genre_song')->name('get_genre_song');
    Route::post('update_song_genre', 'Backend\GenresController@update_song_genre')->name('update_song_genre')->middleware('WorkTextString');
    Route::delete('remove_song/{song_id}', 'Backend\GenresController@remove_song')->name('remove_song');

    Route::get('get_footer_data_info', 'Backend\DashboardController@get_footer_data_info')->name('get_footer_data_info');
    Route::get('get_new_artists_list', 'Backend\DashboardController@get_new_artists_list')->name('get_new_artists_list');
    Route::get('get_new_registered_users_list', 'Backend\DashboardController@get_new_registered_users_list')->name('get_new_registered_users_list');

    Route::get('get_future_concerts_list', 'Backend\DashboardController@get_future_concerts_list')->name('get_future_concerts_list');
    Route::post('activate_user_status', 'Backend\DashboardController@activate_user_status')->name('activate_user_status');

    //                    axios.post(window.API_VERSION_LINK + '/add_song_to_genre/' + this.genre_id + "/" + song_id ).then((response) => {


//    Route::resource('artist_dictionaries', 'Backend\ArtistsController', ['except' => ['create', 'edit']])->middleware('WorkTextString');

    /*
     *     Route::resource('user_task_types', 'UserTaskTypesController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::resource('document_categories', 'DocumentCategoriesController', ['except' => ['create', 'edit']])->middleware('WorkTextString');
    Route::get('document_categories_dictionaries', 'DocumentCategoriesController@dictionaries')->name('document_categories_dictionaries');

    
        Route::get('test', 'Backend\DashboardController@test')->name('test');
        Route::get('dashboard_profile_data', 'Backend\DashboardController@dashboard_profile_data')->name('dashboard_profile_data');
        Route::get('refresh_user_profile_quick_info', 'Backend\DashboardController@refresh_user_profile_quick_info')->name('refresh_user_profile_quick_info');
        Route::get('get_assigning_artists_details', 'Backend\DashboardController@get_assigning_artists_details')->name('get_assigning_artists_details');*/



});
