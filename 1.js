/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/
require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

...
import AppRoot from './components/appRoot.vue';
...

const routes = [
{
    path: '/',
    components: {
        ...
        appRoot: AppRoot,
        ...

    }
    ...

    {path: '/not-found/:invalid_url?', component: NotFound, name: 'notFound'},
    {path: '/login', component: AppLogin, name: 'appLogin'},
    {path: '/', component: AppRoot, name: 'appRoot'},


const router = new VueRouter( {
    mode: 'hash', // default
    routes
})


router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next(  '/not-found/'+encodeURIComponent(to.path)  );
    } else {
        next();
    }
})

export const bus = new Vue();

let lang = 'ru';//document.documentElement.lang.substr(0, 2);
let browserInfo= getBrowserInfo()
if ( browserInfo['name'] =='Chromium' ) { // in Chromium show English
lang= 'en'
}
console.log("lang::")
console.log( lang )
if ( lang == '' || typeof lang == 'undefined' ) {
lang= 'en'
}
const i18n = new VueI18n({    // https://github.com/caouecs/Laravel-lang - Additive langs
locale: lang, // set locale
messages : Locale, // set locale messages
})

// console.log("AFTER i18n::")
// console.log( i18n )

// console.log("AFTER lang::")
// console.log( lang )

new Vue({ router, i18n,

data:{
app_title: '',
loggedUserProfile: {},   /* LOGGED USER INFO BLOCK */
loggedUserInGroups: [],
refsArray:[]
},

mixins : [appMixin],

created() {
}, // created() {

mounted() {

if ( typeof window.logged_user_id == "undefined" || typeof window.logged_user_name == "undefined" ) return;// check if user is logged
axios.get(window.API_VERSION_LINK + 'get_logged_user_info' ).then((response) => {
if ( typeof response.data.loggedUser == "object" ) {
// alert( "get_logged_user_info response.data.loggedUser::"+var_dump(response.data.loggedUser) )
this.loggedUserProfile = response.data.loggedUser;
this.loggedUserInGroups = response.data.loggedUserInGroups;
bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name, this.loggedUserProfile, this.loggedUserInGroups );
}
}).catch((error) => {
this.showRunTimeError(error, this);
});

//
// var app_route_name= this.$route.name
//
// console.log("!!here  app_route_name:::");
// console.log(app_route_name)
//
}, // mounted(){

watch: {
'$route': function(){
alert( "watch::"+var_dump(this.$route) )
console.log("+++ watchthis.$route::")
console.log( this.$route )

console.log(this.$route.params.alias)
bus.$emit('UserProfileIsSetEvent', this.loggedUserProfile.first_name + ' ' + this.loggedUserProfile.last_name );

}
},

beforeRouteEnter (to, from, next) {
alert( " beforeRouteEnter::"+this.var_dump(-86543) )
console.log("beforeRouteEnter to::")
console.log( to )

console.log(to.params.alias)
next()
},

methods: {


}, // methods: {



} ).$mount('#app')  // new Vue({ router,


alert( "AFTER::"+var_dump(11) )

function var_dump(oElem, from_line, till_line) {
if (typeof oElem == 'undefined') return 'undefined';
var sStr = '';
if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
sStr = oElem;
} else {
var sValue = '';
for (var oItem in oElem) {
sValue = oElem[oItem];
if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
}
sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
}
}
//alert( "from_line::"+(typeof from_line) )
if (typeof from_line == "number" && typeof till_line == "number") {
return sStr.substr(from_line, till_line);
}
if (typeof from_line == "number") {
return sStr.substr(from_line);
}
return sStr;
}


function getBrowserInfo(){
var ua=navigator.userAgent,tem,M=ua.match(/(opera|chromium|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
if(/trident/i.test(M[1])){
tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
return {name:'IE ',version:(tem[1]||'')};
}
if(M[1]==='Chrome'){
tem=ua.match(/\bOPR\/(\d+)/)
if(tem!=null)   {return {name:'Opera', version:tem[1]};}
}
M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
return {
name: M[0],
version: M[1]
};
}