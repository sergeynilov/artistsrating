let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

let current_template= 'artists_rating_light';


/*
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_lg.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_md.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_sm.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_xs_320.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_xs_480.scss', 'public/css/'+current_template)
    .sass('resources/assets/sass/style_xs_600.scss', 'public/css/'+current_template);*/
